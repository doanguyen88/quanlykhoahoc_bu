<!DOCTYPE html>
<html >
<head>
<meta charset="UTF-8">
<title>Mẫu 1b. Phiếu đăng ký sáng kiến của cá nhân</title>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
<script src="giaodien/js/scripts3.js" type="text/javascript"></script>
<script src="giaodien/js/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
  $('#nhaptagsinput, select').on('change', function(event) {
    var $element = $(event.target),
      $container = $element.closest('.example');

    if (!$element.data('tagsinput'))
      return;

    var val = $element.val();
    if (val === null)
      val = "null";
    $('#an', $container).val( ($.isArray(val) ? JSON.stringify(val) : "\"" + val.replace('"', '\\"') + "\"") );
	
  }).trigger('change');
});
</script>
</head>

<body>


<div class="example">
        
          
         
            <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput" id="nhaptagsinput" />
          
          
           <input type="hidden"  class="an" id="an"/>
          
           
  </div>

<!-- /.container -->

</body>
</html>
