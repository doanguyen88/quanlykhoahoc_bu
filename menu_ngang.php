  
<div id="sidebar" class="sidebar h-sidebar navbar-collapse collapse ace-save-state">
	<script type="text/javascript">
		try{ace.settings.loadState('sidebar')}catch(e){}
	</script>

	<!-- /.sidebar-shortcuts -->
  	<ul class="nav nav-list">
		<?php 
			// $sql = $db->getSQL('menus',array('where'=>array('vitri'=>'gv/DotDK_NCKH')),array('order_by'=>'ThuTu ASC'));
			// echo "<h1>$sql</h1>";
			// $menus = $db->getRows('menus',array('where'=>array('vitri'=>'gv/DotDK_NCKH')),array('order_by'=>'ThuTu ASC'));
			

			// $sql = "SELECT m.*, md.dot, dot_kk.endtimes as dot_kk_endtimes, dot_dk.endtimes as dot_dk_endtimes,
			// 			(md.dot IS NULL 
			// 				OR (md.dot = 'nncms_quanlydotkhekhainckh' AND CURDATE() BETWEEN dot_kk.starttimes AND dot_kk.endtimes)
			// 				OR (md.dot = 'nncms_quanlydotdangkynckh' AND CURDATE() BETWEEN dot_dk.starttimes AND dot_dk.endtimes)
			// 			) as enable
			// 		FROM menus m
			// 		LEFT JOIN menu_dot md ON m.idMenu = md.id_menu
			// 		LEFT JOIN nncms_quanlydotkhekhainckh dot_kk ON md.dot = 'nncms_quanlydotkhekhainckh'
			// 		LEFT JOIN nncms_quanlydotdangkynckh dot_dk ON md.dot = 'nncms_quanlydotdangkynckh'
					
			// 		WHERE vitri = 'gv/DotDK_NCKH' 
			// 		ORDER BY thutu ASC";

		

			$sql = "SELECT DISTINCT m.*, md.dot,
						EXISTS (Select 1
							From nncms_quanlydotdangkynckh 
								Where md.dot IS NULL 
							OR (md.dot = 'nncms_quanlydotdangkynckh' AND CURDATE() BETWEEN starttimes AND endtimes)
						)
						OR EXISTS (Select 1
							From nncms_quanlydotkhekhainckh 
								Where md.dot IS NULL 
							OR (md.dot = 'nncms_quanlydotkhekhainckh' AND CURDATE() BETWEEN starttimes AND endtimes)
						)
						as enable
					FROM menus m
					LEFT JOIN menu_dot md ON m.idMenu = md.id_menu
					WHERE vitri = 'gv/DotDK_NCKH' 
					ORDER BY thutu ASC";
			
			$menus = $db->runSQL($sql); 
  			
			if(!empty($menus)){ 
				$count = 0; 
				foreach($menus as $menu) { 
					$thong_bao = "alert('Chưa mở đợt')";
					
					?>
        			<li class="hover">
						<a href="<?php if($menu['enable']) echo $menu['LienKet'];?>" onclick="<?php if(!$menu['enable']) echo $thong_bao; ?>" class="<?php echo $menu['class'];?>">
							<i class="<?php echo $menu['icon'];?>"></i>
							<span class="menu-text"> <?php echo $menu['TieuDe'];?></span>
			
							<?php 
								$submenus = $db->getRows('submenu', array('where'=>array('idCapCha'=>$menu['idMenu'], 'vitri'=>'gv/DotDK_NCKH')), array('order_by'=>'ThuTu ASC'));
								if(!empty($submenus)) { 
									$count = 0; 
									foreach($submenus as $submenu) { 
										$count++;
										echo "<b class='arrow fa fa-angle-down'></b>";
									}
								}
							?>
						</a>

						<b class="arrow"></b>
			
						<?php 
							$subme = $db->getRows('submenu', array('where'=>array('idCapCha'=>$menu['idMenu'], 'vitri'=>'gv/DotDK_NCKH')),array('order_by'=>'ThuTu ASC'));
							if(!empty($subme)){ 
								// $count = 0; 
								foreach($subme as $submen) { 
									// $count++;
									?>
									<ul class="submenu">
									<?php 	
										$sub = $db->getRows('submenu', array('where'=>array('idCapCha'=>$menu['idMenu'], 'vitri'=>'gv/DotDK_NCKH')), array('order_by'=>'ThuTu ASC'));
										
										if(!empty($sub)){ 
											// $count = 0; 
											foreach($sub as $subm) { 
												// $count++;
												?>

												<li class="hover">
													<a href="<?php if($menu['enable']) echo $subm['LienKet'];?>" onclick="<?php if(!$menu['enable']) echo $thong_bao; ?>">
														<i class="<?php echo $subm['icon'];?>"></i>
														<?php echo $subm['TenSubmenu'];?>
													</a>
													<b class="arrow"></b>
												</li>

											<?php }
										}// ket thuc menu con?>
									</ul>
								<?php }
							}?>
					</li>
				<?php }
			}// het menu cha?><!-- /.nav-list --> 

	</ul>
</div>











