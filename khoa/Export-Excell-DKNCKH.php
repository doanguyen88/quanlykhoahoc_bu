<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.js"></script>
<script src="../assets/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="../giaodien/js/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script type="text/javascript">
  var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,', template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
      , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
      , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (table, name) {
      if (!table.nodeType) 
        table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
        window.location.href = uri + base64(format(template, ctx))
    }
  })()
</script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<input class="btn btn-xs  btn-success" type="button" onclick="tableToExcel('testTable', 'W3C Example Table')" value="Export to Excel">
<div>
  <table id="testTable" summary="Code page support in different versions of MS Windows." rules="groups" frame="hsides" border="2" class="table table-striped table-bordered table-hover dataTable no-footer" style="width: 100%;">
    <thead valign="top">
      <tr>
        <th colspan="5" style="text-align:center">TRƯỜNG ĐẠI HỌC NGÂN HÀNG TP.HCM</th>
        <th colspan="5" style="text-align:center">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="5" style="text-align:center">Đơn vị: 
          <?php
            $layuser = $db->getRows('user',array('where'=>array('madv'=>$_SESSION['sess_madv'])),array('order_by'=>'id ASC'));
            echo $layuser[0]['donvi'];
          ?>
        </td>
        <td colspan="5" style="text-decoration: underline; text-align:center">Độc lập - Tự do - Hạnh phúc</td>
      </tr>
    </tbody>
    <tr>
      <td colspan="10" align="center" style="font-size:16px;font-weight:bold; padding:20px 0px 20px 0px;">BẢNG TỔNG HỢP DANH SÁCH ĐĂNG KÝ NHIỆM VỤ KH-CN NĂM <?php //$qldkdetai = $db->getRows('nncms_QuanlydotkhekhaiNCKH',array('where'=>array('anHien'=>'on')),array('order_by'=>'idQuanlydangky_Detai ASC'));
        if(!empty($qldkdetai)){ 
          $count = 0; 
          foreach($qldkdetai as $qldkdetais){ 
            $count++;?>
            <?php
              $time1= $qldkdetais['starttimes'];  
              $time2= $qldkdetais['endtimes']; 
              $date = date('m/d/Y h:i:s a', time());
              $moc1 = (strtotime($date) - strtotime($time1)) / (60 * 60 * 24);
              $moc2 = (strtotime($date) - strtotime($time2)) / (60 * 60 * 24);
              if($moc1 >0 && $moc2 < 0 ){ 
                echo $qldkdetais['TenDotkekhai_NCKH'];}
            ?>  
          <?php } 
        }?>
      </td>
    </tr>
    <!--<caption> DANH SÁCH TỔNG HỢP ĐĂNG KÝ NHIỆM VỤ KH-CN NĂM </caption>-->
    <colgroup align="center">
    </colgroup>
    <colgroup align="left">
    </colgroup>
    <colgroup span="2" align="center">
    </colgroup>
    <colgroup span="3" align="center">
    </colgroup>

    <thead valign="top">
      <tr>
      <th colspan="1" style="width:10%" align="center">STT</th>
        <th colspan="3" style="width:30%">Tên đề tài, đề án, tài liệu học tập, tên sáng kiến, chủ đề hội thảo</th>
        <th colspan="3" style="width:30%">Chủ nhiệm đề tài, đề án, chủ biên, người đăng ký, đơn vị đăng ký</th>
        <th colspan="3" style="width:auto">Thời gian thực hiện đề tài, đề án, sáng kiến, hội thảo, địa điểm tổ chức</th>
      </tr>
      <!-- 01 - ĐỀ TÀI -->
      <tr>
        <th colspan="1" style="text-align:left">I</th>
        <th colspan="9" style="text-align:left"><strong>Đề tài cấp cơ sở</strong></th>
      </tr>
      
      <tbody style="border:#CCC 1 solid;">
      <?php
        // $i=0;
        $dsdkdtcapcoso = $db->getRows('nncms_dkdtcapcoso', array('where'=>array('madv'=>$_SESSION['sess_madv'])), array('order_by'=>'Ho_Ten ASC'));
        if(!empty($dsdkdtcapcoso)){ 
          $count = 0; 
          foreach($dsdkdtcapcoso as $detaicapcs){ 
            $count++;?>
            <tr>
              <!-- <td align="center" style="width:30px"><?php //echo $i+1?></td> -->
              <td colspan="1" class="center"><?php echo $count;?></td>
              <td colspan="3" class="hidden-280"><?php echo $detaicapcs['TenDeTaiCapCoSo'];?></td>
              <td colspan="3"><?php echo $detaicapcs['HoTenChuNhiem'];?></td>
              <td colspan="3">
                <strong>TỪ: </strong><?php echo $detaicapcs['starttimes']; ?></br> 
                <strong>ĐẾN: </strong><?php echo $detaicapcs['endtimes']; ?>
              </td>
            </tr>
          <?php  } 
        }?> 
      <!-- 02 - ĐỀ ÁN -->
      <tr>
        <th colspan="1" style="text-align:left">II</th>
        <th colspan="9" style="text-align:left"><strong>Đề án cấp cơ sở</strong></th>
      </tr>
      <tbody style="border:#CCC 1 solid;">
      <?php
        // $i=0;
        $dsdkdacapcoso = $db->getRows('nncms_dkdacapcoso', array('where'=>array('madv'=>$_SESSION['sess_madv'])), array('order_by'=>'Ho_ten ASC'));
        if(!empty($dsdkdacapcoso)){ 
          $count = 0; 
          foreach($dsdkdacapcoso as $deancapcs){ 
            $count++;?>
            <tr>
              <!-- <td align="center" style="width:30px"><?php //echo $i+1?></td> -->
              <td colspan="1" class="center"><?php echo $count;?></td>
              <td colspan="3" class="hidden-280"><?php echo $deancapcs['TenDeAnCapCoSo'];?></td>
              <td colspan="3"><?php echo $deancapcs['HoTenChuNhiem'];?></td>
              <td colspan="3">
                <strong>TỪ: </strong><?php echo $deancapcs['starttimes']; ?></br> 
                <strong>ĐẾN: </strong><?php echo $deancapcs['endtimes']; ?>
              </td>
            </tr>
          <?php } 
        }?>
      <!-- 03 - BIÊN SOẠN TLHT -->
      <tr>
        <th colspan="1" style="text-align:left">III</th>
        <th colspan="9" style="text-align:left"><strong>Biên soạn tài liệu học tập</strong></th>
      </tr>
      <tbody style="border:#CCC 1 solid;">
      <?php
        $vaitro = $_SESSION['sess_vaitro'];
        $madonvi = $_SESSION['sess_madv'];
        $emailnguoidk = $_SESSION['sess_email'];
        $sql = "SELECT *
                FROM nncms_dkbiensoantlht
                WHERE $vaitro = 3
                  OR ($vaitro = 2 AND madv = '$madonvi')
                  OR ($vaitro = 1 AND emailuser = '$emailnguoidk')";
        $dsdkbiensoantlht = $db -> runSQL($sql); 
        if(!empty($dsdkbiensoantlht)){ 
          $count = 0; 
          foreach($dsdkbiensoantlht as $biensoantlht){ 
            $count++;?>
            <tr>
              <td colspan="1" class="center"><?php echo $count;?></td>
              <td colspan="3" class="hidden-280"><?php echo $biensoantlht['TenTaiLieu'];?></td>
              <td colspan="3"><?php echo $biensoantlht['HoTenChuNhiem'];?></td>
              <td colspan="3">
                <strong>TỪ: </strong><?php echo $biensoantlht['starttimes']; ?></br> 
                <strong>ĐẾN: </strong><?php echo $biensoantlht['endtimes']; ?>
              </td>
            </tr>
          <?php } 
        }?>
      <!-- 04 - SÁNG KIẾN -->
      <tr>
        <th colspan="1" style="text-align:left">IV</th>
        <th colspan="9" style="text-align:left"><strong>Quản lý sáng kiến</strong></th>
      </tr>
      <tbody style="border:#CCC 1 solid;">
      <?php
        // $i=0;
        $dsdksangkiencn = $db->getRows('nncms_dksangkiencn', array('where'=>array('madv'=>$_SESSION['sess_madv'])), array('order_by'=>'Ho_Ten ASC'));
        if(!empty($dsdksangkiencn)){ 
          $count = 0; 
          foreach($dsdksangkiencn as $dksangkien){ 
            $count++;?>
            <tr>
              <!-- <td align="center" style="width:30px"><?php //echo $i+1?></td> -->
              <td colspan="1" class="center"><?php echo $count;?></td>
              <td colspan="3" class="hidden-280"><?php echo $dksangkien['TenSangKien'];?></td>
              <td colspan="3"><?php echo $dksangkien['HoTenNguoiDK'];?></td>
              <td colspan="3"><?php echo $dksangkien['TG_DD_CV_ApDungSK']; ?></td>
            </tr>
          <?php } 
        }?>
      <!-- 05 - HỘI THẢO -->
      <tr>
        <th colspan="1" style="text-align:left">V</th>
        <th colspan="9" style="text-align:left"><strong>Quản lý hội thảo</strong></th>
      </tr>
      <tbody style="border:#CCC 1 solid;">
      <?php
        // $i=0;
        $dsdkhoithao = $db->getRows('nncms_dkhoithao', array('where'=>array('madv'=>$_SESSION['sess_madv'])), array('order_by'=>'Ten_Don_Vi ASC'));
        if(!empty($dsdkhoithao)){ 
          $count = 0; 
          foreach($dsdkhoithao as $dkhoithao){ 
            $count++;?>
            <tr>
              <!-- <td align="center" style="width:30px"><?php //echo $i+1?></td> -->
              <td colspan="1" class="center"><?php echo $count;?></td>
              <td colspan="3" class="hidden-280"><?php echo $dkhoithao['ChuDeHoiThao'];?></td>
              <td colspan="3"><?php echo $dkhoithao['TenDonViDK'];?></td>
              <td colspan="3"><?php echo $dkhoithao['ThoiGianDiaDiemTC']; ?></td>
            </tr>
          <?php } 
        }?>
    </thead>
    
        
    </tbody>


    <thead valign="bottom">
      <tr>
        <th colspan="9"></th>
        <th colspan="1" style="text-align:right"><P style="text-align:center" >Người lập bảng <br/> (Ký, ghi rõ họ tên)</P></th>
      </tr>
    </thead>
  </table>
</div>
 