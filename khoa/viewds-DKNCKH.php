<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.js"></script>
<script src="../giaodien/js/upload.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts4.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts7.js" type="text/javascript"></script>
<script src="../assets/js/jquery-ui.min.js"></script>
<script type="text/javascript">
  function getNewVal(item)
  {
    var laydong=item.value;
    $( ".laydong" ).val( laydong );

  }
  $(document).on( "click", '.file',function(e) {
    var filedknckh = $(this).data('filedknckh');
    $(".filedknckh").val(filedknckh);
  });
  

  $(document).on( "click", '.khoaduyet',function(e) {  
    var iddknckh = $(this).data('iddknckh');
    var hoten = $(this).data('hoten');
    var chonqldkdt = $(this).data('chonqldkdt');
    var chonphanloaikhnc = $(this).data('chonphanloaikhnc');
    var masanphamkhnc = $(this).data('masanphamkhnc');
    var tensanpham = $(this).data('tensanpham');
    var chonmahinhthuc = $(this).data('chonmahinhthuc');
    var mavaitro = $(this).data('mavaitro');
    var soquyetdinh = $(this).data('soquyetdinh');
    var thoigianhoanthanh = $(this).data('thoigianhoanthanh');
    var sotietquydoi = $(this).data('sotietquydoi');
    var trangthai = $(this).data('trangthai');
    var action = $(this).data('action'); 
    var tapchihoithao = $(this).data('tapchihoithao'); 

    $(".iddknckh").val(iddknckh);
    $(".hoten").text(hoten);
    $(".chonqldkdt").text(chonqldkdt);
    $(".chonphanloaikhnc").text(chonphanloaikhnc);	
    $(".masanphamkhnc").text(masanphamkhnc);
    $(".tensanpham").text(tensanpham);
    $(".chonmahinhthuc").text(chonmahinhthuc);
    $(".mavaitro").text(mavaitro);
    $(".soquyetdinh").text(soquyetdinh);
    $(".thoigianhoanthanh").text(thoigianhoanthanh);
    $(".sotietquydoi").text(sotietquydoi);
    $(".sotietquydoikn").val(sotietquydoi);
    $(".trangthai").text(trangthai);
    $(".action").val(action);
    $(".tapchihoithao").val(tapchihoithao);
    
  });
</script>

<div class="page-content">
  <div class="ace-settings-container" id="ace-settings-container">
    <div class="row">
      <div class="col-md-1"> 
        <a class="btn btn-info" type="button" href="http://localhost/quanlykhoahoc_bu/khoa/index.php?key=Export-Excell-DKNCKH" > 
          <i class="ace-icon  fa fa-file-excel-o bigger-110" align="right"></i> Xuất báo cáo 
        </a> 
      </div>
    </div>
  </div><!-- /.ace-settings-container --> 
  <!-- /.page-header -->
  <div class="row">
    <div class="col-xs-12"> 
      <!-- 01 - ĐỀ TÀI -->
      <div class="row">
        <div class="col-md-1"> 
          <a class="btn btn-info" type="button"> 
            <i class="ace-icon bigger-110"></i> I - Đề tài cấp cơ sở 
          </a> 
        </div>
          <?php 
            if(isset($_GET['dot'])){
              $dot = $_GET['dot'];
              // echo "<h1>$dot</h1>";
            } else {
              $dot = 1;
              // echo "<h1>KHÔNG CÓ ĐỢT</h1>";
            }
          ?>
                
          <form action="index.php" method="POST" align="center">
            <label for="dotdk"> Đợt đăng ký NCKH : </label>
            <?php 
              $dsdotdknckh = $db -> getRowsgom('nncms_quanlydotdangkynckh', array('where' => array('xoa' => '')));
              $dotDuocChon = 0;
              if(isset($_REQUEST['dot']) && !empty($_REQUEST['dot'])){
                $dotDuocChon = $_REQUEST['dot'];
                // echo "<h1>$dotDuocChon</h1>";
              }
            ?>
            <select id="dot" name="dotdk" onchange="window.location='./index.php?key=viewds-DKNCKH&dot='+ this.options[this.selectedIndex].value">
              <option $selected value='0'>Chọn đợt</option>
            <?php
              if(!empty($dsdotdknckh)){ 
                $count = 0;
                foreach($dsdotdknckh as $dotdknckh) {
                  $maDot = $dotdknckh['idDotdknckh'];
                  $tenDot = $dotdknckh['TenDotdangky_NCKH'];
                  $selected = $maDot == $dotDuocChon ? "selected" : "";
                  echo "<option $selected value='$maDot'>$tenDot</option>";
                }
              }
            ?>
            </select>
          </form> 
      </div>
      
      <div class="row">
        <div class="col-xs-12">
          <table id="dynamic-table" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th class="center"> #STT </th>
                <th class="hidden-280">Họ và tên</th>
                <th class="hidden-280">Tên đề tài</th>
                <th>Trạng thái khoa duyệt</th>
                <th>Cập nhật</th>
                <th><i class="ace-icon fa fa-cog icon-only bigger-130"></i></th>
              </tr>
            </thead>
            
            <tbody>
              <?php 
                $vaitro = $_SESSION['sess_vaitro'];
                $madonvi = $_SESSION['sess_madv'];
                $emailnguoidk = $_SESSION['sess_email'];

                $sql = "SELECT *, CASE
                                    WHEN (TrangthaiDuyet_Boduyet = 'Bỏ duyệt' AND Trangthai_VienDuyet = 'Chưa duyệt') THEN 'Đăng ký mới'
                                    WHEN TrangthaiDuyet_Boduyet = '' THEN 'Đăng ký mới'
                                    WHEN Trangthai_VienDuyet = 'Viện đã duyệt' THEN 'Viện đã duyệt' 
                                    WHEN TrangthaiDuyet_Boduyet = 'Bỏ duyệt' THEN 'Bỏ duyệt'
                                    WHEN TrangthaiDuyet_Boduyet = 'Đơn vị đã duyệt' THEN 'Đơn vị đã duyệt'                               
                                  END as TrangThaiCapNhat
                        FROM nncms_dkdtcapcoso
                        WHERE ($vaitro = 3
                              OR ($vaitro = 2 AND madv = '$madonvi')
                              OR ($vaitro = 1 AND emailuser = '$emailnguoidk'))
                            AND idDotDKNCKH = $dotDuocChon";
                // echo "<h1>$madonvi</h1>";
                $dsdkdtcapcoso = $db -> runSQL($sql);
                // $tsdangky = $db -> demrow('nncms_dkdtcapcoso', array(array('xoa' => '')));
                $sd=30;
                $tsdangky = count($dsdkdtcapcoso);
                $tst=ceil($tsdangky/$sd);// tinh tong so trang
                // Lay trang:
                if(isset($_GET['page'])){
                  $page=intval($_GET['page']);
                }
                else
                $page=1;
                //Tinh vi tri array('where'=>array('xoa'=>'')),
                $vt=($page-1)*$sd;
                $dsdknckh = $db->runSQL($sql);
                $stt=$vt+1;
                if(!empty($dsdknckh)){ 
                  $count = 0; 
                  foreach($dsdknckh as $dknckh){ 
                    $count++;
                ?>
                    <tr>
                        <td class="center"><?php echo $count;?></td>
                        <td class="hidden-280">
                            <?php echo $dknckh['HoTenChuNhiem'];?>
                        </td>
                        <td class="hidden-280">
                            <?php echo $dknckh['TenDeTaiCapCoSo'];?>
                        </td>
                        <td ><?php echo $dknckh['TrangthaiDuyet_Boduyet'];?></td>
                        <td class="hidden-280">
                          <div class="action-buttons"> 
                            <a class="blue khoaduyet" data-toggle="modal" href="#stack3" 
                              data-iddknckh="<?php echo $dknckh['idDKDTCapCoSo'];?>"
                              data-hoten="<?php echo $dknckh['HoTenChuNhiem'];?>"
                              data-trangthai="<?php echo $dknckh['TrangthaiDuyet_Boduyet'];?>"
                              data-action="updatekhoaduyet"> 
                              <i class="ace-icon fa fa-pencil-square-o bigger-130"></i> 
                            </a> 
                          </div>
                        </td>

                        <td>
                          <div class="">
                            <div class="inline pos-rel"> 
                              <!-- FILE -->
                             
                                <a title="Quản lý file đính kèm" class="btn btn-xs btn-info no-radius blue file" href="#stack8"  data-toggle="modal" 
                                data-target="#stack8" 
                                data-filedknckh="<?php echo $dknckh['idDKDTCapCoSo'];?>" 
                                onclick="xemfiledk_dtnckh('<?php echo $dknckh['idfile'];?>','<?php echo $dknckh['file'];?>','<?php echo $dknckh['emailuser'];?>')"> 
                                <i class="ace-icon fa fa-folder-open-o bigger-150"></i> 
                                </a>
                              <!-- BỎ DUYỆT -->
                              <a href="boduyetdknckh.php?action_type=boduyetdknckh&id=<?php echo $dknckh['idDKDTCapCoSo'];?>&email=<?php echo $dknckh['emailuser'];?>&dot=<?php echo $dotDuocChon; ?>" data-type="boduyetdknckh" class="tooltip-error btn btn-xs no-radius btn-danger" onclick="return confirm('Chắc chắn là bạn muốn bỏ duyệt kê khai này?');" title="Bỏ duyệt kê khai">
                                  <i class="ace-icon ace-icon glyphicon glyphicon-remove bigger-150"></i> 
                              </a>
                              <!-- PRINT -->
                              <?php $idDeTai = $dknckh['idDKDTCapCoSo']; ?>
                              <a 
                                href="?key=MauIn-CS01&id_de_tai=<?php echo $idDeTai; ?>"
                                data-type="print" class="btn btn-success btn-xs" onclick="return confirm('Bạn muốn in biểu mẫu này?');" title="In biểu mẫu!">
                                  <i class="ace-icon ace-icon glyphicon glyphicon-print bigger-150"></i> 
                                  <?php 
                                    if(isset($_GET['key']))
                                      switch($_GET['key'])
                                      {
                                        case "MauIn-CS01": 
                                          include("MauIn-CS01.php");
                                          $doc = new DomDocument;
                                          $doc->validateOnParse = true;
                                          $doc->Load('book.xml');
                                          echo "The element whose id is 'php-basics' is: " . $doc->getElementById('php-basics')->tagName . "\n";
                                        break;
                                      }
                                    else {
                                      include("addthanhviencs1.php");
                                    }?>
                              </a>
                            </div>
                          </div>
                        </td>
                    </tr>

                    <tr class="detail-row">
                        <td colspan="10">
                            <div class="table-detail">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="space visible-xs"></div>
                                        <div class="profile-user-info profile-user-info-striped">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Hoạt động </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['chonqldkdt'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Phân Loại </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['chonphanloaikhnc'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Tạp chí/hội thảo </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['tapchihoithao'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Hình Thức </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['chonmahinhthuc'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Vai Trò </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['mavaitro'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Sản phẩm </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['masosanpham'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Tên Sản phẩm </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['tensanpham'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Số quyết định </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['soquyetdinh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Thời gian hoàn thành </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['Thoi_Gian_Hoan_Thanh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Đợt kê khai </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['chondotkknckh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Năm kê khai </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['namhoanthanh'];?></span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        </tr>
                  <?php } 
                }else{ ?>
                  <tr>
                    <td colspan="8">Không có dữ liệu</td>
                  </tr>
                  <?php } ?>
            </tbody>
          </table>
        </div><!-- /.span --> 
      </div>

      <div class="row">
        <div class="col-xs-6">
          <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">Tổng số đăng ký là: <?php echo $tsdangky; ?> dòng</div>
        </div>
        <div class="col-xs-6">
          <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            <ul class="pagination">
              <?php for($i=1;$i<=$tst;$i++)
              if($i==$page){ ?>
                  <li class="active" aria-controls="dynamic-table" tabindex="0"><a href="#"><?php echo $i;?></a></li>
              <?php }
              else {?>
                <li class=''> 
                  <a href='<?php echo "http://localhost/quanlykhoahoc_bu/khoa/index.php?key=viewds-NCKH&page=".$i;?>'><?php echo $i;?></a> 
                </li>
              <?php }?>
            </ul>
          </div>
        </div>
      </div><!-- /.row --> 

      <!-- 02 - ĐỀ ÁN -->
      <div class="row">
        <div class="col-md-1"> 
          <a class="btn btn-info" type="button"> 
            <i class="ace-icon bigger-110"></i> II - Đề án cấp cơ sở 
          </a> 
        </div>
          <?php 
            if(isset($_GET['dot'])){
              $dot = $_GET['dot'];
              // echo "<h1>$dot</h1>";
            } else {
              $dot = 1;
              // echo "<h1>KHÔNG CÓ ĐỢT</h1>";
            }
          ?>
                
          <form action="index.php" method="POST" align="center">
            <label for="dotdk"> Đợt đăng ký NCKH : </label>
            <?php 
              $dsdotdknckh = $db -> getRowsgom('nncms_quanlydotdangkynckh', array('where' => array('xoa' => '')));
              $dotDuocChon = 0;
              if(isset($_REQUEST['dot']) && !empty($_REQUEST['dot'])){
                $dotDuocChon = $_REQUEST['dot'];
                // echo "<h1>$dotDuocChon</h1>";
              }
            ?>
            <select id="dot" name="dotdk" onchange="window.location='./index.php?key=viewds-DKNCKH&dot='+ this.options[this.selectedIndex].value">
              <option $selected value='0'>Chọn đợt</option>
            <?php
              if(!empty($dsdotdknckh)){ 
                $count = 0;
                foreach($dsdotdknckh as $dotdknckh) {
                  $maDot = $dotdknckh['idDotdknckh'];
                  $tenDot = $dotdknckh['TenDotdangky_NCKH'];
                  $selected = $maDot == $dotDuocChon ? "selected" : "";
                  echo "<option $selected value='$maDot'>$tenDot</option>";
                }
              }
            ?>
            </select>
          </form> 
      </div>
      
      <div class="row">
        <div class="col-xs-12">
          <table id="dynamic-table" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th class="center"> #STT </th>
                <th class="hidden-280">Họ và tên</th>
                <th class="hidden-280">Tên đề án</th>
                <th>Trạng thái khoa duyệt</th>
                <th>Cập nhật</th>
                <th><i class="ace-icon fa fa-cog icon-only bigger-130"></i></th>
              </tr>
            </thead>
            
            <tbody>
              <?php 
                $vaitro = $_SESSION['sess_vaitro'];
                $madonvi = $_SESSION['sess_madv'];
                $emailnguoidk = $_SESSION['sess_email'];

                $sql = "SELECT *, CASE
                                    WHEN (TrangthaiDuyet_Boduyet = 'Bỏ duyệt' AND Trangthai_VienDuyet = 'Chưa duyệt') THEN 'Đăng ký mới'
                                    WHEN TrangthaiDuyet_Boduyet = '' THEN 'Đăng ký mới'
                                    WHEN Trangthai_VienDuyet = 'Viện đã duyệt' THEN 'Viện đã duyệt' 
                                    WHEN TrangthaiDuyet_Boduyet = 'Bỏ duyệt' THEN 'Bỏ duyệt'
                                    WHEN TrangthaiDuyet_Boduyet = 'Đơn vị đã duyệt' THEN 'Đơn vị đã duyệt'                               
                                  END as TrangThaiCapNhat
                        FROM nncms_dkdacapcoso
                        WHERE ($vaitro = 3
                              OR ($vaitro = 2 AND madv = '$madonvi')
                              OR ($vaitro = 1 AND emailuser = '$emailnguoidk'))
                            AND idDotDKNCKH = $dotDuocChon";
                // echo "<h1>$madonvi</h1>";
                $dsdkdacapcoso = $db -> runSQL($sql);

                // $tsdangky = $db -> demrow('nncms_dkdacapcoso', array(array('xoa' => '')));
                $sd=30;
                $tsdangky = count($dsdkdacapcoso);
                $tst=ceil($tsdangky/$sd);// tinh tong so trang
                // Lay trang:
                if(isset($_GET['page'])){
                  $page=intval($_GET['page']);
                }
                else
                $page=1;
                //Tinh vi tri array('where'=>array('xoa'=>'')),
                $vt=($page-1)*$sd;
                $dsdknckh = $db->runSQL($sql);
                $stt=$vt+1;
                if(!empty($dsdknckh)){ 
                  $count = 0; 
                  foreach($dsdknckh as $dknckh){ 
                    $count++;
                ?>
                    <tr>
                        <td class="center"><?php echo $count;?></td>
                        <td class="hidden-280">
                            <?php echo $dknckh['HoTenChuNhiem'];?>
                        </td>
                        <td class="hidden-280">
                            <?php echo $dknckh['TenDeAnCapCoSo'];?>
                        </td>
                        <td ><?php echo $dknckh['TrangthaiDuyet_Boduyet'];?></td>
                        <td class="hidden-280">
                          <div class="action-buttons"> 
                            <a class="blue khoaduyet" data-toggle="modal" href="#stack4" 
                              data-iddknckh="<?php echo $dknckh['idDKDACapCoSo'];?>"
                              data-hoten="<?php echo $dknckh['HoTenChuNhiem'];?>"
                              data-trangthai="<?php echo $dknckh['TrangthaiDuyet_Boduyet'];?>"
                              data-action="updatekhoaduyet"> 
                              <i class="ace-icon fa fa-pencil-square-o bigger-130"></i> 
                            </a> 
                          </div>
                        </td>

                        <td>
                          <div class="">
                            <div class="inline pos-rel"> 
                              <!-- FILE -->
                              
                              <a title="Quản lý file đính kèm" class="btn btn-xs btn-info no-radius blue file" href="#stack8"  data-toggle="modal" 
                                data-target="#stack8" 
                                data-filedknckh="<?php echo $dknckh['idDKDACapCoSo'];?>" 
                                onclick="xemfiledk_danckh('<?php echo $dknckh['idfile'];?>','<?php echo $dknckh['file'];?>','<?php echo $dknckh['emailuser'];?>')"> 
                                <i class="ace-icon fa fa-folder-open-o bigger-150"></i> 
                              </a>
                              <!-- BỎ DUYỆT -->
                              <a href="boduyetdk_danckh.php?action_type=boduyetdk_danckh&id=<?php echo $dknckh['idDKDACapCoSo'];?>&email=<?php echo $dknckh['emailuser'];?>&dot=<?php echo $dotDuocChon; ?>" data-type="boduyetdk_danckh" class="tooltip-error btn btn-xs no-radius btn-danger" onclick="return confirm('Chắc chắn là bạn muốn bỏ duyệt kê khai này?');" title="Bỏ duyệt kê khai">
                                  <i class="ace-icon ace-icon glyphicon glyphicon-remove bigger-150"></i> 
                              </a>
                              <!-- PRINT -->
                              <?php $idDeAn = $dknckh['idDKDACapCoSo']; ?>
                              <a 
                                href="?key=MauIn-CS02&id_de_an=<?php echo $idDeAn; ?>"
                                data-type="print" class="btn btn-success btn-xs" onclick="return confirm('Bạn muốn in biểu mẫu này?');" title="In biểu mẫu!">
                                  <i class="ace-icon ace-icon glyphicon glyphicon-print bigger-150"></i> 
                                  <?php 
                                    if(isset($_GET['key']))
                                      switch($_GET['key'])
                                      {
                                        case "MauIn-CS02": 
                                          include("MauIn-CS02.php");
                                          $doc = new DomDocument;
                                          $doc->validateOnParse = true;
                                          $doc->Load('book.xml');
                                          echo "The element whose id is 'php-basics' is: " . $doc->getElementById('php-basics')->tagName . "\n";
                                        break;
                                      }
                                    else {
                                      include("addthanhviencs1.php");
                                    }?>
                              </a>
                            </div>
                          </div>
                        </td>
                    </tr>

                    <tr class="detail-row">
                        <td colspan="10">
                            <div class="table-detail">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="space visible-xs"></div>
                                        <div class="profile-user-info profile-user-info-striped">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Hoạt động </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['chonqldkdt'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Phân Loại </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['chonphanloaikhnc'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Tạp chí/hội thảo </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['tapchihoithao'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Hình Thức </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['chonmahinhthuc'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Vai Trò </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['mavaitro'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Sản phẩm </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['masosanpham'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Tên Sản phẩm </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['tensanpham'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Số quyết định </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['soquyetdinh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Thời gian hoàn thành </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['Thoi_Gian_Hoan_Thanh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Đợt kê khai </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['chondotkknckh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Năm kê khai </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['namhoanthanh'];?></span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        </tr>
                  <?php } 
                }else{ ?>
                  <tr>
                    <td colspan="8">Không có dữ liệu</td>
                  </tr>
                  <?php } ?>
            </tbody>
          </table>
        </div><!-- /.span --> 
      </div>

      <div class="row">
        <div class="col-xs-6">
          <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">Tổng số đăng ký là: <?php echo $tsdangky; ?> dòng</div>
        </div>
        <div class="col-xs-6">
          <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            <ul class="pagination">
              <?php for($i=1;$i<=$tst;$i++)
              if($i==$page){ ?>
                  <li class="active" aria-controls="dynamic-table" tabindex="0"><a href="#"><?php echo $i;?></a></li>
              <?php }
              else {?>
                <li class=''> 
                  <a href='<?php echo "http://localhost/quanlykhoahoc_bu/khoa/index.php?key=viewds-NCKH&page=".$i;?>'><?php echo $i;?></a> 
                </li>
              <?php }?>
            </ul>
          </div>
        </div>
      </div><!-- /.row --> 
      <!-- END ĐỀ ÁN -->
      
      <!-- 03 - BIÊN SOẠN -->
      <div class="row">
        <div class="col-md-1"> 
          <a class="btn btn-info" type="button"> 
            <i class="ace-icon bigger-110"></i> III - Biên soạn tài liệu học tập 
          </a> 
        </div>
          <?php 
            if(isset($_GET['dot'])){
              $dot = $_GET['dot'];
              // echo "<h1>$dot</h1>";
            } else {
              $dot = 1;
              // echo "<h1>KHÔNG CÓ ĐỢT</h1>";
            }
          ?>
                
          <form action="index.php" method="POST" align="center">
            <label for="dotdk"> Đợt đăng ký NCKH : </label>
            <?php 
              $dsdotdknckh = $db -> getRowsgom('nncms_quanlydotdangkynckh', array('where' => array('xoa' => '')));
              $dotDuocChon = 0;
              if(isset($_REQUEST['dot']) && !empty($_REQUEST['dot'])){
                $dotDuocChon = $_REQUEST['dot'];
                // echo "<h1>$dotDuocChon</h1>";
              }
            ?>
            <select id="dot" name="dotdk" onchange="window.location='./index.php?key=viewds-DKNCKH&dot='+ this.options[this.selectedIndex].value">
              <option $selected value='0'>Chọn đợt</option>
            <?php
              if(!empty($dsdotdknckh)){ 
                $count = 0;
                foreach($dsdotdknckh as $dotdknckh) {
                  $maDot = $dotdknckh['idDotdknckh'];
                  $tenDot = $dotdknckh['TenDotdangky_NCKH'];
                  $selected = $maDot == $dotDuocChon ? "selected" : "";
                  echo "<option $selected value='$maDot'>$tenDot</option>";
                }
              }
            ?>
            </select>
          </form> 
      </div>
      
      <div class="row">
        <div class="col-xs-12">
          <table id="dynamic-table" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th class="center"> #STT </th>
                <th class="hidden-280">Họ và tên</th>
                <th class="hidden-280">Tên tài liệu</th>
                <th>Trạng thái khoa duyệt</th>
                <th>Cập nhật</th>
                <th><i class="ace-icon fa fa-cog icon-only bigger-130"></i></th>
              </tr>
            </thead>
            
            <tbody>
              <?php 
                $vaitro = $_SESSION['sess_vaitro'];
                $madonvi = $_SESSION['sess_madv'];
                $emailnguoidk = $_SESSION['sess_email'];

                $sql = "SELECT *, CASE
                                    WHEN (TrangthaiDuyet_Boduyet = 'Bỏ duyệt' AND Trangthai_VienDuyet = 'Chưa duyệt') THEN 'Đăng ký mới'
                                    WHEN TrangthaiDuyet_Boduyet = '' THEN 'Đăng ký mới'
                                    WHEN Trangthai_VienDuyet = 'Viện đã duyệt' THEN 'Viện đã duyệt' 
                                    WHEN TrangthaiDuyet_Boduyet = 'Bỏ duyệt' THEN 'Bỏ duyệt'
                                    WHEN TrangthaiDuyet_Boduyet = 'Đơn vị đã duyệt' THEN 'Đơn vị đã duyệt'                               
                                  END as TrangThaiCapNhat
                        FROM nncms_dkbiensoantlht
                        WHERE ($vaitro = 3
                              OR ($vaitro = 2 AND madv = '$madonvi')
                              OR ($vaitro = 1 AND emailuser = '$emailnguoidk'))
                            AND idDotDKNCKH = $dotDuocChon";
                // echo "<h1>$madonvi</h1>";
                $dsdkbiensoantlht = $db -> runSQL($sql);

                // $tsdangky = $db -> demrow('nncms_dkdacapcoso', array(array('xoa' => '')));
                $sd=30;
                $tsdangky = count($dsdkbiensoantlht);
                $tst=ceil($tsdangky/$sd);// tinh tong so trang
                // Lay trang:
                if(isset($_GET['page'])){
                  $page=intval($_GET['page']);
                }
                else
                $page=1;
                //Tinh vi tri array('where'=>array('xoa'=>'')),
                $vt=($page-1)*$sd;
                $dsdknckh = $db->runSQL($sql);
                $stt=$vt+1;
                if(!empty($dsdknckh)){ 
                  $count = 0; 
                  foreach($dsdknckh as $dknckh){ 
                    $count++;
                ?>
                    <tr>
                        <td class="center"><?php echo $count;?></td>
                        <td class="hidden-280">
                            <?php echo $dknckh['HoTenChuNhiem'];?>
                        </td>
                        <td class="hidden-280">
                            <?php echo $dknckh['TenTaiLieu'];?>
                        </td>
                        <td ><?php echo $dknckh['TrangthaiDuyet_Boduyet'];?></td>
                        <td class="hidden-280">
                          <div class="action-buttons"> 
                            <a class="blue khoaduyet" data-toggle="modal" href="#stack5" 
                              data-iddknckh="<?php echo $dknckh['idDKBienSoanTLHT'];?>"
                              data-hoten="<?php echo $dknckh['HoTenChuNhiem'];?>"
                              data-trangthai="<?php echo $dknckh['TrangthaiDuyet_Boduyet'];?>"
                              data-action="updatekhoaduyet"> 
                              <i class="ace-icon fa fa-pencil-square-o bigger-130"></i> 
                            </a> 
                          </div>
                        </td>

                        <td>
                          <div class="">
                            <div class="inline pos-rel"> 
                              <!-- FILE -->
                              <a title="Quản lý file đính kèm" class="btn btn-xs btn-info no-radius blue file" href="#stack8"  data-toggle="modal" 
                                data-target="#stack8" 
                                data-filedknckh="<?php echo $dknckh['idDKBienSoanTLHT'];?>" 
                                onclick="xemfiledk_biensoantlht('<?php echo $dknckh['idfile'];?>','<?php echo $dknckh['file'];?>','<?php echo $dknckh['emailuser'];?>')"> 
                                <i class="ace-icon fa fa-folder-open-o bigger-150"></i> 
                              </a>
                              <!-- BỎ DUYỆT -->
                              <a href="boduyetdk_biensoantlht.php?action_type=boduyetdk_biensoantlht&id=<?php echo $dknckh['idDKBienSoanTLHT'];?>&email=<?php echo $dknckh['emailuser'];?>&dot=<?php echo $dotDuocChon; ?>" data-type="boduyetdk_biensoantlht" class="tooltip-error btn btn-xs no-radius btn-danger" onclick="return confirm('Chắc chắn là bạn muốn bỏ duyệt đăng ký biên soạn này?');" title="Bỏ duyệt biên soạn tài liệu">
                                  <i class="ace-icon ace-icon glyphicon glyphicon-remove bigger-150"></i> 
                              </a>
                              <!-- PRINT -->
                              <?php $idBienSoan = $dknckh['idDKBienSoanTLHT']; ?>
                              <a 
                                href="?key=MauIn-CS03&id_bs_tl=<?php echo $idBienSoan; ?>"
                                data-type="print" class="btn btn-success btn-xs" onclick="return confirm('Bạn muốn in biểu mẫu này?');" title="In biểu mẫu!">
                                  <i class="ace-icon ace-icon glyphicon glyphicon-print bigger-150"></i> 
                                  <?php 
                                    if(isset($_GET['key']))
                                      switch($_GET['key'])
                                      {
                                        case "MauIn-CS03": 
                                          include("MauIn-CS03.php");
                                          $doc = new DomDocument;
                                          $doc->validateOnParse = true;
                                          $doc->Load('book.xml');
                                          echo "The element whose id is 'php-basics' is: " . $doc->getElementById('php-basics')->tagName . "\n";
                                        break;
                                      }
                                    else {
                                      include("addthanhviencs1.php");
                                    }?>
                              </a>
                            </div>
                          </div>
                        </td>
                    </tr>

                    <tr class="detail-row">
                        <td colspan="10">
                            <div class="table-detail">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="space visible-xs"></div>
                                        <div class="profile-user-info profile-user-info-striped">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Hoạt động </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['chonqldkdt'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Phân Loại </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['chonphanloaikhnc'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Tạp chí/hội thảo </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['tapchihoithao'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Hình Thức </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['chonmahinhthuc'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Vai Trò </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['mavaitro'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Sản phẩm </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['masosanpham'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Tên Sản phẩm </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['tensanpham'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Số quyết định </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['soquyetdinh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Thời gian hoàn thành </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['Thoi_Gian_Hoan_Thanh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Đợt kê khai </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['chondotkknckh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Năm kê khai </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['namhoanthanh'];?></span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        </tr>
                  <?php } 
                }else{ ?>
                  <tr>
                    <td colspan="8">Không có dữ liệu</td>
                  </tr>
                  <?php } ?>
            </tbody>
          </table>
        </div><!-- /.span --> 
      </div>

      <div class="row">
        <div class="col-xs-6">
          <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">Tổng số đăng ký là: <?php echo $tsdangky; ?> dòng</div>
        </div>
        <div class="col-xs-6">
          <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            <ul class="pagination">
              <?php for($i=1;$i<=$tst;$i++)
              if($i==$page){ ?>
                  <li class="active" aria-controls="dynamic-table" tabindex="0"><a href="#"><?php echo $i;?></a></li>
              <?php }
              else {?>
                <li class=''> 
                  <a href='<?php echo "http://localhost/quanlykhoahoc_bu/khoa/index.php?key=viewds-NCKH&page=".$i;?>'><?php echo $i;?></a> 
                </li>
              <?php }?>
            </ul>
          </div>
        </div>
      </div><!-- /.row --> 
      <!-- END ĐỀ ÁN -->
      
      <!-- 04 - SÁNG KIẾN -->
      <div class="row">
        <div class="col-md-1"> 
          <a class="btn btn-info" type="button"> 
            <i class="ace-icon bigger-110"></i> IV - Quản lý sáng kiến cá nhân 
          </a> 
        </div>
          <?php 
            if(isset($_GET['dot'])){
              $dot = $_GET['dot'];
              // echo "<h1>$dot</h1>";
            } else {
              $dot = 1;
              // echo "<h1>KHÔNG CÓ ĐỢT</h1>";
            }
          ?>
                
          <form action="index.php" method="POST" align="center">
            <label for="dotdk"> Đợt đăng ký NCKH : </label>
            <?php 
              $dsdotdknckh = $db -> getRowsgom('nncms_quanlydotdangkynckh', array('where' => array('xoa' => '')));
              $dotDuocChon = 0;
              if(isset($_REQUEST['dot']) && !empty($_REQUEST['dot'])){
                $dotDuocChon = $_REQUEST['dot'];
                // echo "<h1>$dotDuocChon</h1>";
              }
            ?>
            <select id="dot" name="dotdk" onchange="window.location='./index.php?key=viewds-DKNCKH&dot='+ this.options[this.selectedIndex].value">
              <option $selected value='0'>Chọn đợt</option>
            <?php
              if(!empty($dsdotdknckh)){ 
                $count = 0;
                foreach($dsdotdknckh as $dotdknckh) {
                  $maDot = $dotdknckh['idDotdknckh'];
                  $tenDot = $dotdknckh['TenDotdangky_NCKH'];
                  $selected = $maDot == $dotDuocChon ? "selected" : "";
                  echo "<option $selected value='$maDot'>$tenDot</option>";
                }
              }
            ?>
            </select>
          </form> 
      </div>
      
      <div class="row">
        <div class="col-xs-12">
          <table id="dynamic-table" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th class="center"> #STT </th>
                <th class="hidden-280">Họ tên người đăng ký</th>
                <th class="hidden-280">Tên sáng kiến</th>
                <th>Trạng thái khoa duyệt</th>
                <th>Cập nhật</th>
                <th><i class="ace-icon fa fa-cog icon-only bigger-130"></i></th>
              </tr>
            </thead>
            
            <tbody>
              <?php 
                $vaitro = $_SESSION['sess_vaitro'];
                $madonvi = $_SESSION['sess_madv'];
                $emailnguoidk = $_SESSION['sess_email'];

                $sql = "SELECT *, CASE
                                    WHEN (TrangthaiDuyet_Boduyet = 'Bỏ duyệt' AND Trangthai_VienDuyet = 'Chưa duyệt') THEN 'Đăng ký mới'
                                    WHEN TrangthaiDuyet_Boduyet = '' THEN 'Đăng ký mới'
                                    WHEN Trangthai_VienDuyet = 'Viện đã duyệt' THEN 'Viện đã duyệt' 
                                    WHEN TrangthaiDuyet_Boduyet = 'Bỏ duyệt' THEN 'Bỏ duyệt'
                                    WHEN TrangthaiDuyet_Boduyet = 'Đơn vị đã duyệt' THEN 'Đơn vị đã duyệt'                               
                                  END as TrangThaiCapNhat
                        FROM nncms_dksangkiencn
                        WHERE ($vaitro = 3
                              OR ($vaitro = 2 AND madv = '$madonvi')
                              OR ($vaitro = 1 AND emailuser = '$emailnguoidk'))
                            AND idDotDKNCKH = $dotDuocChon";
                // echo "<h1>$madonvi</h1>";
                $dsdksangkiencn = $db -> runSQL($sql);

                // $tsdangky = $db -> demrow('nncms_dkdacapcoso', array(array('xoa' => '')));
                $sd=30;
                $tsdangky = count($dsdksangkiencn);
                $tst=ceil($tsdangky/$sd);// tinh tong so trang
                // Lay trang:
                if(isset($_GET['page'])){
                  $page=intval($_GET['page']);
                }
                else
                $page=1;
                //Tinh vi tri array('where'=>array('xoa'=>'')),
                $vt=($page-1)*$sd;
                $dsdknckh = $db->runSQL($sql);
                $stt=$vt+1;
                if(!empty($dsdknckh)){ 
                  $count = 0; 
                  foreach($dsdknckh as $dknckh){ 
                    $count++;
                ?>
                    <tr>
                        <td class="center"><?php echo $count;?></td>
                        <td class="hidden-280">
                            <?php echo $dknckh['HoTenNguoiDK'];?>
                        </td>
                        <td class="hidden-280">
                            <?php echo $dknckh['TenSangKien'];?>
                        </td>
                        <td ><?php echo $dknckh['TrangthaiDuyet_Boduyet'];?></td>
                        <td class="hidden-280">
                          <div class="action-buttons"> 
                            <a class="blue khoaduyet" data-toggle="modal" href="#stack6" 
                              data-iddknckh="<?php echo $dknckh['idDKSangKienCN'];?>"
                              data-hoten="<?php echo $dknckh['HoTenNguoiDK'];?>"
                              data-trangthai="<?php echo $dknckh['TrangthaiDuyet_Boduyet'];?>"
                              data-action="updatekhoaduyet"> 
                              <i class="ace-icon fa fa-pencil-square-o bigger-130"></i> 
                            </a> 
                          </div>
                        </td>

                        <td>
                          <div class="">
                            <div class="inline pos-rel"> 
                              <!-- FILE -->
                              <a title="Quản lý file đính kèm" class="btn btn-xs btn-info no-radius blue file" href="#stack8"  data-toggle="modal" 
                                data-target="#stack8" 
                                data-filedknckh="<?php echo $dknckh['idDKSangKienCN'];?>" 
                                onclick="xemfiledk_sangkiencn('<?php echo $dknckh['idfile'];?>','<?php echo $dknckh['file'];?>','<?php echo $dknckh['emailuser'];?>')"> 
                                <i class="ace-icon fa fa-folder-open-o bigger-150"></i> 
                              </a>
                              <!-- BỎ DUYỆT -->
                              <a href="boduyetdk_sangkiencn.php?action_type=boduyetdk_sangkiencn&id=<?php echo $dknckh['idDKSangKienCN'];?>&email=<?php echo $dknckh['emailuser'];?>&dot=<?php echo $dotDuocChon; ?>" data-type="boduyetdk_sangkiencn" class="tooltip-error btn btn-xs no-radius btn-danger" onclick="return confirm('Chắc chắn là bạn muốn bỏ duyệt sáng kiến cá nhân này?');" title="Bỏ duyệt sáng kiến cá nhân">
                                  <i class="ace-icon ace-icon glyphicon glyphicon-remove bigger-150"></i> 
                              </a>
                              <!-- PRINT -->
                              <?php $idSangKien = $dknckh['idDKSangKienCN']; ?>
                              <a 
                                href="?key=MauIn-CS04&id_sang_kien=<?php echo $idSangKien; ?>"
                                data-type="print" class="btn btn-success btn-xs" onclick="return confirm('Bạn muốn in biểu mẫu này?');" title="In biểu mẫu!">
                                  <i class="ace-icon ace-icon glyphicon glyphicon-print bigger-150"></i> 
                                  <?php 
                                    if(isset($_GET['key']))
                                      switch($_GET['key'])
                                      {
                                        case "MauIn-CS04": 
                                          include("MauIn-CS04.php");
                                          $doc = new DomDocument;
                                          $doc->validateOnParse = true;
                                          $doc->Load('book.xml');
                                          echo "The element whose id is 'php-basics' is: " . $doc->getElementById('php-basics')->tagName . "\n";
                                        break;
                                      }
                                    else {
                                      include("addthanhviencs1.php");
                                    }?>
                              </a>
                            </div>
                          </div>
                        </td>
                    </tr>

                    <tr class="detail-row">
                        <td colspan="10">
                            <div class="table-detail">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="space visible-xs"></div>
                                        <div class="profile-user-info profile-user-info-striped">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Hoạt động </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['chonqldkdt'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Phân Loại </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['chonphanloaikhnc'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Tạp chí/hội thảo </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['tapchihoithao'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Hình Thức </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['chonmahinhthuc'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Vai Trò </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['mavaitro'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Sản phẩm </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['masosanpham'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Tên Sản phẩm </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['tensanpham'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Số quyết định </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['soquyetdinh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Thời gian hoàn thành </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['Thoi_Gian_Hoan_Thanh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Đợt kê khai </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['chondotkknckh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Năm kê khai </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['namhoanthanh'];?></span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        </tr>
                  <?php } 
                }else{ ?>
                  <tr>
                    <td colspan="8">Không có dữ liệu</td>
                  </tr>
                  <?php } ?>
            </tbody>
          </table>
        </div><!-- /.span --> 
      </div>

      <div class="row">
        <div class="col-xs-6">
          <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">Tổng số đăng ký là: <?php echo $tsdangky; ?> dòng</div>
        </div>
        <div class="col-xs-6">
          <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            <ul class="pagination">
              <?php for($i=1;$i<=$tst;$i++)
              if($i==$page){ ?>
                  <li class="active" aria-controls="dynamic-table" tabindex="0"><a href="#"><?php echo $i;?></a></li>
              <?php }
              else {?>
                <li class=''> 
                  <a href='<?php echo "http://localhost/quanlykhoahoc_bu/khoa/index.php?key=viewds-NCKH&page=".$i;?>'><?php echo $i;?></a> 
                </li>
              <?php }?>
            </ul>
          </div>
        </div>
      </div><!-- /.row --> 
      <!-- END SÁNG KIẾN -->

      <!-- 05 - HỘI THẢO -->
      <div class="row">
        <div class="col-md-1"> 
          <a class="btn btn-info" type="button"> 
            <i class="ace-icon bigger-110"></i> V - Quản lý hội thảo 
          </a> 
        </div>
          <?php 
            if(isset($_GET['dot'])){
              $dot = $_GET['dot'];
              // echo "<h1>$dot</h1>";
            } else {
              $dot = 1;
              // echo "<h1>KHÔNG CÓ ĐỢT</h1>";
            }
          ?>
                
          <form action="index.php" method="POST" align="center">
            <label for="dotdk"> Đợt đăng ký NCKH : </label>
            <?php 
              $dsdotdknckh = $db -> getRowsgom('nncms_quanlydotdangkynckh', array('where' => array('xoa' => '')));
              $dotDuocChon = 0;
              if(isset($_REQUEST['dot']) && !empty($_REQUEST['dot'])){
                $dotDuocChon = $_REQUEST['dot'];
                // echo "<h1>$dotDuocChon</h1>";
              }
            ?>
            <select id="dot" name="dotdk" onchange="window.location='./index.php?key=viewds-DKNCKH&dot='+ this.options[this.selectedIndex].value">
              <option $selected value='0'>Chọn đợt</option>
            <?php
              if(!empty($dsdotdknckh)){ 
                $count = 0;
                foreach($dsdotdknckh as $dotdknckh) {
                  $maDot = $dotdknckh['idDotdknckh'];
                  $tenDot = $dotdknckh['TenDotdangky_NCKH'];
                  $selected = $maDot == $dotDuocChon ? "selected" : "";
                  echo "<option $selected value='$maDot'>$tenDot</option>";
                }
              }
            ?>
            </select>
          </form> 
      </div>
      
      <div class="row">
        <div class="col-xs-12">
          <table id="dynamic-table" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th class="center"> #STT </th>
                <th class="hidden-280">Tên đơn vị đăng ký</th>
                <th class="hidden-280">Chủ đề hội thảo</th>
                <th>Trạng thái khoa duyệt</th>
                <th>Cập nhật</th>
                <th><i class="ace-icon fa fa-cog icon-only bigger-130"></i></th>
              </tr>
            </thead>
            
            <tbody>
              <?php 
                $vaitro = $_SESSION['sess_vaitro'];
                $madonvi = $_SESSION['sess_madv'];
                $emailnguoidk = $_SESSION['sess_email'];

                $sql = "SELECT *, CASE
                                    WHEN (TrangthaiDuyet_Boduyet = 'Bỏ duyệt' AND Trangthai_VienDuyet = 'Chưa duyệt') THEN 'Đăng ký mới'
                                    WHEN TrangthaiDuyet_Boduyet = '' THEN 'Đăng ký mới'
                                    WHEN Trangthai_VienDuyet = 'Viện đã duyệt' THEN 'Viện đã duyệt' 
                                    WHEN TrangthaiDuyet_Boduyet = 'Bỏ duyệt' THEN 'Bỏ duyệt'
                                    WHEN TrangthaiDuyet_Boduyet = 'Đơn vị đã duyệt' THEN 'Đơn vị đã duyệt'                               
                                  END as TrangThaiCapNhat
                        FROM nncms_dkhoithao
                        WHERE ($vaitro = 3
                              OR ($vaitro = 2 AND madv = '$madonvi')
                              OR ($vaitro = 1 AND emailuser = '$emailnguoidk'))
                            AND idDotDKNCKH = $dotDuocChon";
                // echo "<h1>$madonvi</h1>";
                $dsdkhoithao = $db -> runSQL($sql);

                // $tsdangky = $db -> demrow('nncms_dkdacapcoso', array(array('xoa' => '')));
                $sd=30;
                $tsdangky = count($dsdkhoithao);
                $tst=ceil($tsdangky/$sd);// tinh tong so trang
                // Lay trang:
                if(isset($_GET['page'])){
                  $page=intval($_GET['page']);
                }
                else
                $page=1;
                //Tinh vi tri array('where'=>array('xoa'=>'')),
                $vt=($page-1)*$sd;
                $dsdknckh = $db->runSQL($sql);
                $stt=$vt+1;
                if(!empty($dsdknckh)){ 
                  $count = 0; 
                  foreach($dsdknckh as $dknckh){ 
                    $count++;
                ?>
                    <tr>
                        <td class="center"><?php echo $count;?></td>
                        <td class="hidden-280">
                            <?php echo $dknckh['TenDonViDK'];?>
                        </td>
                        <td class="hidden-280">
                            <?php echo $dknckh['ChuDeHoiThao'];?>
                        </td>
                        <td ><?php echo $dknckh['TrangthaiDuyet_Boduyet'];?></td>

                        <td class="hidden-280">
                          <div class="action-buttons"> 
                            <a class="blue khoaduyet" data-toggle="modal" href="#stack7" 
                              data-iddknckh="<?php echo $dknckh['idDKHoiThao'];?>"
                              data-hoten="<?php echo $dknckh['TenDonViDK'];?>"
                              data-trangthai="<?php echo $dknckh['TrangthaiDuyet_Boduyet'];?>"
                              data-action="updatekhoaduyet"> 
                              <i class="ace-icon fa fa-pencil-square-o bigger-130"></i> 
                            </a> 
                          </div>
                        </td>

                        <td>
                          <div class="">
                            <div class="inline pos-rel"> 
                              
                              <!-- BỎ DUYỆT -->
                              <a href="boduyetdk_hoithao.php?action_type=boduyetdk_hoithao&id=<?php echo $dknckh['idDKHoiThao'];?>&email=<?php echo $dknckh['emailuser'];?>&dot=<?php echo $dotDuocChon; ?>" data-type="boduyetdk_hoithao" class="tooltip-error btn btn-xs no-radius btn-danger" onclick="return confirm('Chắc chắn là bạn muốn bỏ duyệt kê khai này?');" title="Bỏ duyệt kê khai">
                                  <i class="ace-icon ace-icon glyphicon glyphicon-remove bigger-150"></i> 
                              </a>
                              <!-- PRINT -->
                              <?php $idHoiThao = $dknckh['idDKHoiThao']; ?>
                              <a 
                                href="?key=MauIn-CS05&id_hoi_thao=<?php echo $idHoiThao; ?>"
                                data-type="print" class="btn btn-success btn-xs" onclick="return confirm('Bạn muốn in biểu mẫu này?');" title="In biểu mẫu!">
                                  <i class="ace-icon ace-icon glyphicon glyphicon-print bigger-150"></i> 
                                  <?php 
                                    if(isset($_GET['key']))
                                      switch($_GET['key'])
                                      {
                                        case "MauIn-CS05": 
                                          include("MauIn-CS05.php");
                                          $doc = new DomDocument;
                                          $doc->validateOnParse = true;
                                          $doc->Load('book.xml');
                                          echo "The element whose id is 'php-basics' is: " . $doc->getElementById('php-basics')->tagName . "\n";
                                        break;
                                      }
                                    else {
                                      include("addthanhviencs1.php");
                                    }?>
                              </a>
                            </div>
                          </div>
                        </td>
                    </tr>

                    <tr class="detail-row">
                        <td colspan="10">
                            <div class="table-detail">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="space visible-xs"></div>
                                        <div class="profile-user-info profile-user-info-striped">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Hoạt động </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['chonqldkdt'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Phân Loại </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['chonphanloaikhnc'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Tạp chí/hội thảo </div>
                                                <div class="profile-info-value"> 
                                                    <span><?php echo $nckh['tapchihoithao'];?></span> 
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Hình Thức </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['chonmahinhthuc'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Vai Trò </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['mavaitro'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Mã Sản phẩm </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['masosanpham'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Tên Sản phẩm </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['tensanpham'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Số quyết định </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['soquyetdinh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Thời gian hoàn thành </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['Thoi_Gian_Hoan_Thanh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Đợt kê khai </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['chondotkknckh'];?></span> </div>
                                            </div>
                                            <div class="profile-info-row">
                                            <div class="profile-info-name"> Năm kê khai </div>
                                            <div class="profile-info-value"> <span><?php echo $nckh['namhoanthanh'];?></span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        </tr>
                  <?php } 
                }else{ ?>
                  <tr>
                    <td colspan="8">Không có dữ liệu</td>
                  </tr>
                  <?php } ?>
            </tbody>
          </table>
        </div><!-- /.span --> 
      </div>

      <div class="row">
        <div class="col-xs-6">
          <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">Tổng số đăng ký là: <?php echo $tsdangky; ?> dòng</div>
        </div>
        <div class="col-xs-6">
          <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            <ul class="pagination">
              <?php for($i=1;$i<=$tst;$i++)
              if($i==$page){ ?>
                  <li class="active" aria-controls="dynamic-table" tabindex="0"><a href="#"><?php echo $i;?></a></li>
              <?php }
              else {?>
                <li class=''> 
                  <a href='<?php echo "http://localhost/quanlykhoahoc_bu/khoa/index.php?key=viewds-NCKH&page=".$i;?>'><?php echo $i;?></a> 
                </li>
              <?php }?>
            </ul>
          </div>
        </div>
      </div><!-- /.row --> 
      <!-- END HỘI THẢO -->

      <!-- PAGE CONTENT ENDS --> 
    </div><!-- /.col -->
  </div><!-- /.row -->
</div>

<!-- 1 - CẬP NHẬT DUYỆT ĐỀ TÀI -->
<div id="stack3" class="modal fade" >
  <form role="form" id="universalModalForm" method="post" action="capnhatduyet_dknckh.php?dot=<?php echo $dotDuocChon; ?>" class="form" >
    <div class="modal-dialog modal-lg"> 
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Đơn vị duyệt</h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> 
            <span class="alert-body"></span> 
          </div>
          <div class="modal-body">
            <table class="table" >
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Họ và tên:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <strong class="hoten"></strong>
                </td>
              </tr>
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Trạng thái:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <strong class="trangthai"></strong>
                </td>
              </tr>
              <tr>
                <td colspan="2" >
                  <input type="hidden" name="email" id="email" class="email-an" value="<?php echo $_SESSION['sess_email'];?>">
                  <input  type="hidden" class="modal-id iddknckh" name="iddknckh"/>
                  <input type="hidden" name="ngay"  value="<?php echo date("m/d/Y");?>"/>
                  <input  type="hidden" value="<?php echo $_GET['key'];?>" name="url"/>
                  <input type="hidden" name="action_type" class="action" value=""/>
                  <button type="submit" name="submit" class="btn btn-danger"> 
                    <i class="glyphicon glyphicon-send bigger-150"></i> Cập nhật
                  </button>
                </td>
              </tr>
              <tr>
                <td colspan="2" ><div id="tnad"></div></td>
              </tr>
            </table>
            <div id="tnad"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </form>
</div>

<!-- 2 - CẬP NHẬT DUYỆT ĐỀ ÁN -->
<div id="stack4" class="modal fade" >
  <form role="form" id="universalModalForm" method="post" action="capnhatduyetdk_danckh.php?dot=<?php echo $dotDuocChon; ?>" class="form" >
    <div class="modal-dialog modal-lg"> 
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Đơn vị duyệt</h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> 
            <span class="alert-body"></span> 
          </div>
          <div class="modal-body">
            <table class="table" >
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Họ và tên:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <strong class="hoten"></strong>
                </td>
              </tr>
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Trạng thái:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <strong class="trangthai"></strong>
                </td>
              </tr>
              <tr>
                <td colspan="2" >
                  <input type="hidden" name="email" id="email" class="email-an" value="<?php echo $_SESSION['sess_email'];?>">
                  <input  type="hidden" class="modal-id iddknckh" name="iddknckh"/>
                  <input type="hidden" name="ngay"  value="<?php echo date("m/d/Y");?>"/>
                  <input  type="hidden" value="<?php echo $_GET['key'];?>" name="url"/>
                  <input type="hidden" name="action_type" class="action" value=""/>
                  <button type="submit" name="submit" class="btn btn-danger"> 
                    <i class="glyphicon glyphicon-send bigger-150"></i> Cập nhật
                  </button>
                </td>
              </tr>
              <tr>
                <td colspan="2" ><div id="tnad"></div></td>
              </tr>
            </table>
            <div id="tnad"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </form>
</div>
<!-- 3 - CẬP NHẬT DUYỆT BIÊN SOẠN -->
<div id="stack5" class="modal fade" >
  <form role="form" id="universalModalForm" method="post" action="capnhatduyetdk_biensoantlht.php?dot=<?php echo $dotDuocChon; ?>" class="form" >
    <div class="modal-dialog modal-lg"> 
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Đơn vị duyệt</h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> 
            <span class="alert-body"></span> 
          </div>
          <div class="modal-body">
            <table class="table" >
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Họ và tên:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <strong class="hoten"></strong>
                </td>
              </tr>
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Trạng thái:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <strong class="trangthai"></strong>
                </td>
              </tr>
              <tr>
                <td colspan="2" >
                  <input type="hidden" name="email" id="email" class="email-an" value="<?php echo $_SESSION['sess_email'];?>">
                  <input  type="hidden" class="modal-id iddknckh" name="iddknckh"/>
                  <input type="hidden" name="ngay"  value="<?php echo date("m/d/Y");?>"/>
                  <input  type="hidden" value="<?php echo $_GET['key'];?>" name="url"/>
                  <input type="hidden" name="action_type" class="action" value=""/>
                  <button type="submit" name="submit" class="btn btn-danger"> 
                    <i class="glyphicon glyphicon-send bigger-150"></i> Cập nhật
                  </button>
                </td>
              </tr>
              <tr>
                <td colspan="2" ><div id="tnad"></div></td>
              </tr>
            </table>
            <div id="tnad"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </form>
</div>

<!-- 4 - CẬP NHẬT DUYỆT SÁNG KIẾN -->
<div id="stack6" class="modal fade" >
  <form role="form" id="universalModalForm" method="post" action="capnhatduyetdk_sangkiencn.php?dot=<?php echo $dotDuocChon; ?>" class="form" >
    <div class="modal-dialog modal-lg"> 
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Đơn vị duyệt</h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> 
            <span class="alert-body"></span> 
          </div>
          <div class="modal-body">
            <table class="table" >
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Họ và tên:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <strong class="hoten"></strong>
                </td>
              </tr>
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Trạng thái:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <strong class="trangthai"></strong>
                </td>
              </tr>
              <tr>
                <td colspan="2" >
                  <input type="hidden" name="email" id="email" class="email-an" value="<?php echo $_SESSION['sess_email'];?>">
                  <input  type="hidden" class="modal-id iddknckh" name="iddknckh"/>
                  <input type="hidden" name="ngay"  value="<?php echo date("m/d/Y");?>"/>
                  <input  type="hidden" value="<?php echo $_GET['key'];?>" name="url"/>
                  <input type="hidden" name="action_type" class="action" value=""/>
                  <button type="submit" name="submit" class="btn btn-danger"> 
                    <i class="glyphicon glyphicon-send bigger-150"></i> Cập nhật
                  </button>
                </td>
              </tr>
              <tr>
                <td colspan="2" ><div id="tnad"></div></td>
              </tr>
            </table>
            <div id="tnad"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </form>
</div>
<!-- 5 - CẬP NHẬT DUYỆT HỘI THẢO -->
<div id="stack7" class="modal fade" >
  <form role="form" id="universalModalForm" method="post" action="capnhatduyetdk_hoithao.php?dot=<?php echo $dotDuocChon; ?>" class="form" >
    <div class="modal-dialog modal-lg"> 
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Đơn vị duyệt</h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> 
            <span class="alert-body"></span> 
          </div>
          <div class="modal-body">
            <table class="table" >
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Họ và tên:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <strong class="hoten"></strong>
                </td>
              </tr>
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Trạng thái:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <strong class="trangthai"></strong>
                </td>
              </tr>
              <tr>
                <td colspan="2" >
                  <input type="hidden" name="email" id="email" class="email-an" value="<?php echo $_SESSION['sess_email'];?>">
                  <input  type="hidden" class="modal-id iddknckh" name="iddknckh"/>
                  <input type="hidden" name="ngay"  value="<?php echo date("m/d/Y");?>"/>
                  <input  type="hidden" value="<?php echo $_GET['key'];?>" name="url"/>
                  <input type="hidden" name="action_type" class="action" value=""/>
                  <button type="submit" name="submit" class="btn btn-danger"> 
                    <i class="glyphicon glyphicon-send bigger-150"></i> Cập nhật
                  </button>
                </td>
              </tr>
              <tr>
                <td colspan="2" ><div id="tnad"></div></td>
              </tr>
            </table>
            <div id="tnad"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </form>
</div>
<!-- 5 - FILE ĐÍNH KÈM -->
<div class="modal fade" id="stack8" role="dialog">
  <div class="modal-dialog modal-lg"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"  data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Quản lý file đính kèm</h4>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div id="tnad"></div>
          <div id="qlfl"></div>
        </div>
      </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>