<div id="sidebar" class="sidebar h-sidebar navbar-collapse collapse ace-save-state"> 
  <script type="text/javascript">
    try{ace.settings.loadState('sidebar')}catch(e){}
  </script>
  <div class="sidebar-shortcuts" id="sidebar-shortcuts">
    <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
      <button class="btn btn-success"> <i class="ace-icon fa fa-signal"></i> </button>
      <button class="btn btn-info"> <i class="ace-icon fa fa-pencil"></i> </button>
      <button class="btn btn-warning"> <i class="ace-icon fa fa-users"></i> </button>
      <button class="btn btn-danger"> <i class="ace-icon fa fa-cogs"></i> </button>
    </div>
    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini"> 
      <span class="btn btn-success"></span> 
      <span class="btn btn-info"></span> 
      <span class="btn btn-warning"></span> 
      <span class="btn btn-danger"></span> 
    </div>
  </div><!-- /.sidebar-shortcuts -->
  
  <ul class="nav nav-list">
		<?php 
			
			$sql = "SELECT m.*, md.dot, dot_kk.endtimes as dot_kk_endtimes, dot_dk.endtimes as dot_dk_endtimes,
						(md.dot IS NULL 
							OR (md.dot = 'nncms_quanlydotkhekhainckh' AND CURDATE() <= dot_kk.endtimes)
							OR (md.dot = 'nncms_quanlydotdangkynckh' AND CURDATE() <= dot_dk.endtimes)
						) as enable
					FROM menus m
					LEFT JOIN menu_dot md ON m.idMenu = md.id_menu
					LEFT JOIN nncms_quanlydotkhekhainckh dot_kk ON md.dot = 'nncms_quanlydotkhekhainckh'
					LEFT JOIN nncms_quanlydotdangkynckh dot_dk ON md.dot = 'nncms_quanlydotdangkynckh'
					WHERE vitri = 'khoa/DS_NCKH'
					ORDER BY thutu ASC";
			$menus = $db->runSQL($sql);
  			
			if(!empty($menus)){ 
				$count = 0; 
				foreach($menus as $menu) { 
					$thong_bao = "alert('Chưa mở đợt')";
					
					?>
        			<li class="hover">
						<a href="<?php if($menu['enable']) echo $menu['LienKet'];?>" onclick="<?php if(!$menu['enable']) echo $thong_bao; ?>" class="<?php echo $menu['class'];?>">
							<i class="<?php echo $menu['icon'];?>"></i>
							<span class="menu-text"> <?php echo $menu['TieuDe'];?></span>
			
							<?php 
								$submenus = $db->getRows('submenu', array('where'=>array('idCapCha'=>$menu['idMenu'], 'vitri'=>'khoa/DS_NCKH')), array('order_by'=>'ThuTu ASC'));
								if(!empty($submenus)) { 
									$count = 0; 
									foreach($submenus as $submenu) { 
										$count++;
										echo "<b class='arrow fa fa-angle-down'></b>";
									}
								}
							?>
						</a>

						<b class="arrow"></b>
			
						<?php 
							$subme = $db->getRows('submenu', array('where'=>array('idCapCha'=>$menu['idMenu'], 'vitri'=>'khoa/DS_NCKH')),array('order_by'=>'ThuTu ASC'));
							if(!empty($subme)){ 
								// $count = 0; 
								foreach($subme as $submen) { 
									// $count++;
									?>
									<ul class="submenu">
									<?php 	
										$sub = $db->getRows('submenu', array('where'=>array('idCapCha'=>$menu['idMenu'], 'vitri'=>'khoa/DS_NCKH')), array('order_by'=>'ThuTu ASC'));
										
										if(!empty($sub)){ 
											// $count = 0; 
											foreach($sub as $subm) { 
												// $count++;
												?>

												<li class="hover">
													<a href="<?php if($menu['enable']) echo $subm['LienKet'];?>" onclick="<?php if(!$menu['enable']) echo $thong_bao; ?>">
														<i class="<?php echo $subm['icon'];?>"></i>
														<?php echo $subm['TenSubmenu'];?>
													</a>
													<b class="arrow"></b>
												</li>

											<?php }
										}// ket thuc menu con?>
									</ul>
								<?php }
							}?>
					</li>
				<?php }
			}// het menu cha?><!-- /.nav-list --> 

	</ul>
</div>
