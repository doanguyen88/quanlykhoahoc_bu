<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.js"></script>
<script src="../giaodien/js/upload.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts4.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts7.js" type="text/javascript"></script>
<script src="../assets/js/jquery-ui.min.js"></script>
<script type="text/javascript">
  function getNewVal(item)
  {
    var laydong=item.value;
    $( ".laydong" ).val( laydong );

  }
  $(document).on( "click", '.file',function(e) {
    var filekhoakknckh = $(this).data('filekhoakknckh');
    $(".filekhoakknckh").val(filekhoakknckh);
  });
	
  $(document).on( "click", '.khoaduyet', function(e) {  //bắt sự kiên onclick của element có class = 'khoaduyet'
    var idkkstnckh = $(this).data('idkkstnckh');
    var hoten = $(this).data('hoten');
    var chonqldkdt = $(this).data('chonqldkdt');
    var chonphanloaikhnc = $(this).data('chonphanloaikhnc');
    var masanphamkhnc = $(this).data('masanphamkhnc');
    var tensanpham = $(this).data('tensanpham');
    var chonmahinhthuc = $(this).data('chonmahinhthuc');
    var mavaitro = $(this).data('mavaitro');
    var soquyetdinh = $(this).data('soquyetdinh');
    var thoigianhoanthanh = $(this).data('thoigianhoanthanh');
    var sotietquydoi = $(this).data('sotietquydoi');
    var trangthai = $(this).data('trangthai');
    var action = $(this).data('action');  //action được gán giá trị từ... 
    var tapchihoithao = $(this).data('tapchihoithao'); 
    $(".idkkstnckh").val(idkkstnckh);
    $(".hoten").text(hoten);
    $(".chonqldkdt").text(chonqldkdt);
    $(".chonphanloaikhnc").text(chonphanloaikhnc);	
    $(".masanphamkhnc").text(masanphamkhnc);
    $(".tensanpham").text(tensanpham);
    $(".chonmahinhthuc").text(chonmahinhthuc);
    $(".mavaitro").text(mavaitro);
    $(".soquyetdinh").text(soquyetdinh);
    $(".thoigianhoanthanh").text(thoigianhoanthanh);
    $(".sotietquydoi").text(sotietquydoi);
    $(".sotietquydoikn").val(sotietquydoi);
    $(".trangthai").text(trangthai);
    $(".action").val(action); //element có class = 'action' sẽ được gán giá trị = action
    $(".tapchihoithao").val(tapchihoithao);
  });
</script>

<div class="page-content">
  <div class="ace-settings-container" id="ace-settings-container">
    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn"> <i class="ace-icon fa fa-cog bigger-130"></i></div>
    <div class="ace-settings-box clearfix" id="ace-settings-box">
      <div class="pull-left width-50">
        <div class="ace-settings-item">
          <div class="pull-left">
            <select id="skin-colorpicker" class="hide">
              <option data-skin="no-skin" value="#438EB9">#438EB9</option>
              <option data-skin="skin-1" value="#222A2D">#222A2D</option>
              <option data-skin="skin-2" value="#C6487E">#C6487E</option>
              <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
            </select>
          </div>
          <span>&nbsp; Choose Skin</span> 
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
          <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
          <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
          <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
          <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
          <label class="lbl" for="ace-settings-add-container"> Inside <b>.container</b> </label>
        </div>
      </div><!-- /.pull-left -->
      
      <div class="pull-left width-50">
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
          <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
          <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
          <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
        </div>
      </div><!-- /.pull-left --> 
    </div><!-- /.ace-settings-box --> 
  </div><!-- /.ace-settings-container --> 
  <!-- /.page-header -->
  <div class="row">
    <div class="col-xs-12"> 
      <!-- PAGE CONTENT BEGINS -->
      <div class="row">
        <div class="col-md-1"> 
          <a class="btn btn-info" type="button" href="http://localhost/quanlykhoahoc_bu/khoa/index.php?key=Export-Excell-NCKH"> 
            <i class="ace-icon  fa fa-file-excel-o bigger-110"></i> Xuất báo cáo 
          </a> 
        </div>
      </div>
       
      <div class="row">
        <div class="col-xs-12">
          <!-- </div> -->
          <table id="dynamic-table" class="table  table-bordered table-hover">
            <thead>
              <tr>
                <th class="center"> #STT </th>
                <th>Chi tiết</th>
                <th class="hidden-280">Họ và tên</th>
                <th>Số tiết quy đổi</th>
                <th>Trạng thái</th>
                <th class="hidden-280">Số tiết khoa duyệt</th>
                <th class="hidden-280">Số tiết Viện NC duyệt</th>
                <th><i class="ace-icon fa fa-cog icon-only bigger-130"></i></th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $tsdangky = $db -> demrow('nncms_KKST_NCKH', array('where' => array('madv' => $_SESSION['sess_madv']), array('xoa' => '')));
                $sd=30;
                $tst=ceil($tsdangky/$sd);// tinh tong so trang
                // Lay trang:
                if(isset($_GET['page'])){
                  $page=intval($_GET['page']);
                }
                else
                $page=1;
                //Tinh vi tri array('where'=>array('xoa'=>'')),
                $vt=($page-1)*$sd;
                $dskkstnckh = $db->getRowsgom('nncms_KKST_NCKH',array('where'=>array('madv'=>$_SESSION['sess_madv']),array('xoa'=>'')),array('order_by'=>'Ho_Ten ASC'),array('start'=>$vt),array('limit'=>$sd));
          
                $stt=$vt+1;
                if(!empty($dskkstnckh)){ 
                  $count = 0; 
                  foreach($dskkstnckh as $nckh){ 
                    $count++;?>
                    <tr>
                      <td class="center"><?php echo $count;?></td>
                      <td class="center">
                        <div class="action-buttons"> 
                          <a href="#" class="green bigger-140 show-details-btn" title="Xem chi tiết"> 
                            <i class="ace-icon fa fa-angle-double-down"></i> 
                            <span class="sr-only">Xem chi tiết</span> 
                          </a> 
                        </div>
                      </td>
                      <td class="hidden-280"><?php echo $nckh['Ho_Ten'];?></td>
                      <td><?php echo $nckh['SoTietQuyDoi'];?></td>
                      <td ><?php echo $nckh['TrangThai'];?></td>
                      <td class="hidden-280"><?php if( $nckh['SoTietKhoaDuyet']!=""){
                        echo $nckh['SoTietKhoaDuyet'];
                        }else{
                          ?>
                          <div class="action-buttons"> 
                            <a class="blue khoaduyet" data-toggle="modal" href="#stack3" 
                              data-idkkstnckh="<?php echo $nckh['id_KKST_NCKH'];?>"
                              data-hoten="<?php echo $nckh['Ho_Ten'];?>"
                              data-chonqldkdt="<?php echo $nckh['chonqldkdt'];?>"
                              data-chonphanloaikhnc="<?php echo $nckh['chonphanloaikhnc'];?>"
                              data-masanphamkhnc="<?php echo $nckh['masosanpham'];?>"
                              data-tensanpham="<?php echo $nckh['tensanpham'];?>"
                              data-tapchihoithao="<?php echo $nckh['tapchihoithao'];?>"
                              data-chonmahinhthuc="<?php echo $nckh['chonmahinhthuc'];?>"
                              data-mavaitro="<?php echo $nckh['mavaitro'];?>"
                              data-soquyetdinh="<?php echo $nckh['soquyetdinh'];?>"
                              data-thoigianhoanthanh="<?php echo $nckh['Thoi_Gian_Hoan_Thanh'];?>"
                              data-sotietquydoi="<?php echo $nckh['SoTietQuyDoi'];?>"
                              data-trangthai="<?php echo $nckh['TrangThai'];?>"
                              data-action="updatekhoaduyet"> 
                              <i class="ace-icon fa fa-pencil-square-o bigger-130"></i> 
                            </a> 
                          </div>
                        <?php }?>
                      </td>
                      <td class="hidden-280"><?php echo $nckh['SoTietVienNCDuyet'];?></td>
                      <td>
                        <div class="">
                          <div class="inline pos-rel"> 
                            <a class="btn btn-xs btn-info no-radius blue file" href="#stack4"  data-toggle="modal" data-target="#stack4" data-filekhoakknckh="<?php echo $nckh['id_KKST_NCKH'];?>"onclick="xemfilekhoakknckh('<?php echo $nckh['idfile'];?>','<?php echo $nckh['file'];?>','<?php echo $nckh['emailuser'];?>')"> 
                              <i class="ace-icon fa fa-folder-open-o bigger-150"></i> 
                            </a>
                
                            <a href="boduyetkknckh.php?action_type=boduyetkknckh&id=<?php echo $nckh['id_KKST_NCKH'];?>&email=<?php echo $nckh['emailuser'];?>" data-type="boduyetkknckh" class="tooltip-error btn btn-xs no-radius btn-danger" onclick="return confirm('Chắc chắn là bạn muốn bỏ duyệt kê khai này?');"  title="Bỏ duyệt kê khai">
                              <i class="ace-icon ace-icon glyphicon glyphicon-remove bigger-150"></i> 
                            </a>
                            
                            <a href="boduyetkknckh.php?action_type=boduyetkknckh&id=<?php echo $nckh['id_KKST_NCKH'];?>&email=<?php echo $nckh['emailuser'];?>" data-type="boduyetkknckh" class="btn btn-success btn-xs" onclick="return confirm('Bạn muốn in biểu mẫu này?');"  title="In mẫu đã đăng ký">
                              <i class="ace-icon ace-icon glyphicon glyphicon-print bigger-150"></i> 
                            </a>
                          </div>
                        </div>
                      </td>
                    </tr>

                    <tr class="detail-row">
                      <td colspan="10">
                        <div class="table-detail">
                          <div class="row">
                            <div class="col-xs-12 col-sm-12">
                              <div class="space visible-xs"></div>
                              <div class="profile-user-info profile-user-info-striped">
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Hoạt động </div>
                                  <div class="profile-info-value"> <span><?php echo $nckh['chonqldkdt'];?></span> </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Phân Loại </div>
                                  <div class="profile-info-value"> <span><?php echo $nckh['chonphanloaikhnc'];?></span> </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Tạp chí/hội thảo </div>
                                  <div class="profile-info-value"> <span><?php echo $nckh['tapchihoithao'];?></span> </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Mã Hình Thức </div>
                                  <div class="profile-info-value"> <span><?php echo $nckh['chonmahinhthuc'];?></span> </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Mã Vai Trò </div>
                                  <div class="profile-info-value"> <span><?php echo $nckh['mavaitro'];?></span> </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Mã Sản phẩm </div>
                                  <div class="profile-info-value"> <span><?php echo $nckh['masosanpham'];?></span> </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Tên Sản phẩm </div>
                                  <div class="profile-info-value"> <span><?php echo $nckh['tensanpham'];?></span> </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Số quyết định </div>
                                  <div class="profile-info-value"> <span><?php echo $nckh['soquyetdinh'];?></span> </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Thời gian hoàn thành </div>
                                  <div class="profile-info-value"> <span><?php echo $nckh['Thoi_Gian_Hoan_Thanh'];?></span> </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Đợt kê khai </div>
                                  <div class="profile-info-value"> <span><?php echo $nckh['chondotkknckh'];?></span> </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Năm kê khai </div>
                                  <div class="profile-info-value"> <span><?php echo $nckh['namhoanthanh'];?></span> </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                  <?php } 
                }else{ ?>
                  <tr>
                    <td colspan="8">Không có dữ liệu</td>
                  </tr>
                  <?php } ?>
            </tbody>
          </table>
        </div><!-- /.span --> 
      </div>

      <div class="row">
        <div class="col-xs-6">
          <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">Tổng số đăng ký là: <?php echo $tsdangky; ?> dòng</div>
        </div>
        <div class="col-xs-6">
          <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            <ul class="pagination">
              <?php for($i=1;$i<=$tst;$i++)
              if($i==$page){ ?>
                  <li class="active" aria-controls="dynamic-table" tabindex="0"><a href="#"><?php echo $i;?></a></li>
              <?php }
              else {?>
                <li class=''> 
                  <a href='<?php echo "http://localhost/quanlykhoahoc_bu/khoa/index.php?key=viewds-NCKH&page=".$i;?>'><?php echo $i;?></a> 
                </li>
              <?php }?>
            </ul>
          </div>
        </div>
      </div><!-- /.row --> 
      <!-- PAGE CONTENT ENDS --> 
    </div><!-- /.col -->
  </div><!-- /.row -->
</div>

<div id="stack3" class="modal fade" >
  <form role="form" id="universalModalForm" method="post" action="capnhatkhoaduyet.php" class="form" >
    <div class="modal-dialog modal-lg"> 
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Đơn vị duyệt</h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
          <div class="modal-body">
            <table class="table" >
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Họ tên:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><strong class="hoten"></strong></td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Hoạt động nckh:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><strong class="chonqldkdt"></strong></td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Phân loại:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><strong class="chonphanloaikhnc"></strong></td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Mã sản phẩm:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><strong class="masanphamkhnc"></strong></td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Tên sản phẩm:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><strong class="tensanpham"></strong></td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Mã hình thức:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><strong class="chonmahinhthuc"></strong></td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Mã vai trò:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><strong class="mavaitro"></strong></td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Số quyết đinh:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><strong class="soquyetdinh"></strong></td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Thời gian hoàn thành:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><strong class="thoigianhoanthanh"></strong></td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Sô tiết quy đổi:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><strong class="sotietquydoi"></strong></td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Trạng thái:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><strong class="trangthai"></strong></td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Số tiết khoa duyệt:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><input  type="text" class="col-xs-8 sotietquydoikn" name="SoTietQuyDoi" id="SoTietQuyDoi" ></td>
              </tr>
              <tr>
                <td colspan="2" ><input type="hidden" name="email" id="email" class="email-an" value="<?php echo $_SESSION['sess_email'];?>">
                  <input  type="hidden" class="modal-id idkkstnckh" name="idkkstnckh"/>
                  <input type="hidden" name="ngay"  value="<?php echo date("m/d/Y");?>"/>
                  <input  type="hidden" value="<?php echo $_GET['key'];?>"  name="url"/>
                  <input type="hidden" name="action_type" class="action" value=""/>
                  <button type="submit" name="submit" class="btn btn-danger"> <i class="glyphicon glyphicon-send bigger-150"></i> Cập nhật</button></td>
              </tr>
              <tr>
                <td colspan="2" ><div id="tnad"></div></td>
              </tr>
            </table>
            <div id="tnad"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </form>
</div>

<div class="modal fade" id="stack4" role="dialog">
  <div class="modal-dialog modal-lg"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"  data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Quản lý file đính kèm</h4>
      </div>
      <div class="row">
        <div class="col-md-12" >
          <div id="trakq"> </div>
        </div>
      </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>

<!-- /.main-content -->