<?php
session_start();
include 'MySQL/DB.php';
$db = new DB();

$dateFormatedtimex = explode('/', $_POST['ngay']);
$datetimex = $dateFormatedtimex[2].'-'.$dateFormatedtimex[0].'-'.$dateFormatedtimex[1];
$isSuccess = true;

if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){
    if($_REQUEST['action_type'] == 'add'){
		$dem = $db->demrow('nncms_dkhoithao', array('where'=>array('idPhanLoai_HT_GT'=>'HT')));
		//kiểm tra số thứ tự chưa tồn tại ------------------------------------------------------------------		
		if($dem == 0) {
			//cập nhật stt đăng ký vào bảng user---------------------------------------------------				
			$dem=1;
			$Ma='HT00';
			$add="$Ma$dem"; //'HT001'
			$userData = array('stt_dt' => $add,);
			$condition = array('email' => $_SESSION['sess_email']);
			$update = $db->update('user',$userData,$condition);
			$sql = "SELECT idDotdknckh
							FROM nncms_quanlydotdangkynckh";
			$result = $db -> runSQL($sql); 
			$iddotdk = $result[0][0];
			echo "<h1>Mã đợt: $iddotdk</h1>";
			echo "<h1>$sql</h1>";
			//ghi thông tin đăng ký------------------------------------------------------------------			
			$ttdangkyData = array(
				'idPhanLoai_HT_GT' => 'HT', 
				'TrangThai' => $_POST['trangthai'],
				'DC_STT'=>$add,
				'DonVi' => $_POST['donvi'],
				'TenDonViDK' => $_POST['Ten_Don_Vi'],
				'ChuDeHoiThao' => $_POST['Chu_De_Hoi_Thao'],
				'LinhVucApDung' => $_POST['Linh_Vuc_Ap_Dung'],
				'TomTatNoiDungHT' => $_POST['Tom_Tat_Noi_Dung'],
				'ThoiGianDiaDiemTC' => $_POST['ThoiGian_DiaDiem_TC'],
				'ThanhPhanThamDu' => $_POST['Thanh_Phan_Tham_Du'],
				'KinhPhi' => $_POST['kinh_phi'],
				'Chuky'=>$_POST['Chu_ky'],
				'idDotDKNCKH' => $iddotdk,
				'DC_NgayDangDK' => $datetimex,
				'AnHien' => $_POST['anhien'],
				'madv' => $_SESSION['sess_madv'],
				'emailuser' => $_SESSION['sess_email'],
			);
			$insert = $db->insert('nncms_dkhoithao',$ttdangkyData);
			
		}
		else //kiểm tra số thứ tự đã tồn tại------------------------------------------------------------------
		{
			//==>Số thứ tự tồn tại nhỏ hơn 10
			if($dem<10){
				//Khởi tạo một số biến trước tiên
				$dem=$dem+1;
				$Ma='HT00';
				$add="$Ma$dem";		
				
				//cập nhật stt đăng ký vào bảng user---------------------------------------------------	
				if ($isSuccess) {
					$userData = array('stt_dt' => $add,);
					$condition = array('email' => $_SESSION['sess_email']);
					$update = $db->update('user',$userData,$condition);
					
					$sql = "SELECT idDotdknckh
							FROM nncms_quanlydotdangkynckh";
					$result = $db -> runSQL($sql); 
					$iddotdk = $result[0][0];
					echo "<h1>Mã đợt: $iddotdk</h1>";
					echo "<h1>$sql</h1>";

					//ghi thông tin đăng ký------------------------------------------------------------------
					$ttdangkyData = array(
						'idPhanLoai_HT_GT' => 'HT', 
						'TrangThai' => $_POST['trangthai'],
						'DC_STT'=>$add,
						'DonVi' => $_POST['donvi'],
						'TenDonViDK' => $_POST['Ten_Don_Vi'],
						'ChuDeHoiThao' => $_POST['Chu_De_Hoi_Thao'],
						'LinhVucApDung' => $_POST['Linh_Vuc_Ap_Dung'],
						'TomTatNoiDungHT' => $_POST['Tom_Tat_Noi_Dung'],
						'ThoiGianDiaDiemTC' => $_POST['ThoiGian_DiaDiem_TC'],
						'ThanhPhanThamDu' => $_POST['Thanh_Phan_Tham_Du'],
						'KinhPhi' => $_POST['kinh_phi'],
						'Chuky'=>$_POST['Chu_ky'],
						'idDotDKNCKH' => $iddotdk,
						'DC_NgayDangDK' => $datetimex,
						'AnHien' => $_POST['anhien'],
						'madv' => $_SESSION['sess_madv'],
						'emailuser' => $_SESSION['sess_email'],
					);
					$insert = $db->insert('nncms_dkhoithao',$ttdangkyData);
					echo "<h1>$insert</h1>";

				}
			}
			//==>Số thứ tự tồn tại lơn hơn 10 nhỏ hơn 99
			if($dem>=10 and $dem<=99) {
				//Ở ĐÂY
				$dem=$dem+1;
				$Ma='HT0';
				$add="$Ma$dem";
				
				//cập nhật stt đăng ký vào bảng user---------------------------------------------------
				if($isSuccess) {
					$userData = array(
						'stt_dt' => $add,
					);
					$condition = array('email' => $_SESSION['sess_email']);
					$update = $db->update('user',$userData,$condition);
					
					$sql = "SELECT idDotdknckh
							FROM nncms_quanlydotdangkynckh";
					$result = $db -> runSQL($sql); 
					$iddotdk = $result[0][0];
					echo "<h1>Mã đợt: $iddotdk</h1>";
					echo "<h1>$sql</h1>";
					//ghi thông tin đăng ký------------------------------------------------------------------
					$ttdangkyData = array(
						'idPhanLoai_HT_GT' => 'HT', 
						'TrangThai' => $_POST['trangthai'],
						'DC_STT'=>$add,
						'DonVi' => $_POST['donvi'],
						'TenDonViDK' => $_POST['Ten_Don_Vi'],
						'ChuDeHoiThao' => $_POST['Chu_De_Hoi_Thao'],
						'LinhVucApDung' => $_POST['Linh_Vuc_Ap_Dung'],
						'TomTatNoiDungHT' => $_POST['Tom_Tat_Noi_Dung'],
						'ThoiGianDiaDiemTC' => $_POST['ThoiGian_DiaDiem_TC'],
						'ThanhPhanThamDu' => $_POST['Thanh_Phan_Tham_Du'],
						'KinhPhi' => $_POST['kinh_phi'],
						'Chuky'=>$_POST['Chu_ky'],
						'idDotDKNCKH' => $iddotdk,
						'DC_NgayDangDK' => $datetimex,
						'AnHien' => $_POST['anhien'],
						'madv' => $_SESSION['sess_madv'],
						'emailuser' => $_SESSION['sess_email'],
					);
					$insert = $db->insert('nncms_dkhoithao',$ttdangkyData);
					echo "<h1>$insert</h1>";
					
				}
			}
			if($dem>=100) {

				$dem=$dem+1;
				$Ma='HT';
				$add="$Ma$dem";
				
				//cập nhật stt đăng ký vào bảng user---------------------------------------------------
				if ($isSuccess) {
					$userData = array(
						'stt_tt' => $add,
					);
					$condition = array('email' => $_SESSION['sess_email']);
					$update = $db->update('user',$userData,$condition);
					$sql = "SELECT idDotdknckh
							FROM nncms_quanlydotdangkynckh";
					$result = $db -> runSQL($sql); 
					$iddotdk = $result[0][0];
					echo "<h1>Mã đợt: $iddotdk</h1>";
					echo "<h1>$sql</h1>";
					//ghi thông tin đăng ký------------------------------------------------------------------
					$ttdangkyData = array(
						'idPhanLoai_HT_GT' => 'HT', 
						'TrangThai' => $_POST['trangthai'],
						'DC_STT'=>$add,
						'DonVi' => $_POST['donvi'],
						'TenDonViDK' => $_POST['Ten_Don_Vi'],
						'ChuDeHoiThao' => $_POST['Chu_De_Hoi_Thao'],
						'LinhVucApDung' => $_POST['Linh_Vuc_Ap_Dung'],
						'TomTatNoiDungHT' => $_POST['Tom_Tat_Noi_Dung'],
						'ThoiGianDiaDiemTC' => $_POST['ThoiGian_DiaDiem_TC'],
						'ThanhPhanThamDu' => $_POST['Thanh_Phan_Tham_Du'],
						'KinhPhi' => $_POST['kinh_phi'],
						'Chuky'=>$_POST['Chu_ky'],
						'idDotDKNCKH' => $iddotdk,
						'DC_NgayDangDK' => $datetimex,
						'AnHien' => $_POST['anhien'],
						'madv' => $_SESSION['sess_madv'],
						'emailuser' => $_SESSION['sess_email'],
					);
					$insert = $db->insert('nncms_dkhoithao',$ttdangkyData);
				}
			}
		}

		$IDDSThanhVien=$add;
		// $sothanhvien=$_POST['sothanhvien'];
        $statusMsg = $isSuccess?'User data has been inserted successfully.':'Some problem occurred, please try again.';
		$_SESSION['statusMsg'] = $statusMsg;
		echo "<h1>$isSuccess</h1>";
		if ($isSuccess) //thanh cong thi moi cho dang ky
			header("Location:giangvien.php?key=thanhviencs1&Line=$sothanhvien&IDDSThanhVien=$IDDSThanhVien");
		
    }
}
?>