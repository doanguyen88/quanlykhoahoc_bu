<?php   
session_start();
if(isset($_SESSION["sess_user"])){
	if($_SESSION["sess_user"]=="gv"&&$_SESSION["sess_vaitro"]=="1"){
		
include 'MySQL/DB.php';
            $db = new DB();?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>
		<?php
			$cauhinhchung = $db->getRows('cauhinhchung');
			if(!empty($cauhinhchung))
			{ 
				$count = 0; 
				foreach($cauhinhchung as $ch){ 
					$count++;
					echo $ch['Tieudetrang'];
				}
			}		
		?>
	</title>
	<meta name="description" content="top menu &amp; navigation" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

	<!-- page specific plugin styles -->
	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />

	<!-- page specific plugin styles -->

	<!-- text fonts -->
	<link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />

	<!-- ace styles -->
	<link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

	<!--[if lte IE 9]>
				<link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
			<![endif]-->
	<link rel="stylesheet" href="assets/css/ace-skins.min.css" />
	<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />

	<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-ie.min.css" />
			<![endif]-->

	<!-- inline styles related to this page -->

	<!-- ace settings handler -->

	<script src="assets/js/ace-extra.min.js"></script>

	<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

	<!--[if lte IE 8]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
			<![endif]-->
</head>

<body class="no-skin">
	<div id="navbar" class="navbar navbar-default ace-save-state">
  		<div class="navbar-container ace-save-state" id="navbar-container">
    		<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar"> 
				<span class="sr-only">Toggle sidebar</span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
			</button>
    		<div class="navbar-buttons navbar-header pull-right" role="navigation">
      			<ul class="nav ace-nav">
        			<li class="" style="width:200px !important"> 
						<a data-toggle="dropdown"  class="dropdown-toggle" aria-expanded="true">  
							<span class="user-info"> <small>Xin chào,</small> <?php echo $_SESSION['sess_username'];?> </span> 
						</a>
					</li>
        			<?php  $laytinhan = $db->demrow('chatlines', array('where'=>array('usersv'=>$_SESSION['sess_email'], 'active'=>'0')));?>
        			
					<li > 
						<a href="?key=xemtinnhan" onClick="actitinnhan('<?php echo $_SESSION['sess_email'];?>')"  style="background:<?php if($laytinhan>0) {echo "#B74635!important;";} ?> "  > 
							<i class="ace-icon fa fa-envelope"></i> 
							<span class="badge badge-success" style="background:<?php if($laytinhan>0) {echo "#be2f64!important;";} ?> "><?php echo $laytinhan;?></span> 
						</a> 
					</li>

        			<li class="light-blue "> <a href="giangvien.php?key=user"> <i class='ace-icon fa fa-user'></i> Thông tin tài khoản </a> </li>
        			<li class="light-blue "> <a href="logout.php"> <i class="ace-icon fa fa-power-off"></i> Thoát </a> </li>
      			</ul>
    		</div>
    		<div class="navbar-header pull-left"> 
				<a href="
					<?php $lienkets = $db->getRows('menus',array('where'=>array('TieuDe'=>'Trang chủ')));
						if(!empty($lienkets))
						{ 
							$count = 0; 
							foreach($lienkets as $links){ 
								$count++;
								echo $links['LienKet'];
							}
						}
					?>
					"class="navbar-brand"> 
					<small> <i class="fa fa-leaf"></i>Hệ thống quản lý khoa học</small> 
				</a>
      			<button class="pull-right navbar-toggle navbar-toggle-img collapsed" type="button" data-toggle="collapse" data-target=".navbar-buttons,.navbar-menu"> 
					<span class="sr-only">menu</span> 
				</button>
    		</div>
  		</div>
	</div><!-- /.navbar-container --> 

	<div class="main-container ace-save-state" id="main-container"> 
  		<script type="text/javascript">
			try{ace.settings.loadState('main-container')}catch(e){}
		</script>
 		 <?php  include 'menu_ngang.php';?>
  		<div class="main-content">
    		<div class="main-content-inner">
				<div class="page-content"> 
					<!-- /.ace-settings-container -->
        			<div class="page-header">
          				<h1><i class="ace-icon fa fa-home home-icon"></i> Trang chủ <small>
			<?php 
			if(isset($_GET['key']))
			switch($_GET['key'])
			{
				case "mau1a":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='menu-icon fa fa-user'></i> Biểu mẫu cá nhân 1a ");break;
				case "mau1b":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='menu-icon fa fa-users'></i> Biểu mẫu tập thể 1b ");break;
				
				case "mauCS-01":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='menu-icon fa fa-users'></i> Biểu mẫu CS-01 ");break;
				case "MauIn-CS01":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='menu-icon fa fa-users'></i> Mẫu In CS-01 ");break;
				case "mauCS-02":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='ace-icon fa fa-envelope'></i> Biểu mẫu CS-02 ");break;
				case "MauIn-CS02":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='menu-icon fa fa-users'></i> Mẫu In CS-02 ");break;
				case "mauCS-03":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='ace-icon fa fa-envelope'></i> Biểu mẫu CS-03 ");break;
				case "MauIn-CS03":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='menu-icon fa fa-users'></i> Mẫu In CS-03 ");break;
				case "mauCS-04":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='ace-icon fa fa-envelope'></i> Biểu mẫu CS-04 ");break;
				case "MauIn-CS04":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='menu-icon fa fa-users'></i> Mẫu In CS-04 ");break;
				case "mauCS-05":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='ace-icon fa fa-envelope'></i> Biểu mẫu CS-05 ");break;
				case "MauIn-CS05":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='menu-icon fa fa-users'></i> Mẫu In CS-05 ");break;

				case "user":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='ace-icon fa fa-user'></i> Thông tin tài khoản ");break;

				case "thanhviencs1":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='ace-icon fa fa-user'></i> Tổng hợp danh sách đăng ký nhiệm vụ KH-CN");break;

				case "xemtinnhan":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='ace-icon fa fa-envelope'></i> Xem tin nhắn ");break;		
				case "KKST-NCKH":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='ace-icon fa fa-envelope'></i> Kê khai số tiết NCKHS");break;	
				case "viewdsNCKH":echo("<i class='ace-icon fa fa-angle-double-right'></i> <i class='ace-icon fa fa-envelope'></i>Dánh sách số tiết NCKHS đã kê khai");break;										

			}
			?>
            </small> </h1>
        </div>
        <!-- /.page-header -->
        
        <div class="row">
          <div class="col-xs-12"> 
            <!-- PAGE CONTENT BEGINS -->
            
            <div class="hidden-sm hidden-xs"> </div>
            <div class="center">
              <?php 
				if(isset($_GET['key']))
					switch($_GET['key'])
					{
						case "mau1a":include("Mau1a.php");break;
						case "mau1b": include("Mau1b.php");break;
						
						case "mauCS-01": include("MauCS-01.php");break;
						case "MauIn-CS01": include("MauIn-CS01.php");break;

						case "mauCS-02": include("MauCS-02.php");break;
						case "MauIn-CS02": include("MauIn-CS02.php");break;

						case "mauCS-03": include("MauCS-03.php");break;
						case "MauIn-CS03": include("MauIn-CS03.php");break;

						case "mauCS-04": include("MauCS-04.php");break;
						case "MauIn-CS04": include("MauIn-CS04.php");break;
						
						case "mauCS-05": include("MauCS-05.php");break;
						case "MauIn-CS05": include("MauIn-CS05.php");break;

						case "KKST-NCKH": include("KKST-NCKH.php");break;

						case "thanhviencs1": include("addthanhviencs1.php");break;
						
						case "xemtinnhan": include("xemtinnhan.php");break;
						case "user": include("user.php");break;
						case "viewdsNCKH": include("viewdsNCKH.php");break;
						case "Export-Excell-NCKH": include("Export-Excell-NCKH.php");break;
					}
				else {
					include("viewdsNCKH.php");
				}?>
            </div>
            
            <!-- PAGE CONTENT ENDS --> 
          </div>
          <!-- /.col --> 
        </div>
        <!-- /.row --> 
      </div>
      <!-- /.page-content --> 
    </div>
  </div>
  <!-- /.main-content -->
  <?php //include 'footer.php'; ?>

  <div class="footer">
    <div class="footer-inner">
      <div class="footer-content"> <span class="bigger-120"> <span class="blue bolder">Hệ thống quản lý khoa học</span> - <?php echo date("Y"); ?></span> &nbsp; &nbsp; <span class="action-buttons"> <a href="#"> <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i> </a> <a href="#"> <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i> </a> <a href="#"> <i class="ace-icon fa fa-rss-square orange bigger-150"></i> </a> </span> </div>
    </div>
  </div>
  <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse"> <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i> </a> </div>
  
<!-- /.main-container --> 

<!-- basic scripts --> 


<!--[if !IE]> -->
		<script src="assets/js/jquery-2.1.4.min.js"></script>

		<!-- <![endif]--> 

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]--> 
<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script> 

<!-- page specific plugin scripts --> 
<script src="assets/js/bootstrap-datepicker.min.js"></script> 
<script src="assets/js/jquery.jqGrid.min.js"></script> 
<script src="assets/js/grid.locale-en.js"></script> 

<!-- ace scripts --> 
<script src="assets/js/ace-elements.min.js"></script> 
<script src="assets/js/ace.min.js"></script> 

<!-- inline scripts related to this page --> 
<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			
			
			});
			
		</script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> 
<script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script> 
<script src="giaodien/js/scripts-dk.js"></script> 
<script src="giaodien/js/kiemtrakknckh.js" type="text/javascript"></script>

<script type="text/javascript" src="giaodien/js/printThis.js"></script>

<script src="giaodien/js/tags.js"></script> 
<script src="assets/js/bootstrap-datepicker.min.js"></script> 
<script src="assets/js/bootstrap-timepicker.min.js"></script>
<script src="assets/js/moment.min.js"></script>
<script src="assets/js/daterangepicker.min.js"></script> 
<script src="assets/js/bootstrap-datetimepicker.min.js"></script> 
<script src="assets/js/bootstrap-colorpicker.min.js"></script> 
<script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('.input-daterange').datepicker({
                    todayBtn: "linked"
                });
            
            });
        </script> 
<script src="giaodien/js/bootstrap-tagsinput.min.js" type="text/javascript"></script>
</body>
</html>
<?php 
			}else {
				header("Location: login.php");
				}?>
<?php } else {

header("Location: login.php");
}?>
