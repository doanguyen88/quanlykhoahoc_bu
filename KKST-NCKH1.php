<!DOCTYPE html>
<html >
<head>
<meta charset="UTF-8">
<title>KÊ KHAI SỐ TIẾT NCKH</title>
<script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
<script src="giaodien/js/scripts12.js" type="text/javascript"></script>
<link rel="stylesheet" href="giaodien/css/style-dk.css">
<link rel="stylesheet" href="giaodien/css/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="giaodien/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="giaodien/css/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="giaodien/css/bootstrap-tagsinput.css">
	<style type="text/css">

		.gst20{

			margin-top:20px;

		}

		#hdTuto_search{

			display: none;

		}

		.list-gpfrm-list a{

			text-decoration: none !important;

		}

		.list-gpfrm li{

			cursor: pointer;

			padding: 4px 0px;

		}

		.list-gpfrm{

			list-style-type: none;

    		background: #d4e8d7;

		}

		.list-gpfrm li:hover{

			color: white;

			background-color: #3d3d3d;

		}

	</style>


<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" /> 
</head>

<body>
<form class="well form-horizontal" action="action-NCKH.php" method="post"  id="DKDT" enctype="multipart/form-data">
  <fieldset>
    
    <!-- Form Name -->
    
    <div class="row">
      
      
      
      <div class="col-md-6 col-md-offset-6 benner-tenmau">TP.HCM, ngày <?php echo date("d")?> tháng <?php echo date("m")?> năm <?php echo date("Y")?></div>
      <div class="col-md-12 tenphieu">
        <h3>KÊ KHAI SỐ TIẾT NCKH</h3>
        
        
 
            <?php $qldkdetai = $db->getRows('nncms_QuanlydotkhekhaiNCKH',array('where'=>array('anHien'=>'on')),array('order_by'=>'idQuanlydangky_Detai ASC'));
            if(!empty($qldkdetai)){ $count = 0; foreach($qldkdetai as $qldkdetais){ $count++;?>
            <?php
			$time1= $qldkdetais['startimes'];
			$time2= $qldkdetais['endtimes'];
			$date = date('m/d/Y h:i:s a', time());
			$moc1 = (strtotime($date) - strtotime($time1)) / (60 * 60 * 24);
			$moc2 = (strtotime($date) - strtotime($time2)) / (60 * 60 * 24);?>
            <input id="chondotkknckh" name="chondotkknckh"  class="textbox"  type="hidden" value=" <?php if($moc1 >0 && $moc2 < 0 ){ echo $qldkdetais['idQuanlydangky_Detai'];}else{echo 0;}?>">
            <?php } }?>

        <div class="form-group">
          <label class="col-md-3  control-label tenfrom" >Họ tên:</label>
          <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input name="ho_ten" placeholder="Nhập họ tên" class="textbox"  type="text">
        </div>
      </div>
        </div>
      </div>
    </div>
    
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" > Hoạt động KHCN:</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="col-md-12">
        <select  name="chonqldkdt" id="chonqldkdt" data-placeholder="Chọn quản lý đk đề tài"  class="textbox" onchange="loadphanloaikhnc(this.value)">
                      <option value="" >--Chọn hoạt động KHCN--</option>
            <?php  $dmhoatdongnckh = $db->getRows('nncms_cachoatdongNCKH',array('where'=>array('anHien'=>'on','id_loai_nckh'=>'0','cap_do'=>'cap_1')),array('order_by'=>'id_nckh ASC'));
            if(!empty($dmhoatdongnckh)){ $count = 0; foreach($dmhoatdongnckh as $chudett){ $count++;?>
                        <option value="<?php echo $chudett['id_nckh'];?>"><?php echo $chudett['TenHoatDongNCKH'];?></option>
                        <?php }}?>
                      </select>
        
        
          
        </div>
      </div>
    </div>
    
    <div  id="phanloai"></div>
   
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >Mã vai trò</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group" > <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
        <div ></div>
          <input id="mavaitro" name="mavaitro"  class="textbox auto"  type="text" value="">
        </div>
      </div>
    </div>
    
    <div class="form-group">
          <label class="col-md-3  control-label tenfrom" >Mã số sản phẩm:</label>
          <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input name="masosanpham" placeholder="Nhập mã số sản phẩm" class="textbox"  type="text">
        </div>
      </div>
        </div>
        <div class="form-group">
          <label class="col-md-3  control-label tenfrom" >Tên sản phẩm:</label>
          <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input name="tensanpham" placeholder="Nhập tên sản phẩm" class="textbox"  type="text">
        </div>
      </div>
        </div>
    <div class="form-group">
      <label class="col-md-4 control-label tenfrom" >Thời gian hoàn thành (nhập ngày/thang/năm)</label>
      <div class="col-md-7  inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input  name="thoigianhoanthanh" placeholder="Nhập Thời gian hoàn thành" class="textbox"  type="text">
        </div>
      </div>
    </div>
    <div class="form-group">
          <label class="col-md-3  control-label tenfrom" >Số quyết định:</label>
          <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input name="soquyetdinh" placeholder="Nhập số quyết đinh" class="textbox"  type="text">
        </div>
      </div>
        </div>
     <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >Số tiết quy đổi </label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input  name="sotietquydoi" placeholder="Nhập số tiết quy đổi" class="textbox"  type="text">
        </div>
      </div>
    </div>
    
    
    <div class="form-group">
    
    <label class="col-md-3 control-label tenfrom" >Đính kèm <a href="javascript:_add_more();" title="Add more"><i class="glyphicon glyphicon-plus"></i></a></label>
    <div class="col-md-8 inputGroupContainer">
      <div class="col-md-12 ">
        <div class="fileupload fileupload-new" data-provides="fileupload">
          <div id="dvFile">
            <input type="file" name="item_file[]">
          </div>
        </div>
      </div>
    </div>
    <!-- Success message -->
    <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>
    </div>
    
    <!-- Button -->
    <div class="form-group">
      <label class="col-md-4 control-label"></label>
      <div class="col-md-4">
      <input name="email" placeholder="<?php echo $_SESSION['sess_email'];?>" class="textbox"  type="hidden" value="<?php echo $_SESSION['sess_email'];?>">
      <input type="hidden"  name="madv"  value="<?php echo $_SESSION['sess_madv'];?>">
        <input  type="hidden"  name="trangthai" value="Đã gửi"/>
        <input type="hidden" name="ngay"  value="<?php echo date("m/d/Y");?>"/>
        <input  type="hidden"  name="anhien" value="on"/>
        <input  type="hidden"  name="action_type" value="add"/>
        <button type="submit" class="btn btn-warning" >Gửi <span class="glyphicon glyphicon-send"></span></button>
      </div>
    </div>
  </fieldset>
</form>

<div class="container">

	

	<div class="row justify-content-center gst20">

		<div class="col-sm-6">

			<form id="hdTutoForm" method="POST" action="">

				<div class="input-gpfrm input-gpfrm-lg">

				  	<input type="text" id="querystr" name="querystr" class="form-control" placeholder="Search Name" aria-describedby="basic-addon2">

				</div>

			</form>

			<ul class="list-gpfrm" id="hdTuto_search"></ul>

		</div>

	</div>

</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
	//Autocomplete search using PHP, MySQLi, Ajax and jQuery
		//generate suggestion on keyup
		$('#querystr').keyup(function(e){
			e.preventDefault();
			var form = $('#querystr').val();
			$.ajax({
				type: 'POST',
				url: 'search-mavaitro.php',
				data: form,
				dataType: 'json',
				success: function(response){
					if(response.error){
						$('#hdTuto_search').hide();
					}
					else{
						$('#hdTuto_search').show().html(response.data);
					}
				}
			});
		});
		$(document).on('click', '.list-gpfrm-list', function(e){
			e.preventDefault();
			$('#hdTuto_search').hide();
			var fullname = $(this).data('fullname');
			$('#querystr').val(fullname);
		});
	});

</script>

</div>
<!-- /.container -->
</body>
</html>

<script language="javascript">
<!--
	function _add_more() {
		var txt = "<br><input type=\"file\" name=\"item_file[]\">";
		document.getElementById("dvFile").innerHTML += txt;
	}
	
//-->
</script>