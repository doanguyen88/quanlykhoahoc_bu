
<?php   
session_start();

include 'MySQL/DB.php';
            $db = new DB();?>
    
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Hệ thống quản lý khoa học - <?php echo date("Y"); ?></title>
		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
					<link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
				<![endif]-->
		<link rel="stylesheet" href="assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
				<link rel="stylesheet" href="assets/css/ace-ie.min.css" />
				<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="assets/js/ace-extra.min.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]> 
				<script src="assets/js/html5shiv.min.js"></script>
				<script src="assets/js/respond.min.js"></script>
				<![endif]-->
	</head>

	<body class="no-skin">

	<div id="navbar" class="navbar navbar-default ace-save-state">
	<div class="navbar-container ace-save-state" id="navbar-container">
		<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar"> <span class="sr-only">Toggle sidebar</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
		<div class="navbar-buttons navbar-header pull-right" role="navigation">
		<ul class="nav ace-nav">
			<?php if($_SESSION["sess_user"]=="gv"&&$_SESSION["sess_vaitro"]=="1"){?>
			<li class="" style="width:200px !important"> <a data-toggle="dropdown"  class="dropdown-toggle" aria-expanded="true"> <span class="user-info"> <small>Xin chào,</small> <?php echo $_SESSION['sess_username'];?> </span> </a> </li>
			<?php  $laytinhan = $db->demrow('chatlines',array('where'=>array('usersv'=>$_SESSION['sess_email'],'active'=>'0')));?>
			<li > <a href="?key=xemtinnhan" onClick="actitinnhan('<?php echo $_SESSION['sess_email'];?>')"  style="background:<?php if($laytinhan>0) {echo "#B74635!important;";} ?> "  > <i class="ace-icon fa fa-envelope"></i> <span class="badge badge-success" style="background:<?php if($laytinhan>0) {echo "#be2f64!important;";} ?> "><?php echo $laytinhan;?></span> </a> </li>
		
								
			<li class="light-blue "> <a href="giangvien.php?key=user"> <i class="ace-icon fa fa-user"></i> Thông tin tài khoản </a> </li>
			<li class="light-blue "> <a href="logout.php"> <i class="ace-icon fa fa-power-off"></i> Thoát </a> </li>
			<?php }else {?>
			<li class="light-blue "> <a href="http://localhost/quanlykhoahoc_bu/login.php"> <i class="ace-icon fa fa-sign-in"></i> Đăng nhập </a> </li>
			<?php }?>
			
		</ul>
		</div>
		<div class="navbar-header pull-left"> <a href="		 <?php
				$lienkets = $db->getRows('menus',array('where'=>array('TieuDe'=>'Trang chủ')));
				if(!empty($lienkets)){ $count = 0; foreach($lienkets as $links){ $count++;
				echo $links['LienKet'];
				}
				}
				
				?>
	" class="navbar-brand"> <small> <i class="fa fa-leaf"></i>
		Hệ thống quản lý khoa học
		</small> </a>
		<button class="pull-right navbar-toggle navbar-toggle-img collapsed" type="button" data-toggle="collapse" data-target=".navbar-buttons,.navbar-menu"> <span class="sr-only">menu</span> </button>
		</div>
	</div>
	<!-- /.navbar-container --> 
	</div>

	<div class="main-container ace-save-state container" id="main-container"> <img src="assets/images/logo.png" alt="" class="img-responsive"/>
	<div id="sidebar" class="sidebar      h-sidebar                navbar-collapse collapse          ace-save-state"> 
		<script type="text/javascript">
						try{ace.settings.loadState('sidebar')}catch(e){}
					</script> 
		
		<!-- /.sidebar-shortcuts -->
		
		<ul class="nav nav-list">
		<li class="active open hover"> <a href="http://localhost/quanlykhoahoc_bu"> <i class="menu-icon ace-icon fa fa-home home-icon"></i> <span class="menu-text"> Trang chủ </span> </a> <b class="arrow"></b> </li>
		<li class="hover"> <a href="#"> <i class="menu-icon fa fa-bullhorn"></i> <span class="menu-text"> Thông báo </span> </a> </li>
		<li class="hover"> <a href="#"> <i class="menu-icon fa fa-life-ring"></i> <span class="menu-text"> Hướng dẫn sử dụng </span> </a> </li>
		<li class="hover"> <a href="login.php" > <i class="menu-icon fa fa-lock"></i> <span class="menu-text"> Đăng nhập hệ thống </span> </a> </li>
		</ul>
		<!-- /.nav-list --> 
	</div>
	<div class="main-content">
		<div class="main-content-inner">
		<div class="page-content">
			<div class="page-header"> 
			<!--<h1>
									Top Menu Style
									<small>
										<i class="ace-icon fa fa-angle-double-right"></i>
										top menu &amp; navigation
									</small>
								</h1>--> 
			</div>
			<!-- /.page-header --> 
			
			<!-- /.row --> 
		</div>
		<!-- /.page-content --> 
		</div>
	</div>
	<!-- /.main-content -->
	
	<div class="footer">
		<div class="footer-inner">
		<div class="footer-content"> <span class="bigger-120"> <span class="blue bolder">Hệ thống quản lý khoa học - <?php echo date("Y"); ?></span> </span>  </span> &nbsp; &nbsp; <span class="action-buttons"> <a href="#"> <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i> </a> <a href="#"> <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i> </a> <a href="#"> <i class="ace-icon fa fa-rss-square orange bigger-150"></i> </a> </span> </div>
		</div>
	</div>
	<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse"> <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i> </a> </div>
	<!-- /.main-container --> 

	<!-- basic scripts --> 

	<!--[if !IE]> -->
			<script src="assets/js/jquery-2.1.4.min.js"></script>

			<!-- <![endif]--> 

	<!--[if IE]>
	<script src="assets/js/jquery-1.11.3.min.js"></script>
	<![endif]--> 
	<script type="text/javascript">
				if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
			</script> 
	<script src="assets/js/bootstrap.min.js"></script> 

	<!-- page specific plugin scripts --> 

	<!-- ace scripts --> 
	<script src="assets/js/ace-elements.min.js"></script> 
	<script src="assets/js/ace.min.js"></script> 

	<!-- inline scripts related to this page --> 
	<script type="text/javascript">
				jQuery(function($) {
				var $sidebar = $('.sidebar').eq(0);
				if( !$sidebar.hasClass('h-sidebar') ) return;
				
				$(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
					if( event_name !== 'sidebar_fixed' ) return;
				
					var sidebar = $sidebar.get(0);
					var $window = $(window);
				
					//return if sidebar is not fixed or in mobile view mode
					var sidebar_vars = $sidebar.ace_sidebar('vars');
					if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
						$sidebar.removeClass('lower-highlight');
						//restore original, default marginTop
						sidebar.style.marginTop = '';
				
						$window.off('scroll.ace.top_menu')
						return;
					}
				
				
					var done = false;
					$window.on('scroll.ace.top_menu', function(e) {
				
						var scroll = $window.scrollTop();
						scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
						if (scroll > 17) scroll = 17;
				
				
						if (scroll > 16) {			
							if(!done) {
								$sidebar.addClass('lower-highlight');
								done = true;
							}
						}
						else {
							if(done) {
								$sidebar.removeClass('lower-highlight');
								done = false;
							}
						}
				
						sidebar.style['marginTop'] = (17-scroll)+'px';
					}).triggerHandler('scroll.ace.top_menu');
				
				}).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
				
				$(window).on('resize.ace.top_menu', function() {
					$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
				});
				
				
				});
			</script>
	</body>
</html>
