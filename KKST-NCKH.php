<script type="text/javascript" charset="utf-8" async defer>
	function _add_more() {
		var txt = "<br><input type=\"file\" name=\"item_file[]\">";
		document.getElementById("dvFile").innerHTML += txt;
	}
	function phanloaionchange(item){
		var phanloai = item.value;
		
		if(phanloai=="Tạp chí khoa học, kỷ yếu hội thảo hoặc sách (book series) thuộc danh mục ISI có IF ≥ 2" 
		|| phanloai=="Tạp chí khoa học, kỷ yếu hội thảo hoặc sách (book series) thuộc danh mục ISI có IF ≥ 1/SCOPUS Q1"
		|| phanloai=="Tạp chí khoa học, kỷ yếu hội thảo hoặc sách (book series) thuộc danh mục ISI có IF < 1"
		|| phanloai=="Tạp chí khoa học, kỷ yếu hội thảo hoặc sách (book series) thuộc danh mục SCOPUS Q2"
		|| phanloai=="Tạp chí khoa học, kỷ yếu hội thảo hoặc sách (book series) thuộc danh mục SCOPUS Q3"
		|| phanloai=="Tạp chí khoa học, kỷ yếu hội thảo hoặc sách (book series) thuộc danh mục SCOPUS Q4 hoặc thuộc danh mục SCOPUS nhưng chưa xếp hạng"
		|| phanloai=="Tạp chí khoa học quốc tế có mã số ISSN không nằm trong danh mục ISI, SCOPUS"
		|| phanloai=="Tạp chí khoa học trong nước có mã số ISSN thuộc danh mục hội đồng giáo sư nhà nước"
		|| phanloai=="Tạp chí khoa học trong nước có mã số ISSN"
		|| phanloai=="Bài tham luận hội thảo khoa học  (có xuất bản kỷ yếu)"
		) {
			document.getElementById('hidden_div').style.display = "block";
		} else{
			document.getElementById('hidden_div').style.display = "none";
		}
	} 
	function kiemsotietquydoi(item){
		var sotietquydoi = item.value;
		if(sotietquydoi<1) {
			alert("Số tiết quy đổi phải lớn hơn 1");
			$("#sotietquydoi").focus();
		} 
	} 	
</script>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>KÊ KHAI SỐ TIẾT NCKH</title>
        <script src="giaodien/js/scripts12.js" type="text/javascript"></script>
        <link rel="stylesheet" href="giaodien/css/style-dk.css">
        <link rel="stylesheet" href="giaodien/css/bootstrap-datepicker3.min.css">
        <link rel="stylesheet" href="giaodien/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="giaodien/css/bootstrap-timepicker.min.css">
        <link rel="stylesheet" href="giaodien/css/bootstrap-tagsinput.css">
        <link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css" />
        <link rel="stylesheet" href="assets/css/chosen.min.css" />
    </head>
        
    <body>
		<form class="well form-horizontal" action="action-NCKH.php" method="post"  id="hdTutoForm" enctype="multipart/form-data">
          	<fieldset>
   			 <!-- Form Name -->
			    <div class="row">
              		<div class="col-md-6 col-md-offset-6 benner-tenmau">TP.HCM, ngày <?php echo date("d")?> tháng <?php echo date("m")?> năm <?php echo date("Y")?></div>
              		<div class="col-md-12 tenphieu">
        				<h3>KÊ KHAI SỐ TIẾT NCKH</h3>
						<?php $qldkdetai = $db->getRows('nncms_QuanlydotkhekhaiNCKH',array('where'=>array('anHien'=>'on')),array('order_by'=>'idQuanlydangky_Detai ASC'));
						if(!empty($qldkdetai)){ $count = 0; foreach($qldkdetai as $qldkdetais){ 
								$count++;
							?>
							<?php
								$time1= $qldkdetais['starttimes'];
								$time2= $qldkdetais['endtimes'];
								$date = date('m/d/Y h:i:s a', time());
								$moc1 = (strtotime($date) - strtotime($time1)) / (60 * 60 * 24);
								$moc2 = (strtotime($date) - strtotime($time2)) / (60 * 60 * 24);
							?>
							<input id="iddotkknckh" name="iddotkknckh" class="textbox" type="hidden" value=" <?php if($moc1 >0 && $moc2 < 0 ){ echo $qldkdetais['idQuanlydangky_Detai'];}else{echo 0;}?>">    
							<input id="chondotkknckh" name="chondotkknckh" class="textbox" type="hidden" value=" <?php if($moc1 >0 && $moc2 < 0 ){ echo $qldkdetais['TenDotkekhai_NCKH'];}else{echo 0;}?>">
						<?php } }?>
        				<div class="form-group">
                  			<label class="col-md-3  control-label tenfrom" >Họ tên:</label>
                  			<div class="col-md-8 inputGroupContainer">
            					<div class="input-group"> 
									<span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                      				<input name="ho_ten" placeholder="Nhập họ tên" class="textbox"  type="text" value="<?php echo $_SESSION['sess_username'];?>">
                   	 			</div>
          					</div>
                		</div>
      				</div>
            	</div>

    			<div class="form-group">
              		<label class="col-md-3 control-label tenfrom" > Hoạt động NCKH:</label>
              		<div class="col-md-8 inputGroupContainer">
        				<div class="input-group" > 
							<span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  			<select  name="hoatdongnckh" data-placeholder="Chọn Hoạt động NCKH" class="chosen-select textbox auto" id="hoatdongnckh">
            					<option value=""> </option>
								<?php  $hoatdongnckh = $db->getRows('NCKH_HoatDong',array('where'=>array('AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
								foreach($hoatdongnckh as $hdnckh){?>
            						<option value="<?php echo $hdnckh['Ten_HoatDong'];?>"> <?php echo $hdnckh['Ten_HoatDong'];?></option>
            					<?php  }?>
          					</select>
                		</div>
      				</div>
            	</div>

    			<div class="form-group">
              		<label class="col-md-3 control-label tenfrom" > Phân Loại NCKH:</label>
              		<div class="col-md-8 inputGroupContainer">
              			<div class="input-group" > 
							<span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  			<select  name="phanloainckh" data-placeholder="Chọn Phân Loại NCKH" onChange="phanloaionchange(this)"   class="chosen-select textbox auto" id="phanloainckh">
            					<option value=""> </option>
            					<?php  $phanloainckh = $db->getRows('NCKH_Phanloai_HD',array('where'=>array('AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
            					foreach($phanloainckh as $plnckh){?>
            						<option value="<?php echo $plnckh['Ten_PhanLoai_HD'];?>"> <?php echo $plnckh['Ten_PhanLoai_HD'];?></option>
            					<?php  }?>
          					</select>
               			</div>
        			</div>
            	</div>
    			<div class="form-group" id="hidden_div" style="display:none;">
              		<label class="col-md-3 control-label tenfrom" > Tên tạp chí/tên hội thảo:</label>
              		<div class="col-md-8 inputGroupContainer">
        				<div class="input-group"> 
                  			<div ></div>
                  			<span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
							<input type="text" id="tapchihoithao" name="tapchihoithao" class="textbox auto" placeholder="Tên tạp chí/tên hội thảo">
                  		</div>
        			</div>
            	</div>        

    			<div class="form-group">
              		<label class="col-md-3 control-label tenfrom" >Mã Hình Thức NCKH:</label>
              		<div class="col-md-8 inputGroupContainer">
        				<div class="input-group" > 
							<span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  			<select  name="mahinhthucnckh" data-placeholder="Chọn Mã Hình Thức NCKH"   class="chosen-select textbox auto" id="mahinhthucnckh">
            					<option value=""> </option>
            					<?php  $mahinhthucnckh = $db->getRows('NCKH_MaHinhThuc',array('where'=>array('AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
            					foreach($mahinhthucnckh as $mhtnckh){?>
            						<option value="<?php echo $mhtnckh['Ten_MaHinhThuc'];?>"> <?php echo $mhtnckh['Ten_MaHinhThuc'];?></option>
            					<?php }?>
          					</select>
                		</div>
      				</div>
            	</div>

    			<div class="form-group">
              		<label class="col-md-3 control-label tenfrom" >Mã vai trò NCKH</label>
              		<div class="col-md-8 inputGroupContainer">
        				<div class="input-group"> 
							<span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  			<select  name="mavaitronckh" data-placeholder="Chọn Mã vai trò NCKH"   class="chosen-select textbox auto" id="mavaitronckh">
            					<option value=""> </option>
            					<?php  $mavaitronckh = $db->getRows('NCKH_MaVaiTro',array('where'=>array('AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
            					foreach($mavaitronckh as $mvtnckh){?>
            						<option value="<?php echo $mvtnckh['Ten_MaVaiTro'];?>"> <?php echo $mvtnckh['Ten_MaVaiTro'];?></option>
            					<?php  }?>
          					</select>
                		</div>
      				</div>
            	</div>

    			<div class="form-group">
              		<label class="col-md-3  control-label tenfrom" >Mã số sản phẩm:</label>
              		<div class="col-md-8 inputGroupContainer">
        				<div class="input-group"> 
							<span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  			<input name="masosanpham" placeholder="Nhập mã số sản phẩm" class="textbox"  type="text">
                		</div>
      				</div>
            	</div>

    			<div class="form-group">
              		<label class="col-md-3  control-label tenfrom" >Tên sản phẩm:</label>
              		<div class="col-md-8 inputGroupContainer">
        				<div class="input-group"> 
							<span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  			<input name="tensanpham" placeholder="Nhập tên sản phẩm" class="textbox"  type="text">
                		</div>
      				</div>
            	</div>

    			<div class="form-group">
              		<label class="col-md-4 control-label tenfrom" >Thời gian hoàn thành (nhập ngày/thang/năm)</label>
              		<div class="col-md-7 inputGroupContainer">
        				<div class="input-group"> 
							<span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  			<input class="form-control date-picker" name="thoigianhoanthanh" id="thoigianhoanthanh" type="text" data-date-format="dd/mm/yyyy" />
                  			<span class="input-group-addon"> <i class="fa fa-calendar bigger-110"></i> </span> 
						</div>
      				</div>
            	</div>
    			
				<div class="form-group">
              		<label class="col-md-3  control-label tenfrom" >Số quyết định:</label>
              		<div class="col-md-8 inputGroupContainer">
        				<div class="input-group"> 
							<span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  			<input name="soquyetdinh" placeholder="Nhập số quyết đinh" class="textbox"  type="text">
                		</div>
      				</div>
            	</div>
    			
				<div class="form-group">
              		<label class="col-md-3 control-label tenfrom" >Số tiết quy đổi </label>
              		<div class="col-md-8 inputGroupContainer">
        				<div class="input-group"> 
							<span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  			<input  onChange="kiemsotietquydoi(this)" OnkeyUp="kiemsotietquydoi(this)" onblur="kiemsotietquydoi('this')" id="sotietquydoi" name="sotietquydoi" placeholder="Nhập số tiết quy đổi" class="textbox"  type="text">
                		</div>
      				</div>
            	</div>
    			
				<div class="form-group">
              		<label class="col-md-3 control-label tenfrom" >Đính kèm 
						<a href="javascript:_add_more();" title="Add more">
							<i class="glyphicon glyphicon-plus"></i>
						</a>
					</label>
              		<div class="col-md-8 inputGroupContainer">
        				<div class="col-md-12 ">
                  			<div class="fileupload fileupload-new" data-provides="fileupload">
            					<div id="dvFile">
                      				<input type="file" name="item_file[]">
                    			</div>
          					</div>
                		</div>
      				</div>
              		<!-- Success message -->
              		<div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>
            	</div>
    
				<!-- Button -->
				<div class="form-group">
              		<label class="col-md-4 control-label"></label>
              		<div class="col-md-4">
        				<input name="email" placeholder="<?php echo $_SESSION['sess_email'];?>" class="textbox"  type="hidden" value="<?php echo $_SESSION['sess_email'];?>">
						<input type="hidden"  name="madv"  value="<?php echo $_SESSION['sess_madv'];?>">
						<input  type="hidden"  name="anhien" value="on"/>
						<input  type="hidden"  name="action_type" value="add"/>
						<button type="submit" class="btn btn-warning" >Gửi <span class="glyphicon glyphicon-send"></span></button>
      				</div>
            	</div>
  			</fieldset>
        </form>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script> 

		<script type="text/javascript">
			//hoạt động NCKH
			$(document).ready(function(){
				$('#hoatdongnckh').keyup(function(e){
					e.preventDefault();
					var form = $('#hdTutoForm').serialize();
					$.ajax({
						type: 'POST',
						url: 'search-hoatdongnckh.php',
						data: form,
						dataType: 'json',
						success: function(response){
							if(response.error){
								$('#list_hoatdong').hide();
							}
							else{
								$('#list_hoatdong').show().html(response.data);
							}
						}
					});
				});
				$(document).on('click', '.list_hoatdong_nckh', function(e){
					e.preventDefault();
					$('#list_hoatdong').hide();
					var fullname = $(this).data('fullname');
					$('#hoatdongnckh').val(fullname);
				});
			});

			//Phân loại NCKH
			$(document).ready(function(){
				$('#phanloainckh').keyup(function(e){
					e.preventDefault();
					var form = $('#hdTutoForm').serialize();
					$.ajax({
						type: 'POST',
						url: 'search-phanloainckh.php',
						data: form,
						dataType: 'json',
						success: function(response){
							if(response.error){
								$('#list_phanloai').hide();
							}
							else{
								$('#list_phanloai').show().html(response.data);
							}
						}
					});
				});
				$(document).on('click', '.list_phanloai_nckh', function(e){
					e.preventDefault();
					$('#list_phanloai').hide();
					var fullname = $(this).data('fullname');
					$('#phanloainckh').val(fullname);
				});
			});

			//Mã Hình Thức NCKH
			$(document).ready(function(){
				$('#mahinhthucnckh').keyup(function(e){
					e.preventDefault();
					var form = $('#hdTutoForm').serialize();
					$.ajax({
						type: 'POST',
						url: 'search-mahinhthucnckh.php',
						data: form,
						dataType: 'json',
						success: function(response){
							if(response.error){
								$('#list_mahinhthuc').hide();
							}
							else{
								$('#list_mahinhthuc').show().html(response.data);
							}
						}
					});
				});
				$(document).on('click', '.list_mahinhthuc_nckh', function(e){
					e.preventDefault();
					$('#list_mahinhthuc').hide();
					var fullname = $(this).data('fullname');
					$('#mahinhthucnckh').val(fullname);
				});
			});

			//Mã Vai Trò NCKH	
			$(document).ready(function(){
				$('#mavaitronckh').keyup(function(e){
					e.preventDefault();
					var form = $('#hdTutoForm').serialize();
					$.ajax({
						type: 'POST',
						url: 'search-mavaitro.php',
						data: form,
						dataType: 'json',
						success: function(response){
							if(response.error){
								$('#list_mavaitro').hide();
							}
							else{
								$('#list_mavaitro').show().html(response.data);
							}
						}
					});
				});
				$(document).on('click', '.list_mavaitro_nckh', function(e){
					e.preventDefault();
					$('#list_mavaitro').hide();
					var fullname = $(this).data('fullname');
					$('#mavaitronckh').val(fullname);
				});
			});
			
			//Tên tạp chí/ hội thảo
			//$(document).ready(function(){
			//		$('#tapchihoithao').keyup(function(e){
			//			e.preventDefault();
			//			var form = $('#hdTutoForm').serialize();
			//			$.ajax({
			//				type: 'POST',
			//				url: 'search-tapchihoithao.php',
			//				data: form,
			//				dataType: 'json',
			//				success: function(response){
			//					if(response.error){
			//						$('#list_tapchihoithao').hide();
			//					}
			//					else{
			//						$('#list_tapchihoithao').show().html(response.data);
			//					}
			//				}
			//			});
			//		});
			//		$(document).on('click', '.list_tapchihoithao_nckh', function(e){
			//			e.preventDefault();
			//			$('#list_tapchihoithao').hide();
			//			var fullname = $(this).data('fullname');
			//			$('#tapchihoithao').val(fullname);
			//		});
			//	});
		</script>
		</div>
		<!-- /.container -->
	</body>
</html>

<script src="assets/js/jquery-2.1.4.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/chosen.jquery.min.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script src="assets/js/bootstrap-timepicker.min.js"></script>
<script src="assets/js/daterangepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/js/jquery.inputlimiter.min.js"></script>
<script src="assets/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript">
	jQuery(function($) {
		if(!ace.vars['touch']) {
			$('.chosen-select').chosen({allow_single_deselect:true}); 
			//resize the chosen on window resize
			$(window)
			.off('resize.chosen')
			.on('resize.chosen', function() {
				$('.chosen-select').each(function() {
					var $this = $(this);
					$this.next().css({'width': $this.parent().width()});
				})
			}).trigger('resize.chosen');
			//resize chosen on sidebar collapse/expand
			$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
				if(event_name != 'sidebar_collapsed') 
					return;
				$('.chosen-select').each(function() {
					var $this = $(this);
					$this.next().css({'width': $this.parent().width()});
				})
			});
			$('#chosen-multiple-style .btn').on('click', function(e){
				var target = $(this).find('input[type=radio]');
				var which = parseInt(target.val());
				if(which == 2) 
					$('#form-field-select-4').addClass('tag-input-style');
				else 
					$('#form-field-select-4').removeClass('tag-input-style');
			});
		}
		//datepicker plugin
		//link
		$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		})
		//show datepicker when clicking on the icon
		.next().on(ace.click_event, function(){
			$(this).prev().focus();
		});
	
		//or change it into a date range picker
		$('.input-daterange').datepicker({autoclose:true});

		//to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
		$('input[name=date-range-picker]').daterangepicker({
			'applyClass' : 'btn-sm btn-success',
			'cancelClass' : 'btn-sm btn-default',
			locale: {
				applyLabel: 'Apply',
				cancelLabel: 'Cancel',
			}
		})
		.prev().on(ace.click_event, function(){
			$(this).next().focus();
		});

		$('#timepicker1').timepicker({
			minuteStep: 1,
			showSeconds: true,
			showMeridian: false,
			disableFocus: true,
			icons: {
				up: 'fa fa-chevron-up',
				down: 'fa fa-chevron-down'
			}
		}).on('focus', function() {
			$('#timepicker1').timepicker('showWidget');
		}).next().on(ace.click_event, function(){
			$(this).prev().focus();
		});
		
		if(!ace.vars['old_ie']) $('#date-timepicker1').datetimepicker({
			//format: 'MM/DD/YYYY h:mm:ss A',//use this option to display seconds
			icons: {
			time: 'fa fa-clock-o',
			date: 'fa fa-calendar',
			up: 'fa fa-chevron-up',
			down: 'fa fa-chevron-down',
			previous: 'fa fa-chevron-left',
			next: 'fa fa-chevron-right',
			today: 'fa fa-arrows ',
			clear: 'fa fa-trash',
			close: 'fa fa-times'
			}
		}).next().on(ace.click_event, function(){
			$(this).prev().focus();
		});
	});
</script>