<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>MẫuCS-01. PHIẾU ĐĂNG KÝ ĐỀ TÀI CẤP CƠ SỞ</title>

  </head>

  <body>
    
    <?php 
      
      $key = $_GET['id_de_tai'];
      $sql = "Select * From nncms_dkdtcapcoso Where idDKDTCapCoSo = ".$key;
      $result = $db -> runSQL($sql);
      $dtcapcoso = $result[0]; //get by id nên chỉ có 1 cái thôi
      // $hh_hv = $dtcapcoso['HocHamHocVi'];
      // echo "<h1>$hh_hv</h1>";

      $idDKDTCapCoSo = $dtcapcoso['idDKDTCapCoSo'];
      $sql = "Select * From nncms_thanhvienthamgia Where idDKDTCapCoSo = ".$idDKDTCapCoSo;
      $result = $db -> runSQL($sql);
      // echo "$sql";

      // if(!empty($result)) { 
      //   foreach($result as $thanhvientg) {
      //     $ntg = $thanhvientg['HoTen_NTG'];
      //     // echo "<h2>$ntg</h2>";
      //   }
      // } else {
      //   echo "<h2>Không có dữ liệu</h2>";
      // }
    ?>
    
    <div class="container">
        <fieldset>
          <!-- Form Name -->
          <div class="row">
            <div class="col-md-6 col-md-offset-6 benner-tenmau">MẫuCS-01. PHIẾU ĐĂNG KÝ ĐỀ TÀI CẤP CƠ SỞ</div>
            
            <div class="col-md-6 banner-tentruong">
              <div class="col-md-12 banner-tentruong">NGÂN HÀNG NHÀ NƯỚC VIỆT NAM</div>
              <div class="col-md-12 banner-tentruong"><strong>TRƯỜNG ĐẠI HỌC NGÂN HÀNG TP.HCM</strong></div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-md-2 col-md-offset-3 control-label tenfrom" >Đơn vị:</label>
                  <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                      <?php 
                        $donvi = $dtcapcoso['DonVi']; 
                        echo "<div class='input-group-addon textbox-icon' style='width: 100%'>$donvi</div>";
                      ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6 banner-tentruong">
              <div class="col-md-12">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</div>
              <div class="col-md-12"> <strong><ins>Độc lập - Tự do - Hạnh phúc</ins></strong></div>
            </div>

            <div class="col-md-6 col-md-offset-6 benner-tenmau">TP.HCM, ngày <?php echo date("d")?> tháng <?php echo date("m")?> năm <?php echo date("Y")?></div>
            
            <div class="col-md-12 tenphieu">
              <h3>PHIẾU ĐĂNG KÝ ĐỀ TÀI CẤP CƠ SỞ</h3>
              <div class="form-group">
                <label class="col-md-2 col-md-offset-4 control-label tenfrom">Năm học:</label>
                <div class="col-md-3 inputGroupContainer">
                  <div class="input-group">
                    <?php 
                    $namhoc = $dtcapcoso['NamHoc']; 
                    echo "<div class='input-group-addon textbox-icon' style='width: 100%'>$namhoc</div>";
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        
          <div class="form-group">
            <label class="col-md-3 control-label tenfrom" >Chọn danh mục đăng ký đề tài:</label>
            <div class="col-md-8 inputGroupContainer">
              <div class="col-md-12">
                <select  name="chonqldkdt" id="chonqldkdt" data-placeholder="Chọn quản lý đk đề tài"  class="textbox" >
                  <?php $qldkdetai = $db->getRows('nncms_Quanlydangkydetai',array('where'=>array('anHien'=>'on')),array('order_by'=>'idQuanlydangky_Detai ASC'));
                  if(!empty($qldkdetai)){ 
                    $count = 0; 
                    foreach($qldkdetai as $qldkdetais){ 
                      $count++; ?>

                      <?php
                      $time1= $qldkdetais['starttimes'];
                      $time2= $qldkdetais['endtimes'];
                      $date = date('m/d/Y h:i:s a', time());
                      $moc1 = (strtotime($date) - strtotime($time1)) / (60 * 60 * 24);
                      $moc2 = (strtotime($date) - strtotime($time2)) / (60 * 60 * 24);
                      
                      if($moc1 >0 && $moc2 < 0 )
                      { ?>
                        <option value="<?php echo $qldkdetais['idQuanlydangky_Detai'];?>"> <?php echo $qldkdetais['TenQuanlydangky_Detai'];?></option>
                      <? }else
                      {?>
                      <option value="0"> Không có đợt đăng ký</option>
                      <?php } ?>

                    <?php } 
                  }?>
                </select>
              </div>
            </div>
          </div>
        
          <div class="form-group" style="width: 100%">
            <label class="col-md-3 control-label tenfrom" >1.	Tên đề tài:</label>
            <div class="col-md-8 inputGroupContainer">
              <div class="input-group" align="left">
                <?php 
                  $ten_de_tai = $dtcapcoso['TenDeTaiCapCoSo']; 
                  echo "<div class='input-group-addon textbox-icon'>$ten_de_tai</div>";
                ?>
              </div>
            </div>
          </div>
        
          <div class="form-group"><label class="col-md-3 control-label tenfrom" >2. Thời gian thực hiện  </label>
            <div class="col-md-8 inputGroupContainer">
              <div class="input-daterange" id="datepicker" >
                <div class="input-group ">
                  <label class="col-md-3 control-label tenfrom" > Từ ngày: </label>
                  <div class="col-md-3">
                    <?php 
                      $tu_ngay = $dtcapcoso['starttimes']; 
                      echo "<div class='input-group-addon textbox-icon' style='width: 100%'>$tu_ngay</div>";
                    ?>
                  </div>
                  <label class="col-md-3 control-label tenfrom" > Tới ngày: </label>
                  <div class="col-md-3">
                    <?php 
                      $toi_ngay = $dtcapcoso['endtimes']; 
                      echo "<div class='input-group-addon textbox-icon'>$toi_ngay</div>";
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
      
          <div class="form-group">
            <label class="col-md-3 control-label tenfrom" >3. Chủ nhiệm:</label>
            <div class="col-md-12">
              <div class="col-md-12">
              <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-2 col-md-offset-1 control-label tenfrom">Họ và tên:</label>
                  <div class="col-md-8 inputGroupContainer">
                    <div class="input-group" align="left"> 
                      <?php 
                        $hoten = $dtcapcoso['HoTenChuNhiem']; 
                        echo "<div class='input-group-addon textbox-icon'>$hoten</div>";
                      ?>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-2  col-md-offset-1 control-label tenfrom">Năm sinh:</label>
                  <div class="col-md-8 inputGroupContainer">
                    <div class="input-group"> <span class="input-group-addon  textbox-icon"> </span>
                      <?php 
                        $namsinh = $dtcapcoso['NamSinh']; 
                        echo "<div class='input-group-addon textbox-icon'>$namsinh</div>";
                      ?>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-3  col-md-offset-1 control-label tenfrom" >Chức danh khoa học:</label>
                  <div class="col-md-6 inputGroupContainer">
                    <div class="input-group"> <span class="input-group-addon textbox-icon"> </span>
                      <div class="example">
                        <?php 
                          $chucdanhkh = $dtcapcoso['ChucDanhKhoaHoc']; 
                          echo "<div class='input-group-addon textbox-icon'>$chucdanhkh</div>"; 
                        ?>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-2  col-md-offset-1 control-label tenfrom" >Học hàm, học vị:</label>
                  <div class="col-md-8 inputGroupContainer">
                    <div class="input-group"> <span class="input-group-addon textbox-icon"> </span>
                      <?php 
                        $hh_hv = $dtcapcoso['HocHamHocVi'];
                        echo "<div class='input-group-addon textbox-icon'>$hh_hv</div>";
                      ?>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-2  col-md-offset-1 control-label tenfrom" >Điện thoại di động:</label>
                  <div class="col-md-8 inputGroupContainer">
                    <div class="input-group"> <span class="input-group-addon textbox-icon"> </span>
                      <?php 
                        $phone = $dtcapcoso['DienThoaiDiDong']; 
                        echo "<div class='input-group-addon textbox-icon'>$phone</div>";
                      ?>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-2  col-md-offset-1 control-label tenfrom" >Email:</label>
                  <div class="col-md-8 inputGroupContainer">
                    <div class="input-group"> <span class="input-group-addon textbox-icon"> </span>
                      <input name="email" placeholder="<?php echo $_SESSION['sess_email'];?>" class="textbox" value="<?php echo $_SESSION['sess_email'];?>">
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        
          <div class="form-group">
            <label class="col-md-3 control-label tenfrom" >4. Thành viên tham gia : </label>
            <div class="col-md-8 inputGroupContainer">
              <table width="100%" border="1" class="myGridClass" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5%" height="33" align="center"><strong>STT</strong></td>
                  <td width="16%" align="center"><strong>Họ và tên</strong></td>
                  <td width="16%" align="center"><strong>Học hàm học vị</strong></td>
                  <td width="19%" align="center"><strong>Đơn vị công tác</strong></td>
                  <td width="19%" align="center"><strong>Lĩnh vực chuyên môn</strong></td>
                  <td width="13%" align="center"><strong>chữ ký</strong></td>
                </tr>
                <?php
                if(!empty($result)){ 
                  foreach($result as $thanhvientg) { ?>
                    <tr>
                      <td height="33" align="center">1</td>
                      <td align="center">
                        <div class="input-group"> <span class="input-group-addon textbox-icon" > </span>
                          <textarea class="textarea-from" name="hoten_ntg1" placeholder="Nhập họ tên người tham gia vào đây">
                            <?php 
                              $ntg = $thanhvientg['HoTen_NTG'];
                              echo "$ntg";
                            ?>
                          </textarea>
                        </div>
                      </td>
                      <td align="center">
                        <div class="input-group"> <span class="input-group-addon textbox-icon" > </span>
                          <textarea class="textarea-from" name="hhhv_ntg1" placeholder="Nhập học hàm, học vị người tham gia vào đây"><?php
                            $ntg = $thanhvientg['HocHamHocVi_NTG'];
                            echo "$ntg";
                            ?>
                          </textarea>
                        </div>
                      </td>
                      <td align="center">
                        <div class="input-group"> <span class="input-group-addon textbox-icon" > </span>
                          <textarea class="textarea-from" name="donvi_ntg1" placeholder="Nhập đơn vị công tác vào đây"><?php
                            $ntg = $thanhvientg['DonVi_NTG'];
                            echo "$ntg";
                            ?>
                          </textarea>
                        </div>
                      </td>
                      <td align="center">
                        <div class="input-group"> <span class="input-group-addon textbox-icon" > </span>
                          <textarea class="textarea-from" name="linhvuccm_ntg1" placeholder="Nhập lĩnh vực chuyên môn vào đây"><?php
                            $ntg = $thanhvientg['LinhVucCM_NTG'];
                            echo "$ntg";
                            ?>
                          </textarea>
                        </div>
                      </td>
                      <td align="center">
                        <div class="input-group"> <span class="input-group-addon textbox-icon" > </span>
                          <textarea class="textarea-from" name="chuky_ntg1" placeholder="Nhập chữ ký vào đây"><?php
                            $ntg = $thanhvientg['ChuKy_NTG'];
                            echo "$ntg";
                            ?></textarea>
                        </div>
                      </td>
                    </tr>
                  <?php }
                }else{ ?>
                  <tr>
                    <td colspan="8">Không có dữ liệu</td>
                  </tr>
                  <?php } ?>
              </table>
            </div>
          </div>
        
          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-3 control-label tenfrom" >5. Tính cấp thiết của đề tài</label>
            <div class="col-md-8 inputGroupContainer">
              <div class="input-group"> <span class="input-group-addon textbox-icon" > </span>
                  <textarea class="textarea-from" name="Tinh_Cap_Thiet_DT" placeholder="vui lòng nhập"><?php $tinh_cap_thiet = $dtcapcoso['TinhCapThietCuaDeTai']; echo "$tinh_cap_thiet"; ?>
                  </textarea>
              </div>
            </div>
          </div>
          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-3 control-label tenfrom" >6.	Mục tiêu và nội dung nghiên cứu của đề tài:</label>
            <div class="col-md-8 inputGroupContainer">
              <div class="input-group"> <span class="input-group-addon textbox-icon"></span>
                  <textarea class="textarea-from" name="Muc_Tieu_Noi_Dung_NC" placeholder="Nhập Mục tiêu và nội dung nghiên cứu của đề tài vào đây"><?php $muctieu_noidung = $dtcapcoso['MucTieuNoiDungNghienCuu']; echo "$muctieu_noidung"; ?>
                  </textarea>
              </div>
            </div>
          </div>
          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-3 control-label tenfrom" >7.	Dự kiến đóng góp của đề tài </label>
            <div class="col-md-8 inputGroupContainer">
              <div class="input-group"> <span class="input-group-addon textbox-icon"></span>
                  <textarea class="textarea-from" name="Du_Kien_Dong_Gop_DT" placeholder="Vui lòng nhập Đơn vị áp dụng sáng kiến vào đây"><?php $du_kien_dong_gop = $dtcapcoso['DuKienDongGop']; echo "$du_kien_dong_gop"; ?></textarea>
              </div>
            </div>
          </div>

          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-3 col-md-offset-8 chuky" >Chủ nhiệm đề tài</label>
            <div class="col-md-3 col-md-offset-8 inputGroupContainer">
              <div class="input-group1">
                <?php 
                  $hoten = $dtcapcoso['HoTenChuNhiem']; 
                  echo "<div class='input-group-addon textbox-icon' style='width: 100%'>$hoten</div>";
                ?>
              </div>
            </div>
          </div>
        
        </fieldset>
    </div><!-- /.container -->

    <!-- Button -->
    <div class="form-group">
      <label class="col-md-4 control-label"></label>
      <div class="col-md-4">
        <button id="print" class="w3-button w3-blue" >print</button>
      </div>
    </div>

    <script>
      $('#print').click(function(){
        var cssLink = "<?php // path to additional css file - use an array [] for multiple
          $rieuPat = realpath('giaodien/css/style-dk.css');
          echo str_replace("\\","/",$rieuPat);
          ?>";
        $('.container').printThis({
          debug: false,               // show the iframe for debugging
          importCSS: true,            // import parent page css
          importStyle: true,         // chỗ này cũng chưa sửa????
          printContainer: true,       // print outer container/$.selector
          loadCSS: cssLink
        });
      })
    </script>
    
  </body>
</html>

                