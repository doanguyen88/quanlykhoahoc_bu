<?php
session_start();
include 'MySQL/DB.php';
$db = new DB();
   
if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){			
   if($_REQUEST['action_type'] == 'updatedkhoithao') {
		if(!empty($_POST['iddkhoithao'])){
			$userData = array(
				'DonVi' => $_POST['donvi'], 
                'TenDonViDK' => $_POST['tendonvidangky'],
                'ChuDeHoiThao' => $_POST['chudehoithao'],
                'LinhVucApDung' => $_POST['linhvucapdung'],
                'TomTatNoiDungHT' => $_POST['tomtatnoidung'],
                'ThoiGianDiaDiemTC' => $_POST['thoigiandiadiemtc'],
                'ThanhPhanThamDu' => $_POST['thanhphanthamdu'],
                'KinhPhi' => $_POST['kinhphi'],
                'Chuky' => $_POST['chuky'],
			);
			
			//update hội thảo
			$condition = array('idDKHoiThao' => $_POST['iddkhoithao']);
			$update = $db->update("nncms_dkhoithao",$userData,$condition);
			$statusMsg = $update?'User data has been updated successfully.':'Some problem occurred, please try again.';
			$_SESSION['statusMsg'] = $statusMsg;
			header("Location:giangvien.php?key=thanhviencs1");
			
 		} 
    } else if($_GET['action_type'] == 'deletedkhoithao'){
       	if(!empty($_GET['id'])){
            $condition = array('idDKHoiThao' => $_GET['id']);
            $delete = $db->delete('nncms_dkhoithao',$condition);
            $statusMsg = $delete?'User data has been deleted successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:giangvien.php?key=thanhviencs1");
        }
    }
}