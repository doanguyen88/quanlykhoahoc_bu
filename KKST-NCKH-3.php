<!DOCTYPE html>
<html >
	<head>
	<meta charset="UTF-8">
	<title>KÊ KHAI SỐ TIẾT NCKH</title>
	<script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
	<script src="giaodien/js/scripts12.js" type="text/javascript"></script>
	<link rel="stylesheet" href="giaodien/css/style-dk.css">
	<link rel="stylesheet" href="giaodien/css/bootstrap-datepicker3.min.css">
	<link rel="stylesheet" href="giaodien/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="giaodien/css/bootstrap-timepicker.min.css">
	<link rel="stylesheet" href="giaodien/css/bootstrap-tagsinput.css">
		<link rel="stylesheet" href="assets/css/chosen.min.css" />

	
	
	</head>

	<body>
<form class="well form-horizontal" action="action-NCKH.php" method="post"  id="hdTutoForm" enctype="multipart/form-data">
      <fieldset>
    
    <!-- Form Name -->
    
    <div class="row">
          <div class="col-md-6 col-md-offset-6 benner-tenmau">TP.HCM, ngày <?php echo date("d")?> tháng <?php echo date("m")?> năm <?php echo date("Y")?></div>
          <div class="col-md-12 tenphieu">
        <h3>KÊ KHAI SỐ TIẾT NCKH</h3>
        <?php $qldkdetai = $db->getRows('nncms_QuanlydotkhekhaiNCKH',array('where'=>array('anHien'=>'on')),array('order_by'=>'idQuanlydangky_Detai ASC'));
            if(!empty($qldkdetai)){ $count = 0; foreach($qldkdetai as $qldkdetais){ $count++;?>
        <?php
			$time1= $qldkdetais['startimes'];
			$time2= $qldkdetais['endtimes'];
			$date = date('m/d/Y h:i:s a', time());
			$moc1 = (strtotime($date) - strtotime($time1)) / (60 * 60 * 24);
			$moc2 = (strtotime($date) - strtotime($time2)) / (60 * 60 * 24);?>
        <input id="chondotkknckh" name="chondotkknckh"  class="textbox"  type="hidden" value=" <?php if($moc1 >0 && $moc2 < 0 ){ echo $qldkdetais['idQuanlydangky_Detai'];}else{echo 0;}?>">
        <?php } }?>
        <div class="form-group">
              <label class="col-md-3  control-label tenfrom" >Họ tên:</label>
              <div class="col-md-8 inputGroupContainer">
            <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  <input name="ho_ten" placeholder="Nhập họ tên" class="textbox"  type="text" value="<?php echo $_SESSION['sess_email'];?>">
                </div>
          </div>
            </div>
      </div>
        </div>
    <div class="form-group">
    <label class="col-md-3 control-label tenfrom" > Hoạt động KHCN:</label>
    <div class="col-md-8 inputGroupContainer">
        
        <div class="input-group" > <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <div ></div>
              <input type="text" id="hoatdongnckh" name="hoatdongnckh" class="textbox auto" placeholder="Hoạt động NCKH" aria-describedby="basic-addon1">
              <div class="input-gpfrm input-gpfrm-lg">
                <ul class="list-gpfrm" id="list_hoatdong"></ul>
          </div>
            </div>
      </div>
        </div>
        
     <div class="form-group">
    <label class="col-md-3 control-label tenfrom" > Phân Loại KHCN:</label>
    <div class="col-md-8 inputGroupContainer">
        <div class="input-group" > <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <div ></div>
              <select class="chosen-select textbox auto" id="form-field-select-3" data-placeholder="Choose a State...">
																<option value="">  </option>
																<option value="AL">Alabama</option>
																<option value="AK">Alaska</option>
																<option value="AZ">Arizona</option>
																<option value="AR">Arkansas</option>
																<option value="CA">California</option>
																<option value="CO">Colorado</option>
																<option value="CT">Connecticut</option>
																<option value="DE">Delaware</option>
																<option value="FL">Florida</option>
																<option value="GA">Georgia</option>
																<option value="HI">Hawaii</option>
																<option value="ID">Idaho</option>
																<option value="IL">Illinois</option>
																<option value="IN">Indiana</option>
																<option value="IA">Iowa</option>
																<option value="KS">Kansas</option>
																<option value="KY">Kentucky</option>
																<option value="LA">Louisiana</option>
																<option value="ME">Maine</option>
																<option value="MD">Maryland</option>
																<option value="MA">Massachusetts</option>
																<option value="MI">Michigan</option>
																<option value="MN">Minnesota</option>
																<option value="MS">Mississippi</option>
																<option value="MO">Missouri</option>
																<option value="MT">Montana</option>
																<option value="NE">Nebraska</option>
																<option value="NV">Nevada</option>
																<option value="NH">New Hampshire</option>
																<option value="NJ">New Jersey</option>
																<option value="NM">New Mexico</option>
																<option value="NY">New York</option>
																<option value="NC">North Carolina</option>
																<option value="ND">North Dakota</option>
																<option value="OH">Ohio</option>
																<option value="OK">Oklahoma</option>
																<option value="OR">Oregon</option>
																<option value="PA">Pennsylvania</option>
																<option value="RI">Rhode Island</option>
																<option value="SC">South Carolina</option>
																<option value="SD">South Dakota</option>
																<option value="TN">Tennessee</option>
																<option value="TX">Texas</option>
																<option value="UT">Utah</option>
																<option value="VT">Vermont</option>
																<option value="VA">Virginia</option>
																<option value="WA">Washington</option>
																<option value="WV">West Virginia</option>
																<option value="WI">Wisconsin</option>
																<option value="WY">Wyoming</option>
															</select>
              
              
              
            
              
              
            </div>
      </div>
        </div> 
    <div class="form-group">
          <label class="col-md-3 control-label tenfrom" >Mã Hình Thức</label>
          <div class="col-md-8 inputGroupContainer">
          
        <div class="input-group" > <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <div ></div>
              <input type="text" id="mahinhthucnckh" name="mahinhthucnckh" class="textbox auto" placeholder="Mã Hình Thức NCKH" aria-describedby="basic-addon3">
              <div class="input-gpfrm input-gpfrm-lg">
                <ul class="list-gpfrm" id="list_mahinhthuc">
                </ul>
          </div>
            </div>
      </div>
        </div>      
    <div class="form-group">
          <label class="col-md-3 control-label tenfrom" >Mã vai trò</label>
          <div class="col-md-8 inputGroupContainer">
          
        <div class="input-group" > <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <div ></div>
              <input type="text" id="mavaitronckh" name="mavaitronckh" class="textbox auto" placeholder="Mã Vai Trò NCKH" aria-describedby="basic-addon3">
              <div class="input-gpfrm input-gpfrm-lg">
                <ul class="list-gpfrm" id="list_mavaitro">
                </ul>
          </div>
            </div>
      </div>
        </div>
    <div class="form-group">
          <label class="col-md-3  control-label tenfrom" >Mã số sản phẩm:</label>
          <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input name="masosanpham" placeholder="Nhập mã số sản phẩm" class="textbox"  type="text">
            </div>
      </div>
        </div>
    <div class="form-group">
          <label class="col-md-3  control-label tenfrom" >Tên sản phẩm:</label>
          <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input name="tensanpham" placeholder="Nhập tên sản phẩm" class="textbox"  type="text">
            </div>
      </div>
        </div>
    <div class="form-group">
          <label class="col-md-4 control-label tenfrom" >Thời gian hoàn thành (nhập ngày/thang/năm)</label>
          <div class="col-md-7  inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input  name="thoigianhoanthanh" placeholder="Nhập Thời gian hoàn thành" class="textbox"  type="text">
            </div>
      </div>
        </div>
    <div class="form-group">
          <label class="col-md-3  control-label tenfrom" >Số quyết định:</label>
          <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input name="soquyetdinh" placeholder="Nhập số quyết đinh" class="textbox"  type="text">
            </div>
      </div>
        </div>
    <div class="form-group">
          <label class="col-md-3 control-label tenfrom" >Số tiết quy đổi </label>
          <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input  name="sotietquydoi" placeholder="Nhập số tiết quy đổi" class="textbox"  type="text">
            </div>
      </div>
        </div>
    <div class="form-group">
          <label class="col-md-3 control-label tenfrom" >Đính kèm <a href="javascript:_add_more();" title="Add more"><i class="glyphicon glyphicon-plus"></i></a></label>
          <div class="col-md-8 inputGroupContainer">
        <div class="col-md-12 ">
              <div class="fileupload fileupload-new" data-provides="fileupload">
            <div id="dvFile">
                  <input type="file" name="item_file[]">
                </div>
          </div>
            </div>
      </div>
          <!-- Success message -->
          <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>
        </div>
    
    <!-- Button -->
    <div class="form-group">
          <label class="col-md-4 control-label"></label>
          <div class="col-md-4">
        <input name="email" placeholder="<?php echo $_SESSION['sess_email'];?>" class="textbox"  type="hidden" value="<?php echo $_SESSION['sess_email'];?>">
        <input type="hidden"  name="madv"  value="<?php echo $_SESSION['sess_madv'];?>">
        <input  type="hidden"  name="trangthai" value="Đã gửi"/>
        <input type="hidden" name="ngay"  value="<?php echo date("m/d/Y");?>"/>
        <input  type="hidden"  name="anhien" value="on"/>
        <input  type="hidden"  name="action_type" value="add"/>
        <button type="submit" class="btn btn-warning" >Gửi <span class="glyphicon glyphicon-send"></span></button>
      </div>
        </div>
  </fieldset>
    </form>

<!--<div class="container">

	

	<div class="row justify-content-center gst20">

		<div class="col-sm-6">

			<form id="hdTutoForm" method="POST" action="">
					<input type="text" id="mavaitronckh" name="mavaitronckh" class="form-control" placeholder="Mã Vai trò NCKH" aria-describedby="basic-addon1">
					<div class="input-gpfrm input-gpfrm-lg"> <ul class="list-gpfrm" id="list_mavaitro"></ul></div>
                    
	                <input type="text" id="hoatdongnckh" name="hoatdongnckh" class="form-control" placeholder="Hoạt động NCKH" aria-describedby="basic-addon2">
                   <div class="input-gpfrm input-gpfrm-lg"> <ul class="list-gpfrm" id="list_hoatdong"></ul></div>
			

			</form>

			

		</div>

	</div>

</div>--> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script> 
<script type="text/javascript">
//hoạt động NCKH

		$(document).ready(function(){

		$('#hoatdongnckh').keyup(function(e){
			e.preventDefault();
			var form = $('#hdTutoForm').serialize();
			$.ajax({
				type: 'POST',
				url: 'search-hoatdongnckh.php',
				data: form,
				dataType: 'json',
				success: function(response){
					if(response.error){
						$('#list_hoatdong').hide();
					}
					else{
						$('#list_hoatdong').show().html(response.data);
					}
				}
			});
		});
		$(document).on('click', '.list_hoatdong_nckh', function(e){
			e.preventDefault();
			$('#list_hoatdong').hide();
			var fullname = $(this).data('fullname');
			$('#hoatdongnckh').val(fullname);
		});
	});
//Phân loại NCKH
		$(document).ready(function(){
		$('#phanloainckh').keyup(function(e){
			e.preventDefault();
			var form = $('#hdTutoForm').serialize();
			$.ajax({
				type: 'POST',
				url: 'search-phanloainckh.php',
				data: form,
				dataType: 'json',
				success: function(response){
					if(response.error){
						$('#list_phanloai').hide();
					}
					else{
						$('#list_phanloai').show().html(response.data);
					}
				}
			});
		});
		$(document).on('click', '.list_phanloai_nckh', function(e){
			e.preventDefault();
			$('#list_phanloai').hide();
			var fullname = $(this).data('fullname');
			$('#phanloainckh').val(fullname);
		});
	});
//Mã Hình Thức NCKH
		$(document).ready(function(){
		$('#mahinhthucnckh').keyup(function(e){
			e.preventDefault();
			var form = $('#hdTutoForm').serialize();
			$.ajax({
				type: 'POST',
				url: 'search-mahinhthucnckh.php',
				data: form,
				dataType: 'json',
				success: function(response){
					if(response.error){
						$('#list_mahinhthuc').hide();
					}
					else{
						$('#list_mahinhthuc').show().html(response.data);
					}
				}
			});
		});
		$(document).on('click', '.list_mahinhthuc_nckh', function(e){
			e.preventDefault();
			$('#list_mahinhthuc').hide();
			var fullname = $(this).data('fullname');
			$('#mahinhthucnckh').val(fullname);
		});
	});
//Mã Vai Trò NCKH	
	$(document).ready(function(){
		$('#mavaitronckh').keyup(function(e){
			e.preventDefault();
			var form = $('#hdTutoForm').serialize();
			$.ajax({
				type: 'POST',
				url: 'search-mavaitro.php',
				data: form,
				dataType: 'json',
				success: function(response){
					if(response.error){
						$('#list_mavaitro').hide();
					}
					else{
						$('#list_mavaitro').show().html(response.data);
					}
				}
			});
		});
		$(document).on('click', '.list_mavaitro_nckh', function(e){
			e.preventDefault();
			$('#list_mavaitro').hide();
			var fullname = $(this).data('fullname');
			$('#mavaitronckh').val(fullname);
		});
	});

</script>
</div>
<!-- /.container -->
</body>
</html>
<script language="javascript">
<!--
	function _add_more() {
		var txt = "<br><input type=\"file\" name=\"item_file[]\">";
		document.getElementById("dvFile").innerHTML += txt;
	}
	
//-->
</script>

		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/chosen.jquery.min.js"></script>
		<script type="text/javascript">
			jQuery(function($) {
				if(!ace.vars['touch']) {
					$('.chosen-select').chosen({allow_single_deselect:true}); 
					//resize the chosen on window resize
			
					$(window)
					.off('resize.chosen')
					.on('resize.chosen', function() {
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					}).trigger('resize.chosen');
					//resize chosen on sidebar collapse/expand
					$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
						if(event_name != 'sidebar_collapsed') return;
						$('.chosen-select').each(function() {
							 var $this = $(this);
							 $this.next().css({'width': $this.parent().width()});
						})
					});
			

				}
			
			});
		</script>