<html>
    <head>
        <meta charset="UTF-8">
        <title>PHIẾU ĐĂNG KÝ ĐỀ ÁN CẤP CƠ SỞ</title>

        <!-- jQuery -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

		<script>
            function printPage() {
                var prtContent = document.getElementById("container");
                var WinPrint = window.open('', '', 'left=300,top=50,width=800,height=600,toolbar=0,scrollbars=0,status=0');
                var style = "<style> body { background-color: linen; } h1 { color: maroon; margin-left: 40px; } #container { padding: 50px; background-color: lightgreen; text-align: center; font-size: 20px; font-weight: bold; } .abc { font-size: 50px; text-align: left; } </style>";
                WinPrint.document.write(style + prtContent.innerHTML);
                WinPrint.document.close();
                WinPrint.focus();
                WinPrint.print();
                //WinPrint.close();
            }
        </script>

		<!-- CSS –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <style>
            #container {
                margin: 1% 1%;
                background-color: #F5F5F5;
            }
            
            .container{
                display: flex;
            }

            .top-header{
                text-align: right;
                font-style: italic;
            }

            .page-header {
                display: flex;
            }

            .header01 {
                flex: 1 1;
                padding: 35px 10px;
                /* background-color: lightgreen; */
                text-align: center;
                vertical-align: middle;
            }
            .header01-donvi{
                flex-basis: 100%;
                padding: 1px;
                /* background-color: lightgreen; */
                display: flex;
                text-align: center;
            }
            .header01-tendonvi{
                flex: 1 1 45%;
                padding: 10px;
                text-align: right;
                flex-grow: 1;
            }
            .header01-value_donvi{
                flex: 1 1 50%;
                padding: 10px;
                text-align: left;
            }



            .header02 {
                flex: 1 1;
                padding: 35px 10px;
                /* background-color: lightskyblue; */
                text-align: center;
                vertical-align: middle;
            }

            .bottom-header-time{
                text-align: right;
                font-style: italic;
                padding: 11px 5px;
            }

            .page-body {
                display: flex;
                flex-wrap: wrap;
            }

            .body01 {
                flex-basis: 100%;
                padding: 4px;
                /* background-color: beige; */
                text-align: center;
                font-size: 20px;
            }

            /* NĂM HỌC */
            .body02 {
                flex-basis: 100%;
                padding: 1px;
                /* background-color: snow; */
                display: flex;
                text-align: center;
            }
            .namhoc {
                flex: 1 1 45%;
                padding: 10px;
                text-align: right;
                flex-grow: 1;
            }
            .namhoc-value {
                flex: 1 1 50%;
                padding: 10px;
                text-align: left;
            }
           
            .body03 {
                flex-basis: 100%;
                padding: 10px;
                /* background-color: lavender; */
                text-align: left;
            }

            /* TÊN ĐỀ ÁN */
            .body04 {
                flex-basis: 100%;
                padding: 10px;
                /* background-color: mistyrose; */
                text-align: left;
                display: flex;
                flex-wrap: wrap;
            }
            .body04-tendetai{
                flex: 1 1 20%;
            }
            .body04-value_tendetai{
                flex: 1 1 60%;
                text-align: left;
            }

            /* THỜI GIAN THỰC HIỆN */
            .body05 {
                flex-basis: 100%;
                padding: 10px;
                /* background-color: silver; */
                display: flex;
                flex-wrap: wrap;
            }
            .body05-timethuchien{
                text-align: left;
                flex: 1 1 20%;
            }
            .body05-value_timethuchien{
                display: flex;
                flex-wrap: wrap;
                flex: 1 1 60%;
            }
            .body05-timethuchien_tungay{
                flex: 1 1 5%;
                text-align: left;
                flex-wrap: wrap;
            }
            .body05-value_timethuchien_tungay{
                flex: 1 1 10%;
                text-align: left;
            }
            .body05-timethuchien_toingay{
                flex: 1 1 5%;
                text-align: left;
                flex-wrap: wrap;
            }
            .body05-value_timethuchien_toingay{
                flex: 1 1 25%;
                text-align: left;
            }


            /* CHỦ NHIỆM */
            .body06 {
                flex-basis: 100%;
                padding: 10px;
                /* background-color: seagreen; */
            }
            .body06-chunhiem{
                text-align: left;
            }
            /* họ tên */
            .body06-hoten{
                display: flex;
                flex-wrap: wrap;
            }
            .body06-hotencn {
                flex: 1 1 10%;
                text-align: left;
                padding-left: 117px;
            }
            .body06-value_hotencn{
                flex: 1 1 50%;
                text-align: left;
            }
            /* năm sinh */
            .body06-namsinh{
                display: flex;
                flex-wrap: wrap;
            }
            .body06-namsinhcn{
                flex: 1 1 10%;
                text-align: left;
                padding-left: 117px;
            }
            .body06-value_namsinhcn{
                flex: 1 1 50%;
                text-align: left;
            }

            /* chức danh khoa học */
            .body06-chucdanh{
                display: flex;
                flex-wrap: wrap;
            }
            .body06-chucdanhkh{
                flex: 1 1 10%;
                text-align: left;
                padding-left: 117px;
            }
            .body06-value_chucdanhkh{
                flex: 1 1 50%;
                text-align: left;
            }
            
            /* hhhv*/
            .body06-hhhv{
                display: flex;
                flex-wrap: wrap;
            }
            .body06-hhhvcn{
                flex: 1 1 10%;
                text-align: left;
                padding-left: 117px;
            }
            .body06-value_hhhvcn{
                flex: 1 1 50%;
                text-align: left;
            }
            /* phone */
            .body06-phone{
                display: flex;
                flex-wrap: wrap;
            }
            .body06-phonecn{
                flex: 1 1 10%;
                text-align: left;
                padding-left: 117px;
            }
            .body06-value_phonecn{
                flex: 1 1 50%;
                text-align: left;
            }

            /* email */
            .body06-email{
                display: flex;
                flex-wrap: wrap;
            }
            .body06-emailcn{
                flex: 1 1 10%;
                text-align: left;
                padding-left: 117px;
            }
            .body06-value_emailcn{
                flex: 1 1 50%;
                text-align: left;
            }

            
            /* THÀNH VIÊN THAM GIA  */
            .body07 {
                flex-basis: 100%;
                padding: 10px;
                /* background-color: orange; */
                /* text-align: left */
                display: flex;
                flex-wrap: wrap;
            }
            .body07-tuade_tvtg{
                text-align: left;
                flex: 1 1 100%;
            }
            .body07-tvtg{
                flex: 1 1 100%;
                flex-wrap: wrap;
                padding: 10px;
                text-align: left;
                border-style: solid;
                border-width: 1px
            }


            .body07-header_tvtg{
                padding: 10px 0px;
                display: flex;
                flex-wrap: wrap;
                flex: 1 1 100%;
            }
            
            .body07-stt_tvtg{
                flex: 1 1 5%;
                text-align: left;
                flex-wrap: wrap;
            }
            .body07-hoten_tvtg{
                flex: 1 1 13%;
                text-align: left;
                flex-wrap: wrap;
                padding-left: 1px;
            }
            .body07-hhhv_tvtg{
                flex: 1 1 15%;
                text-align: left;
                flex-wrap: wrap;
            }
            .body07-donvict_tvtg{
                flex: 1 1 18%;
                text-align: left;
                flex-wrap: wrap;
            }
            .body07-linhvuccm_tvtg{
                flex: 1 1 23%;
                text-align: left;
                flex-wrap: wrap;
            }
            .body07-chuky_tvtg{
                flex: 1 1 10%;
                text-align: left;
                flex-wrap: wrap;
                padding-left: 14px;
            }
            .body07-value_tvtg{
                padding: 10px 0px;
                display: flex;
                flex-wrap: wrap;
                flex: 1 1 100%;
            }
            .body07-value_stt_tvtg{
                flex: 1 1 5%;
                text-align: left;
                flex-wrap: wrap;
                padding-left: 5px;
                padding-right: 1px;
            }
            .body07-value_hoten_tvtg{
                flex: 1 1 13%;
                text-align: left;
                padding-left: 1px;
            }
            .body07-value_hhhv_tvtg{
                flex: 1 1 15%;
                text-align: left;
            }
            .body07-value_donvict_tvtg{
                flex: 1 1 18%;
                text-align: left;
            }
            .body07-value_linhvuccm_tvtg{
                flex: 1 1 23%;
                text-align: left;
            }
            .body07-value_chuky_tvtg{
                flex: 1 1 10%;
                text-align: left;
                padding-left: 14px;
            }

            

            /* TÍNH CẤP THIẾT */
            .body08{
                flex-basis: 100%;
                padding: 10px;
                text-align: left;
                display: flex;
                flex-wrap: wrap;
            }
            .body08-lyluan_ynghia{
                flex: 1 1;
            }
            .body08-value_lyluan_ynghia{
                flex: 1 1;
                text-align: left;
            }

            /* MỤC TIÊU VÀ NỘI DUNG NC */
            .body09{
                flex-basis: 100%;
                padding: 10px;
                /* background-color: tan; */
                text-align: left;
                display: flex;
                flex-wrap: wrap;
            }
            .body09-duytri_morong{
                flex: 1 1;
            }
            .body09-value_dtmr{
                flex: 1 1;
                text-align: left;
            }


            /* DỰ KIẾN ĐÓNG GÓP */
            .body10{
                flex-basis: 100%;
                padding: 10px;
                /* background-color: thistle; */
                text-align: left;
                display: flex;
                flex-wrap: wrap;
            }
            .body10-dukien_donggop{
                flex: 1 1;
            }
            .body10-value_dkdg{
                flex: 1 1;
                text-align: left;
            }

            /* CHỮ KÝ cndt */
            .body11{
                flex-basis: 100%;
                padding: 15px 28px;
                /* background-color: steelblue; */
                text-align: right;
            }
            .body11-chuky{
                flex: 1 1 20%;
                padding-right: 17px;
            }
            .body11-value_chuky{
                flex: 1 1 60%;
                text-align: right;
                padding-top:4px;
            }

        </style>
  
    </head>

    <body>
        <?php 
            $key = $_GET['id_hoi_thao'];
            $sql = "Select * From nncms_dkhoithao Where idDKHoiThao = ".$key;
            $result = $db -> runSQL($sql);
            $hoithao = $result[0];
            // $idDKHoiThao = $hoithao['idDKHoiThao'];
            // $sql = "Select * From nncms_thanhvienthamgia Where idDKHoiThao = ".$idDKHoiThao;
            // $result = $db -> runSQL($sql);    
        ?>

        <div id="container">
            <div class="top-header">PHIẾU ĐĂNG KÝ HỘI THẢO</div>
            <div class="page-header">
                <div class="header01">
                    <div>NGÂN HÀNG NHÀ NƯỚC VIỆT NAM</div>
                    <div><strong>TRƯỜNG ĐẠI HỌC NGÂN HÀNG TP.HCM</strong></div>
                    <div class="header01-donvi">
                        <div class="header01-tendonvi">Đơn vị:</div>
                        <div class="header01-value_donvi">
                            <?php 
                                $donvi = $hoithao['DonVi']; 
                                echo "<div class='input-group-addon textbox-icon' style='width: 100%'>$donvi</div>";
                            ?>
                        </div>
                    </div>
                </div>

                <div class="header02">
                    <div>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</div>
                    <div><strong><ins>Độc lập - Tự do - Hạnh phúc</ins></strong></div>
                </div>
            </div>
            <div class="bottom-header-time">TP.HCM, ngày <?php echo date("d")?> tháng <?php echo date("m")?> năm <?php echo date("Y")?></div>
            <div class="page-body">    
                <div class="body01">PHIẾU ĐĂNG KÝ HỘI THẢO</div>
                
                <div class="body04">
                    <div class="body04-tendetai" >1. Tên đơn vị đăng ký:</div>
                    <div class="body04-value_tendetai">
                        <?php 
                            $ten_dv_dk = $hoithao['TenDonViDK']; 
                            echo "<div class='input-group-addon textbox-icon'>$ten_dv_dk</div>";
                        ?>
                    </div>
                </div>
                <div class="body04">
                    <div class="body04-tendetai" >2. Chủ đề hội thảo:</div>
                    <div class="body04-value_tendetai">
                        <?php 
                            $chu_de_ht = $hoithao['ChuDeHoiThao']; 
                            echo "<div class='input-group-addon textbox-icon'>$chu_de_ht</div>";
                        ?>
                    </div>
                </div>
                <div class="body04">
                    <div class="body04-tendetai" >3. Lĩnh vực áp dụng:</div>
                    <div class="body04-value_tendetai">
                        <?php 
                            $linh_vuc_ad = $hoithao['LinhVucApDung']; 
                            echo "<div class='input-group-addon textbox-icon'>$linh_vuc_ad</div>";
                        ?>
                    </div>
                </div>
                <div class="body04">
                    <div class="body04-tendetai" >4. Tóm tắt nội dung hội thảo:</div>
                    <div class="body04-value_tendetai">
                        <?php 
                            $tom_tat_noi_dung = $hoithao['TomTatNoiDungHT']; 
                            echo "<div class='input-group-addon textbox-icon'>$tom_tat_noi_dung</div>";
                        ?>
                    </div>
                </div>
                <div class="body04">
                    <div class="body04-tendetai" >5. Thời gian, địa điểm tổ chức:</div>
                    <div class="body04-value_tendetai">
                        <?php 
                            $thoigian_diadiem_tc = $hoithao['ThoiGianDiaDiemTC']; 
                            echo "<div class='input-group-addon textbox-icon'>$thoigian_diadiem_tc</div>";
                        ?>
                    </div>
                </div>
                <div class="body04">
                    <div class="body04-tendetai" >6. Thành phần tham dự:</div>
                    <div class="body04-value_tendetai">
                        <?php 
                            $thanh_phan_tham_du = $hoithao['ThanhPhanThamDu']; 
                            echo "<div class='input-group-addon textbox-icon'>$thanh_phan_tham_du</div>";
                        ?>
                    </div>
                </div>
                <div class="body04">
                    <div class="body04-tendetai" >7. Kinh phí:</div>
                    <div class="body04-value_tendetai">
                        <?php 
                            $kinh_phi = $hoithao['KinhPhi']; 
                            echo "<div class='input-group-addon textbox-icon'>$kinh_phi</div>";
                        ?>
                    </div>
                </div>
                
                <div class="body11">
                    <div class="body11-chuky">Người đăng ký</div>
                    <div class="body11-value_chuky">
                        <?php 
                            $chuky = $hoithao['Chuky']; 
                            echo "$chuky";
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <button id="print" class="btn-primary"><strong>print</strong></button>
        
        <script>
            $('#print').click(function(){
                $('#container').printThis({
                    debug: false,               // show the iframe for debugging
                    importCSS: false,            // import parent page css
                    importStyle: true,         // chỗ này cũng chưa sửa????
                    printContainer: true,       // print outer container/$.selector
                    // loadCSS: cssLink
                });
            })
        </script>
    </body>
</html>