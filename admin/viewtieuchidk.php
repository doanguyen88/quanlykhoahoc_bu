<script src="../giaodien/js/scripts5.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts6.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts8.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts9.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts10.js" type="text/javascript"></script>
<?php
function url(){
  return sprintf(
    "%s://%s%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME'],
    $_SERVER['REQUEST_URI']
  );
}
?>

<div  class="sidebar h-sidebar navbar-collapse collapse ace-save-state" >
  <div class="table-header">Đánh giá sáng kiến theo từng tiêu chí xét duyệt</div>
  <ul class="nav nav-list">
    <li class="hover"> <a href="#" class="dropdown-toggle"> <i class="menu-icon fa fa-desktop"></i> <span class="menu-text"> Lọc đăng ký theo lĩnh vực</span> <b class='arrow fa fa-angle-down'></b> </a> <b class="arrow"></b>
      <ul class="submenu" style="width: 390px !important;">
        <?php 

			$linhvucdk = $db->getRows('nncms_linhvucdk',array('where'=>array('anHien'=>'on')),array('order_by'=>'idLVDK DESC'));
            if(!empty($linhvucdk)){ $count = 0; foreach($linhvucdk as $linhvuc){ $count++;?>
        <li class=""> <a href="#" class="dropdown-toggle" onclick="laylinhvucxet('<?php echo $linhvuc['idLVDK'];?>')"> <i class="menu-icon fa fa-caret-right" ></i> <span style="margin-left:20px;"><?php echo $linhvuc['TieuDe'];?></span> </a> <b class="arrow"></b> </li>
        <?php }}?>
      </ul>
    </li>
    <li class="hover"> <a href="#" class="dropdown-toggle"> <i class="menu-icon fa fa-laptop"></i> <span class="menu-text"> Lọc đăng ký theo chuyên môn</span> <b class='arrow fa fa-angle-down'></b> </a> <b class="arrow"></b>
      <ul class="submenu" style="width: 390px !important;">
        <?php 

			$linhvucdk = $db->getRows('nncms_linhvucdk',array('where'=>array('anHien'=>'on')),array('order_by'=>'idLVDK DESC'));
            if(!empty($linhvucdk)){ $count = 0; foreach($linhvucdk as $linhvuc){ $count++;?>
        <li class=""> <a href="#" class="dropdown-toggle"  > <i class="menu-icon fa fa-caret-right" ></i> <span style="margin-left:20px;"><?php echo $linhvuc['TieuDe'];?></span> </a> <b class="arrow"></b>
          <ul class="submenu">
            <?php 
							$chuyenmondk = $db->getRows('nncms_chuyenmon',array('where'=>array('idLVDK'=>$linhvuc['idLVDK'])),array('order_by'=>'idLVDK DESC'));
						if(!empty($chuyenmondk)){ $count = 0; foreach($chuyenmondk as $chuyenmon){ $count++;
							?>
            <li class="hover"> <a href="#" onclick="laychuyenmonxet('<?php echo $linhvuc['idChuyenMon'];?>')"> <i class="menu-icon fa fa-caret-right"></i> <?php echo $chuyenmon['TenChuyenMon']; ?> </a> <b class="arrow"></b> </li>
            <?php }}?>
          </ul>
        </li>
        <?php }}?>
      </ul>
    </li>
    <li class="hover"> <a href="#" onclick="laydknotlv()"> <i class="menu-icon fa fa-list-alt"></i> <span class="menu-text"> Lọc đăng ký chưa được add lĩnh vực </span> </a> <b class="arrow"></b> </li>
  </ul>
</div>
<div id="sidebar" class="sidebar   responsive    ace-save-state " style="width:100% !important; border-left:1px solid #e5e5e5">
  <div id="kq">
    <ul class="nav nav-list">
      <?php 
	 $tsdangky = $db->demrow('nncms_ttdangky',array('order_by'=>'idTTDK ASC'));
	 $sd=8;
	$tst=ceil($tsdangky/$sd);// tinh tong so trang
	// Lay trang:
	if(isset($_GET['page']))
	{
	$page=intval($_GET['page']);
	}
	else
	$page=1;
	//Tinh vi tri
	$vt=($page-1)*$sd;
	$chondksks = $db->getRows('nncms_ttdangky',array('start'=>$vt,'limit'=>$sd));
            if(!empty($chondksks)){ $count = 0; foreach($chondksks as $chondksk){ $count++;?>
      <li class=""> <a href="#" class="dropdown-toggle"> <i class="glyphicon glyphicon-triangle-right"></i> <span class="menu-text"> <?php echo $chondksk['DC_TenSangKien'];?></span>
        <?php 
						$tieuchitc = $db->getRows('nncms_tieuchi',array('where'=>array('idChuDeTieuChi'=>$chondksk['idChuDeTieuChi'],'AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
						if(!empty($tieuchitc)){ $count = 0; foreach($tieuchitc as $tieuchi){ $count++;
								echo "<b class='arrow fa fa-angle-down'></b>";
							}}

							?>
        </a> <b class="arrow"></b>
        <ul class="submenu">
          <?php 
						
						$tctc = $db->getRows('nncms_tieuchi',array('where'=>array('idChuDeTieuChi'=>$chondksk['idChuDeTieuChi'],'AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
						if(!empty($tctc)){ $count = 0; foreach($tctc as $tctchi){ $count++;?>
          <li class="" > <a  > <i class="glyphicon glyphicon-arrow-right"></i> <?php echo $tctchi['TenTieuChi'];?> <i style="margin-left:50px;" class="glyphicon glyphicon-random"></i>
         
       
            
            <select  style="margin-left:50px; display:<?php $getketquaxet = $db->getRows('nncms_ketqua',array('where'=>array('idTTDK'=>$chondksk['idTTDK'],'IdTieuChi'=>$tctchi['IdTieuChi'])),array('order_by'=>'idKetQua ASC'));
            if(!empty($getketquaxet)){ $count = 0; foreach($getketquaxet as $getketqua){ $count++; if($getketqua['TenKetQua']=="Chờ duyệt"||$getketqua['TenKetQua']=="Đang duyệt"){echo "inline"; }else {echo "none";} }}?>"  name="choncdtc" id="chondg<?php echo $tctchi['IdTieuChi'];?>" data-placeholder="Chọn năm ĐK"  class=""  >
              <option value="0" >--Chọn đánh giá--</option>
              <?php
					 $trangthais = $db->getRows('nncms_trangthai',array('where'=>array('AnHien'=>'on','idLoaiTrangThai'=>'SK')),array('order_by'=>'ThuTu ASC'));
						if(!empty($trangthais)){ $count = 0; foreach($trangthais as $trangthai){ $count++;?>
              <option  value="<?php echo $trangthai['idTrangThai'];?>"> <?php echo $trangthai['TenTrangThai'];?></option>
              <?php } }?>
            </select>
            
                    <label class="typeahead scrollable" style="margin-left:30px"><?php $getketquaxet = $db->getRows('nncms_ketqua',array('idTTDK'=>$chondksk['idTTDK'],'where'=>array('IdTieuChi'=>$tctchi['IdTieuChi'])),array('order_by'=>'idKetQua ASC'));
            if(!empty($getketquaxet)){ $count = 0; foreach($getketquaxet as $getketqua){ $count++; echo "Đánh giá : ".$getketqua['TenKetQua']; }}?></label>
         
         
              <input type="text"  style="display:<?php $getketquaxet = $db->getRows('nncms_ketqua',array('where'=>array('idTTDK'=>$chondksk['idTTDK'],'IdTieuChi'=>$tctchi['IdTieuChi'])),array('order_by'=>'idKetQua ASC'));
            if(!empty($getketquaxet)){ $count = 0; foreach($getketquaxet as $getketqua){ $count++; if($getketqua['TenKetQua']=="Chờ duyệt"||$getketqua['TenKetQua']=="Đang duyệt"){echo "inline"; }else {echo "none";} }}?>" id="nhanxet<?php echo $tctchi['IdTieuChi'];?>" name="nhanxet" placeholder="Nhận xét" class="typeahead scrollable tt-input" value="">
                       <label class="typeahead scrollable" style="margin-left:30px"><?php $getketquaxet = $db->getRows('nncms_ketqua',array('where'=>array('idTTDK'=>$chondksk['idTTDK'],'IdTieuChi'=>$tctchi['IdTieuChi'])),array('order_by'=>'idKetQua ASC'));
            if(!empty($getketquaxet)){ $count = 0; foreach($getketquaxet as $getketqua){ $count++; echo "Nhận xét : ".$getketqua['Nhanxet']; }}?></label>

            <button style="display:<?php $getketquaxet = $db->getRows('nncms_ketqua',array('where'=>array('idTTDK'=>$chondksk['idTTDK'],'IdTieuChi'=>$tctchi['IdTieuChi'])),array('order_by'=>'idKetQua ASC'));
            if(!empty($getketquaxet)){ $count = 0; foreach($getketquaxet as $getketqua){ $count++; if($getketqua['TenKetQua']=="Chờ duyệt"||$getketqua['TenKetQua']=="Đang duyệt"){echo "inline"; }else {echo "none";} }}?>" type="button" class="btn btn-danger" onclick="adddanhgia('<?php echo $chondksk['idTTDK']?>','<?php echo $chondksk['idChuDeTieuChi']?>','<?php echo $tctchi['IdTieuChi']?>','<?php echo url();?>')">Lưu</button>
            <?php $getketquaxet = $db->getRows('nncms_ketqua',array('where'=>array('idTTDK'=>$chondksk['idTTDK'],'IdTieuChi'=>$tctchi['IdTieuChi'])),array('order_by'=>'idKetQua ASC'));
            if(!empty($getketquaxet)){ $count = 0; foreach($getketquaxet as $getketqua){ $count++; if($getketqua['TenKetQua']=="Chờ duyệt"||$getketqua['TenKetQua']=="Đang duyệt"){echo "<span class='label label-success arrowed'><i class='ace-icon fa fa-refresh bigger-120'></i> Hãy cập nhật lại </span>"; }else {echo "<span class='label label-danger arrowed-in'><i class='ace-icon fa fa-exclamation-triangle bigger-120'></i> Đã khóa dữ</span>"; } }}?>
            
            <div style="margin-left:20px;" id="dgtc"></div>
          
            </a> <b class="arrow"></b> </li>
        
          <?php }
						 	}// ket thuc menu con?>
        </ul>
        <ul class="" style="background-color: #C7E5F9 !important;   border-color: #6FB3E0 !important;   color: #31708f !important;">
          <li> 
            <!--      <select style="margin-left:50px;"  name="chondgcc" id="chondgcc<?php /*?><?php echo $chondksk['idTTDK']?><?php */?>" data-placeholder="Chọn Đánh giá cuối cùng"  class=""  >
            <option value="0" >--Chọn Đánh giá cuối cùng--</option>
            <option value="1" >Đạt</option>
            <option value="2" >Không đạt</option>
          </select>-->
    
            <select  style="margin-left:50px; display:<?php $tieuchitc = $db->getRows('nncms_ketqua',array('where'=>array('idChuDeTieuChi'=>$chondksk['idChuDeTieuChi'],'idTTDK'=>$chondksk['idTTDK'],'AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
						if(!empty($tieuchitc)){ $count = 0; foreach($tieuchitc as $tieuchi){ $count++; if($tieuchi['KetQuaCC']=="Chờ duyệt"||$tieuchi['KetQuaCC']=="Đang duyệt"){echo "inline"; }else {echo "none";}  }}?>"  name="choncdtc" id="chondgcc<?php echo $chondksk['idTTDK']?>" data-placeholder="Chọn năm ĐK"  class=""  > 
              <option value="0" >--Chọn đánh giá--</option>
              <?php
					 $trangthais = $db->getRows('nncms_trangthai',array('where'=>array('AnHien'=>'on','idLoaiTrangThai'=>'SK')),array('order_by'=>'ThuTu ASC'));
						if(!empty($trangthais)){ $count = 0; foreach($trangthais as $trangthai){ $count++;?>
              <option  value="<?php echo $trangthai['idTrangThai'];?>"> <?php echo $trangthai['TenTrangThai'];?></option>
              <?php } }?>
            </select>
            
                     <label class="typeahead scrollable" style="margin-left:30px"><?php $tieuchitc = $db->getRows('nncms_ketqua',array('where'=>array('idChuDeTieuChi'=>$chondksk['idChuDeTieuChi'],'idTTDK'=>$chondksk['idTTDK'],'AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
						if(!empty($tieuchitc)){ $count = 0; foreach($tieuchitc as $tieuchi){ $count++; echo "Đánh giá : ".$tieuchi['KetQuaCC']; }}?></label>
            
            
      

            <input style="margin:10px 50px; width:30% !important;display:<?php $tieuchitc = $db->getRows('nncms_ketqua',array('where'=>array('idChuDeTieuChi'=>$chondksk['idChuDeTieuChi'],'idTTDK'=>$chondksk['idTTDK'],'AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
						if(!empty($tieuchitc)){ $count = 0; foreach($tieuchitc as $tieuchi){ $count++; if($tieuchi['KetQuaCC']=="Chờ duyệt"||$tieuchi['KetQuaCC']=="Đang duyệt"){echo "inline"; }else {echo "none";} }}?>""  type="text" id="nhanxettongthe<?php echo $chondksk['idTTDK']?>" name="nhanxettongthe" placeholder="Nhận xét tổng thể cho đề tài" class="typeahead scrollable tt-input" value="">
                        
                        
                     <label class="typeahead scrollable" style="margin-left:30px"><?php $tieuchitc = $db->getRows('nncms_ketqua',array('where'=>array('idChuDeTieuChi'=>$chondksk['idChuDeTieuChi'],'idTTDK'=>$chondksk['idTTDK'],'AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
						if(!empty($tieuchitc)){ $count = 0; foreach($tieuchitc as $tieuchi){ $count++; echo "Nhận xét : ".$tieuchi['Nhanxettongthe']; }}?></label>         
                        
            <button style="display:<?php $tieuchitc = $db->getRows('nncms_ketqua',array('where'=>array('idChuDeTieuChi'=>$chondksk['idChuDeTieuChi'],'idTTDK'=>$chondksk['idTTDK'],'AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
						if(!empty($tieuchitc)){ $count = 0; foreach($tieuchitc as $tieuchi){ $count++; if($tieuchi['KetQuaCC']=="Chờ duyệt"||$tieuchi['KetQuaCC']=="Đang duyệt"){echo "inline"; }else {echo "none";} }}?>" type="button" class="btn btn-primary" onclick="adddanhgiatongthe('<?php echo $chondksk['idTTDK']?>','<?php echo $chondksk['idChuDeTieuChi']?>','<?php echo $tctchi['IdTieuChi']?>')">Lưu</button>
          <?php $tieuchitc = $db->getRows('nncms_ketqua',array('where'=>array('idChuDeTieuChi'=>$chondksk['idChuDeTieuChi'],'idTTDK'=>$chondksk['idTTDK'],'AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
						if(!empty($tieuchitc)){ $count = 0; foreach($tieuchitc as $tieuchi){ $count++; if($tieuchi['KetQuaCC']=="Chờ duyệt"||$tieuchi['KetQuaCC']=="Đang duyệt"){echo "<span class='label label-success arrowed'><i class='ace-icon fa fa-refresh bigger-120'></i> Hãy cập nhật lại </span>"; }else {echo "<span class='label label-danger arrowed-in'><i class='ace-icon fa fa-exclamation-triangle bigger-120'></i> Đã khóa dữ</span>"; } }}?>
          </li>
        </ul>
      </li>
      <?php }
			}// ket menu cha?>
    </ul>
    <!-- /.nav-list -->
    
    <div class="col-xs-6">
      <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
        <ul class="pagination">
          <?php for($i=1;$i<=$tst;$i++)
					if($i==$page)
					{ ?>
          <li class="active" aria-controls="dynamic-table" tabindex="0"><a href="#"><?php echo $i;?></a></li>
          <?php }
					else
                    {?>
          <li class=''> <a href='<?php echo "index.php?key=xetdangket&page=".$i;?>'><?php echo $i;?></a> </li>
          <?php }?>
        </ul>
      </div>
    </div>
  </div>
</div>
