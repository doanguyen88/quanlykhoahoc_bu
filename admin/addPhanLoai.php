<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">
$(document).on( "click", '.edit_button',function(e) {

        var TenPhanLoai = $(this).data('name');
		var ThuTu = $(this).data('content'); 
		var AnHien = $(this).data('quote');
		var idNam = $(this).data('direction');
        var idPhanLoai_CN_TT = $(this).data('id');
		

		
       
                  
        $(".business_TieuDe").val(TenPhanLoai);
        $(".business_ThuTu").val(ThuTu);
       	$(".business_AnHien").val(AnHien);
		$(".business_idNam").val(idNam);

		
  		$(".business_id").val(idPhanLoai_CN_TT);


    });
</script>
<script type="text/javascript">
 $(document).ready(function(){
	 $("#myModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title');
		var idData=button.data('id'); 
		var typeData=button.data('type'); 
        $(this).find('.modal-title').text('Form'+ titleData );
		 $(this).find('.modal-type').val(typeData);
    });
	 
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.closest('.ace-spinner')
				.on('changed.fu.spinbox', function(){
					//console.log($('#spinner1').val())
				}); 
	$('input[name="anhien').on('switchChange.bootstrapSwitch', function(event, state) {
  console.log(this); // DOM element
  console.log(event); // jQuery event
  console.log(state); // true | false
})
	
});

</script>
<script type="text/javascript">
function showDiv(select){
   if(select.value==1){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
} 
</script>
<div class="bs-example">
  <div class="container">
    <h4> 
      <!-- Button HTML (to Trigger Modal) -->
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-title="Thêm Phân loại" data-id="addphanloai" data-type="addphanloai">Thêm Phân loại</button>
    </h4>
    
    <!-- Modal HTML -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog"> 
        
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Quản lý phân loại</h4>
          </div>
          <form role="form" id="universalModalForm" method="post" action="actionPhanLoai.php" class="form" >
            <div class="modal-body">
              <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label> Chọn loại </label>
                        <select id="chonloai" name="chonloai">
                          <option value="TT">Tập thể</option>
                          <option value="CN">Cá nhân</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Chọn năm áp dụng</label>
                        <select  name="namapdung" id="namapdung" data-placeholder="Chọn năm"  class="business_idNam" >
                          <?php $nams = $db->getRows('nncms_nam',array('where'=>array('AnHien'=>'on')),array('order_by'=>'idNam ASC'));
            if(!empty($nams)){ $count = 0; foreach($nams as $nam){ $count++;?>
                          <option value="<?php echo $nam['idNam'];?>"> <?php echo $nam['TNam'];?></option>
                          <?php } }?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Tên Phân loại</label>
                  <input type="text" class="form-control business_TieuDe" name="TenPhanLoai"/>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Thứ tự</label>
                        <input type="text" id="spinner1" name="thutu"  class="input-small business_ThuTu"/>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label> Ẩn Hiện</label>
                        <input name="anhien" id="anhien" class="ace ace-switch ace-switch-4 business_AnHien" type="checkbox">
                        <span class="lbl"></span> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
              <input  type="hidden" class="modal-id business_id"  name="id"/>
              <input type="hidden" name="action_type" class="modal-type" value=""/>
              <input type="submit" class="btn btn-primary" name="submit" value="Thêm Phân Loại"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12">
  <div class="panel panel-default users-content">
    <div class="panel-heading">Danh mục phân loại </div>
    <table class="table">
      <tr>
        <th width="10%">id phân loại</th>
        <th width="15%">Năm áp dụng</th>
        <th width="20%">Tên phân loại</th>
        <th width="10%">Thứ Tự</th>
        <th width="10%">Ẩn hiện</th>
        <th width="10%">Mã phân loại</th>
        <th width="10%">id năm</th>
      </tr>
      <?php
          
            $phanloais = $db->getRows('nncms_phanloaidk',array('order_by'=>'idPhanLoai_CN_TT ASC'));
            if(!empty($phanloais)){ $count = 0; foreach($phanloais as $phanloai){ $count++;?>
      <tr>
        <td><?php echo $phanloai['idPhanLoai_CN_TT'];?></td>
        <td><?php 
				$lay=$phanloai['idNam'];
				$laynam = $db->getRows('nncms_nam',array('where'=>array('idNam'=>$lay,'anHien'=>'on')),array('order_by'=>'idNam ASC'));
            if(!empty($laynam)){ $count = 0; foreach($laynam as $layna){ $count++;
				echo $layna['TNam'];}}?></td>
        <td><?php echo $phanloai['TenPhanLoai'];?></td>
        <td><?php echo $phanloai['ThuTu'];?></td>
        <td><?php echo $phanloai['AnHien'];?></td>
        <td><?php echo $phanloai['Maphanloai'];?></td>
        <td><?php echo $phanloai['idNam'];?></td>
        <td><a 
                    
                    data-toggle="modal" data-target="#myModal"
                    data-name="<?php echo $phanloai['TenPhanLoai'];?>"
                    data-content="<?php echo $phanloai['ThuTu'];?>"
                  	data-quote="<?php echo $phanloai['AnHien'];?>"
                    data-direction="<?php echo $phanloai['idNam'];?>"

                   
                    data-title="Sửa Phân Loại" 
                    data-id="<?php echo $phanloai['idPhanLoai_CN_TT'];?>"  
                    data-type="editphanloai" 
                    class="glyphicon glyphicon-edit edit_button"></a> <a href="actionPhanLoai.php?action_type=deletephanloai&id=<?php echo $phanloai['idPhanLoai_CN_TT'];?>" data-type="delete"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a></td>
      </tr>
      <?php } }else{ ?>
      <tr>
        <td colspan="4">No user(s) found......</td>
        <?php } ?>
    </table>
  </div>
</div>
