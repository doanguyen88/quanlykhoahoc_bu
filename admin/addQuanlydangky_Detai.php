<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">

$(document).on( "click", '.addquanlydangkydetai',function(e) {	
		 var datatype = $(this).data('type');
		 $(".modal-type").val(datatype);    
});	
					
$(document).on( "click", '.editquanlydangkydetai',function(e) {
        var tenlinhvukckh = $(this).data('tenlinhvukckh');
		
		var tenquanlydangkydetai = $(this).data('tenquanlydangkydetai'); 
		var idlinhvucnckh = $(this).data('linhvucnckh');
		var idloaicongtrinhnckh = $(this).data('loaicongtrinhnckh');
 		var iddanhmuccapdetai = $(this).data('danhmuccapdetai');
		var datatype = $(this).data('type');
		var startimes = $(this).data('startimes');
		var endtimes = $(this).data('endtimes');
		var thutu = $(this).data('thutu'); 
		var anhien = $(this).data('anhien');
        var id = $(this).data('id');

       
        $(".business_tenquanlydangkydetai").val(tenquanlydangkydetai);    
        $(".business_idlinhvucnckh").val(idlinhvucnckh);
		$(".business_idloaicongtrinhnckh").val(idloaicongtrinhnckh);
		$(".business_iddanhmuccapdetai").val(iddanhmuccapdetai);
        $(".business_thutu").val(thutu);
       	$(".business_anhien").val(anhien);
  		$(".business_id").val(id);
		$(".modal-type").val(datatype); 
		$(".business_startimes").val(startimes); 
		$(".business_endtimes").val(endtimes);
    });
</script>
<script type="text/javascript">
 $(document).ready(function(){
	
	 
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.closest('.ace-spinner')
				.on('changed.fu.spinbox', function(){
					//console.log($('#spinner1').val())
				}); 
	$('input[name="anhien').on('switchChange.bootstrapSwitch', function(event, state) {
  console.log(this); // DOM element
  console.log(event); // jQuery event
  console.log(state); // true | false
})
	
});

</script>
<script type="text/javascript">
function showDiv(select){
   if(select.value==1){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
} 
</script>

<div class="bs-example">
  <div class="container">
    <h4> 
      <!-- Button HTML (to Trigger Modal) -->
      <button type="button" class="btn btn-primary addquanlydangkydetai"  data-toggle="modal" data-target="#qldk" data-title="Thêm đăng ký đề tài" data-id="addquanlydangkydetai" data-type="addquanlydangkydetai">Thêm đăng ký đề tài</button>
    </h4>
    
    <!-- Modal HTML -->
    <div class="modal fade" id="qldk" role="dialog">
      <div class="modal-dialog"> 
        
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Quản lý đăng ký đề tài</h4>
          </div>
          <form role="form" id="universalModalForm" method="post" action="actionQuanlydangky_Detai.php" class="form" >
            <div class="modal-body">
              <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
              <div class="modal-body">
                
                <div class="form-group">
                  <label>Tên đăng ký đề tài</label>
                  <input type="text" class="form-control business_tenquanlydangkydetai" name="tenquanlydangkydetai"/>
                </div>

              <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Lĩnh vực NCKH:</label>
                        <select  name="linhvucnckh" id="linhvucnckh" data-placeholder="Chọn Lĩnh vực NCKH"  class="business_idlinhvucnckh" >
                 <?php $linhvucnckh = $db->getRows('nncms_linhvucKCKH',array('where'=>array('anHien'=>'on')),array('order_by'=>'idlinhvuc_KCKH ASC'));
            if(!empty($linhvucnckh)){ $count = 0; foreach($linhvucnckh as $linhvucnckhs){ $count++;?>
                          <option value="<?php echo $linhvucnckhs['idlinhvuc_KCKH'];?>"> <?php echo $linhvucnckhs['Tenlinhvuc_KCKH'];?></option>
                          <?php } }?>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Cấp đề tài:</label>
                        <select  name="danhmuccapdetai" id="danhmuccapdetai" data-placeholder="Chọn Cấp đề tài"  class="business_iddanhmuccapdetai" >
      <?php $danhmuccapdetai = $db->getRows('nncms_DanhmucCapdetai',array('where'=>array('anHien'=>'on')),array('order_by'=>'idDanhmuc_CapdeTai ASC'));
            if(!empty($danhmuccapdetai)){ $count = 0; foreach($danhmuccapdetai as $danhmuccapdetais){ $count++;?>
                          <option value="<?php echo $danhmuccapdetais['idDanhmuc_CapdeTai'];?>"> <?php echo $danhmuccapdetais['TenDanhmuc_Capdetai'];?></option>
                          <?php } }?>
                        </select>
                      </div>
                    </div>
                    
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Loại công trình NCKH:</label>
                        <select  name="loaicongtrinhnckh" id="loaicongtrinhnckh" data-placeholder="Chọn Loại công trình NCKH"  class="business_idloaicongtrinhnckh" >
      <?php $loaicongtrinhnckh = $db->getRows('nncms_LoaicongtrinhNCKH',array('where'=>array('anHien'=>'on')),array('order_by'=>'idLoaicongtrinh_NCKH ASC'));
        if(!empty($loaicongtrinhnckh)){ $count = 0; foreach($loaicongtrinhnckh as $loaicongtrinhnckhs){ $count++;;?>
                          <option value="<?php echo $loaicongtrinhnckhs['idLoaicongtrinh_NCKH'];?>"> <?php echo $loaicongtrinhnckhs['TenLoaicongtrinh_NCKH'];?></option>
                          <?php } }?>
                        </select>
                      </div>
                    </div>
                    
                  </div>
                </div> 
                
                
                 
                
               
                <div class="form-group">
                <div class="row">
                <div class="input-daterange" id="datepicker" >
                  <div class="input-group col-md-12 ">
                  
                  <div class="col-md-6">
                      <label class="  control-label tenfrom" > Từ ngày </label>
                        
                          <input type="text" class="textbox business_startimes" name="start" />
                  
                  </div>
                        
                        
                        <div class="col-md-6 ">
                        <label class=" control-label tenfrom" > tới ngày </label>
                        
                          <input type="text" class="textbox business_endtimes" name="end" />
                        
                        </div>
                    </div>
                  </div>
                </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Thứ tự</label>
                        <input type="text" id="spinner1" name="thutu"  class="input-small business_thutu"/>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label> Ẩn Hiện</label>
                        <input name="anhien" id="anhien" class="ace ace-switch ace-switch-4 business_anhien" type="checkbox">
                        <span class="lbl"></span> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
              <input  type="hidden" class="modal-id business_id"  name="id"/>
              <input type="hidden" name="action_type" class="modal-type" value=""/>
              <input type="submit" class="btn btn-primary" name="submit" value="Thêm Lĩnh vực KCKH"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12">
  <div class="panel panel-default users-content">
    <div class="panel-heading">Quản lý đăng ký đề tài</div>
    <table class="table">
      <tr>
        <th width="5%">id</th>
        <th width="35%">Tên đăng ký đề tài</th>
        <th width="35%">Danh mục</th>
        <th width="10%">Thứ Tự</th>
        <th width="10%">Ẩn hiện</th>
       
      </tr>
      <?php
          
            $quanlydangkydetai = $db->getRows('nncms_Quanlydangkydetai',array('order_by'=>'idQuanlydangky_Detai ASC'));
            if(!empty($quanlydangkydetai)){ $count = 0; foreach($quanlydangkydetai as $quanlydangkydetais){ $count++;?>
      <tr>
        <td><?php echo $quanlydangkydetais['idQuanlydangky_Detai'];?></td>
        <td><?php echo $quanlydangkydetais['TenQuanlydangky_Detai'];?></td>
       		 <td> 
        		
                
                <div><span class="label label-sm label-success">Lĩnh vực NCKH:</span>
                <?php 
				$laylinhvucnckh=$quanlydangkydetais['idlinhvuc_KCKH'];
				$linhvucnckh = $db->getRows('nncms_linhvucKCKH',array('where'=>array('idlinhvuc_KCKH'=>$laylinhvucnckh,'anHien'=>'on')),array('order_by'=>'idlinhvuc_KCKH ASC'));
            if(!empty($linhvucnckh)){ $count = 0; foreach($linhvucnckh as $linhvucnckhs){ $count++;
				echo $linhvucnckhs['Tenlinhvuc_KCKH'];}}?>                
                </div>
                
                <div><span class="label label-sm label-success">Cấp đề tài:</span>  <?php 
				$laydanhmuccapdetai=$quanlydangkydetais['idDanhmuc_CapdeTai'];
				$danhmuccapdetai = $db->getRows('nncms_DanhmucCapdetai',array('where'=>array('idDanhmuc_CapdeTai'=>$laydanhmuccapdetai,'anHien'=>'on')),array('order_by'=>'idDanhmuc_CapdeTai ASC'));
            if(!empty($danhmuccapdetai)){ $count = 0; foreach($danhmuccapdetai as $danhmuccapdetais){ $count++;
				echo $danhmuccapdetais['TenDanhmuc_Capdetai'];}}?>  </div>
                
                
                <div><span class="label label-sm label-success">Loại CT NCKH:</span>
				 <?php 
				$layloaicongtrinhnckh=$quanlydangkydetais['idLoaicongtrinh_NCKH'];
				$loaicongtrinhnckh = $db->getRows('nncms_LoaicongtrinhNCKH',array('where'=>array('idLoaicongtrinh_NCKH'=>$layloaicongtrinhnckh,'anHien'=>'on')),array('order_by'=>'idLoaicongtrinh_NCKH ASC'));
            if(!empty($loaicongtrinhnckh)){ $count = 0; foreach($loaicongtrinhnckh as $loaicongtrinhnckhs){ $count++;
				echo $loaicongtrinhnckhs['TenLoaicongtrinh_NCKH'];}}?>
                
                 </div>
                 <div><span class="label label-sm label-warning">Thời gian bắt đầu:</span><?php
					$dateFormatedstart = split('-',$quanlydangkydetais['startimes']);
					$datestart = $dateFormatedstart[2].'/'.$dateFormatedstart[1].'/'.$dateFormatedstart[0];
				 	echo $datestart;?></div>
                 <div><span class="label label-sm label-warning">Thời gian kết thúc</span><?php 
				 	$dateFormatedend = split('-',$quanlydangkydetais['endtimes']);
				 	$dateend  = $dateFormatedend[2].'/'.$dateFormatedend[1].'/'.$dateFormatedend[0];
					echo $dateend;?></div>
                
                </td>
        <td><?php echo $quanlydangkydetais['ThuTu'];?></td>
        <td><?php echo $quanlydangkydetais['AnHien'];?></td>
        
        <td><a 
                    
                    data-toggle="modal" data-target="#qldk"
                    data-tenquanlydangkydetai="<?php echo $quanlydangkydetais['TenQuanlydangky_Detai'];?>"
                    data-linhvucnckh="<?php echo $quanlydangkydetais['idlinhvuc_KCKH'];?>"
                    data-loaicongtrinhnckh="<?php echo $quanlydangkydetais['idLoaicongtrinh_NCKH'];?>"
                    data-danhmuccapdetai="<?php echo $quanlydangkydetais['idDanhmuc_CapdeTai'];?>"
                    data-thutu="<?php echo $quanlydangkydetais['ThuTu'];?>"
                  	data-anhien="<?php echo $quanlydangkydetais['AnHien'];?>"
					data-startimes="<?php echo $quanlydangkydetais['startimes'];?>"
                   	data-endtimes="<?php echo $quanlydangkydetais['endtimes'];?>"
                    data-title="Sửa Quản lý đăng ký đề tài" 
                    data-id="<?php echo $quanlydangkydetais['idQuanlydangky_Detai'];?>"  
                    data-type="editquanlydangkydetai" 
                    class="glyphicon glyphicon-edit editquanlydangkydetai"></a>
                    <a href="actionQuanlydangky_Detai.php?action_type=deletequanlydangkydetai&id=<?php echo $quanlydangkydetais['idQuanlydangky_Detai'];?>" data-type="delete"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a></td>
      </tr>
      <?php } }else{ ?>
      <tr>
        <td colspan="4">No user(s) found......</td>
        <?php } ?>
    </table>
  </div>
</div>
