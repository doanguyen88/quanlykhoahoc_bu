<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="../giaodien/js/scripts3.js" type="text/javascript"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">
$(document).on( "click", '.edit_button',function(e) {

        var TenChuDeTieuChi = $(this).data('tentc');
		var ThuTu = $(this).data('thutu'); 
		var AnHien = $(this).data('anhien');
		var idlvdk = $(this).data('idlvdk');
        var idChuyenMon = $(this).data('idChuyenMon');
		var idChuDeTieuChi = $(this).data('id');
		var idNam = $(this).data('idNam');		
                        
                  
        $(".business_TenChuDeTieuChi").val(TenChuDeTieuChi);
        $(".business_ThuTu").val(ThuTu);
       	$(".business_AnHien").val(AnHien);
		$(".business_idlvdk").val(idlvdk);
		$(".business_idChuyenMon").val(idChuyenMon);
  		$(".business_id").val(idChuDeTieuChi);
		$(".business_idNam").val(idNam);

    });
</script>
<script type="text/javascript">
 $(document).ready(function(){
	 $("#myModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title');
		var idData=button.data('id'); 
		var typeData=button.data('type'); 
        $(this).find('.modal-title').text('Form'+ titleData );
		 $(this).find('.modal-type').val(typeData);
    });
	 
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.closest('.ace-spinner')
				.on('changed.fu.spinbox', function(){
					//console.log($('#spinner1').val())
				}); 
					$('input[name="anhien').on('switchChange.bootstrapSwitch', function(event, state) {
  console.log(this); // DOM element
  console.log(event); // jQuery event
  console.log(state); // true | false
})
	
});

</script>
<script type="text/javascript">
function showDiv(select){
   if(select.value==1){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
} 
</script>
<div class="bs-example">
  <div class="container">
    <h4> 
      <!-- Button HTML (to Trigger Modal) -->
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-title="Thêm Danh mục tiêu chí" data-id="adddmtc" data-type="adddmtc">Thêm Danh mục tiêu chí</button>
    </h4>
    
    <!-- Modal HTML -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog"> 
        
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Quản lý Danh mục tiêu chí</h4>
          </div>
          <form role="form" id="universalModalForm" method="post" action="actionChuDeTieuChi.php" class="form" >
            <div class="modal-body">
              <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Tên danh mục tiêu chí</label>
                  <input type="text" class="form-control business_TenChuDeTieuChi" name="danhmuctieuchi"/>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-4">
                      <div class="form-group">
                  <label> Ẩn Hiện</label>
                  <input name="anhien" class="ace ace-switch ace-switch-4 business_AnHien" type="checkbox">
                  <span class="lbl"></span> </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label>Thứ tự</label>
                        <input type="text" id="spinner1" name="thutu"  class="input-small business_ThuTu"/>
                      </div>
                    </div> 
                    <div class="col-md-4">
                      <label>Chọn Năm</label>
                      <select  name="chonnam" id="chonnam" data-placeholder="Chọn năm ĐK"  class="idNam" >
                      <option value="" >--Chọn năm--</option>
                        <?php $namdk = $db->getRows('nncms_nam',array('where'=>array('anHien'=>'on')),array('order_by'=>'idNam ASC'));
                        if(!empty($namdk)){ $count = 0; foreach($namdk as $nam){ $count++;?>
                        <option value="<?php echo $nam['idNam'];?>"> <?php echo $nam['TNam'];?></option>
                        <?php } }?>
                      </select>
                    </div>
                  </div>
                </div>
                 <div class="row">
                 <div class="form-group">
      <div class="col-md-12 control-label tenfrom">
      
        <div class="col-md-6">
          <label>Chọn Lĩnh vực</label>
          <select  name="linhvucdk" id="linhvucdk" data-placeholder="Chọn Lĩnh vực ĐK"  class="business_idlvdk" onchange="loadXMLDoc(this.value);">
          <option value="" >--Chọn lĩnh vực--</option>
            <?php $linhvucdk = $db->getRows('nncms_linhvucdk',array('where'=>array('anHien'=>'on')),array('order_by'=>'idLVDK ASC'));
            if(!empty($linhvucdk)){ $count = 0; foreach($linhvucdk as $linhvuc){ $count++;?>
            <option value="<?php echo $linhvuc['idLVDK'];?>"> <?php echo $linhvuc['TieuDe'];?></option>
            <?php } }?>
          </select>
        </div>
        <div class="col-md-6">
          <div id="lv"></div>
          <div id="cm"></div>
        </div>
      </div>
    </div>
                 </div>
                
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
              <input  type="hidden" class="modal-id business_id"  name="id"/>
              <input type="hidden" name="action_type" class="modal-type" value=""/>
              <input type="submit" class="btn btn-primary" name="submit" value="Thêm danh mục tiêu chí"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12">
  <div class="panel panel-default users-content">
    <div class="panel-heading">Danh mục tiêu chí</div>
    <table class="table">
      <tr>
        <th width="30%">Tên chủ đề tiêu chí</th>
        <th width="30%">Tên Lính vực ĐK</th>
        <th width="25%">Tên Chuyên môn</th>
        <th width="5%">Thứ Tự</th>
        <th width="5%">Ẩn hiện</th>
      </tr>
      <?php
          
            $chudetieuchi = $db->getRows('nncms_chudetieuchi',array('order_by'=>'idChuDeTieuChi ASC'));
            if(!empty($chudetieuchi)){ $count = 0; foreach($chudetieuchi as $chudett){ $count++;?>
      <tr>
        <td><?php echo $chudett['TenChuDeTieuChi'];?></td>
        <td><?php 
				$lay=$chudett['idLVDK'];
				$laylinhvucdk = $db->getRows('nncms_linhvucdk',array('where'=>array('idLVDK'=>$lay,'anHien'=>'on')),array('order_by'=>'idLVDK ASC'));
            if(!empty($laylinhvucdk)){ $count = 0; foreach($laylinhvucdk as $laylinhvuc){ $count++;
				echo $laylinhvuc['TieuDe'];}}?></td>
                
        <td><?php 
				$laycm=$chudett['idChuyenMon'];
				$laychuyenmondk = $db->getRows('nncms_chuyenmon',array('where'=>array('idChuyenMon'=>$laycm,'anHien'=>'on')),array('order_by'=>'idChuyenMon ASC'));
            if(!empty($laychuyenmondk)){ $count = 0; foreach($laychuyenmondk as $laychuyenmon){ $count++;
				echo $laychuyenmon['TenChuyenMon'];}}
		?></td>
        <td><?php echo $chudett['ThuTu'];?></td>
        <td><?php echo $chudett['AnHien'];?></td>
        <td><a 
                    
                    data-toggle="modal" data-target="#myModal"
                    data-tentc="<?php echo $chudett['TenChuDeTieuChi'];?>"
                    data-thutu="<?php echo $chudett['ThuTu'];?>"
                  	data-anhien="<?php echo $chudett['AnHien'];?>"
                    data-idlvdk="<?php echo $chudett['idLVDK'];?>"
                    data-idChuyenMon="<?php echo $chudett['idChuyenMon'];?>"
                    data-idNam="<?php echo $chudett['idNam'];?>"                    
                    data-title="Sửa Phân Loại" 
                    data-id="<?php echo $chudett['idChuDeTieuChi'];?>"  
                    data-type="editdmtc" 
                    class="glyphicon glyphicon-edit edit_button"></a> <a href="actionChuDeTieuChi.php?action_type=deletedmtc&id=<?php echo $chudett['idChuDeTieuChi'];?>" data-type="delete"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a></td>
      </tr>
      <?php } }else{ ?>
      <tr>
        <td colspan="4">No user(s) found......</td>
        <?php } ?>
    </table>
  </div>
</div>
