<?php
session_start();
include '../MySQL/DB.php';
$db = new DB();
$tblName='nncms_trangthai';
if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){
    if($_REQUEST['action_type'] == 'addtrangthai'){
        if($_POST['anhien']=="")
				{
					$anhien="off";
					}
					else
					{$anhien="on";
						}
				$userData = array(
				'TenTrangThai' => $_POST['trangthai'],
				'AnHien' => $anhien,
				'ThuTu' => $_POST['thutu'],
				'icon' => $_POST['icon'],
				'class' => $_POST['classtt'],
				'idLoaiTrangThai' => $_POST['idloaitrangthai'],
			);
			
			$insert = $db->insert($tblName,$userData);
			$statusMsg = $insert?'User data has been inserted successfully.':'Some problem occurred, please try again.';
			$_SESSION['statusMsg'] = $statusMsg;
			header("Location:index.php?key=Trangthai");
	}elseif($_REQUEST['action_type'] == 'edittrangthai'){
        if(!empty($_POST['id'])){
       if($_POST['anhien']=="")
				{
					$anhien="off";
					}
					else
					{$anhien="on";
						}
				$userData = array(
				'TenTrangThai' => $_POST['trangthai'],
				'AnHien' => $anhien,
				'ThuTu' => $_POST['thutu'],
				'icon' => $_POST['icon'],
				'class' => $_POST['classtt'],
				'idLoaiTrangThai' => $_POST['idloaitrangthai'],
			);
		$condition = array('idTrangThai' => $_POST['id']);
       $update = $db->update($tblName,$userData,$condition);
            $statusMsg = $update?'User data has been updated successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:index.php?key=Trangthai");
 }
    }	elseif($_GET['action_type'] == 'deletetrangthai'){
       if(!empty($_GET['id'])){
            $condition = array('idTrangThai' => $_GET['id']);
            $delete = $db->delete($tblName,$condition);
            $statusMsg = $delete?'User data has been deleted successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:index.php?key=Trangthai");
        }
    }
	
}