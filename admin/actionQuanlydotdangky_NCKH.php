<?php
session_start();
include '../MySQL/DB.php';
$db = new DB();
$tblName='nncms_quanlydotdangkynckh';

if($_POST['start']==""||$_POST['end']=="")
{
	$datestart="null";
	$dateend="null";

}else
{
	$dateFormatedstart = explode('/',$_POST['start']);
	$datestart = $dateFormatedstart[2].'-'.$dateFormatedstart[0].'-'.$dateFormatedstart[1];
	$dateFormatedend = explode('/',$_POST['end']);
	$dateend  = $dateFormatedend[2].'-'.$dateFormatedend[0].'-'.$dateFormatedend[1];
}
	$ngay=date("Y/m/d");

if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){
    if($_REQUEST['action_type'] == 'addquanlydotdangkynckh'){
        if($_POST['anhien']=="")
		{
			$anhien="off";
		}
		else
		{
			$anhien="on";
		}
		$userData = array(
			'TenDotdangky_NCKH' => $_POST['tendotdangkynckh'],
			'maDotdangky_NCKH' => $_POST['idmadotdangkynckh'],
			'ThuTu' => $_POST['thutu'],
			'loaidot' => $_POST['loaidot'],
			'AnHien' => $anhien,
			'starttimes'=> $datestart,
			'endtimes'=> $dateend,
			'ngaygiodk'=> $ngay,
			'idNam' => $_POST['namapdung'],
		);
		$insert = $db->insert($tblName,$userData);
		$statusMsg = $insert?'User data has been inserted successfully.':'Some problem occurred, please try again.';
		$_SESSION['statusMsg'] = $statusMsg;
		header("Location:index.php?key=dotdknckh");
	}elseif($_REQUEST['action_type'] == 'editquanlydotdangkynckh'){
        if(!empty($_POST['id'])){
    		if($_POST['anhien']=="")
			{
				$anhien="off";
			}
			else
			{
				$anhien="on";
			}

			$userData = array(
				'TenDotdangky_NCKH' => $_POST['tendotdangkynckh'],
				'maDotdangky_NCKH' => $_POST['cnmadotdangkynckh'],
				'ThuTu' => $_POST['thutu'],
				'loaidot' => $_POST['loaidot'],
				'AnHien' => $anhien,
				'starttimes'=> $datestart,
				'endtimes'=> $dateend,
				'ngaygiodk'=> $ngay,
				'idNam' => $_POST['namapdung'],
			);
		
			$condition = array('idDotdknckh' => $_POST['id']);
			$update = $db->update($tblName,$userData,$condition);
			  
			echo "<h1>$update</h1>";

            $statusMsg = $update?'User data has been updated successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
       		header("Location:index.php?key=dotdknckh");
 		}
    }elseif($_GET['action_type'] == 'deletequanlydotdangkyNCKH'){
       if(!empty($_GET['id'])){
            $condition = array('idDotdknckh' => $_GET['id']);
            $delete = $db->delete($tblName,$condition);
            $statusMsg = $delete?'User data has been deleted successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:index.php?key=dotdknckh");
        }
    }
}