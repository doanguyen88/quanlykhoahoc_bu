<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">

$(document).on( "click", '.addquanlydotkekhainckh',function(e) {	

		 var datatype = $(this).data('type');
		 $(".modal-type").val(datatype);    
});	
					
$(document).on( "click", '.editquanlydotkekhainckh',function(e) {
	
	
		var tendotkekhainckh = $(this).data('tendotkekhainckh'); 
		var datatype = $(this).data('type');
		var startimes = $(this).data('startimes');
		var endtimes = $(this).data('endtimes');
		var thutu = $(this).data('thutu'); 
		var anhien = $(this).data('anhien');
    var id = $(this).data('id');
		var idmadotkekhainckh = $(this).data('idmadotkekhainckh'); 
 		var idnam= $(this).data('idnam');
		var loaidot= $(this).data('loaidot');
		$(".business_idnam").val(idnam); 
		$(".business_idmadotkekhainckh").val(idmadotkekhainckh); 
    $(".business_tendotkekhainckh").val(tendotkekhainckh);    
    $(".business_thutu").val(thutu);
    if(anhien=='on'){document.getElementById("anhien").checked = true;}
    $(".business_id").val(id);
		$(".modal-type").val(datatype); 
		$(".business_startimes").val(startimes); 
		$(".business_endtimes").val(endtimes);
		$(".business_loaidot").val(loaidot);
		$(".business_cnmadotkekhainckh").val(idmadotkekhainckh);

		document.getElementById("idmadotkekhainckh").disabled = true;
		
    });
</script>

<script type="text/javascript">
  $(document).ready(function(){
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
		$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
			.closest('.ace-spinner')
			.on('changed.fu.spinbox', function(){
				//console.log($('#spinner1').val())
			}); 
	  $('input[name="anhien').on('switchChange.bootstrapSwitch', function(event, state) {
      console.log(this); // DOM element
      console.log(event); // jQuery event
      console.log(state); // true | false
    })
  });
</script>

<script type="text/javascript">
  function showDiv(select){
    if(select.value==1){
      document.getElementById('hidden_div').style.display = "block";
    } else{
      document.getElementById('hidden_div').style.display = "none";
    }
  } 
</script>

<div class="bs-example">
  <div class="container">
    <h4> 
      <!-- Button HTML (to Trigger Modal) -->
      <button type="button" class="btn btn-primary addquanlydotkekhainckh"  data-toggle="modal" data-target="#qldk" data-title="Thêm đợt kê khai NCKH" data-id="addquanlydotkekhainckh" data-type="addquanlydotkekhainckh">Thêm đợt kê khai NCKH</button>
    </h4>
    <!-- Modal HTML -->
    <div class="modal fade" id="qldk" role="dialog">
      <div class="modal-dialog"> 
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Quản lý đợt kê khai NCKH</h4>
          </div>
          <form role="form" id="universalModalForm" method="post" action="actionQuanlydotkekhai_NCKH.php" class="form" >
            <div class="modal-body">
            <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
            <div class="modal-body">
              <div class="form-group">
                <label>Mã Đợt</label>
                <input type="text" class="form-control business_idmadotkekhainckh" name="idmadotkekhainckh" id="idmadotkekhainckh"/>
              </div>
              <div class="form-group">
                <label>Tên đợt</label>
                <input type="text" class="form-control business_tendotkekhainckh" name="tendotkekhainckh"/>
              </div>
              <div class="input-group col-md-12 ">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Chọn năm áp dụng</label>
                    <select  name="namapdung" id="namapdung" data-placeholder="Chọn năm"  class="business_idnam" >
                      <?php $nams = $db->getRows('nncms_nam',array('where'=>array('AnHien'=>'on')),array('order_by'=>'idNam ASC'));
            if(!empty($nams)){ $count = 0; foreach($nams as $nam){ $count++;?>
                      <option value="<?php echo $nam['idNam'];?>"> <?php echo $nam['TNam'];?></option>
                      <?php } }?>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Chọn loại đợt</label>
                    <select  name="loaidot" id="loaidot" data-placeholder="Chọn năm"  class="business_loaidot" >
                      <option value=""> Chọn loại đợt</option>
                      <option value="dot1"> Đợt - 1</option>
                      <option value="dot2"> Đợt - 2</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="input-daterange" id="datepicker" >
                      <div class="input-group col-md-12 ">
                        <div class="col-md-6">
                          <label class="  control-label tenfrom" > Từ ngày </label>
                          <input type="text" class="textbox business_startimes" name="start" />
                        </div>
                        <div class="col-md-6 ">
                          <label class=" control-label tenfrom" > tới ngày </label>
                          <input type="text" class="textbox business_endtimes" name="end" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Thứ tự</label>
                        <input type="text" id="spinner1" name="thutu"  class="input-small business_thutu"/>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>
                          <input id="anhien" name="anhien" type="checkbox" class="ace business_anhien">
                          <span class="lbl"> Ẩn Hiện</span> </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
              <input  type="hidden" class="modal-id business_id"  name="id"/>
              <input  type="hidden" class="modal-id business_cnmadotkekhainckh"  name="cnmadotkekhainckh"/>
              <input type="hidden" name="action_type" class="modal-type" value=""/>
              <input type="submit" class="btn btn-primary" name="submit" value="Thêm đợt kê khai NCKH"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12">
  <div class="panel panel-default users-content">
    <div class="panel-heading">Quản lý đợt kê khai</div>
    <table class="table">
      <tr>
        <th width="10%">Mã đợt</th>
        <th width="10%">Loại đợt</th>
        <th width="20%">Tên đợt</th>
        <th width="35%">Thời gian</th>
        <th width="10%">Năm</th>
        <th width="10%">Thứ Tự</th>
        <th width="10%">Ẩn hiện</th>
      </tr>
      <?php
        $quanlydangkydetai = $db->getRows('nncms_QuanlydotkhekhaiNCKH',array('order_by'=>'idQuanlydangky_Detai ASC'));
        if(!empty($quanlydangkydetai)){ 
          $count = 0; 
          foreach($quanlydangkydetai as $quanlydangkydetais){ 
            $count++;
            ?>
            <tr>
              <td><?php echo $quanlydangkydetais['madotkekhai_NCKH'];?></td>
              <td><?php echo $quanlydangkydetais['loaidot'];?></td>
              <td><?php echo $quanlydangkydetais['TenDotkekhai_NCKH'];?></td>
              <td>
                <div><span class="label label-sm label-warning">Thời gian bắt đầu:</span>
                  <?php
                  $dateFormatedstart = explode('-',$quanlydangkydetais['starttimes']);
                  $datestart = $dateFormatedstart[2].'/'.$dateFormatedstart[1].'/'.$dateFormatedstart[0];
                  echo $datestart;?>
                </div>
                <div><span class="label label-sm label-warning">Thời gian kết thúc</span>
                  <?php 
                  $dateFormatedend = explode('-',$quanlydangkydetais['endtimes']);
                  $dateend  = $dateFormatedend[2].'/'.$dateFormatedend[1].'/'.$dateFormatedend[0];
                  echo $dateend;?>
                </div>
              </td>
              <td><?php $lay=$quanlydangkydetais['idNam']; $laynam = $db -> getRows('nncms_nam', array('where' => array('idNam' => $lay,'anHien'=>'on')), array('order_by' => 'idNam ASC'));
                if(!empty($laynam)){ 
                  $count = 0; 
                  foreach($laynam as $layna){ 
                    $count++;
                    echo $layna['TNam'];
                  }
                }?>
              </td>
              <td><?php echo $quanlydangkydetais['ThuTu'];?></td>
              <td><?php echo $quanlydangkydetais['AnHien'];?></td>
              <td>
                <a        
                  data-toggle="modal" data-target="#qldk"
                  data-tendotkekhainckh="<?php echo $quanlydangkydetais['TenDotkekhai_NCKH'];?>"
                  data-idmadotkekhainckh="<?php echo $quanlydangkydetais['madotkekhai_NCKH'];?>"
                  data-thutu="<?php echo $quanlydangkydetais['ThuTu'];?>" 
                  data-loaidot="<?php echo $quanlydangkydetais['loaidot'];?>"
                  data-idnam="<?php echo $quanlydangkydetais['idNam'];?>"
                  data-anhien="<?php echo $quanlydangkydetais['AnHien'];?>"
                  data-startimes="<?php echo $quanlydangkydetais['startimes'];?>"
                  data-endtimes="<?php echo $quanlydangkydetais['endtimes'];?>"
                  data-title="Sửa Quản lý đợt kê khai NCKH" 
                  data-id="<?php echo $quanlydangkydetais['idQuanlydangky_Detai'];?>"  
                  data-type="editquanlydotkekhainckh" 
                  class="glyphicon glyphicon-edit editquanlydotkekhainckh">
                </a> 
                <a href="actionQuanlydotkekhai_NCKH.php?action_type=deletequanlydotkekhaiNCKH&id=<?php echo $quanlydangkydetais['idQuanlydangky_Detai'];?>" data-type="delete"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');">
                </a>
              </td>
            </tr>
           <?php } 
        }else{ ?>
          <tr>
            <td colspan="6">Không có dữ liệu</td>
        <?php } ?>
    </table>
  </div>
</div>
