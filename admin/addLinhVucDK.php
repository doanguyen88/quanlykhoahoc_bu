<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">
$(document).on( "click", '.edit_button',function(e) {

        var TieuDe = $(this).data('name');
		var ThuTu = $(this).data('content'); 
		var AnHien = $(this).data('quote');
		var idPhanLoai_CN_TT = $(this).data('direction');
        var idLVDK = $(this).data('id');
		
					
		
       
                  
        $(".business_TieuDe").val(TieuDe);
        $(".business_ThuTu").val(ThuTu);
       	$(".business_AnHien").val(AnHien);
		$(".business_idPhanLoai_CN_TT").val(idPhanLoai_CN_TT);
  		$(".business_id").val(idLVDK);


    });
</script>
<script type="text/javascript">
 $(document).ready(function(){
	 $("#myModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title');
		var idData=button.data('id'); 
		var typeData=button.data('type'); 
        $(this).find('.modal-title').text('Form'+ titleData );
		 $(this).find('.modal-type').val(typeData);
    });
	 
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.closest('.ace-spinner')
				.on('changed.fu.spinbox', function(){
					//console.log($('#spinner1').val())
				}); 
					$('input[name="anhien').on('switchChange.bootstrapSwitch', function(event, state) {
  console.log(this); // DOM element
  console.log(event); // jQuery event
  console.log(state); // true | false
})
	
});

</script>
<script type="text/javascript">
function showDiv(select){
   if(select.value==1){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
} 
</script>
<div class="bs-example">
  <div class="container">
    <h4> 
      <!-- Button HTML (to Trigger Modal) -->
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-title="Thêm Lính vực ĐK" data-id="addlinhvucdangky" data-type="addlinhvucdangky">Thêm Lính vực ĐK</button>
    </h4>
    
    <!-- Modal HTML -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog"> 
        
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Quản lý Lính vực ĐK</h4>
          </div>
          <form role="form" id="universalModalForm" method="post" action="actionLinhVucDK.php" class="form" >
            <div class="modal-body">
              <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Tên Lính vực ĐK</label>
                  <input type="text" class="form-control business_TieuDe" name="TieuDe"/>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Chọn phân loại ĐK</label>
                        <select  name="phanloaidk" id="phanloaidk" data-placeholder="Chọn phân loại ĐK"  class="business_idPhanLoai_CN_TT" >
                          <?php $phanloaidk = $db->getRows('nncms_phanloaidk',array('where'=>array('anHien'=>'on')),array('order_by'=>'idPhanLoai_CN_TT ASC'));
            if(!empty($phanloaidk)){ $count = 0; foreach($phanloaidk as $phanloai){ $count++;?>
                          <option value="<?php echo $phanloai['idPhanLoai_CN_TT'];?>"> <?php echo $phanloai['TenPhanLoai'];?></option>
                          <?php } }?>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Thứ tự</label>
                        <input type="text" id="spinner1" name="thutu"  class="input-small business_ThuTu"/>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label> Ẩn Hiện</label>
                  <input name="anhien" class="ace ace-switch ace-switch-4 business_AnHien" type="checkbox">
                  <span class="lbl"></span> </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
              <input  type="hidden" class="modal-id business_id"  name="id"/>
              <input type="hidden" name="action_type" class="modal-type" value=""/>
              <input type="submit" class="btn btn-primary" name="submit" value="Thêm Lính vực ĐK"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12">
  <div class="panel panel-default users-content">
    <div class="panel-heading">Danh mục Lính vực ĐK </div>
    <table class="table">
      <tr>
        <th width="10%">id LVĐK </th>
        <th width="30%">Phân loại ĐK</th>
        <th width="30%">Tên Lính vực ĐK</th>
        <th width="10%">Thứ Tự</th>
        <th width="10%">Ẩn hiện</th>
      </tr>
      <?php
          
            $linhvucdk = $db->getRows('nncms_linhvucdk',array('order_by'=>'idLVDK ASC'));
            if(!empty($linhvucdk)){ $count = 0; foreach($linhvucdk as $linhvuc){ $count++;?>
      <tr>
        <td><?php echo $linhvuc['idLVDK'];?></td>
        <td><?php 
				$lay=$linhvuc['idPhanLoai_CN_TT'];
				$layPhanLoai = $db->getRows('nncms_phanloaidk',array('where'=>array('idPhanLoai_CN_TT'=>$lay,'anHien'=>'on')),array('order_by'=>'nncms_phanloaidk ASC'));
            if(!empty($layPhanLoai)){ $count = 0; foreach($layPhanLoai as $layPhanLoaidk){ $count++;
				echo $layPhanLoaidk['TenPhanLoai'];}}?></td>
        <td><?php echo $linhvuc['TieuDe'];?></td>
        <td><?php echo $linhvuc['ThuTu'];?></td>
        <td><?php echo $linhvuc['AnHien'];?></td>
        <td><a 
                    
                    data-toggle="modal" data-target="#myModal"
                    data-name="<?php echo $linhvuc['TieuDe'];?>"
                    data-content="<?php echo $linhvuc['ThuTu'];?>"
                  	data-quote="<?php echo $linhvuc['AnHien'];?>"
                    data-direction="<?php echo $linhvuc['idPhanLoai_CN_TT'];?>"

                   
                    data-title="Sửa Phân Loại" 
                    data-id="<?php echo $linhvuc['idLVDK'];?>"  
                    data-type="editlinhvucdangky" 
                    class="glyphicon glyphicon-edit edit_button"></a> <a href="actionLinhVucDK.php?action_type=deletelinhvucdangky&id=<?php echo $linhvuc['idLVDK'];?>" data-type="delete"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a></td>
      </tr>
      <?php } }else{ ?>
      <tr>
        <td colspan="4">No user(s) found......</td>
        <?php } ?>
    </table>
  </div>
</div>
