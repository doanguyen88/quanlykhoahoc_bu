<?php
session_start();
include '../MySQL/DB.php';
$db = new DB();
$hoten = $_POST['hoten'];
?>
 <div class="row" >
 <div class="col-xs-12">
          <table id="dynamic-table" class="table  table-bordered table-hover">
            <thead>
              <tr>
                <th class="center"> #STT </th>
                <th>Chi tiết</th>
                <th class="hidden-280">Họ và tên</th>
                <th>Số tiết quy đổi</th>
                <th>Trạng thái</th>
                <th class="hidden-280">Số tiết khoa duyệt</th>
                <th class="hidden-280">Số tiết Viện NC duyệt</th>
                <th><i class="ace-icon fa fa-cog icon-only bigger-130"></i></th>
              </tr>
            </thead>
            <tbody>
              <?php 
	

 $dskkstnckh = $db->getRows('nncms_KKST_NCKH',array('where'=>array('Ho_Ten'=>$hoten)),array('order_by'=>'id_KKST_NCKH DESC'));

	        if(!empty($dskkstnckh)){ $count = 0; foreach($dskkstnckh as $nckh){ $count++;?>
              <tr>
                <td class="center"><?php echo $count;?></td>
                <td class="center"><div class="action-buttons">
                 <a href="#" class="green bigger-140 show-details-btn show-details-kknckh" data-iddetailskknckh="<?php echo $nckh['id_KKST_NCKH'];?>" title="Xem chi tiết"> <i id="idiconnckh-<?php echo $nckh['id_KKST_NCKH'];?>" class="ace-icon fa fa-angle-double-up"></i> <span class="sr-only">Xem chi tiết</span> </a> </div></td>
                <td class="hidden-280"><?php echo $nckh['Ho_Ten'];?></td>
                <td><?php echo $nckh['SoTietQuyDoi'];?></td>
                <td ><?php echo $nckh['TrangThai'];?></td>
                <td class="hidden-280"><?php echo $nckh['SoTietKhoaDuyet'];?></td>
                <td class="hidden-280"><?php echo $nckh['SoTietVienNCDuyet'];?></td>
                <td><div class="">
                    <div class="inline pos-rel"> <a class="btn btn-xs btn-warning no-radius giangvienedit" data-toggle="modal" href="#stack3" 
            data-idkkstnckh="<?php echo $nckh['id_KKST_NCKH'];?>"
            data-hoten="<?php echo $nckh['Ho_Ten'];?>"
            data-chonqldkdt="<?php echo $nckh['chonqldkdt'];?>"
            data-chonphanloaikhnc="<?php echo $nckh['chonphanloaikhnc'];?>"
            data-masanphamkhnc="<?php echo $nckh['masosanpham'];?>"
            data-tensanpham="<?php echo $nckh['tensanpham'];?>"
            data-tapchihoithao="<?php echo $nckh['tapchihoithao'];?>"
            data-chonmahinhthuc="<?php echo $nckh['chonmahinhthuc'];?>"
            data-mavaitro="<?php echo $nckh['mavaitro'];?>"
            data-soquyetdinh="<?php echo $nckh['soquyetdinh'];?>"
            data-thoigianhoanthanh="<?php echo $nckh['Thoi_Gian_Hoan_Thanh'];?>"
            data-sotietquydoi="<?php echo $nckh['SoTietQuyDoi'];?>"
            data-trangthai="<?php echo $nckh['TrangThai'];?>"
            
            data-action="updatekknckh" title="View"> <i class="ace-icon fa fa-pencil-square-o bigger-150"></i> </a> <a   class="btn btn-xs btn-info no-radius blue file" href="#stack3" data-toggle="modal" data-target="#stack4"
          	 data-idkknckh="<?php echo $nckh['idfile'];?>"  onclick="qlfilekknckhadmin('<?php echo $nckh['idfile'];?>','<?php echo $nckh['file'];?>')" title="Edit"> <i class="ace-icon fa fa-folder-open-o bigger-150"></i> </a> <a href="action-NCKH.php?action_type=deletekknckh&id=<?php echo $nckh['id_KKST_NCKH'];?>" data-type="delete"  
             class="tooltip-error btn btn-xs no-radius btn-danger" onclick="return confirm('Chắc chắn là bạn muốn xoá kê khai này?');"  title="Delete"><i class="ace-icon fa fa-trash-o bigger-150"></i> </a> </div>
                  </div></td>
              </tr>
              <tr class="detail-row" id="iddetailskknckh-<?php echo $nckh['id_KKST_NCKH'];?>">
                <td colspan="10"><div class="table-detail">
                    <div class="row">
                      <div class="col-xs-12 col-sm-12">
                        <div class="space visible-xs"></div>
                        <div class="profile-user-info profile-user-info-striped">
                          <div class="profile-info-row">
                            <div class="profile-info-name"> Hoạt động </div>
                            <div class="profile-info-value"> <span><?php echo $nckh['chonqldkdt'];?></span> </div>
                          </div>
                          <div class="profile-info-row">
                            <div class="profile-info-name"> Phân Loại </div>
                            <div class="profile-info-value"> <span><?php echo $nckh['chonphanloaikhnc'];?></span> </div>
                          </div>
                          <div class="profile-info-row">
                            <div class="profile-info-name"> Tạp chí/hội thảo </div>
                            <div class="profile-info-value"> <span><?php echo $nckh['tapchihoithao'];?></span> </div>
                          </div>
                          <div class="profile-info-row">
                            <div class="profile-info-name"> Mã Hình Thức </div>
                            <div class="profile-info-value"> <span><?php echo $nckh['chonmahinhthuc'];?></span> </div>
                          </div>
                          <div class="profile-info-row">
                            <div class="profile-info-name"> Mã Vai Trò </div>
                            <div class="profile-info-value"> <span><?php echo $nckh['mavaitro'];?></span> </div>
                          </div>
                          <div class="profile-info-row">
                            <div class="profile-info-name"> Mã Sản phẩm </div>
                            <div class="profile-info-value"> <span><?php echo $nckh['masosanpham'];?></span> </div>
                          </div>
                          <div class="profile-info-row">
                            <div class="profile-info-name"> Tên Sản phẩm </div>
                            <div class="profile-info-value"> <span><?php echo $nckh['tensanpham'];?></span> </div>
                          </div>
                          <div class="profile-info-row">
                            <div class="profile-info-name"> Số quyết định </div>
                            <div class="profile-info-value"> <span><?php echo $nckh['soquyetdinh'];?></span> </div>
                          </div>
                          <div class="profile-info-row">
                            <div class="profile-info-name"> Thời gian hoàn thành </div>
                            <div class="profile-info-value"> <span><?php echo $nckh['Thoi_Gian_Hoan_Thanh'];?></span> </div>
                          </div>
                          <div class="profile-info-row">
                            <div class="profile-info-name"> Đợt kê khai </div>
                            <div class="profile-info-value"> <span><?php echo $nckh['chondotkknckh'];?></span> </div>
                          </div>
                          <div class="profile-info-row">
                            <div class="profile-info-name"> Năm kê khai </div>
                            <div class="profile-info-value"> <span><?php echo $nckh['namhoanthanh'];?></span> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div></td>
              </tr>
              <?php } }else{ ?>
              <tr>
                <td colspan="8">Không có dữ liệu</td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
 </div>
 
