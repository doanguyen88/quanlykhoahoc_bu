<?php
session_start();
include '../MySQL/DB.php';
$db = new DB();
$tblName='nncms_phanloaidk';
if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){
    if($_REQUEST['action_type'] == 'addphanloai'){
         if($_POST['anhien']=="")
				{
					$anhien="off";
					}
					else
					{$anhien="on";
		}
		
			$userData = array(
				'TenPhanLoai' => $_POST['TenPhanLoai'],
				'idNam' => $_POST['namapdung'],
				'ThuTu' => $_POST['thutu'],
				'AnHien' => $anhien,
				'Maphanloai' => $_POST['chonloai']
		
			);
			
			
			$insert = $db->insert($tblName,$userData);
			$statusMsg = $insert?'User data has been inserted successfully.':'Some problem occurred, please try again.';
			$_SESSION['statusMsg'] = $statusMsg;
			header("Location:index.php?key=Phanloaidk");
	}elseif($_REQUEST['action_type'] == 'editphanloai'){
        if(!empty($_POST['id'])){
    		 if($_POST['anhien']=="")
				{
					$anhien="off";
					}
					else
					{$anhien="on";
		}
		
			$userData = array(
				'TenPhanLoai' => $_POST['TenPhanLoai'],
				'idNam' => $_POST['namapdung'],
				'ThuTu' => $_POST['thutu'],
				'AnHien' => $anhien,
				'Maphanloai' => $_POST['chonloai']
		
			);
		
			$condition = array('idPhanLoai_CN_TT' => $_POST['id']);
      		 $update = $db->update($tblName,$userData,$condition);
            $statusMsg = $update?'User data has been updated successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
       		header("Location:index.php?key=Phanloaidk");
 }
    }	elseif($_GET['action_type'] == 'deletephanloai'){
       if(!empty($_GET['id'])){
            $condition = array('idPhanLoai_CN_TT' => $_GET['id']);
            $delete = $db->delete($tblName,$condition);
            $statusMsg = $delete?'User data has been deleted successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:index.php?key=Phanloaidk");
        }
    }
	
}