<?php
session_start();
include '../MySQL/DB.php';
$db = new DB();
$tblName='nncms_QuanlydotkhekhaiNCKH';

if($_POST['start']==""||$_POST['end']=="")
{
	$datestart="null";
	$dateend="null";
}else
{

	$dateFormatedstart = explode('/',$_POST['start']);
	$datestart = $dateFormatedstart[2].'-'.$dateFormatedstart[0].'-'.$dateFormatedstart[1];
	$dateFormatedend = explode('/',$_POST['end']);
	$dateend  = $dateFormatedend[2].'-'.$dateFormatedend[0].'-'.$dateFormatedend[1];

}
	$ngay=date("Y/m/d");

if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){
    if($_REQUEST['action_type'] == 'addquanlydotkekhainckh'){
        if($_POST['anhien']=="")
			{
				$anhien="off";
			}
			else
			{
				$anhien="on";
			}
			$userData = array(
				'TenDotkekhai_NCKH' => $_POST['tendotkekhainckh'],
				'madotkekhai_NCKH' => $_POST['idmadotkekhainckh'],
				'ThuTu' => $_POST['thutu'],
				'loaidot' => $_POST['loaidot'],
				'AnHien' => $anhien,
				'starttimes'=> $datestart,
				'endtimes'=> $dateend,
				'ngaygiodk'=> $ngay,
				'idNam' => $_POST['namapdung'],
			);
			$insert = $db->insert($tblName,$userData);
			$statusMsg = $insert?'User data has been inserted successfully.':'Some problem occurred, please try again.';
			$_SESSION['statusMsg'] = $statusMsg;
			header("Location:index.php?key=dotkekhainckh");
	}elseif($_REQUEST['action_type'] == 'editquanlydotkekhainckh'){
        if(!empty($_POST['id'])){
    		if($_POST['anhien']=="")
			{
				$anhien="off";
			}
			else
			{
				$anhien="on";
			}
			$userData = array(
				'TenDotkekhai_NCKH' => $_POST['tendotkekhainckh'],
				'madotkekhai_NCKH' => $_POST['cnmadotkekhainckh'],
				'ThuTu' => $_POST['thutu'],
				'loaidot' => $_POST['loaidot'],
				'AnHien' => $anhien,
				'starttimes'=> $datestart,
				'endtimes'=> $dateend,
				'ngaygiodk'=> $ngay,
				'idNam' => $_POST['namapdung'],
			);
		
			$condition = array('idQuanlydangky_Detai' => $_POST['id']);
      		$update = $db->update($tblName,$userData,$condition);
            $statusMsg = $update?'User data has been updated successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
       		header("Location:index.php?key=dotkekhainckh");
 		}
    }elseif($_GET['action_type'] == 'deletequanlydotkekhaiNCKH'){
       if(!empty($_GET['id'])){
            $condition = array('idQuanlydangky_Detai' => $_GET['id']);
            $delete = $db->delete($tblName,$condition);
            $statusMsg = $delete?'User data has been deleted successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:index.php?key=dotkekhainckh");
        }
    }	
}