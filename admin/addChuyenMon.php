<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">
$(document).on( "click", '.edit_button',function(e) {

        var TenChuyenMon = $(this).data('name');
		var ThuTu = $(this).data('content'); 
		var AnHien = $(this).data('quote');
		var idLVDK = $(this).data('direction');
        var idChuyenMon = $(this).data('id');
		
			                  
        $(".business_TenChuyenMon").val(TenChuyenMon);
        $(".business_ThuTu").val(ThuTu);
       	$(".business_AnHien").val(AnHien);
		$(".business_idLVDK").val(idLVDK);
  		$(".business_id").val(idChuyenMon);


    });
</script>
<script type="text/javascript">
 $(document).ready(function(){
	 $("#myModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title');
		var idData=button.data('id'); 
		var typeData=button.data('type'); 
        $(this).find('.modal-title').text('Form'+ titleData );
		 $(this).find('.modal-type').val(typeData);
    });
	 
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.closest('.ace-spinner')
				.on('changed.fu.spinbox', function(){
					//console.log($('#spinner1').val())
				}); 
									$('input[name="anhien').on('switchChange.bootstrapSwitch', function(event, state) {
  console.log(this); // DOM element
  console.log(event); // jQuery event
  console.log(state); // true | false
})
	
});

</script>
<script type="text/javascript">
function showDiv(select){
   if(select.value==1){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
} 
</script>
<div class="bs-example">
  <div class="container">
    <h4> 
      <!-- Button HTML (to Trigger Modal) -->
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-title="Thêm Chuyên môn" data-id="addchuyenmon" data-type="addchuyenmon">Thêm Chuyên môn</button>
    </h4>
    
    <!-- Modal HTML -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog"> 
        
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Quản lý Chuyên môn</h4>
          </div>
          <form role="form" id="universalModalForm" method="post" action="actionChuyenMon.php" class="form" >
            <div class="modal-body">
              <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Tên chuyên môn</label>
                  <input type="text" class="form-control business_TenChuyenMon" name="TenChuyenMon"/>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Chọn Lĩnh vực</label>
                        <select  name="linhvucdk" id="linhvucdk" data-placeholder="Chọn Lĩnh vực ĐK"  class="business_idPhanLoai_CN_TT" >
                          <?php $linhvucdk = $db->getRows('nncms_linhvucdk',array('where'=>array('anHien'=>'on')),array('order_by'=>'idLVDK ASC'));
            if(!empty($linhvucdk)){ $count = 0; foreach($linhvucdk as $linhvuc){ $count++;?>
                          <option value="<?php echo $linhvuc['idLVDK'];?>"> <?php echo $linhvuc['TieuDe'];?></option>
                          <?php } }?>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Thứ tự</label>
                        <input type="text" id="spinner1" name="thutu"  class="input-small business_ThuTu"/>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label> Ẩn Hiện</label>
                  <input name="anhien" class="ace ace-switch ace-switch-4 business_AnHien" type="checkbox">
                  <span class="lbl"></span> </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
              <input  type="hidden" class="modal-id business_id"  name="id"/>
              <input type="hidden" name="action_type" class="modal-type" value=""/>
              <input type="submit" class="btn btn-primary" name="submit" value="Thêm chuyên môn ĐK"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12">
  <div class="panel panel-default users-content">
    <div class="panel-heading">Danh mục chuyên môn ĐK </div>
    <table class="table">
      <tr>
        <th width="10%">id chuyên môn </th>
        <th width="30%">Lính vực ĐK</th>
        <th width="30%">Tên chuyên môn ĐK</th>
        <th width="10%">Thứ Tự</th>
        <th width="10%">Ẩn hiện</th>
      </tr>
      <?php
          
            $chuyenmondk = $db->getRows('nncms_chuyenmon',array('order_by'=>'idChuyenMon ASC'));
            if(!empty($chuyenmondk)){ $count = 0; foreach($chuyenmondk as $chuyenmon){ $count++;?>
      <tr>
        <td><?php echo $chuyenmon['idChuyenMon'];?></td>
        <td><?php 
				$lay=$chuyenmon['idLVDK'];
				$laylinhvuc = $db->getRows('nncms_linhvucdk',array('where'=>array('idLVDK'=>$lay,'anHien'=>'on')),array('order_by'=>'idLVDK ASC'));
            if(!empty($laylinhvuc)){ $count = 0; foreach($laylinhvuc as $laylinhvucdk){ $count++;
				echo $laylinhvucdk['TieuDe'];}}?></td>
        <td><?php echo $chuyenmon['TenChuyenMon'];?></td>
        <td><?php echo $chuyenmon['ThuTu'];?></td>
        <td><?php echo $chuyenmon['AnHien'];?></td>
        <td><a 
                    data-toggle="modal" data-target="#myModal"
                    data-name="<?php echo $chuyenmon['TenChuyenMon'];?>"
                    data-content="<?php echo $chuyenmon['ThuTu'];?>"
                  	data-quote="<?php echo $chuyenmon['AnHien'];?>"
                    data-direction="<?php echo $chuyenmon['idLVDK'];?>"

                   
                    data-title="Sửa Phân Loại" 
                    data-id="<?php echo $chuyenmon['idChuyenMon'];?>"  
                    data-type="editchuyenmondk" 
                    class="glyphicon glyphicon-edit edit_button"></a> <a href="actionChuyenMon.php?action_type=deletechuyenmondangky&id=<?php echo $chuyenmon['idChuyenMon'];?>" data-type="delete"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a></td>
      </tr>
      <?php } }else{ ?>
      <tr>
        <td colspan="4">No user(s) found......</td>
        <?php } ?>
    </table>
  </div>
</div>
