<?php
session_start();
include '../MySQL/DB.php';
$db = new DB();
$tblName='NCKH_MaHinhThuc';


if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){
    if($_REQUEST['action_type'] == 'addmahinhthucnckh'){
         if($_POST['anhien']=="")
				{
					$anhien="off";
					}
					else
					{$anhien="on";
		}
			$userData = array(
				'Ten_MaHinhThuc' => $_POST['tenmahinhthucnckh'],
				'ThuTu' => $_POST['thutu'],
				'AnHien' => $anhien
				
		
			);
			
			
			$insert = $db->insert($tblName,$userData);
			$statusMsg = $insert?'User data has been inserted successfully.':'Some problem occurred, please try again.';
			$_SESSION['statusMsg'] = $statusMsg;
			header("Location:index.php?key=ADD_MaHinhThuc_KKKHCN");
			
	}elseif($_REQUEST['action_type'] == 'editmahinhthucnckh'){
        if(!empty($_POST['id'])){
    		 if($_POST['anhien']=="")
				{
					$anhien="off";
					}
					else
					{$anhien="on";
		}
		
			$userData = array(
				'Ten_MaHinhThuc' => $_POST['tenmahinhthucnckh'],
				'ThuTu' => $_POST['thutu'],
				'AnHien' => $anhien
				
		
			);
		
			$condition = array('id_MaHinhThuc' => $_POST['id']);
      		 $update = $db->update($tblName,$userData,$condition);
            $statusMsg = $update?'User data has been updated successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
       		header("Location:index.php?key=ADD_MaHinhThuc_KKKHCN");
 }
    }	elseif($_GET['action_type'] == 'deletemahinhthucnckh'){
		
       if(!empty($_GET['id'])){
            $condition = array('id_MaHinhThuc' => $_GET['id']);
            $delete = $db->delete($tblName,$condition);
            $statusMsg = $delete?'User data has been deleted successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:index.php?key=ADD_MaHinhThuc_KKKHCN");
        }
    }
	
}

