<?php 
$db = new DB();

			$demdk = $db->demrow('nncms_ttdangky',array('where'=>array('anHien'=>'on')),array('order_by'=>'idTTDK ASC'));
			$demdat = $db->demrow('nncms_ttdangky',array('where'=>array('DC_KetQuaCC'=>"Đạt")),array('order_by'=>'idTTDK ASC'));
			$demcduyet = $db->demrow('nncms_ttdangky',array('where'=>array('DC_KetQuaCC'=>"Chờ duyệt")),array('order_by'=>'idTTDK ASC'));
			$demdcduyet = $db->demrow('nncms_ttdangky',array('where'=>array('DC_KetQuaCC'=>"Đabg chờ duyệt")),array('order_by'=>'idTTDK ASC'));
			$demttdt = $db->demrow('nncms_ttdangky',array('where'=>array('DC_KetQuaCC'=>"Trùng tên đề tài")),array('order_by'=>'idTTDK ASC'));
			$demkocn = $db->demrow('nncms_ttdangky',array('where'=>array('DC_KetQuaCC'=>"Không đạt")),array('order_by'=>'idTTDK ASC'));
			$demkhac = $db->demrow('nncms_ttdangky',array('where'=>array('DC_KetQuaCC'=>"Chưa có kết quả")),array('order_by'=>'idTTDK ASC'));

			$ptdk=$demdk/100;
			$ptdat= ($demdat *100)/$demdk;
			$ptdemcduyet= ($demcduyet *100)/$demdk;
			$ptdemdcduyet= ($demdcduyet *100)/$demdk;
			$ptdemttdt= ($demttdt *100)/$demdk;
			$ptdemkocn= ($demkocn *100)/$demdk;
			$ptdemkhac=($demkhac *100)/$demdk;
			?>

<div class="col-xs-12 col-sm-12">
<h3 class="header smaller lighter green">Thông kê </h3>
<div class="row">
<div class="col-xs-8">
  <div class="text-primary">Tổng số đăng ký là: <?php echo $demdk;?> </div>
  <div class="progress progress-striped" data-percent="<?php echo $demdk;?>">
    <div class="progress-bar" style="width:<?php echo $ptdk;  ?>%;"></div>
  </div>
  <div class="text-primary">Số sáng kiến "Đạt" là : <?php echo $demdat;?> </div>
  <div class="progress progress-striped"="<?php echo $demdat;?>">
    <div class="progress-bar progress-bar-success" style="width: <?php echo $ptdat; ?>%;"></div>
  </div>
  <div class="text-primary">Số sáng kiến "Chờ duyệt" là : <?php echo $demcduyet;?> </div>
  <div class="progress progress-striped">
    <div class="progress-bar progress-bar-yellow" style="width: <?php echo $ptdemcduyet; ?>%;"></div>
  </div>
  <div class="text-primary">Số sáng kiến "Đang chờ duyệt" là : <?php echo $demdcduyet;?> </div>
  <div class="progress progress-striped">
    <div class="progress-bar progress-danger" style="width: <?php echo $ptdemdcduyet;?>%;"></div>
  </div>
  <div class="text-primary">Số sáng kiến "Không được công nhận" là : <?php echo $demkocn;?> </div>
  <div class="progress progress-striped">
    <div class="progress-bar progress-bar-arrowed" style="width: <?php echo $ptdemkocn;?>%"></div>
  </div>
  <div class="text-primary">Số sáng kiến "Trùng tên Đề tài" là : <?php echo $demttdt;?> </div>
  <div class="progress progress-striped">
    <div class="progress-bar progress-bar-inverse" style="width: <?php echo $ptdemttdt;?>%"></div>
  </div>
    <div class="text-primary">Số sáng kiến "Chưa có kết quả" là : <?php echo $demkhac;?> </div>
  <div class="progress progress-striped">
	<div class="progress-bar progress-bar-purple" style="width: <?php echo $ptdemkhac;?>%"></div>
	</div>
</div>
<!-- /.col -->

<div class="col-xs-4 left">
  <div class="infobox-data"> <span class="infobox-text"><?php echo "Đăng ký: ".$demdk." bản ghi";  ?></span> </div>
  <hr>
  <div class="infobox-data"> <span class="infobox-text"><?php echo "Đạt: ".$ptdat?>%</span> </div>
  <hr>
  <div class="infobox-data"> <span class="infobox-text"><?php echo "Chờ duyệt: ".$ptdemcduyet?>%</span> </div>
  <hr>
  <div class="infobox-data"> <span class="infobox-text"><?php echo "Đang chờ duyệt: ".$ptdemdcduyet?>%</span> </div>
  <hr>
  <div class="infobox-data"> <span class="infobox-text"><?php echo "Đang chờ duyệt: ".$ptdemdcduyet?>%</span> </div>
  <hr>
  <div class="infobox-data"> <span class="infobox-text"><?php echo "Không được công: ".$ptdemkocn?>%</span> </div>
  <hr>
  <div class="infobox-data"> <span class="infobox-text"><?php echo "Trùng tên đề tài: ".$ptdemttdt?>%</span> </div>
  <hr>
  <div class="infobox-data"> <span class="infobox-text"><?php echo "Chưa có kết quả: ".$ptdemkhac?>%</span> </div>
  <hr>
</div>
