<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">
$(document).on( "click", '.edit_button',function(e) {

        var TenHoiDong = $(this).data('name');
		var ThuTu = $(this).data('thutu'); 
		var AnHien = $(this).data('anhien');
		var linhvuc = $(this).data('linhvuc');
        var idHoiDong = $(this).data('id');
		
					
		
       
                  
        $(".business_TenHoiDong").val(TenHoiDong);
        $(".business_ThuTu").val(ThuTu);
       	$(".business_AnHien").val(AnHien);
		$(".business_linhvuc").val(linhvuc);
  		$(".business_id").val(idHoiDong);


    });
</script>
<script type="text/javascript">
 $(document).ready(function(){
	 $("#myModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title');
		var idData=button.data('id'); 
		var typeData=button.data('type'); 
        $(this).find('.modal-title').text('Form'+ titleData );
		 $(this).find('.modal-type').val(typeData);
    });
	 
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.closest('.ace-spinner')
				.on('changed.fu.spinbox', function(){
					//console.log($('#spinner1').val())
				}); 
					$('input[name="anhien').on('switchChange.bootstrapSwitch', function(event, state) {
  console.log(this); // DOM element
  console.log(event); // jQuery event
  console.log(state); // true | false
})
	
});

</script>
<script type="text/javascript">
function showDiv(select){
   if(select.value==1){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
} 
</script>
<div class="bs-example">
  <div class="container">
    <h4> 
      <!-- Button HTML (to Trigger Modal) -->
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-title="Thêm Hội đồng đánh giá" data-id="addhoidongdanhgia" data-type="addhoidongdanhgia">Thêm Hội đồng đánh giá</button>
    </h4>
    
    <!-- Modal HTML -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog"> 
        
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Quản lý Hội đồng đánh giá</h4>
          </div>
          <form role="form" id="universalModalForm" method="post" action="actionHoidongdanhgia.php" class="form" >
            <div class="modal-body">
              <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Tên Hội đồng</label>
                  <input type="text" class="form-control business_TenHoiDong" name="tenhoidong"/>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Thứ tự</label>
                        <input type="text" id="spinner1" name="thutu"  class="input-small business_ThuTu"/>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label> Ẩn Hiện</label>
                  <input name="anhien" class="ace ace-switch ace-switch-4 business_AnHien" type="checkbox">
                  <span class="lbl"></span> </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
              <input  type="hidden" class="modal-id business_id"  name="id"/>
              <input type="hidden" name="action_type" class="modal-type" value=""/>
              <input type="submit" class="btn btn-primary" name="submit" value="Thêm Hội đồng đánh giá"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12">
  <div class="panel panel-default users-content">
    <div class="panel-heading">Danh sách Hội đồng đánh giá </div>
    <table class="table">
      <tr>
        <th width="30%">Tên Hội đồng </th>
        <th width="30%">Lính vực áp dung của HĐ</th>
         <th width="10%">Thứ Tự</th>
        <th width="10%">Ẩn hiện</th>
      </tr>
      <?php
          
            $Hoidongdg = $db->getRows('nncms_Hoidong',array('order_by'=>'idHoiDong ASC'));
            if(!empty($Hoidongdg)){ $count = 0; foreach($Hoidongdg as $Hoidong){ $count++;?>
      <tr>
        <td><?php echo $Hoidong['TenHoiDong'];?></td>
        <td><?php 
				$lay=$Hoidong['idLVDK'];
				$laylinhvucdk = $db->getRows('nncms_linhvucdk',array('where'=>array('idLVDK'=>$lay,'anHien'=>'on')),array('order_by'=>'idLVDK ASC'));
            if(!empty($laylinhvucdk)){ $count = 0; foreach($laylinhvucdk as $laylinhvuc){ $count++;
				echo $laylinhvuc['TieuDe'];}}?></td>
        <td><?php echo $Hoidong['ThuTu'];?></td>
        <td><?php echo $Hoidong['AnHien'];?></td>
        <td><a 
                    
                    data-toggle="modal" data-target="#myModal"
                    data-name="<?php echo $Hoidong['TenHoiDong'];?>"
                    data-thutu="<?php echo $Hoidong['ThuTu'];?>"
                  	data-anhien="<?php echo $Hoidong['AnHien'];?>"
                    data-linhvuc="<?php echo $Hoidong['idLVDK'];?>"

                   
                    data-title="Sửa Thông tin Hội đồng ĐG" 
                    data-id="<?php echo $Hoidong['idHoiDong'];?>"  
                    data-type="edithoidongdanhgia" 
                    class="glyphicon glyphicon-edit edit_button"></a>
                     <a href="actionHoidongdanhgia.php?action_type=xoahoidong&id=<?php echo $Hoidong['idHoiDong'];?>" data-type="xoahoidong"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a></td>
      </tr>
      <?php } }else{ ?>
      <tr>
        <td colspan="4">No user(s) found......</td>
        <?php } ?>
    </table>
  </div>
</div>
