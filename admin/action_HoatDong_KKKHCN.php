<?php
session_start();
include '../MySQL/DB.php';
$db = new DB();
$tblName='NCKH_HoatDong';

if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){
    if($_REQUEST['action_type'] == 'addhoatdongnckh'){
        if($_POST['anhien']=="")
			{
				$anhien="off";
			}
			else
			{
				$anhien="on";
			}
			$userData = array(
				'Ten_HoatDong' => $_POST['tenhoatdongnckh'],
				'ThuTu' => $_POST['thutu'],
				'AnHien' => $anhien
			);
	
			$insert = $db->insert($tblName,$userData);
			$statusMsg = $insert?'User data has been inserted successfully.':'Some problem occurred, please try again.';
			$_SESSION['statusMsg'] = $statusMsg;
			header("Location:index.php?key=ADD_HoatDong_KKKHCN");
			
	}elseif($_REQUEST['action_type'] == 'edithoatdongnckh'){
        if(!empty($_POST['id'])){
			if($_POST['anhien']=="")
			{
				$anhien="off";
			}
			else
			{
				$anhien="on";
			}
		
			$userData = array(
				'Ten_HoatDong' => $_POST['tenhoatdongnckh'],
				'ThuTu' => $_POST['thutu'],
				'AnHien' => $anhien
			);
		
			$condition = array('id_HoatDong' => $_POST['id']);
      		$update = $db->update($tblName,$userData,$condition);
            $statusMsg = $update?'User data has been updated successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
       		header("Location:index.php?key=ADD_HoatDong_KKKHCN");
 		}
    }	elseif($_GET['action_type'] == 'deletehoatdonginckh'){
       if(!empty($_GET['id'])){
            $condition = array('id_HoatDong' => $_GET['id']);
            $delete = $db->delete($tblName,$condition);
            $statusMsg = $delete?'User data has been deleted successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:index.php?key=ADD_HoatDong_KKKHCN");
        }
    }
}