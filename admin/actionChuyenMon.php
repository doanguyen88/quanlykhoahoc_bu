<?php
session_start();
include '../MySQL/DB.php';
$db = new DB();
$tblName='nncms_chuyenmon';
if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){
    if($_REQUEST['action_type'] == 'addchuyenmon'){
          if($_POST['anhien']=="")
				{
					$anhien="off";
					}
					else
					{$anhien="on";
				}
		
			$userData = array(
				'idLVDK' => $_POST['linhvucdk'],
				'TenChuyenMon' => $_POST['TenChuyenMon'],
				'ThuTu' => $_POST['thutu'],
				'AnHien' => $anhien
		
			);
			
			
			$insert = $db->insert($tblName,$userData);
			$statusMsg = $insert?'User data has been inserted successfully.':'Some problem occurred, please try again.';
			$_SESSION['statusMsg'] = $statusMsg;
			header("Location:index.php?key=chuyenmondk");
	}elseif($_REQUEST['action_type'] == 'editchuyenmondk'){
        if(!empty($_POST['id'])){
    	  if($_POST['anhien']=="")
				{
					$anhien="off";
					}
					else
					{$anhien="on";
				}
		
			$userData = array(
				'idLVDK' => $_POST['linhvucdk'],
				'TenChuyenMon' => $_POST['TenChuyenMon'],
				'ThuTu' => $_POST['thutu'],
				'AnHien' => $anhien
		
			);
		
			$condition = array('idChuyenMon' => $_POST['id']);
      		 $update = $db->update($tblName,$userData,$condition);
            $statusMsg = $update?'User data has been updated successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
       		header("Location:index.php?key=chuyenmondk");
 }
    }	elseif($_GET['action_type'] == 'deletechuyenmondangky'){
       if(!empty($_GET['id'])){
            $condition = array('idChuyenMon' => $_GET['id']);
            $delete = $db->delete($tblName,$condition);
            $statusMsg = $delete?'User data has been deleted successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:index.php?key=chuyenmondk");
        }
    }
	
}