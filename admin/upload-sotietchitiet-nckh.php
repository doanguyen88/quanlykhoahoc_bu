<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">

  
$(document).on( "click", '.view_button',function(e) {
				
       		var id = $(this).data('id');	
			var hoten = $(this).data('hoten');
			var donvi = $(this).data('donvi');
			var nam = $(this).data('nam');
			var anhien = $(this).data('anhien');
			var dotkekhai = $(this).data('dotkekhai');
			var dinhmucone = $(this).data('dinhmucone');
			var sotietthuathieu = $(this).data('sotietthuathieu');
			var sotietthuchien = $(this).data('sotietthuchien');
			var action = $(this).data('action');
			
	if(anhien=='on'){document.getElementById("anhien").checked = true;}
      			
				$(".id").val(id);
				$(".hoten").text(hoten);
				$(".donvi").text(donvi);
				$(".nam").text(nam);
				$(".dotkekhainckh").text(dotkekhai);
				$(".dinhmuc").val(dinhmucone);
				$(".sotietthuathieu").val(sotietthuathieu);
				$(".sotietthuchien").val(sotietthuchien);
				$(".action").val(action);
				
    });
</script>
<script type="text/javascript">
 $(document).ready(function(){
	 $("#myModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title');
		var idData=button.data('id'); 
		var typeData=button.data('type'); 
        $(this).find('.modal-title').text('Form'+ titleData );
		 $(this).find('.modal-type').val(typeData);
    });
	 
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.closest('.ace-spinner')
				.on('changed.fu.spinbox', function(){
					//console.log($('#spinner1').val())
				}); 
$('input[name="anhien').on('switchChange.bootstrapSwitch', function(event, state) {
  console.log(this); // DOM element
  console.log(event); // jQuery event
  console.log(state); // true | false
});

});

</script>

<div class="col-xs-12 col-sm-12">
  <div class="panel panel-default users-content">
    <div class="panel-heading">Cập nhật Số tiết chi tiết kê khai từ file Excel </div>
    <div class="row" style="padding-top:16px;">
      <div class="col-xs-10">
        <form action="upload-sotietkknckh-send.php" method="post" enctype="multipart/form-data">
          <div class="col-xs-2">
            <label>Chọn năm</label>
            <select  name="nam" id="nam" data-placeholder="Chọn năm"  class="" >
              <?php $nams = $db->getRows('nncms_nam',array('where'=>array('AnHien'=>'on')),array('order_by'=>'idNam ASC'));
            if(!empty($nams)){ $count = 0; foreach($nams as $nam){ $count++;?>
              <option value="<?php echo $nam['TNam'];?>"> <?php echo $nam['TNam'];?></option>
              <?php } }?>
            </select>
          </div>
          <div class="col-xs-4">
            <label>Chọn đợt kê khai</label>
            <select  name="madotkekhai" id="madotkekhai" data-placeholder="Chọn đợt kê khai"  class="" >
              <?php $qldkdetai = $db->getRows('nncms_QuanlydotkhekhaiNCKH',array('where'=>array('anHien'=>'on')),array('order_by'=>'idQuanlydangky_Detai ASC'));
            if(!empty($qldkdetai)){ $count = 0; foreach($qldkdetai as $qldkdetais){ $count++;?>
              <option value="<?php echo $qldkdetais['madotkekhai_NCKH'];?>"> <?php echo $qldkdetais['TenDotkekhai_NCKH'];?></option>
              <?php } }?>
            </select>
          </div>
          <div class=" col-xs-4 fileupload fileupload-new" data-provides="fileupload">
            <div class="col-xs-12">
            <label class="ace-file-input">
            <input type="file" name="file" id="id-input-file-2">
            </div>
            <div class="col-xs-12"> <span class="ace-file-container" data-title="chọn file"> </span></span>
              </label>
            </div>
          </div>
          <div class="col-xs-2">
           <input type="hidden" name="ngaycapnhat"  value="<?php echo date("m/d/Y");?>"/>
            <button type="submit" name="btn-upload-sotiet" class="btn btn-sm btn-success" value=""> <i class="ace-icon ace-icon fa fa-cloud-upload bigger-110"></i> Improt dữ liệu </button>
          </div>
        </form>
      </div>
      <div class="col-xs-2"> <a class="btn btn-sm  btn-danger" href="import-excel/file-upload/sotietkk-nckh.xlsx"> <i class="ace-icon fa fa-download align-top bigger-125"></i> Tải xuống mẫu file </a> </div>
    </div>
    <div class="modal-footer"> </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12">
  <div class="panel panel-default users-content">
    <div class="panel-heading">Danh sách Số tiết chi tiết kê khai NCKH </div>
    <table class="table">
      <tr>
        <th width="10%">Họ Tên</th>
        <th width="15%">Đơn vị</th>
        <th width="15%">Năm</th>
        <th width="15%">Mã Đợt</th>
        <th width="15%">Đợt</th>
        <th width="15%">Số tiết định mức</th>
        <th width="15%">Số tiết thực hiện</th>
        <th width="15%">Số tiết thừa thiếu</th>
        <th width="15%">Chỉnh sửa</th>
      </tr>
      <?php 
	
    $dinhmuc = $db->demrow('nncms_SoTietChiTiet_NCKH',array('where'=>array('AnHien'=>'on')));
	 $sd=50;
	$tst=ceil($dinhmuc/$sd);// tinh tong so trang
	// Lay trang:
	if(isset($_GET['page']))
	{
	$page=intval($_GET['page']);
	}
	else
	$page=1;
	//Tinh vi tri array('where'=>array('xoa'=>'')),
	$vt=($page-1)*$sd;

            $dsdinhmuc = $db->getRowsgom('nncms_SoTietChiTiet_NCKH',array('where'=>array('AnHien'=>'on')),array('order_by'=>'id_SoTietChiTiet_NCKH DESC'),array('start'=>$vt),array('limit'=>$sd));
			$stt=$vt+1;
            if(!empty($dsdinhmuc)){ $count = 0; foreach($dsdinhmuc as $dsdm){ $count++;?>
      <tr>
        <td><?php echo $dsdm['Ho_Ten'];?></td>
        <td><?php echo $dsdm['DonVi'];?></td>
        <td><?php echo $dsdm['nam'];?></td>
        <td><?php echo $dsdm['iddotkknckh'];?></td>
        <td><?php echo $dsdm['dotkknckh'];?></td>
        <td><?php echo $dsdm['DinhMuc'];?></td>
        <td><?php echo $dsdm['SoTietThucHien'];?></td>
        <td><?php echo $dsdm['SoTietThuaThieu'];?></td>
        <td><a  class="glyphicon glyphicon-edit view_button"href="#" data-toggle="modal" data-target="#myModal"
          	data-hoten="<?php echo $dsdm['Ho_Ten'];?>"
            data-donvi="<?php echo $dsdm['DonVi'];?>"
            data-nam="<?php echo $dsdm['nam'];?>"
            data-trangtrai="<?php echo $dsdm['trangthai'];?>"
             data-anhien="<?php echo $dsdm['AnHien'];?>"
            data-dotkekhai="<?php echo $dsdm['iddotkknckh'];?>"
            data-dinhmucone="<?php echo $dsdm['DinhMuc'];?>" 
            data-sotietthuchien="<?php echo $dsdm['SoTietThucHien'];?>" 
            data-sotietthuathieu="<?php echo $dsdm['SoTietThuaThieu'];?>" 
             data-id="<?php echo $dsdm['id_SoTietChiTiet_NCKH'];?>"
              data-action="editsotietkekhai" ></a> 
              <a href="luusotietkknckh.php?action_type=delete&id=<?php echo $dsdm['id_SoTietChiTiet_NCKH'];?>" data-type="delete"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a></td>
      </tr>
      <?php } }else{ ?>
      <tr>
        <td colspan="6">Không có dữ liệu</td>
        <?php } ?>
    </table>
    <div class="modal-footer"> </div>
  </div>
  <div class="row">
    <div class="col-xs-6">
      <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">Tổng số dòng là: <?php echo $dinhmuc; ?> dòng</div>
    </div>
    <div class="col-xs-6">
      <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
        <ul class="pagination">
          <?php for($i=1;$i<=$tst;$i++)
					if($i==$page)
					{ ?>
          <li class="active" aria-controls="dynamic-table" tabindex="0"><a href="#"><?php echo $i;?></a></li>
          <?php }
					else
                    {?>
          <li class=''> <a href='<?php echo "http://localhost/quanlykhoahoc_bu/admin/index.php?key=Upload-SoTietChiTiet-NCKH&page=".$i;?>'><?php echo $i;?></a> </li>
          <?php }?>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModal" role="dialog" aria-hidden="true">
  <div class="modal-dialog "> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" aria-hidden="true" onclick="loadpage()" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Thông tin Số tiết định mức</h4>
      </div>
      <form role="form" id="universalModalForm" method="post" action="luusotietkknckh.php" class="form" >
        <table class="table">
          <tr>
            <td colspan="1" style="text-align:left; border-right: 1px solid #e5e5e5;background-color: #EFF3F8;" width="10%"> Họ Tên:</td>
            <td style="text-align:left; " width="16%" ><div class="hoten"></div></td>
          </tr>
          <tr >
            <td colspan="1" style="text-align:left; border-right: 1px solid #e5e5e5;background-color: #EFF3F8;" width="10%"> Đơn vị công tác:</td>
            <td colspan="5" style="text-align:left" ><div class="donvi"></div></td>
          </tr>
          <tr >
            <td colspan="1" style="text-align:left; border-right: 1px solid #e5e5e5;background-color: #EFF3F8;" width="10%"> Năm:</td>
            <td colspan="5" style="text-align:left" ><div class="nam"></div></td>
          </tr>
          <tr >
            <td colspan="1" style="text-align:left; border-right: 1px solid #e5e5e5;background-color: #EFF3F8;" width="10%"> Chọn đợt kê khai:</td>
            <td colspan="5" style="text-align:left" >            
			<div class="dotkekhainckh"></div>
            </td>
          </tr>
          <tr >
            <td colspan="1" style="text-align:left; border-right: 1px solid #e5e5e5;background-color: #EFF3F8;" width="10%"> Số tiết định mức:</td>
            <td colspan="5" style="text-align:left" ><input role="checkbox" type="text" id="dinhmuc" class="cbox dinhmuc" name="dinhmuc"></td>
          </tr>
          <tr >
            <td colspan="1" style="text-align:left; border-right: 1px solid #e5e5e5;background-color: #EFF3F8;" width="10%"> Số tiết định mức:</td>
            <td colspan="5" style="text-align:left" ><input role="checkbox" type="text" id="sotietthuchien" class="cbox sotietthuchien" name="sotietthuchien"></td>
          </tr>
          <tr >
            <td colspan="1" style="text-align:left; border-right: 1px solid #e5e5e5;background-color: #EFF3F8;" width="10%"> Số tiết định mức:</td>
            <td colspan="5" style="text-align:left" ><input role="checkbox" type="text" id="sotietthuathieu" class="cbox sotietthuathieu" name="sotietthuathieu"></td>
          </tr>
          <tr >
            <td colspan="1" style="text-align:left; border-right: 1px solid #e5e5e5;background-color: #EFF3F8;" width="10%"> Ẩn hiện:</td>
            <td colspan="5" style="text-align:left" ><label>
                <input id="anhien" name="anhien" type="checkbox" class="ace business_anhien">
                <span class="lbl"> Ẩn Hiện</span> </label></td>
          </tr>
        </table>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
          <input  type="hidden" class="modal-id id"  name="id"/>
          <input type="hidden" name="action_type" class="action" value=""/>
          <input type="hidden" name="ngaysua"  value="<?php echo date("m/d/Y");?>"/>
          <input type="submit" class="btn btn-primary" name="submit" value="Lưu Số tiết định mức"/>
        </div>
      </form>
    </div>
  </div>
</div>
