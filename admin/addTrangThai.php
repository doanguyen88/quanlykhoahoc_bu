<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">

$(document).on( "click", '.edit_button',function(e) {

        var trangthai = $(this).data('direction');
		var AnHien = $(this).data('anhien'); 
		var ThuTu = $(this).data('thutu');
		var hinh = $(this).data('hinh');
		var lop = $(this).data('lop');
		var idloaitrangthai = $(this).data('idloaitrangthai');
        var idTrangThai = $(this).data('id');

      
        $(".business_trangthai").val(trangthai);
		$(".business_idloaitrangthai").val(idloaitrangthai);
        $(".business_AnHien").val(AnHien);
       	$(".business_ThuTu").val(ThuTu);
		  $(".business_lop").val(lop);
       	$(".business_hinh").val(hinh);
  		$(".business_id").val(idTrangThai);


    });
	
</script>
<script type="text/javascript">
 $(document).ready(function(){
	 $("#myModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title');
		var idData=button.data('id'); 
		var typeData=button.data('type'); 
        $(this).find('.modal-title').text('Form'+ titleData );
		 $(this).find('.modal-type').val(typeData);
    });
	 
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.closest('.ace-spinner')
				.on('changed.fu.spinbox', function(){
					//console.log($('#spinner1').val())
				}); 
$('input[name="anhien').on('switchChange.bootstrapSwitch', function(event, state) {
  console.log(this); // DOM element
  console.log(event); // jQuery event
  console.log(state); // true | false
});

});

</script>
<script type="text/javascript">
function showDiv(select){
   if(select.value==1){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
} 
</script>
<div class="bs-example">
  <div class="container">
    <h4> 
      <!-- Button HTML (to Trigger Modal) -->
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-title="Thêm Trạng thái" data-id="addtrangthai" data-type="addtrangthai">Thêm Trạng thái</button>
    </h4>
    
    <!-- Modal HTML -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog"> 
        
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Quản lý Trạng thái</h4>
          </div>
          <form role="form" id="universalModalForm" method="post" action="actionTrangThai.php" class="form" >
            <div class="modal-body">
              <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Nhập tên Trạng thái</label>
                  <input type="text" class="form-control business_trangthai" name="trangthai"/>
                </div>
                <div class="row">
                  
                    
                      <div class="form-group">
                      <div class="col-sm-12">
                      <div class="col-sm-4"><label>Chọn phân loại Trạng thái</label> </div>
                      <div class="col-sm-8"><select  name="idloaitrangthai" id="idloaitrangthai" data-placeholder="Chọn phân loại trạng thái"  class="business_idloaitrangthai" >
                          
                          <option value="SK"> Trạng thái của Sáng kiến kinh nghiệm</option>
                          <option value="NCKH"> Trạng thái của Nghiên cứu khoa học</option>
                          
                        </select></div>
                        
                        
                      </div>
                    
                    
                  </div>
                </div>
                
                  <div class="col-sm-12">
                    <div class="col-sm-6">
                      <label>Thứ tự</label>
                      <input type="text" id="spinner1" name="thutu"  class="input-small business_ThuTu"/>
                    </div>
                    <div class="col-sm-6">
                      <label> Ẩn Hiện</label>
                      <input name="anhien" id="anhien" class="ace ace-switch ace-switch-4 business_AnHien" type="checkbox">
                      <span class="lbl"></span>
                    </div>
                  </div>
                <div class="form-group">
                        <label>icon</label>
                        <input type="text" class="form-control business_hinh" name="icon"/>
                      </div>
                      <div class="form-group">
                        <label>class</label>
                        <input type="text" class="form-control business_lop" name="classtt"/>
                      </div>
              </div>
          </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
              <input  type="hidden" class="modal-id business_id"  name="id"/>
              <input type="hidden" name="action_type" class="modal-type" value=""/>
              <input type="submit" class="btn btn-primary" name="submit" value="Thêm Trạng thái"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12">
  <div class="panel panel-default users-content">
    <div class="panel-heading">Danh sách Trạng thái </div>
    <table class="table">
      <tr>
        <th width="20%">Tên Trạng thái</th> 
        <th width="15%">Phân loại Trạng thái</th>
        <th width="10%">Thứ Tự</th>
        <th width="10%">Ẩn Hiện</th>
        <th width="20%">Icon</th>
        <th width="20%">Kiểu định dạng</th>
      </tr>
      <?php
          
            $trangthais = $db->getRows('nncms_trangthai',array('order_by'=>'idTrangThai DESC'));
            if(!empty($trangthais)){ $count = 0; foreach($trangthais as $trangthai){ $count++;?>
      <tr>
        <td><?php echo $trangthai['TenTrangThai'];?></td>
        <td><?php  if($trangthai['idLoaiTrangThai']=="SK") {echo "Thuộc sáng kiến kinh nghiệm";} else if($trangthai['idLoaiTrangThai']=="NCKH"){echo "Thuộc Nghiên cứu khoa học";}?></td>
        <td><?php echo $trangthai['ThuTu'];?></td>
        <td><?php echo $trangthai['AnHien'];?></td>
        <td><?php echo $trangthai['icon'];?></td>
        <td><span class="<?php echo $trangthai['class'];?>"><?php echo $trangthai['class'];?></span></td>
        <td><a 
                    
                    data-toggle="modal" data-target="#myModal"
                    data-direction="<?php echo $trangthai['TenTrangThai'];?>"
                    data-anhien="<?php echo $trangthai['AnHien'];?>"
                  	data-thutu="<?php echo $trangthai['ThuTu'];?>"
                   	data-hinh="<?php echo $trangthai['icon'];?>"
                  	data-lop="<?php echo $trangthai['class'];?>"
                    data-idloaitrangthai="<?php echo $trangthai['idLoaiTrangThai'];?>"
                    data-title="Sửa trang thái" 
                    data-id="<?php echo $trangthai['idTrangThai'];?>"  
                    data-type="edittrangthai" 
                    class="glyphicon glyphicon-edit edit_button"></a> <a href="actionTrangThai.php?action_type=deletetrangthai&id=<?php echo $trangthai['idTrangThai'];?>" data-type="delete"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a></td>
      </tr>
      <?php } }else{ ?>
      <tr>
        <td colspan="4">No user(s) found......</td>
        <?php } ?>
        </tr>
    </table>
  </div>
</div>
