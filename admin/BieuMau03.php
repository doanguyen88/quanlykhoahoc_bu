<!DOCTYPE html>
<html >
<head>
<meta charset="UTF-8">
<title>Biểu Mẫu-03. PHIẾU BIÊN SOẠN TÀI LIỆU HỌC TẬP</title>
<script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts3.js" type="text/javascript"></script>
<link rel="stylesheet" href="../giaodien/css/style-dk.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-tagsinput.css">
</head>

<body>
<?php include 'menu_ngang_admin.php';?>
<form class="well form-horizontal" action="actionCS-02.php" method="post"  id="DKDT" enctype="multipart/form-data">
  <fieldset>
    
    <!-- Form Name -->
    
    <div class="row">
      <div class="col-md-6 col-md-offset-6 benner-tenmau">Biểu Mẫu-03. PHIẾU BIÊN SOẠN TÀI LIỆU HỌC TẬP</div>
      <div class="col-md-6 banner-tentruong">
        <div class="col-md-12 banner-tentruong">NGÂN HÀNG NHÀ NƯỚC VIỆT NAM</div>
        <div class="col-md-12 banner-tentruong"><strong>TRƯỜNG ĐẠI HỌC NGÂN HÀNG TP.HCM</strong></div>
        
      </div>
      <div class="col-md-6 banner-tentruong">
        <div class="col-md-12">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</div>
        <div class="col-md-12"> <strong><ins>Độc lập - Tự do - Hạnh phúc</ins></strong></div>
      </div>
      <div class="col-md-6 col-md-offset-6 benner-tenmau">TP.HCM, ngày <?php echo date("d")?> tháng <?php echo date("m")?> năm <?php echo date("Y")?></div>
      <div class="col-md-12 tenphieu">
        <h3>BIÊN SOẠN TÀI LIỆU HỌC TẬP</h3>
        <div class="form-group">
          <label class="col-md-1 col-md-offset-4 control-label tenfrom"  >Năm học:</label>
          <div class="col-md-3 inputGroupContainer">
            <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input name="namhoc" placeholder="Vui lòng nhập năm học" class="textbox"  type="text">
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >1.	Tên tài liệu:</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input name="Ten_De_Tai" placeholder="Vui lòng nhập Tên tài liệu vào đây" class="textbox"  type="text">
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >2.	Loại sản phẩm đăng ký:</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="control-group" style="text-align:left !important">
        
        <div class="radio">
            <label> <span class="lbl">Giáo trình</span> </label>
          </div>
        	<div class="radio" style="padding-left:50px;">
            <label>
              <input name="loaisanphamdk" type="radio" value="Tài liệu tham khảo" class="ace">
              <span class="lbl"> 1. Tài liệu tham khảo </span> </label>
          </div>
           <div class="radio" style="padding-left:50px;">
            <label>
              <input name="loaisanphamdk" type="radio" value="Tài liệu hướng dẫn học tập  " class="ace">
              <span class="lbl"> 2. Tài liệu hướng dẫn học tập </span> </label>
          </div>
          <div class="radio" style="padding-left:50px;">
            <label>
              <input name="loaisanphamdk" type="radio" value="Giáo trình " class="ace">
              <span class="lbl"> 3. Giáo trình </span> </label>
          </div>
          
          <div class="radio" style="padding-left:50px;">
            <label>
              <input name="loaisanphamdk" type="radio" value="Sách chuyên khảo" class="ace">
              <span class="lbl"> 4. Sách chuyên khảo</span> </label>
          </div>
          <div class="radio" style="padding-left:50px;">
            <label>
              <input name="loaisanphamdk" type="radio" value="Sách dịch thuật" class="ace">
              <span class="lbl"> 5. Sách dịch thuật</span> </label>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >3. Hình thức:</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="control-group" style="text-align:left !important">
          <div class="radio">
            <label>
              <input name="hinhthucdk" type="radio" value="Biên soạn mới" class="ace">
              <span class="lbl">Biên soạn mới</span> </label>
          </div>
          <div class="radio">
            <label>
              <input name="hinhthucdk" type="radio" value="Sửa chữa, bổ sung" class="ace">
              <span class="lbl">Sửa chữa, bổ sung</span> </label>
          </div>
          
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >4. Thời gian thực hiện </label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-daterange" id="datepicker" >
          <div class="input-group ">
            <label class="col-md-2 control-label tenfrom" > Từ ngày </label>
            <div class="col-md-3">
              <input type="text" class="textbox" name="start" />
            </div>
            <label class="col-md-2 control-label tenfrom" > tới ngày </label>
            <div class="col-md-3">
              <input type="text" class="textbox" name="end" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >5. Chủ nhiệm:</label>
      <div class="col-md-12">
        <div class="col-md-12"> 
          <!-- Text input-->
          
          <div class="form-group">
            <label class="col-md-2  col-md-offset-1 control-label tenfrom">Họ và tên:</label>
            <div class="col-md-6 inputGroupContainer">
              <div class="input-group"> <span class="input-group-addon  textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                <input  name="Ho_ten" placeholder="Nhập tên chủ nhiệm tài liệu vào đây" class="textbox"  type="text">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-2  col-md-offset-1 control-label tenfrom">Năm sinh:</label>
            <div class="col-md-6 inputGroupContainer">
              <div class="input-group"> <span class="input-group-addon  textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                <input  name="nam_sinh" placeholder="Nhập năm sinh Chủ nhiệm tài liệu vào đây" class="textbox"  type="text">
              </div>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-md-2  col-md-offset-1 control-label tenfrom" >Học hàm, học vị:</label>
            <div class="col-md-8 inputGroupContainer">
              <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                <input name="Hoc_Ham_Hoc_vi" placeholder="Nhập Học hàm, học vị vào đây" class="textbox"  type="text">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-2  col-md-offset-1 control-label tenfrom" >Điện thoại di động:</label>
            <div class="col-md-8 inputGroupContainer">
              <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                <input name="Dien_Thoai_Di_Dong" placeholder="Nhập điện thoại di động vào đây" class="textbox"  type="text" >
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-2  col-md-offset-1 control-label tenfrom" >Email:</label>
            <div class="col-md-8 inputGroupContainer">
              <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                <input name="email" placeholder="<?php echo $_SESSION['sess_email'];?>" class="textbox"  type="text" value="<?php echo $_SESSION['sess_email'];?>">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label tenfrom" >6. Thành viên tham gia : </label>

        <div class="col-md-8 inputGroupContainer">
          <table width="100%" border="1" class="myGridClass" cellspacing="0" cellpadding="0">
            <tr>
              <td width="5%" height="33" align="center"><strong>STT</strong></td>
              <td width="16%" align="center"><strong>Họ và tên(Bao gồm học hàm và học vị)</strong></td>
              <td width="19%" align="center"><strong>Đơn vị công tác và lĩnh vực chuyên môn</strong></td>
              <td width="19%" align="center"><strong>Nội dung biên soạn được giao</strong></td>
              <td width="13%" align="center"><strong>chữ ký</strong></td>
            </tr>
            <tr>
              <td height="33" align="center">1</td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập họ và tên vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập đơn vị công tác và lĩnh vực chuyên môn vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập nội dung biên soạn được giao vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập chữ ký vào đây"></textarea>
                </div>
              </td>
            </tr>

            <tr>
              <td height="33" align="center">2</td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập họ và tên vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập đơn vị công tác và lĩnh vực chuyên môn vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập nội dung biên soạn được giao vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập chữ ký vào đây"></textarea>
                </div>
              </td>
            </tr>

            <tr>
              <td height="33" align="center">3</td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập họ và tên vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập đơn vị công tác và lĩnh vực chuyên môn vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập nội dung biên soạn được giao vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập chữ ký vào đây"></textarea>
                </div>
              </td>
            </tr>

            <tr>
              <td height="33" align="center">4</td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập họ và tên vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập đơn vị công tác và lĩnh vực chuyên môn vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập nội dung biên soạn được giao vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập chữ ký vào đây"></textarea>
                </div>
              </td>
            </tr>

            <tr>
              <td height="33" align="center">5</td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập họ và tên vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập đơn vị công tác và lĩnh vực chuyên môn vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập nội dung biên soạn được giao vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập chữ ký vào đây"></textarea>
                </div>
              </td>
            </tr>
          </table>
        </div>

      </div>
    
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >7. Dùng cho môn học</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
          <textarea class="textarea-from" name="Dungchomonhoc" placeholder="Nhập Tính cấp thiết của đề tài vào đây"></textarea>
        </div>
      </div>
    </div>
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-2 col-md-offset-1 control-label tenfrom" >Chuyên ngành</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <textarea class="textarea-from" name="chuyennganh" placeholder="Nhập Mục tiêu và nội dung nghiên cứu của đề tài vào đây"></textarea>
        </div>
      </div>
    </div>
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >8. Sản phẩm phục vụ đào tạo cho bậc:</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="control-group" style="text-align:left !important">
          <div class="radio">
            <label>
              <input name="Sanphamphucvudaotaochobac" type="radio" value="NCS" class="ace">
              <span class="lbl">NCS: </span> </label>
          </div>
          <div class="radio">
            <label>
              <input name="Sanphamphucvudaotaochobac" type="radio" value="Cao học" class="ace">
              <span class="lbl">Cao học: </span> </label>
          </div>
          <div class="radio">
            <label>
              <input name="Sanphamphucvudaotaochobac" type="radio" value="Đại học" class="ace">
              <span class="lbl">Đại học: </span> </label>
          </div>
          <div class="radio">
            <label>
              <input name="Sanphamphucvudaotaochobac" type="radio" value="Cao đẳng" class="ace">
              <span class="lbl">Cao đẳng: </span> </label>
          </div>
          
          
        </div>
      </div>
    </div>
    
    <!-- Text input-->
    <div class="form-group">
    <label class="col-md-12 control-label tenfrom" >9. Đề cương chi tiết và danh mục TLTK kèm theo (chỉ yêu cầu với Giáo trình và Sách chuyên khảo)</label>
    <label class="col-md-3 control-label tenfrom" >Đính kèm file thuyết minh <a href="javascript:_add_more();" title="Add more"><i class="glyphicon glyphicon-plus"></i></a></label>
    <div class="col-md-8 inputGroupContainer">
      <div class="col-md-12 ">
        <div class="fileupload fileupload-new" data-provides="fileupload">
          <div id="dvFile">
            <input type="file" name="item_file[]">
          </div>
        </div>
      </div>
    </div>
    
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-3 col-md-offset-8 chuky" >Chủ bên</label>
      <div class="col-md-3 col-md-offset-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input  name="Chu_ky" placeholder="Nhập chữ ký vào đây" class="textbox"  type="text">
        </div>
      </div>
    </div>
    
    <!-- Success message -->
    <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>
    
    <!-- Button -->
    <div class="form-group">
      <label class="col-md-4 control-label"></label>
      <div class="col-md-4">
        <input  type="hidden"  name="trangthai" value="Đăng ký mới"/>
        <input type="hidden" name="ngay"  value="<?php echo date("m/d/Y");?>"/>
        <input  type="hidden"  name="anhien" value="on"/>
        <input  type="hidden"  name="action_type" value="add"/>
        <button type="submit" class="btn btn-warning" >Đăng ký <span class="glyphicon glyphicon-send"></span></button>
      </div>
    </div>
  </fieldset>
</form>
</div>
<!-- /.container -->
</body>
</html>
<script language="javascript">
<!--
	function _add_more() {
		var txt = "<br><input type=\"file\" name=\"item_file[]\">";
		document.getElementById("dvFile").innerHTML += txt;
	}
	
//-->
</script>