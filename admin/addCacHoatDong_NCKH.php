<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">

$(document).on( "click", '.edit_button',function(e) {

        var TenTieuChi = $(this).data('direction');
		var AnHien = $(this).data('anhien'); 
		var ThuTu = $(this).data('thutu');
        var idTenTieuChi = $(this).data('id');
  		var idcdtc = $(this).data('idcdtc');
      
        $(".business_TenTieuChi").val(TenTieuChi);
        $(".business_AnHien").val(AnHien);
       	$(".business_ThuTu").val(ThuTu);
		  $(".business_idcdtc").val(idcdtc);
  		$(".business_id").val(idTenTieuChi);


    });
	
</script>
<script type="text/javascript">
 $(document).ready(function(){
	 $("#myModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title');
		var idData=button.data('id'); 
		var typeData=button.data('type'); 
        $(this).find('.modal-title').text('Form'+ titleData );
		 $(this).find('.modal-type').val(typeData);
    });
	 
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.closest('.ace-spinner')
				.on('changed.fu.spinbox', function(){
					//console.log($('#spinner1').val())
				}); 
$('input[name="anhien').on('switchChange.bootstrapSwitch', function(event, state) {
  console.log(this); // DOM element
  console.log(event); // jQuery event
  console.log(state); // true | false
});

});

</script>
<script type="text/javascript">
function showDiv(select){
   if(select.value==1){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
} 
</script>
<div class="bs-example">
  <div class="container">
    <h4> 
      <!-- Button HTML (to Trigger Modal) -->
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-title="Thêm Tiêu chí" data-id="addtieuchi" data-type="addtieuchi">Thêm Tiêu chí</button>
    </h4>
    
    <!-- Modal HTML -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog"> 
        
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Quản lý Tiêu chí</h4>
          </div>
          <form role="form" id="universalModalForm" method="post" action="actionTieuChi.php" class="form" >
            <div class="modal-body">
              <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Nhập tên Tiêu chí</label>
                  <input type="text" class="form-control business_TenTieuChi" name="danhmuctieuchi"/>
                </div>
              <div class="form-group">
              
                      <label>Chọn Danh mục tiêu chí</label>
                      <select  name="choncdtc" id="choncdtc" data-placeholder="Chọn năm ĐK"  class="business_idcdtc" >
                      <option value="" >--Chọn Danh mục tiêu chí--</option>
                        <?php  $chudetieuchi = $db->getRows('nncms_chudetieuchi',array('where'=>array('anHien'=>'on')),array('order_by'=>'idChuDeTieuChi ASC'));
            if(!empty($chudetieuchi)){ $count = 0; foreach($chudetieuchi as $chudett){ $count++;?>
                        <option value="<?php echo $chudett['idChuDeTieuChi'];?>"> <?php echo $chudett['TenChuDeTieuChi'];?></option>
                        <?php } }?>
                      </select>
                    
               </div>
                    <div class="form-group">
                    <div class="row">
                    <div class="col-xs-12 col-sm-12">
                    <div class="col-sm-6">
                      <label>Thứ tự</label>
                      <input type="text" id="spinner1" name="thutu"  class="input-small business_ThuTu"/>
                    </div>
                  <div class="col-sm-6">
                      <label> Ẩn Hiện</label>
                      <input name="anhien" id="anhien" class="ace ace-switch ace-switch-4 business_AnHien" type="checkbox">
                      <span class="lbl"></span>
                    </div>
                    </div>
                    </div>
                    </div>
                 
               
                   
                  </div>
                  </div>
                  
               
          
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
              <input  type="hidden" class="modal-id business_id"  name="id"/>
              <input type="hidden" name="action_type" class="modal-type" value=""/>
              <input type="submit" class="btn btn-primary" name="submit" value="Thêm Danh mục tiêu chí"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12">
  <div class="panel panel-default users-content">
    <div class="panel-heading">Quản lý tiêu chí </div>
    <table class="table">
      <tr>
        <th width="30%">Tiêu chí</th>
        <th width="30%">Danh mục Tiêu chí</th>
        <th width="10%">Thứ Tự</th>
        <th width="10%">Ẩn Hiện</th>
      
      </tr>
      <?php
          
            $danhmuctieuchi = $db->getRows('nncms_tieuchi',array('order_by'=>'IdTieuChi DESC'));
            if(!empty($danhmuctieuchi)){ $count = 0; foreach($danhmuctieuchi as $danhmuctc){ $count++;?>
      <tr>
        <td><?php echo $danhmuctc['TenTieuChi'];?></td>
           <td><?php
		   $chudetieuchi = $db->getRows('nncms_chudetieuchi',array('where'=>array('anHien'=>'on','idChuDeTieuChi'=>$danhmuctc['idChuDeTieuChi'])),array('order_by'=>'idChuDeTieuChi ASC'));
            if(!empty($chudetieuchi)){ $count = 0; foreach($chudetieuchi as $chudett){ $count++;
		    echo $chudett['TenChuDeTieuChi'];
			}}
			?></td>
        <td><?php echo $danhmuctc['ThuTu'];?></td>
        <td><?php echo $danhmuctc['AnHien'];?></td>
        <td><a 
                    
                    data-toggle="modal" data-target="#myModal"
                    data-direction="<?php echo $danhmuctc['TenTieuChi'];?>"
                    data-anhien="<?php echo $danhmuctc['AnHien'];?>"
                  	data-thutu="<?php echo $danhmuctc['ThuTu'];?>"
                    data-title="Sửa danh mục tiêu chí" 
                    data-id="<?php echo $danhmuctc['IdTieuChi'];?>"  
                    data-idcdtc="<?php echo $danhmuctc['idChuDeTieuChi'];?>"
                    data-type="edittieuchi" 
                    class="glyphicon glyphicon-edit edit_button"></a> <a href="actionTieuChi.php?action_type=deletetieuchi&id=<?php echo $danhmuctc['IdTieuChi'];?>" data-type="delete"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a></td>
      </tr>
      <?php } }else{ ?>
      <tr>
        <td colspan="4">No user(s) found......</td>
        <?php } ?>
        </tr>
    </table>
  </div>
</div>

