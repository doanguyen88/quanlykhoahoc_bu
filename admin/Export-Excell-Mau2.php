<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.js"></script>
<script src="../assets/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="../giaodien/js/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
    </script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<input class="btn btn-xs  btn-success" type="button" onclick="tableToExcel('testTable', 'W3C Example Table')" value="Export to Excel">
<div>
  <table id="testTable" summary="Code page support in different versions of MS Windows."
        rules="groups" frame="hsides" border="2" class="table table-striped table-bordered table-hover dataTable no-footer" >
    <thead valign="top">
      <tr>
        <th colspan="3" style="text-align:center">NGÂN HÀNG NHÀ NƯỚC VIỆT NAM</th>
        <th colspan="2" style="text-align:center">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="3" style="text-decoration: underline; text-align:center">TRƯỜNG ĐẠI HỌC NGÂN HÀNG TP.HCM</td>
        <td colspan="2" style="text-decoration: underline; text-align:center">Độc lập - Tự do - Hạnh phúc</td>
      </tr>
    </tbody>
    <tr>
      <td colspan="5" align="center" style="font-size:16px;font-weight:bold; padding:20px 0px 20px 0px;">DANH SÁCH TỔNG HỢP ĐĂNG KÝ NHIỆM VỤ KH-CN NĂM</td>
    </tr>
    
    <!--     <caption>
        
            DANH SÁCH TỔNG HỢP ĐĂNG KÝ NHIỆM VỤ KH-CN NĂM </caption>-->
    <colgroup align="center">
    </colgroup>
    <colgroup align="left">
    </colgroup>
    <colgroup span="2" align="center">
    </colgroup>
    <colgroup span="3" align="center">
    </colgroup>
    <thead valign="top">
      <tr>
        <th  style="width:30px" align="center">TT</th>
        <th>Tên đề tài, Tài liệu học tập</th>
        <th>Chủ nhiệm đề tài, chủ biên</th>
        <th>Thời gian thực hiện</th>
        <th>Cấp độ</th>
      </tr>
    </thead>
    <tbody style="border:#CCC 1 solid;">
      <tr>
        <td align="center" style="margin-left:20px; background-color: #307ECC !important; " class="table-header">I</td>
        <td style="margin-left:20px; background-color: #307ECC !important;" class="table-header" colspan="4">ĐỀ TÀI NGÀNH</td>
      </tr>
      <?php
$i=0;
$laydetainganh = $db->getRows('nncms_DKDTCapCoSo',array('where'=>array('AnHien'=>'on','idPhanLoai_DT_GT'=>'DTN')),array('order_by'=>'idDKDTCapCoSo ASC'));
if(!empty($laydetainganh)){ $count = 0; foreach($laydetainganh as $laydetainganhe){ $count++;?>
      <tr>
        <td align="center" style="width:30px"><?php echo $i+1?></td>
        <td ><?php echo $laydetainganhe['TenDeTaiCapCoSo'];?></td>
        <td><?php echo $laydetainganhe['HoTenChuNhiem'];?></td>
        <td><?php echo "Từ ngày: ".$laydetainganhe['starttimes']."Tới ngày: ".$laydetainganhe['endtimes'];?></td>
        <td>Đề tài cấp Ngành</td>
      </tr>
      <?php } }?>
      <tr>
        <td align="center" style="margin-left:20px; background-color: #307ECC !important;" class="table-header" >II</td>
        <td style="margin-left:20px;background-color: #307ECC !important;"class="table-header"  colspan="4">ĐỀ TÀI CẤP CƠ SỞ</td>
      </tr>
      <?php
$i=0;
$laydetainganh = $db->getRows('nncms_DKDTCapCoSo',array('where'=>array('AnHien'=>'on','idPhanLoai_DT_GT'=>'DT')),array('order_by'=>'idDKDTCapCoSo ASC'));
if(!empty($laydetainganh)){ $count = 0; foreach($laydetainganh as $laydetainganhe){ $count++;?>
      <tr>
        <td style="width:30px"><?php echo $i+1?></td>
        <td ><?php echo $laydetainganhe['TenDeTaiCapCoSo'];?></td>
        <td><?php echo $laydetainganhe['HoTenChuNhiem'];?></td>
        <td><?php echo "Từ ngày: ".$laydetainganhe['starttimes']."Tới ngày: ".$laydetainganhe['endtimes'];?></td>
        <td>Đề tài cơ sở</td>
      </tr>
      <?php } }?>
       <tr>
        <td align="center" style="margin-left:20px;background-color: #307ECC !important;" class="table-header">III</td>
        <td style="margin-left:20px;background-color: #307ECC !important;" class="table-header" colspan="4">TÀI LIỆU HỌC TẬP</td>
      </tr>
      <tr>
        <td  align="center" style="margin-left:20px; background:#6fb3e0  !important;" class="table-header"><em>1</em></td>
        <td style="margin-left:20px;background:#6fb3e0  !important;" class="table-header" colspan="4"><em>TÀI LIỆU THAM KHẢO</em></td>
      </tr>
 <?php
$i=0;
$laydetainganh = $db->getRows('nncms_DKDTCapCoSo',array('where'=>array('AnHien'=>'on','loaisanphamdk'=>'Tài liệu tham khảo')),array('order_by'=>'idDKDTCapCoSo ASC'));
if(!empty($laydetainganh)){ $count = 0; foreach($laydetainganh as $laydetainganhe){ $count++;?>
      <tr>
        <td align="center" style="width:30px"><?php echo $i+1?></td>
        <td ><?php echo $laydetainganhe['TenDeTaiCapCoSo'];?></td>
        <td><?php echo $laydetainganhe['HoTenChuNhiem'];?></td>
        <td><?php echo "Từ ngày: ".$laydetainganhe['starttimes']."Tới ngày: ".$laydetainganhe['endtimes'];?></td>
        <td>TLTK</td>
      </tr>
      <?php } }?>  
      <tr>
        <td align="center" style="margin-left:20px;background:#6fb3e0  !important;" class="table-header"><em>2</em></td>
        <td style="margin-left:20px;background:#6fb3e0  !important;" class="table-header" colspan="4"><em>TÀI LIỆU HƯỚNG DẪN HỌC TẬP</em></td>
      </tr>
 <?php
$i=0;
$laydetainganh = $db->getRows('nncms_DKDTCapCoSo',array('where'=>array('AnHien'=>'on','loaisanphamdk'=>'Tài liệu hướng dẫn học tập')),array('order_by'=>'idDKDTCapCoSo ASC'));
if(!empty($laydetainganh)){ $count = 0; foreach($laydetainganh as $laydetainganhe){ $count++;?>
      <tr>
        <td align="center" style="width:30px"><?php echo $i+1?></td>
        <td ><?php echo $laydetainganhe['TenDeTaiCapCoSo'];?></td>
        <td><?php echo $laydetainganhe['HoTenChuNhiem'];?></td>
        <td><?php echo "Từ ngày: ".$laydetainganhe['starttimes']."Tới ngày: ".$laydetainganhe['endtimes'];?></td>
        <td>Tài liệu hướng dẫn học tập</td>
      </tr>
      <?php } }?>    
      <tr>
        <td align="center" style="margin-left:20px;background:#6fb3e0  !important;" class="table-header"><em>3</em></td>
        <td style="margin-left:20px;background:#6fb3e0  !important;" class="table-header" colspan="4"><em>GIÁO TRÌNH</em></td>
      </tr>
 <?php
$i=0;
$laydetainganh = $db->getRows('nncms_DKDTCapCoSo',array('where'=>array('AnHien'=>'on','loaisanphamdk'=>'Giáo trình')),array('order_by'=>'idDKDTCapCoSo ASC'));
if(!empty($laydetainganh)){ $count = 0; foreach($laydetainganh as $laydetainganhe){ $count++;?>
      <tr>
        <td align="center"  style="width:30px"><?php echo $i+1?></td>
        <td ><?php echo $laydetainganhe['TenDeTaiCapCoSo'];?></td>
        <td><?php echo $laydetainganhe['HoTenChuNhiem'];?></td>
        <td><?php echo "Từ ngày: ".$laydetainganhe['starttimes']."Tới ngày: ".$laydetainganhe['endtimes'];?></td>
        <td>Giáo trình</td>
      </tr>
      <?php } }?>  
      <tr>
        <td align="center" style="margin-left:20px;background:#6fb3e0  !important;" class="table-header"><em>4</em></td>
        <td style="margin-left:20px;background:#6fb3e0  !important;" class="table-header" colspan="4"><em>SÁCH CHUYÊN KHẢO</em></td>
      </tr>
 <?php
$i=0;
$laydetainganh = $db->getRows('nncms_DKDTCapCoSo',array('where'=>array('AnHien'=>'on','loaisanphamdk'=>'Sách chuyên khảo')),array('order_by'=>'idDKDTCapCoSo ASC'));
if(!empty($laydetainganh)){ $count = 0; foreach($laydetainganh as $laydetainganhe){ $count++;?>
      <tr>
        <td align="center" style="width:30px"><?php echo $i+1?></td>
        <td ><?php echo $laydetainganhe['TenDeTaiCapCoSo'];?></td>
        <td><?php echo $laydetainganhe['HoTenChuNhiem'];?></td>
        <td><?php echo "Từ ngày: ".$laydetainganhe['starttimes']."Tới ngày: ".$laydetainganhe['endtimes'];?></td>
        <td>Sách chuyên khảo</td>
      </tr>
      <?php } }?> 
      <tr>
        <td align="center" style="margin-left:20px;background:#6fb3e0 !important;" class="table-header"><em>5</em></td>
        <td style="margin-left:20px;background:#6fb3e0 !important;" class="table-header" colspan="4"><em>SÁCH DỊCH THUẬT</em></td>
      </tr>
 <?php
$i=0;
$laydetainganh = $db->getRows('nncms_DKDTCapCoSo',array('where'=>array('AnHien'=>'on','loaisanphamdk'=>'Sách dịch thuật')),array('order_by'=>'idDKDTCapCoSo ASC'));
if(!empty($laydetainganh)){ $count = 0; foreach($laydetainganh as $laydetainganhe){ $count++;?>
      <tr>
        <td align="center" style="width:30px"><?php echo $i+1?></td>
        <td ><?php echo $laydetainganhe['TenDeTaiCapCoSo'];?></td>
        <td><?php echo $laydetainganhe['HoTenChuNhiem'];?></td>
        <td><?php echo "Từ ngày: ".$laydetainganhe['starttimes']."Tới ngày: ".$laydetainganhe['endtimes'];?></td>
        <td>Sách dịch thuật</td>
      </tr>
      <?php } }?>  
      <tr>
        <td align="center" style="margin-left:20px;background-color: #307ECC !important;" class="table-header">IV</td>
        <td style="margin-left:20px;background-color: #307ECC !important;" class="table-header" colspan="4">ĐỀ TÀI SINH VIÊN</td>
      </tr>
      <tr>
        <td align="center" style="margin-left:20px;background-color: #307ECC !important;" class="table-header">V</td>
        <td style="margin-left:20px;background-color: #307ECC !important;" class="table-header" colspan="4">HỘI THẢO</td>
      </tr>   
    </tbody>
  </table>
</div>
