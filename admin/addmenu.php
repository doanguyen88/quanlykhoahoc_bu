<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">
$(document).on( "click", '.edit_button',function(e) {

        var TieuDe = $(this).data('name');
		var LienKet = $(this).data('content'); 
		var ThuTu = $(this).data('quote');
		var vitri = $(this).data('collapsed');
		var class3 = $(this).data('direction');
        var idMenu = $(this).data('id');
		 var anhien = $(this).data('anhien');
		
		var icon= $(this).data('icon');
		

		
		
		
        $(".business_TieuDe").val(TieuDe);
        $(".business_LienKet").val(LienKet);
       	$(".business_ThuTu").val(ThuTu);
		$(".business_class").val(class3);
        $(".business_vitri").val(vitri);
		$(".business_icon").val(icon);
		
  		$(".business_id").val(idMenu);
if(anhien=='on'){document.getElementById("anhien").checked = true;}

    });
</script>
<script type="text/javascript">
 $(document).ready(function(){
	 $("#myModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title');
		var idData=button.data('id'); 
		var typeData=button.data('type'); 
        $(this).find('.modal-title').text('Form'+ titleData );
		 $(this).find('.modal-type').val(typeData);
    });
	 
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.closest('.ace-spinner')
				.on('changed.fu.spinbox', function(){
					//console.log($('#spinner1').val())
				}); 
	
});

</script>
<script type="text/javascript">
function showDiv(select){
   if(select.value==1){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
} 
</script>
<div class="bs-example">
<div class="container">
<h4>
    <!-- Button HTML (to Trigger Modal) -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-title="Thêm menu" data-id="addmain" data-type="addmain">Thêm main menu</button>
     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-title="Thêm menu" data-id="addsub" data-type="addsub">Thêm sub menu</button>

    </h4>
    
    <!-- Modal HTML -->
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog"> 
      
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
          <h4 class="modal-title">Quản lý menu</h4>
          
        </div>  
     
        <form role="form" id="universalModalForm" method="post" action="actionmenu.php" class="form" >
       
          
       
        <div class="modal-body">
            <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
            <div class="modal-body">
              <div class="form-group">
                <label>Tên Menu</label>
                <input type="text" class="form-control business_TieuDe" name="TieuDe"/>
              </div>
              <div class="form-group">
                <label>Liên Kết</label>
                <input type="text" class="form-control business_LienKet" name="LienKet"/>
              </div>
              <div class="row">
                <div class="col-sm-3" >
                  <label>Thứ tự</label>
                  <input type="text" id="spinner1" name="thutu"  class="input-small business_ThuTu"/>
                </div>
                <div class="col-sm-3">
                <div class="col-sm-12">
                  <label> Check</label></div>
                  <div class="col-sm-12">
                  <div class="checkbox">
                          <label>
                              <input id="anhien" name="anhien" type="checkbox" class="ace business_anhien">
                              <span class="lbl"> Ẩn Hiện</span>
                          </label>
												</div>
                  
                  
                   </div></div>
                  
                  
                <div class="col-sm-3">
                  <label> Cấp menu </label>
                  <select id="test" name="Capmenu" onchange="showDiv(this)">
                    <option value="0">Main</option>
                    <option value="1">sub</option>
                  </select>
                </div>
                <div class="col-sm-3">
                  <label>Chọn vị Trí</label>
                  <select  class="business_vitri" name="vitri" id="vitri" data-placeholder="Chọn vị trí.." >
                    <option value="donvi">Đơn vị</option>
                    <option value="vien">Viện</option>
                    <option value="admin">admin</option>
                    <option value="giangvien">Giảng viện</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label>icon</label>
                <input type="text" class="form-control business_icon " name="icon"/>
              </div>
              <div class="form-group">
                <label>Class</label>
                <input type="text" class="form-control business_class " name="class"/>
              </div>
              <div id="hidden_div" style="display:none;">
                <label>Chọn Menu cha</label>
                <select  name="menucha" id="menucha" data-placeholder="Chọn menu cha"  >
                  <?php $menus = $db->getRows('menus',array('order_by'=>'idMenu ASC'));
            if(!empty($menus)){ $count = 0; foreach($menus as $menu){ $count++;?>
                  <option value="<?php echo $menu['idMenu'];?>"> <?php echo $menu['TieuDe'];?></option>
                  <?php } }?>
                </select>
              </div>
            </div>
            
        
        </div>
        <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                <input  type="hidden" class="modal-id business_id"  name="id"/>
              <input type="hidden" name="action_type" class="modal-type" value=""/>
              <input type="submit" class="btn btn-primary" name="submit" value="Thêm menu"/>
            </div> 
      </form>
      </div>
    </div>
  </div>
</div>
</div>



<div class="col-xs-12 col-sm-12">

    <div class="panel panel-default users-content">
        <div class="panel-heading">Danh sách main menu </div>
        <table class="table">
            <tr>
                
                <th width="10%">id Menu</th>
                <th width="21%">Tiêu Đề</th>
                <th width="15%">Liên Kết</th>
                <th width="10%">Thứ Tự</th>
                <th width="16%">icon</th>
                <th width="16%">class</th>
                <th width="6%">Vị Trí</th>
                
            </tr>
            <?php
          
            $menus = $db->getRows('menus',array('order_by'=>'idMenu DESC'));
            if(!empty($menus)){ $count = 0; foreach($menus as $menu){ $count++;?>
            <tr>
                
                
                <td><?php echo $menu['idMenu'];?></td>
                <td><?php echo $menu['TieuDe'];?></td>
                <td><?php echo $menu['LienKet'];?></td>
                <td><?php echo $menu['ThuTu'];?></td>
                <td><?php echo $menu['icon'];?></td>
                <td><?php echo $menu['class'];?></td>
                <td><?php echo $menu['vitri'];?></td>
                <td>
                    <a 
                    
                    data-toggle="modal" data-target="#myModal"
                    data-name="<?php echo $menu['TieuDe'];?>"
                    data-content="<?php echo $menu['LienKet'];?>"
                  	data-quote="<?php echo $menu['ThuTu'];?>"
                    data-direction="<?php echo $menu['class'];?>"
                    data-collapsed="<?php echo $menu['vitri'];?>"
                    data-icon="<?php echo $menu['icon'];?>"
                    data-anhien="<?php echo $menu['AnHien'];?>"
                    data-title="Sửa menu" 
                    data-id="<?php echo $menu['idMenu'];?>"  
                    data-type="edit" 
                    class="glyphicon glyphicon-edit edit_button"></a>
                    <a href="actionmenu.php?action_type=deletemain&id=<?php echo $menu['idMenu'];?>" data-type="delete"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a>
                </td>
            </tr>
            <?php } }else{ ?>
            <tr><td colspan="4">No user(s) found......</td>
            <?php } ?>
        </table>
    </div>
</div>
<div class="col-xs-12 col-sm-12">

    <div class="panel panel-default users-content">
        <div class="panel-heading">Danh sách sub menu </div>
        <table class="table">
            <tr>
                <th width="10%">id Cha</th>
                <th width="21%">Tiêu Đề</th>
                <th width="15%">Liên Kết</th>
                <th width="10%">Thứ Tự</th>
                <th width="16%">icon</th>
                <th width="16%">class</th>
                <th width="6%">Vị Trí</th>
                
            </tr>
            <?php
          
            $submenus = $db->getRows('submenu',array('order_by'=>'idsubmenu DESC'));
            if(!empty($submenus)){ $count = 0; foreach($submenus as $submenu){ $count++;?>
            <tr>
                
                 <td><?php echo $submenu['idCapCha'];?></td>
                <td><?php echo $submenu['TenSubmenu'];?></td>
                <td><?php echo $submenu['LienKet'];?></td> 
                <td><?php echo $submenu['ThuTu'];?></td>
                <td><?php echo $submenu['icon'];?></td>
                <td><?php echo $submenu['Class'];?></td>
                <td><?php echo $submenu['ViTri'];?></td>
                <td>
                    <a 
                    
                    data-toggle="modal" data-target="#myModal"
                    data-name="<?php echo $submenu['TenSubmenu'];?>"
                    data-content="<?php echo $submenu['LienKet'];?>"
                  	data-quote="<?php echo $submenu['ThuTu'];?>"
                    data-direction="<?php echo $submenu['Class'];?>"
                    data-collapsed="<?php $submenu['ViTri'];?>"
                    data-icon="<?php echo $submenu['icon'];?>"
                    data-anhien="<?php echo $submenu['AnHien'];?>"
                    data-title="Sửa sub menu" 
                    data-id="<?php echo $submenu['idsubmenu'];?>"  
                    data-type="editsub" 
                    class="glyphicon glyphicon-edit edit_button"></a>
                    <a href="actionmenu.php?action_type=deletesub&id=<?php echo $submenu['idsubmenu'];?>" data-type="delete"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a>
                </td>
            </tr>
            <?php } }else{ ?>
            <tr><td colspan="4">No user(s) found......</td>
            <?php } ?>
        </table>
    </div>
</div>
   