<!DOCTYPE html>
<html >
<head>
<meta charset="UTF-8">
<title>Biểu Mẫu-01. PHIẾU ĐĂNG KÝ ĐỀ TÀI CẤP CƠ SỞ</title>

<script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts3.js" type="text/javascript"></script>
<link rel="stylesheet" href="../giaodien/css/style-dk.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-tagsinput.css">
</head>

<body>

  <?php 
  include 'menu_ngang_admin.php';
  ?>

  <form class="well form-horizontal" action="actionBM-01.php" method="post"  id="DKDT" enctype="multipart/form-data">
    <fieldset>
    
      <!-- Form Name -->
    
      <div class="row">
        <div class="col-md-6 col-md-offset-6 benner-tenmau">Biểu Mẫu-01. PHIẾU ĐĂNG KÝ ĐỀ TÀI CẤP CƠ SỞ</div>
        <div class="col-md-6 banner-tentruong">
          <div class="col-md-12 banner-tentruong">NGÂN HÀNG NHÀ NƯỚC VIỆT NAM</div>
          <div class="col-md-12 banner-tentruong"><strong>TRƯỜNG ĐẠI HỌC NGÂN HÀNG TP.HCM</strong></div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="col-md-2 col-md-offset-3 control-label tenfrom" >Đơn vị:</label>
      	      <div class="col-md-4 inputGroupContainer">
                <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  <input name="donvi" placeholder="Vui lòng nhập đơn vị" class="textbox"  type="text">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6 banner-tentruong">
          <div class="col-md-12">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</div>
          <div class="col-md-12"> <strong><ins>Độc lập - Tự do - Hạnh phúc</ins></strong></div>
        </div>

        <div class="col-md-6 col-md-offset-6 benner-tenmau">TP.HCM, ngày <?php echo date("d")?> tháng <?php echo date("m")?> năm <?php echo date("Y")?></div>
        
        <div class="col-md-12 tenphieu">
          <h3>PHIẾU ĐĂNG KÝ ĐỀ TÀI CẤP CƠ SỞ</h3>
          <div class="form-group">
            <label class="col-md-1 col-md-offset-4 control-label tenfrom"  >Năm học:</label>
      	    <div class="col-md-3 inputGroupContainer">
              <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                <input name="namhoc" placeholder="Vui lòng nhập năm học" class="textbox"  type="text">
              </div>
            </div>
          </div>
        </div>
      </div>
    
      <div class="form-group">
        <label class="col-md-3 control-label tenfrom" >Chọn danh mục đăng ký đề tài:</label>
        <div class="col-md-8 inputGroupContainer">
          <div class="col-md-12">
            <select  name="chonqldkdt" id="chonqldkdt" data-placeholder="Chọn quản lý đk đề tài"  class="textbox" >
              <?php $qldkdetai = $db->getRows('nncms_Quanlydangkydetai',array('where'=>array('anHien'=>'on')),array('order_by'=>'idQuanlydangky_Detai ASC'));
              if(!empty($qldkdetai)){ $count = 0; foreach($qldkdetai as $qldkdetais){ $count++;?>

              <?php
              $time1= $qldkdetais['starttimes'];
              $time2= $qldkdetais['endtimes'];
              $date = date('m/d/Y h:i:s a', time());
              $moc1 = (strtotime($date) - strtotime($time1)) / (60 * 60 * 24);
              $moc2 = (strtotime($date) - strtotime($time2)) / (60 * 60 * 24);
              
              if($moc1 >0 && $moc2 < 0 )
              { ?>
            
              <option value="<?php echo $qldkdetais['idQuanlydangky_Detai'];?>"> <?php echo $qldkdetais['TenQuanlydangky_Detai'];?></option>

              <? }else
              {?>

			        <option value="0"> Không có đợt đăng ký</option>
              <?php }
              ?>

              <?php } }?>
            </select>
          </div>
        </div>
      </div>
    
      <div class="form-group">
        <label class="col-md-3 control-label tenfrom" >1.	Tên đề tài:</label>
        <div class="col-md-8 inputGroupContainer">
          <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
            <input name="Ten_De_Tai" placeholder="Vui lòng nhập Tên đề tài vào đây" class="textbox"  type="text">
          </div>
        </div>
      </div>
    
      <div class="form-group"><label class="col-md-3 control-label tenfrom" >2. Thời gian thực hiện  </label>
        <div class="col-md-8 inputGroupContainer">
          <div class="input-daterange" id="datepicker" >
            <div class="input-group ">
              <label class="col-md-2 control-label tenfrom" > Từ ngày </label>
              <div class="col-md-3">
                <input type="text" class="textbox" name="start" />
              </div>
              <label class="col-md-2 control-label tenfrom" > tới ngày </label>
              <div class="col-md-3">
                <input type="text" class="textbox" name="end" />
              </div>
            </div>
          </div>
        </div>
      </div>
   
      <div class="form-group">
        <label class="col-md-3 control-label tenfrom" >3. Chủ nhiệm:</label>
        <div class="col-md-12">
          <div class="col-md-12">
          <!-- Text input-->
            <div class="form-group">
              <label class="col-md-2  col-md-offset-1 control-label tenfrom">Họ và tên:</label>
              <div class="col-md-6 inputGroupContainer">
                <div class="input-group"> <span class="input-group-addon  textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  <input  name="Ho_ten" placeholder="Nhập tên chủ nhiệm đề tài vào đây" class="textbox"  type="text">
                </div>
              </div>
            </div>

       	    <div class="form-group">
              <label class="col-md-2  col-md-offset-1 control-label tenfrom">Năm sinh:</label>
              <div class="col-md-6 inputGroupContainer">
                <div class="input-group"> <span class="input-group-addon  textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  <input  name="nam_sinh" placeholder="Nhập năm sinh Chủ nhiệm đề tài vào đây" class="textbox"  type="text">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-2  col-md-offset-1 control-label tenfrom" >Chức danh khoa học:</label>
              <div class="col-md-8 inputGroupContainer">
                <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  <div class="example">
                    <input name="Chuc_Danh_Khoa_Hoc" placeholder="Nhập Chức vụ vào đây" class="textbox"  type="text">
                  </div>
                </div>
              </div>
            </div>

       	    <div class="form-group">
              <label class="col-md-2  col-md-offset-1 control-label tenfrom" >Học hàm, học vị:</label>
              <div class="col-md-8 inputGroupContainer">
                <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  <input name="Hoc_Ham_Hoc_vi" placeholder="Nhập Học hàm, học vị vào đây" class="textbox"  type="text">
                </div>
              </div>
            </div>

    	      <div class="form-group">
              <label class="col-md-2  col-md-offset-1 control-label tenfrom" >Điện thoại di động:</label>
              <div class="col-md-8 inputGroupContainer">
                <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  <input name="Dien_Thoai_Di_Dong" placeholder="Nhập điện thoại di động vào đây" class="textbox"  type="text">
                </div>
              </div>
            </div>

    	      <div class="form-group">
              <label class="col-md-2  col-md-offset-1 control-label tenfrom" >Email:</label>
              <div class="col-md-8 inputGroupContainer">
                <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                  <input name="email" placeholder="<?php echo $_SESSION['sess_email'];?>" class="textbox"  type="text" value="<?php echo $_SESSION['sess_email'];?>">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    
      <div class="form-group">
        <label class="col-md-3 control-label tenfrom" >4. Thành viên tham gia : </label>

        <div class="col-md-8 inputGroupContainer">
          <table width="100%" border="1" class="myGridClass" cellspacing="0" cellpadding="0">
            <tr>
              <td width="5%" height="33" align="center"><strong>STT</strong></td>
              <td width="16%" align="center"><strong>Họ và tên(Bao gồm học hàm và học vị)</strong></td>
              <td width="19%" align="center"><strong>Đơn vị công tác và lĩnh vực chuyên môn</strong></td>
              <td width="13%" align="center"><strong>chữ ký</strong></td>
            </tr>
            <tr>
              <td height="33" align="center">1</td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập họ và tên vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập đơn vị công tác và lĩnh vực chuyên môn vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập chữ ký vào đây"></textarea>
                </div>
              </td>
            </tr>

            <tr>
              <td height="33" align="center">2</td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập họ và tên vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập đơn vị công tác và lĩnh vực chuyên môn vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập chữ ký vào đây"></textarea>
                </div>
              </td>
            </tr>

            <tr>
              <td height="33" align="center">3</td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập họ và tên vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập đơn vị công tác và lĩnh vực chuyên môn vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập chữ ký vào đây"></textarea>
                </div>
              </td>
            </tr>

            <tr>
              <td height="33" align="center">4</td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập họ và tên vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập đơn vị công tác và lĩnh vực chuyên môn vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập chữ ký vào đây"></textarea>
                </div>
              </td>
            </tr>

            <tr>
              <td height="33" align="center">5</td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập họ và tên vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập đơn vị công tác và lĩnh vực chuyên môn vào đây"></textarea>
                </div>
              </td>
              <td align="center">
                <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
                  <textarea class="textarea-from" name="Ho_Ten" placeholder="Nhập chữ ký vào đây"></textarea>
                </div>
              </td>
            </tr>
          </table>
        </div>

      </div>
    
      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-3 control-label tenfrom" >5. Tính cấp thiết của đề tài</label>
        <div class="col-md-8 inputGroupContainer">
          <div class="input-group"> <span class="input-group-addon textbox-icon" ><i class="glyphicon glyphicon-pencil"></i></span>
            <textarea class="textarea-from" name="Tinh_Cap_Thiet_DT" placeholder="Nhập Tính cấp thiết của đề tài vào đây"></textarea>
          </div>
        </div>
      </div>
      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-3 control-label tenfrom" >6.	Mục tiêu và nội dung nghiên cứu của đề tài:</label>
        <div class="col-md-8 inputGroupContainer">
          <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
            <textarea class="textarea-from" name="Muc_Tieu_Noi_Dung_NC" placeholder="Nhập Mục tiêu và nội dung nghiên cứu của đề tài vào đây"></textarea>
          </div>
        </div>
      </div>
      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-3 control-label tenfrom" >7.	Dự kiến đóng góp của đề tài </label>
        <div class="col-md-8 inputGroupContainer">
          <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
            <textarea class="textarea-from" name="Du_Kien_Dong_Gop_DT" placeholder="Vui lòng nhập Đơn vị áp dụng sáng kiến vào đây"></textarea>
          </div>
        </div>
      </div>
      <!-- Text input-->
      <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >Đính kèm file thuyết minh <a href="javascript:_add_more();" title="Add more"><i class="glyphicon glyphicon-plus"></i></a></label>
 
      <div class="col-md-8 inputGroupContainer">
        <div class="col-md-12 ">
          <div class="fileupload fileupload-new" data-provides="fileupload"> 
            <div id="dvFile"><input type="file" name="item_file[]"></div>
          </div>
        </div>
      </div>
    
      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-3 col-md-offset-8 chuky" >Chủ nhiệm đề tài</label>
        <div class="col-md-3 col-md-offset-8 inputGroupContainer">
          <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
            <input  name="Chu_ky" placeholder="Nhập chữ ký vào đây" class="textbox"  type="text">
          </div>
        </div>
      </div>
    
      <!-- Success message -->
      <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>
    
      <!-- Button -->
      <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4">
          <input  type="hidden"  name="trangthai" value="Đăng ký mới"/>
          <input type="hidden" name="ngay"  value="<?php echo date("m/d/Y");?>"/>
          <input  type="hidden"  name="anhien" value="on"/>
          <input  type="hidden"  name="action_type" value="add"/>
          <button type="submit" class="btn btn-warning" >Đăng ký <span class="glyphicon glyphicon-send"></span></button>
        </div>
      </div>
    </fieldset>
  </form>
</div>
<!-- /.container -->
</body>
</html>
<script language="javascript">
<!--
	function _add_more() {
		var txt = "<br><input type=\"file\" name=\"item_file[]\">";
		document.getElementById("dvFile").innerHTML += txt;
	}
	function _add_more_member() {
		var txt = "<div><div class=\"input-group\"> <span class=\"input-group-addon textbox-icon\"><i class=\"glyphicon glyphicon-pencil\"></i></span><div class=\"example\"><input  placeholder=\"Nhập Họ và tên(Bao gồm học hàm và học vị), Đơn vị công tác và lĩnh vực chuyên môn, Đã ký\"  data-role=\"tagsinput\" type=\"text\" id=\"nhaptagsinput[]\"><input type=\"hidden\" name=\"an\"   class=\"an\" id=\"an\"></div></div>\ </div>";
		
		document.getElementById("dvmember").innerHTML += txt;
	}
//-->
</script>