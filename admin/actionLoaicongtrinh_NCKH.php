<?php
session_start();
include '../MySQL/DB.php';
$db = new DB();
$tblName='nncms_LoaicongtrinhNCKH';
if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){
    if($_REQUEST['action_type'] == 'addloaicongtrinhnckh'){
         if($_POST['anhien']=="")
				{
					$anhien="off";
					}
					else
					{$anhien="on";
		}
			$userData = array(
				'TenLoaicongtrinh_NCKH' => $_POST['tenloaicongtrinhnckh'],
				'ThuTu' => $_POST['thutu'],
				'AnHien' => $anhien
				
		
			);
			
			
			$insert = $db->insert($tblName,$userData);
			$statusMsg = $insert?'User data has been inserted successfully.':'Some problem occurred, please try again.';
			$_SESSION['statusMsg'] = $statusMsg;
			header("Location:index.php?key=QLKBloaicongtrinhNCKH");
	}elseif($_REQUEST['action_type'] == 'editloaicongtrinhnckh'){
        if(!empty($_POST['id'])){
    		 if($_POST['anhien']=="")
				{
					$anhien="off";
					}
					else
					{$anhien="on";
		}
		
			$userData = array(
				'TenLoaicongtrinh_NCKH' => $_POST['tenloaicongtrinhnckh'],
				'ThuTu' => $_POST['thutu'],
				'AnHien' => $anhien
				
		
			);
		
			$condition = array('idLoaicongtrinh_NCKH' => $_POST['id']);
      		 $update = $db->update($tblName,$userData,$condition);
            $statusMsg = $update?'User data has been updated successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
       		header("Location:index.php?key=QLKBloaicongtrinhNCKH");
 }
    }	elseif($_GET['action_type'] == 'deleteloaicongtrinhnckh'){
       if(!empty($_GET['id'])){
            $condition = array('idLoaicongtrinh_NCKH' => $_GET['id']);
            $delete = $db->delete($tblName,$condition);
            $statusMsg = $delete?'User data has been deleted successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:index.php?key=QLKBloaicongtrinhNCKH");
        }
    }
	
}