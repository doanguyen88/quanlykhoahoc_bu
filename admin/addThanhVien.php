<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">
$(document).on( "click", '.edit_button',function(e) {

        var thanhvien = $(this).data('name');
		var chucvu = $(this).data('chucvu');
		var Nhiemvu = $(this).data('Nhiemvu');
		var ThuTu = $(this).data('thutu'); 
		var AnHien = $(this).data('anhien');
		var hoidong = $(this).data('hoidong');
        var idThanhVien = $(this).data('id');
		
					
		
       
                  
        $(".business_thanhvien").val(thanhvien);
		$(".business_chucvu").val(chucvu);
		$(".business_Nhiemvu").val(Nhiemvu);
        $(".business_ThuTu").val(ThuTu);
       	$(".business_AnHien").val(AnHien);
		$(".business_hoidong").val(hoidong);
  		$(".business_id").val(idThanhVien);


    });
</script>
<script type="text/javascript">
 $(document).ready(function(){
	 $("#myModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title');
		var idData=button.data('id'); 
		var typeData=button.data('type'); 
        $(this).find('.modal-title').text('Form'+ titleData );
		 $(this).find('.modal-type').val(typeData);
    });
	 
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.closest('.ace-spinner')
				.on('changed.fu.spinbox', function(){
					//console.log($('#spinner1').val())
				}); 
					$('input[name="anhien').on('switchChange.bootstrapSwitch', function(event, state) {
  console.log(this); // DOM element
  console.log(event); // jQuery event
  console.log(state); // true | false
})
	
});

</script>
<script type="text/javascript">
function showDiv(select){
   if(select.value==1){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
} 
</script>
<div class="bs-example">
  <div class="container">
    <h4> 
      <!-- Button HTML (to Trigger Modal) -->
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-title="Thêm Thành viên hội đồng" data-id="addthanhvienhoidong" data-type="addthanhvienhoidong">Thêm Thành viên hội đồng</button>
    </h4>
    
    <!-- Modal HTML -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog"> 
        
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Quản lý Thành viên hội đồng</h4>
          </div>
          <form role="form" id="universalModalForm" method="post" action="actionthanhvien.php" class="form" >
            <div class="modal-body">
              <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Tên Thành viên</label>
                  <input type="text" class="form-control business_thanhvien" name="tenthanhvien"/>
                </div>
                 <div class="form-group">
                  <label>Chức vục</label>
                  <input type="text" class="form-control business_chucvu" name="chucvu"/>
                </div>
                  <div class="form-group">
                  <label>Nhiệm vục</label>
                  <input type="text" class="form-control business_nhiemvu" name="nhiemvu"/>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Chọn Hội đồng </label>
                        <select  name="hoidongdk" id="hoidongdk" data-placeholder="Chọn Hội đồng đánh giá"  class="business_hoidong" >
                          <?php $hoidongdg = $db->getRows('nncms_Hoidong',array('where'=>array('anHien'=>'on')),array('order_by'=>'idHoiDong ASC'));
            if(!empty($hoidongdg)){ $count = 0; foreach($hoidongdg as $hoidong){ $count++;?>
                          <option value="<?php echo $hoidong['idHoiDong'];?>"> <?php echo $hoidong['TenHoiDong'];?></option>
                          <?php } }?>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Thứ tự</label>
                        <input type="text" id="spinner1" name="thutu"  class="input-small business_ThuTu"/>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label> Ẩn Hiện</label>
                  <input name="anhien" class="ace ace-switch ace-switch-4 business_AnHien" type="checkbox">
                  <span class="lbl"></span> </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
              <input  type="hidden" class="modal-id business_id"  name="id"/>
              <input type="hidden" name="action_type" class="modal-type" value=""/>
              <input type="submit" class="btn btn-primary" name="submit" value="Thêm Thành viên HĐ"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-12">
  <div class="panel panel-default users-content">
    <div class="panel-heading">Danh sách Thành viên HĐ</div>
    <table class="table">
      <tr>
        <th width="20%">Họ và Tên Thành viên </th>
        <th width="15%">Chức vụ </th>
        <th width="15%">Nhiệm vụ </th>
        <th width="20%">Thuộc Hội đồng</th>
         <th width="10%">Thứ Tự</th>
        <th width="10%">Ẩn hiện</th>
      </tr>
      <?php
          
            $thanhvienhd = $db->getRows('nncms_ThanhVienHoiDong',array('order_by'=>'idThanhVien ASC'));
            if(!empty($thanhvienhd)){ $count = 0; foreach($thanhvienhd as $thanhvien){ $count++;?>
      <tr>
        <td><?php echo $thanhvien['TenThanhVien'];?></td>
        <td><?php echo $thanhvien['Chucvu'];?></td>
         <td><?php echo $thanhvien['Nhiemvu'];?></td>
        <td><?php 
				$lay=$thanhvien['idHoiDong'];
				$layhoidongdg = $db->getRows('nncms_Hoidong',array('where'=>array('idHoiDong'=>$lay,'anHien'=>'on')),array('order_by'=>'idHoiDong ASC'));
            if(!empty($layhoidongdg)){ $count = 0; foreach($layhoidongdg as $layhoidong){ $count++;
				echo $layhoidong['TenHoiDong'];}}?></td>
        <td><?php echo $layhoidong['ThuTu'];?></td>
        <td><?php echo $layhoidong['AnHien'];?></td>
        <td><a 
                    
                    data-toggle="modal" data-target="#myModal"
                    data-name="<?php echo $thanhvien['TenThanhVien'];?>"
                    data-chucvu="<?php echo $thanhvien['Chucvu'];?>"
                     data-nhiemvu="<?php echo $thanhvien['nhiemvu'];?>"
                    data-thutu="<?php echo $thanhvien['ThuTu'];?>"
                  	data-anhien="<?php echo $thanhvien['AnHien'];?>"
                    data-hoidong="<?php echo $thanhvien['idHoiDong'];?>"

                   
                    data-title="Sửa TT Thành viên" 
                    data-id="<?php echo $thanhvien['idThanhVien'];?>"  
                    data-type="editthanhvien" 
                    class="glyphicon glyphicon-edit edit_button"></a>
                     <a href="actionthanhvien.php?action_type=xoathanhvien&id=<?php echo $thanhvien['idThanhVien'];?>" data-type="xoathanhvien"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a></td>
      </tr>
      <?php } }else{ ?>
      <tr>
        <td colspan="4">No user(s) found......</td>
        <?php } ?>
    </table>
  </div>
</div>
