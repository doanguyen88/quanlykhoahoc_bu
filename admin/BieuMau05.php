<!DOCTYPE html>
<html >
<head>
<meta charset="UTF-8">
<title>Biểu Mẫu-05. Phiếu đăng ký hội thảo</title>
<script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts3.js" type="text/javascript"></script>

<link rel="stylesheet" href="../giaodien/css/style-dk.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-timepicker.min.css">
</head>

<body>
<?php include 'menu_ngang_admin.php';?>
  <form class="well form-horizontal" action="actionCN.php" method="post"  id="contact_form" enctype="multipart/form-data">
    <fieldset>
    
      <!-- Form Name -->
    
      <div class="row">
        <div class="col-md-6 col-md-offset-6 benner-tenmau">Biểu Mẫu-05. Phiếu đăng ký hội thảo </div>
          <div class="col-md-6 banner-tentruong">
            <div class="col-md-12 banner-tentruong">NGÂN HÀNG NHÀ NƯỚC VIỆT NAM</div>
            <div class="col-md-12 banner-tentruong"><strong>TRƯỜNG ĐẠI HỌC NGÂN HÀNG TP.HCM</strong></div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="col-md-2 col-md-offset-3 control-label tenfrom" >Đơn vị:</label>
                <div class="col-md-4 inputGroupContainer">
                  <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
                    <input name="donvi" placeholder="Vui lòng nhập đơn vị" class="textbox"  type="text">
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-6 banner-tentruong">
            <div class="col-md-12">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</div>
            <div class="col-md-12"> <strong><ins>Độc lập - Tự do - Hạnh phúc</ins></strong></div>
          </div>

          <div class="col-md-6 col-md-offset-6 benner-tenmau">TP.HCM, ngày <?php echo date("d")?> tháng <?php echo date("m")?> năm <?php echo date("Y")?></div>
          <div class="col-md-12 tenphieu">
            <h3>PHIẾU ĐĂNG KÝ HỘI THẢO</h3>
          </div>
        </div>
    
        <!-- Text input-->
    
        <div class="form-group">
          <label class="col-md-3 control-label tenfrom"><strong>1. Tên đơn vị đăng ký</strong></label>
          <div class="col-md-6 inputGroupContainer">
            <div class="input-group"> <span class="input-group-addon  textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input  name="Ho_ten" placeholder="Nhập tên đơn vị đăng ký vào đây" class="textbox"  type="text">
            </div>
          </div>
        </div>
    
        <!-- Text input-->
        
        <div class="form-group">
          <label class="col-md-3 control-label tenfrom" >2.	Chủ đề hội thảo:</label>
          <div class="col-md-8 inputGroupContainer">
            <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input name="chuc_vu" placeholder="Nhập Chủ đề hội thảo vào đây" class="textbox"  type="text">
            </div>
          </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-3 control-label tenfrom" >3.	Lĩnh vực áp dụng: Nêu rõ lĩnh vực có thể áp dụng và vấn đề giải quyết của hội thảo:</label>
          <div class="col-md-8 inputGroupContainer">
            <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input name="Donvictnctnc" placeholder="Nhập lĩnh vực áp dụng vào đây" class="textbox"  type="text">
            </div>
          </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-3 control-label tenfrom" >4.	Tóm tắt nội dung hội thảo:</label>
          <div class="col-md-8 inputGroupContainer">
            <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input name="NV_DCG" placeholder="Vui lòng tóm tắt nội dung hội thảo vào đây" class="textbox"  type="text">
            </div>
          </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-3 control-label tenfrom" >5.	Thời gian, địa điểm tổ chức:</label>
          <div class="col-md-8 inputGroupContainer">
            <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input name="TenSangKien" placeholder="Vui lòng nhập thời gian và địa điểm tổ chức vào đây" class="textbox"  type="text">
            </div>
          </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-3 control-label tenfrom" >6.	Thành phần tham dự:</label>
          <div class="col-md-8 inputGroupContainer">
            <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input name="Linh_vuc" placeholder="Vui lòng nhập thành phần tham dự vào đây" class="textbox"  type="text">
            </div>
          </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-3 control-label tenfrom" >7.	Kinh phí:</label>
          <div class="col-md-8 inputGroupContainer">
            <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input name="kinh_phi" placeholder="Vui lòng nhập kinh phí vào đây" class="textbox"  type="text">
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-12 control-label tenfrom">
            <div class="col-md-4">
              <div id="lv"></div>
              <div id="cm"></div>
            </div>
          </div>
        </div>
    
        <div class="row">
          <div class="col-md-6">
          </div>
        </div>
   
        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-3 col-md-offset-8 chuky" >Người đăng ký</label>
          <div class="col-md-3 col-md-offset-8 inputGroupContainer">
            <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
              <input  name="Chu_ky" placeholder="Nhập chữ ký vào đây" class="textbox"  type="text">
            </div>
          </div>
        </div>
    
        <!-- Success message -->
        <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> 
        Thanks for contacting us, we will get back to you shortly.</div>
        
        <!-- Button -->
        <div class="form-group">
          <label class="col-md-4 control-label"></label>
          <div class="col-md-4">
            <input  type="hidden"  name="trangthai" value="Chờ duyệt"/>
            <input type="hidden" name="ngay"  value="<?php echo date("m/d/Y");?>"/>
            <input  type="hidden"  name="kqccl" value="Chưa có kết quả"/>
            <input  type="hidden"  name="nhanxettt" value="Chưa có đánh giá"/>
            <input  type="hidden"  name="anhien" value="on"/>
            <input  type="hidden"  name="action_type" value="add"/>
            <button type="submit" class="btn btn-warning" >Đăng ký <span class="glyphicon glyphicon-send"></span></button>
          </div>
        </div>
      </div>
    </fieldset>
  </form>

<!-- /.container -->

</body>
</html>
<script language="javascript">
<!--
	function _add_more() {
		var txt = "<br><input type=\"file\" name=\"item_file[]\">";
		document.getElementById("dvFile").innerHTML += txt;
	}
//-->
</script>