<!DOCTYPE html>
<html >
<head>
<meta charset="UTF-8">
<title>BIỂU MẪU-04. Phiếu đăng ký sáng kiến của cá nhân</title>
<script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts3.js" type="text/javascript"></script>

<link rel="stylesheet" href="../giaodien/css/style-dk.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="../giaodien/css/bootstrap-timepicker.min.css">
</head>

<body>
<?php include 'menu_ngang_admin.php';?>
<form class="well form-horizontal" action="actionCN.php" method="post"  id="contact_form" enctype="multipart/form-data">
  <fieldset>
    
    <!-- Form Name -->
    
    <div class="row">
      <div class="col-md-6 col-md-offset-6 benner-tenmau">Biểu Mẫu-04. Phiếu đăng ký sáng kiến của cá nhân </div>
      <div class="col-md-6 banner-tentruong">
        <div class="col-md-12 banner-tentruong">NGÂN HÀNG NHÀ NƯỚC VIỆT NAM</div>
        <div class="col-md-12 banner-tentruong"><strong>TRƯỜNG ĐẠI HỌC NGÂN HÀNG TP.HCM</strong></div>
        <div class="col-md-12 banner-tentruong"><strong><ins>Tên đơn vị</ins></strong></div>
          
      </div>
      <div class="col-md-6 banner-tentruong">
        <div class="col-md-12">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</div>
        <div class="col-md-12"> <strong><ins>Độc lập - Tự do - Hạnh phúc</ins></strong></div>
      </div>
      <div class="col-md-6 col-md-offset-6 benner-tenmau">TP.HCM, ngày <?php echo date("d")?> tháng <?php echo date("m")?> năm <?php echo date("Y")?></div>
      <div class="col-md-12 tenphieu">
        <h3>PHIẾU ĐĂNG KÝ SÁNG KIẾN</h3>
      </div>
    </div>
    
    <!-- Text input-->
    
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom"><strong>1. Họ và tên người đăng ký</strong></label>
      <div class="col-md-6 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon  textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input  name="Ho_ten" placeholder="Nhập Họ và tên vào đây" class="textbox"  type="text">
        </div>
      </div>
    </div>
    
    <!-- Text input-->
    
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >2.	Chức vụ:</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input name="chuc_vu" placeholder="Nhập Chức vụ vào đây" class="textbox"  type="text">
        </div>
      </div>
    </div>
    <!-- Text input-->
    
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >3.	Đơn vị công tác:</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input name="Donvictnctnc" placeholder="Nhập Đơn vị công tác vào đây" class="textbox"  type="text">
        </div>
      </div>
    </div>
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >4.	Nhiệm vụ được giao trong đơn vị:</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input name="NV_DCG" placeholder="Vui lòng nhập thông tin Nhiệm vụ được giao Đơn vị vào đây" class="textbox"  type="text">
        </div>
      </div>
    </div>
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >5.	Tên sáng kiến:</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input name="TenSangKien" placeholder="Vui lòng nhập Tên sáng kiến vào đây" class="textbox"  type="text">
        </div>
      </div>
    </div>
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >6.	Lĩnh vực áp dụng: Nêu rõ lĩnh vực có thể áp dụng sáng kiến và vấn đề mà sáng kiến giải quyết:</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input name="Linh_vuc" placeholder="Vui lòng Nhập Lĩnh vực áp dụng vào đây" class="textbox"  type="text">
        </div>
      </div>
    </div>

    
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >7.	Tóm tắt nội dung sáng kiến:</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <textarea class="textarea-from" name="tomtat" placeholder="Vui lòng nhập Tóm tắt nội dung sáng kiến vào đây"></textarea>
        </div>
      </div>
    </div>
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >8.	Thời gian, địa điểm, công việc áp dụng sáng kiến:</label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <textarea class="textarea-from" name="tgddcvadsk" placeholder="Vui lòng nhập Thời gian, địa điểm, công việc áp dụng sáng kiến vào đây"></textarea>
        </div>
      </div>
    </div>
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >9.	Đơn vị áp dụng sáng kiến: </label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <textarea class="textarea-from" name="DonViADSK" placeholder="Vui lòng nhập Đơn vị áp dụng sáng kiến vào đây"></textarea>
        </div>
      </div>
    </div>
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-3 control-label tenfrom" >10.	Sơ bộ kết quả đạt được: </label>
      <div class="col-md-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <textarea class="textarea-from" name="KetQuaDD" placeholder="Vui lòng nhập Sơ bộ kết quả đạt được vào đây"></textarea>
        </div>
      </div>
    </div>
    
   <div class="form-group">
   <label class="col-md-3 control-label tenfrom" >Đính kèm file thuyết minh <a href="javascript:_add_more();" title="Add more"><i class="glyphicon glyphicon-plus"></i></a></label>
    <div class="col-md-8 inputGroupContainer">
      <div class="col-md-12 ">
        <div class="fileupload fileupload-new" data-provides="fileupload">
          <div id="dvFile">
            <input type="file" name="item_file[]">
          </div>
        </div>
      </div>
    </div>
   </div>
      
    
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-3 col-md-offset-8 chuky" >Người đăng ký</label>
      <div class="col-md-3 col-md-offset-8 inputGroupContainer">
        <div class="input-group"> <span class="input-group-addon textbox-icon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input  name="Chu_ky" placeholder="Nhập chữ ký vào đây" class="textbox"  type="text">
        </div>
      </div>
    </div>
    
    <!-- Success message -->
    <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> 
    Thanks for contacting us, we will get back to you shortly.</div>
    
    <!-- Button -->
    <div class="form-group">
      <label class="col-md-4 control-label"></label>
      <div class="col-md-4">
        <input  type="hidden"  name="trangthai" value="Chờ duyệt"/>
        <input type="hidden" name="ngay"  value="<?php echo date("m/d/Y");?>"/>
        <input  type="hidden"  name="kqccl" value="Chưa có kết quả"/>
        <input  type="hidden"  name="nhanxettt" value="Chưa có đánh giá"/>
        <input  type="hidden"  name="anhien" value="on"/>
        <input  type="hidden"  name="action_type" value="add"/>
        <button type="submit" class="btn btn-warning" >Đăng ký <span class="glyphicon glyphicon-send"></span></button>
      </div>
    </div>
  </fieldset>
</form>
</div>
<!-- /.container -->

</body>
</html>
<script language="javascript">
<!--
	function _add_more() {
		var txt = "<br><input type=\"file\" name=\"item_file[]\">";
		document.getElementById("dvFile").innerHTML += txt;
	}
//-->
</script>