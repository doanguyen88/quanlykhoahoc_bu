<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<script type="text/javascript">

$(document).on( "click", '.edit_button',function(e) {

        var TNam = $(this).data('direction');
		var anhien = $(this).data('anhien'); 
		var ThuTu = $(this).data('thutu');
        var idNam = $(this).data('id');

      
        $(".business_TNam").val(TNam);
		if(anhien=='on'){document.getElementById("anhien").checked = true;}
       	$(".business_ThuTu").val(ThuTu);
  		$(".business_id").val(idNam);


    });
	
</script>
<script type="text/javascript">
 $(document).ready(function(){
	 $("#myModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title');
		var idData=button.data('id'); 
		var typeData=button.data('type'); 
        $(this).find('.modal-title').text('Form'+ titleData );
		 $(this).find('.modal-type').val(typeData);
    });
	 
		$('#spinner1').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				$('#spinner2').ace_spinner({value:0,min:0,max:200,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.closest('.ace-spinner')
				.on('changed.fu.spinbox', function(){
					//console.log($('#spinner1').val())
				}); 
$('input[name="anhien').on('switchChange.bootstrapSwitch', function(event, state) {
  console.log(this); // DOM element
  console.log(event); // jQuery event
  console.log(state); // true | false
});

});

</script>
<script type="text/javascript">
function showDiv(select){
   if(select.value==1){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
} 
</script>
<div class="bs-example">
<div class="container">
<h4>
    <!-- Button HTML (to Trigger Modal) -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-title="Thêm menu" data-id="addnam" data-type="addnam">Thêm Năm</button>

    </h4>
    
    <!-- Modal HTML -->
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog"> 
      
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
          <h4 class="modal-title">Quản lý Năm</h4>
          
        </div>  
     
        <form role="form" id="universalModalForm" method="post" action="actionnam.php" class="form" >
        <div class="modal-body">
            <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
            <div class="modal-body">
              <div class="form-group">
                <label>Nhập Năm</label>
                <input type="text" class="form-control business_TNam" name="TNam"/>
              </div>
                <div class="form-group" >
                  <label>Thứ tự</label>
                  <input type="text" id="spinner1" name="thutu"  class="input-small business_ThuTu"/>
                </div>
                <div class="form-group">


                          <label>
                              <input id="anhien" name="anhien" type="checkbox" class="ace business_anhien">
                              <span class="lbl"> Ẩn Hiện</span>
                          </label> 
           
            </div>
        </div>
        <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                <input  type="hidden" class="modal-id business_id"  name="id"/>
              <input type="hidden" name="action_type" class="modal-type" value=""/>
              <input type="submit" class="btn btn-primary" name="submit" value="Thêm Năm"/>
            </div> 
      </form>
      </div>
    </div>
  </div>
</div>
</div>



<div class="col-xs-12 col-sm-12">

    <div class="panel panel-default users-content">
        <div class="panel-heading">Danh sách main menu </div>
        <table class="table">
            <tr>
                
                <th width="25%">id Năm</th>
                <th width="25%">Năm</th>
                <th width="20%">Ẩn Hiện</th>
                <th width="20%">Thứ Tự</th>
               
                
            </tr>
            <?php
          
            $nams = $db->getRows('nncms_nam',array('order_by'=>'idNam DESC'));
            if(!empty($nams)){ $count = 0; foreach($nams as $nam){ $count++;?>
            <tr>
                
                
                <td><?php echo $nam['idNam'];?></td>
                <td><?php echo $nam['TNam'];?></td>
                <td><?php echo $nam['AnHien'];?></td>
                <td><?php echo $nam['ThuTu'];?></td>
                <td>
                    <a 
                    
                    data-toggle="modal" data-target="#myModal"
                    data-direction="<?php echo $nam['TNam'];?>"
                    data-anhien="<?php echo $nam['AnHien'];?>"
                  	data-thutu="<?php echo $nam['ThuTu'];?>"
                   
                    data-title="Sửa năm" 
                    data-id="<?php echo $nam['idNam'];?>"  
                    data-type="editnam" 
                    class="glyphicon glyphicon-edit edit_button"></a>
                    <a href="actionnam.php?action_type=deleteman&id=<?php echo $nam['idNam'];?>" data-type="delete"  class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure?');"></a>
                </td>
            </tr>
            <?php } }else{ ?>
            <tr><td colspan="4">No user(s) found......</td>
            <?php } ?>
        </table>
    </div>
</div>

   