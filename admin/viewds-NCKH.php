<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.js"></script>
<script src="../giaodien/js/upload.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts4.js" type="text/javascript"></script>
<script src="../giaodien/js/scripts7.js" type="text/javascript"></script>
<script src="../assets/js/jquery-ui.min.js"></script>

<script type="text/javascript">
  <!-- View -->
  $(document).on( "click", '.nckhview',function(e) {  
    var idkkstnckh = $(this).data('idkkstnckh');
    var hoten = $(this).data('hoten');
    var chonqldkdt = $(this).data('chonqldkdt');
    var chonphanloaikhnc = $(this).data('chonphanloaikhnc');
    var masanphamkhnc = $(this).data('masanphamkhnc');
    var tensanpham = $(this).data('tensanpham');
    var chonmahinhthuc = $(this).data('chonmahinhthuc');
    var mavaitro = $(this).data('mavaitro');
    var soquyetdinh = $(this).data('soquyetdinh');
    var thoigianhoanthanh = $(this).data('thoigianhoanthanh');
    var sotietquydoi = $(this).data('sotietquydoi');
    var trangthai = $(this).data('trangthai');
    var action = $(this).data('action'); 
    var tapchihoithao = $(this).data('tapchihoithao'); 
    var chondotkknckh = $(this).data('chondotkknckh'); 
    var namhoanthanh = $(this).data('namhoanthanh'); 
    var sotietkhoaduyet = $(this).data('sotietkhoaduyet'); 
    var sotietvienduyet = $(this).data('sotietvienduyet'); 

    $(".idkkstnckh").text(idkkstnckh);
    $(".hoten").text(hoten);
    $(".chonqldkdt").text(chonqldkdt);
    $(".chonphanloaikhnc").text(chonphanloaikhnc);	
    $(".masanphamkhnc").text(masanphamkhnc);
    $(".tensanpham").text(tensanpham);
    $(".chonmahinhthuc").text(chonmahinhthuc);
    $(".mavaitro").text(mavaitro);
    $(".soquyetdinh").text(soquyetdinh);
    $(".thoigianhoanthanh").text(thoigianhoanthanh);
    $(".sotietquydoi").text(sotietquydoi);
    $(".sotietquydoikn").text(sotietquydoi);
    $(".trangthai").text(trangthai);
    $(".action").text(action);
    $(".tapchihoithao").text(tapchihoithao);
    $(".chondotkknckh").text(chondotkknckh);
    $(".namhoanthanh").text(namhoanthanh);
    $(".sotietkhoaduyet").text(sotietkhoaduyet);
    $(".sotietvienduyet").text(sotietvienduyet);
  });

  <!--edit -->
  $(document).on( "click", '.giangvienedit', function(e) {  
    var idkkstnckh = $(this).data('idkkstnckh');
    var hoten = $(this).data('hoten');
    var chonqldkdt = $(this).data('chonqldkdt');
    var chonphanloaikhnc = $(this).data('chonphanloaikhnc');
    var masanphamkhnc = $(this).data('masanphamkhnc');
    var tensanpham = $(this).data('tensanpham');
    var chonmahinhthuc = $(this).data('chonmahinhthuc');
    var mavaitro = $(this).data('mavaitro');
    var soquyetdinh = $(this).data('soquyetdinh');
    var thoigianhoanthanh = $(this).data('thoigianhoanthanh');
    var sotietquydoi = $(this).data('sotietquydoi');
    var trangthai = $(this).data('trangthai');
    var action = $(this).data('action'); 
    var tapchihoithao = $(this).data('tapchihoithao'); 

    $(".idkkstnckh").val(idkkstnckh);
    $(".hoten").val(hoten);
    $(".chonqldkdt").val(chonqldkdt);
    $(".chonphanloaikhnc").val(chonphanloaikhnc);	
    $(".masanphamkhnc").val(masanphamkhnc);
    $(".tensanpham").val(tensanpham);
    $(".chonmahinhthuc").val(chonmahinhthuc);
    $(".mavaitro").val(mavaitro);
    $(".soquyetdinh").val(soquyetdinh);
    $(".thoigianhoanthanh").val(thoigianhoanthanh);
    $(".sotietquydoi").val(sotietquydoi);
    $(".sotietquydoikn").val(sotietquydoi);
    $(".trangthai").text(trangthai);
    $(".action").val(action);
    $(".tapchihoithao").val(tapchihoithao);
  });

  <!-- file -->
  $(document).on( "click", '.file',function(e) {
    var idkknckh = $(this).data('idkknckh');
    $(".idkknckh").val(idkknckh);   
  });
</script>

<script type="text/javascript">
	$(document).on( "click", '.show-details-kknckh',function(e) {  
    var iddetailskknckh = $(this).data('iddetailskknckh');
    var details='iddetailskknckh-'+iddetailskknckh;
    var idiconnckh='idiconnckh-'+iddetailskknckh;
    var element = document.getElementById(details);
    //element.classList.add("open");
    $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');

    
    var idiconnckh = document.getElementById(idiconnckh);
    var tenclass = idiconnckh.className;
    
    
    if(tenclass=="ace-icon fa fa-angle-double-up"){element.classList.remove("open")}
    if(tenclass=="ace-icon fa fa-angle-double-down"){element.classList.add("open")}
  });
</script>

<script type="text/javascript">
//hoạt động NCKH
  $(document).ready(function(){
    $('#hoten').keyup(function(e){
      e.preventDefault();
      var form = $('#hdTutoForm').serialize();
      $.ajax({
        type: 'POST',
        url: 'search-hoten-thanhvien.php',
        data: form,
        dataType: 'json',
        success: function(response){
          if(response.error){
            $('#list_hoten').hide();
          }
          else{
            $('#list_hoten').show().html(response.data);
          }
        }
      });
    });

    $(document).on('click', '.list_hoten_thanhvien', function(e){
      e.preventDefault();
      $('#list_hoten').hide();
      var fullname = $(this).data('fullname');
      var iduser = $(this).data('iduser');
      $.ajax({
        url: "aj_loadkekhainckh.php",
        type:'POST',
        data:"hoten="+fullname,
        success: function(data){
            $("#dskekhai_nckh").html(data);
        }
      })
      $('#hoten').val(fullname);
    });
	});
</script>

<div class="page-content">
  <div class="ace-settings-container" id="ace-settings-container">
    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn"> <i class="ace-icon fa fa-cog bigger-130"></i> </div>
    <div class="ace-settings-box clearfix" id="ace-settings-box">
      <div class="pull-left width-50">
        <div class="ace-settings-item">
          <div class="pull-left">
            <select id="skin-colorpicker" class="hide">
              <option data-skin="no-skin" value="#438EB9">#438EB9</option>
              <option data-skin="skin-1" value="#222A2D">#222A2D</option>
              <option data-skin="skin-2" value="#C6487E">#C6487E</option>
              <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
            </select>
          </div>
          <span>&nbsp; Choose Skin</span> </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
          <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
          <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
          <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
          <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
          <label class="lbl" for="ace-settings-add-container"> Inside <b>.container</b> </label>
        </div>
      </div>
      <!-- /.pull-left -->
      
      <div class="pull-left width-50">
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
          <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
          <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
        </div>
        <div class="ace-settings-item">
          <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
          <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
        </div>
      </div>
      <!-- /.pull-left --> 
    </div>
    <!-- /.ace-settings-box --> 
  </div>
  <!-- /.ace-settings-container --> 
  <!-- /.page-header -->
  <div class="col-md-12 infobox-container"> 
    <!--<div class="infobox infobox-blue ">
      <div class="infobox-data" style="padding-top:10px !important">
        <form class="" action="#" method="post"  id="hdTutoForm" enctype="multipart/form-data">
          <input style="height:42px" type="text" id="hoten" name="hoten" class="textbox auto" placeholder="Họ tên" aria-describedby="basic-addon3">
          <div class="input-gpfrm input-gpfrm-lg">
            <ul class="list-gpfrm list-unstyled" id="list_hoten">
            </ul>
          </div>
        </form>
      </div>
    </div>-->
    <div class="infobox infobox-grey ">
      <div class="infobox-data"> <a class="btn btn-info" type="button" href="http://localhost/quanlykhoahoc_bu/admin/index.php?key=Export-Excell-NCKH"> <i class="ace-icon  fa fa-file-excel-o bigger-110"></i> Xuất báo cáo</a> </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-xs-12">
      <form action="upload-dinhmucnckh-send.php" method="post" enctype="multipart/form-data">
        <div class="col-xs-2">
          <label>Chọn năm</label>
          <select  name="nam" id="nam" data-placeholder="Chọn năm"  class="" >
            <?php $nams = $db->getRows('nncms_nam',array('where'=>array('AnHien'=>'on')),array('order_by'=>'idNam ASC'));
            if(!empty($nams)){ 
              $count = 0; 
              foreach($nams as $nam){ 
                $count++;?>
                <option value="<?php echo $nam['TNam'];?>"> <?php echo $nam['TNam'];?></option>
              <?php } 
            }?>
          </select>
        </div>
        <!-- CHỌN ĐỢT KÊ KHAI -->
        <div class="col-xs-4">
          <label>Chọn đợt kê khai</label>
          <select  name="madotkekhai" id="madotkekhai" data-placeholder="Chọn đợt kê khai"  class="" >
            <?php $qldkdetai = $db -> getRows('nncms_QuanlydotkhekhaiNCKH', array('where' => array('anHien' => 'on')), array('order_by' => 'idQuanlydangky_Detai ASC'));
            if(!empty($qldkdetai)){ 
              $count = 0; 
              foreach($qldkdetai as $qldkdetais){ 
                $count++;?>
                <option value="<?php echo $qldkdetais['madotkekhai_NCKH'];?>"> <?php echo $qldkdetais['TenDotkekhai_NCKH'];?></option>
              <?php } 
            }?>
          </select>
        </div>
        <!-- BÁO CÁO 1 -->
        <div class="col-xs-2">
          <input type="hidden" name="ngaycapnhat"  value="<?php echo date("m/d/Y");?>"/>
          <button type="submit" name="btn-upload-dinhmuc" class="btn btn-sm btn-success" value=""> <i class="ace-icon ace-icon fa fa-cloud-upload bigger-110"></i> Báo cáo 1</button>
        </div>
      </form>
      <!-- SEARCH, COPY CLIPBOARD, EXPORT, PRINT -->
      <div class="clearfix">
        <div class="pull-right tableTools-container"></div>
      </div>

      <div class="table-header"> Danh sách NCKH đã kê khai </div>
      <!-- div.table-responsive --> 
      <!-- div.dataTables_borderWrap -->
      <div>
        <table id="dynamic-table" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="center"> 
                <label class="pos-rel">
                  <input type="checkbox" class="ace"/>
                  <span class="lbl"></span> 
                </label>
              </th>
              <th class="center"> #STT </th>
              <th class="hidden-280"> Họ và tên </th>
              <th class="hidden-280">Phân Loại</th>
              <th >Trạng thái</th>
              <th><i class="ace-icon fa fa-cog icon-only bigger-130"></i></th>
            </tr>
          </thead>

          <tbody>
            <?php $dskkstnckh = $db->getRowsNew('nncms_KKST_NCKH',array('where'=>array('xoa'=>'')),array('order_by'=>'Ho_Ten ASC'));
              if(!empty($dskkstnckh)){ 
                $count = 0; 
                foreach($dskkstnckh as $nckh){ 
                  $count++;?>
                  <tr>
                    <td class="center">
                      <label class="pos-rel">
                        <input type="checkbox" class="ace"/>
                        <span class="lbl"></span> 
                      </label>
                    </td>
                    <!-- STT -->
                    <td class="center"><?php echo $count;?></td>
                    <!-- HỌ TÊN -->
                    <td class="hidden-280"><?php echo $nckh['Ho_Ten'];?></td>
                    <!-- PHÂN LOẠI -->
                    <td class="hidden-280"><?php echo $nckh['chonphanloaikhnc'];?></td>
                    <!-- TRẠNG THÁI -->
                    <td ><?php echo $nckh['TrangThai'];?></td>
                    <!-- VIEW, EDIT, FILE, DELETE -->
                    <td>
                      <div class="">
                        <div class="inline pos-rel">
                          <!-- View-->
                          <a class="blue btn btn-xs btn-success no-radius nckhview" href="#" href="#stack5" 
                            data-toggle="modal"   data-target="#stack5"
                            data-idkkstnckh="<?php echo $nckh['id_KKST_NCKH'];?>"
                            data-hoten="<?php echo $nckh['Ho_Ten'];?>"
                            data-chonqldkdt="<?php echo $nckh['chonqldkdt'];?>"
                            data-chonphanloaikhnc="<?php echo $nckh['chonphanloaikhnc'];?>"
                            data-masanphamkhnc="<?php echo $nckh['masosanpham'];?>"
                            data-tensanpham="<?php echo $nckh['tensanpham'];?>"
                            data-tapchihoithao="<?php echo $nckh['tapchihoithao'];?>"
                            data-chonmahinhthuc="<?php echo $nckh['chonmahinhthuc'];?>"
                            data-mavaitro="<?php echo $nckh['mavaitro'];?>"
                            data-soquyetdinh="<?php echo $nckh['soquyetdinh'];?>"
                            data-thoigianhoanthanh="<?php echo $nckh['Thoi_Gian_Hoan_Thanh'];?>"
                            data-sotietquydoi="<?php echo $nckh['SoTietQuyDoi'];?>"
                            data-sotietkhoaduyet="<?php echo $nckh['SoTietKhoaDuyet'];?>"
                            data-sotietvienduyet="<?php echo $nckh['SoTietVienNCDuyet'];?>"
                            data-trangthai="<?php echo $nckh['TrangThai'];?>"
                            data-chondotkknckh="<?php echo $nckh['chondotkknckh'];?>"
                            data-namhoanthanh="<?php echo $nckh['namhoanthanh'];?>" > 
                            
                            <i class="ace-icon fa fa-search-plus bigger-130"></i> 
                          </a>
                        
                          <!-- Edit--> 
                          <a class="btn btn-xs btn-warning no-radius giangvienedit" data-toggle="modal" href="#stack3" 
                            data-idkkstnckh="<?php echo $nckh['id_KKST_NCKH'];?>"
                            data-hoten="<?php echo $nckh['Ho_Ten'];?>"
                            data-chonqldkdt="<?php echo $nckh['chonqldkdt'];?>"
                            data-chonphanloaikhnc="<?php echo $nckh['chonphanloaikhnc'];?>"
                            data-masanphamkhnc="<?php echo $nckh['masosanpham'];?>"
                            data-tensanpham="<?php echo $nckh['tensanpham'];?>"
                            data-tapchihoithao="<?php echo $nckh['tapchihoithao'];?>"
                            data-chonmahinhthuc="<?php echo $nckh['chonmahinhthuc'];?>"
                            data-mavaitro="<?php echo $nckh['mavaitro'];?>"
                            data-soquyetdinh="<?php echo $nckh['soquyetdinh'];?>"
                            data-thoigianhoanthanh="<?php echo $nckh['Thoi_Gian_Hoan_Thanh'];?>"
                            data-sotietquydoi="<?php echo $nckh['SoTietQuyDoi'];?>"
                            data-trangthai="<?php echo $nckh['TrangThai'];?>"
                            data-action="updatekknckh" title="edit"> 
                            <i class="ace-icon fa fa-pencil-square-o bigger-150"></i> 
                          </a> 
                  
                          <!-- File--> 
                          <a class="btn btn-xs btn-info no-radius blue file" href="#stack4" 
                            data-toggle="modal" data-target="#stack4" 
                            data-idkknckh="<?php echo $nckh['id_KKST_NCKH'];?>"  
                            onclick="qlfilekknckhadmin('<?php echo $nckh['idfile'];?>','<?php echo $nckh['file'];?>','<?php echo $nckh['emailuser'];?>')" title="file"> 
                            <i class="ace-icon fa fa-folder-open-o bigger-150"></i> 
                          </a> 

                          <!-- Delete--> 
                          <a href="action-NCKH.php?action_type=deletekknckh&id=<?php echo $nckh['id_KKST_NCKH'];?>" 
                            data-type="delete"  
                            class="tooltip-error btn btn-xs no-radius btn-danger" 
                            onclick="return confirm('Chắc chắn là bạn muốn xoá kê khai này?');"  title="Delete">
                            <i class="ace-icon fa fa-trash-o bigger-150"></i> 
                          </a>
                        </div>
                      </div>
                    </td>

                    <div class="detail-row" id="iddetailskknckh-<?php echo $nckh['id_KKST_NCKH'];?>"></div>
                  </tr>
                <?php } 
              } else { ?>
                <tr>
                  <td colspan="8">Không có dữ liệu</td>
                </tr>
              <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div><!-- /.row --> 
</div><!-- /.page content --> 

<!-- FORM SỬA KÊ KHAI NCKH -->
<div id="stack3" class="modal fade" >
  <form role="form" id="universalModalForm" method="post" action="action-NCKH.php" class="form">
    <div class="modal-dialog modal-lg">       
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">admin sửa kê khai NCKH</h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> 
            <span class="alert-body"></span> 
          </div>
          <div class="modal-body">
            <table class="table" >
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Họ tên:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important">
                  <input type="text" class="col-xs-12 hoten" name="ho_ten" id="ho_ten">
                  <strong class="hoten"></strong>
                </td>
              </tr>

              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Hoạt động nckh:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <select name="hoatdongnckh" data-placeholder="Chọn Hoạt động NCKH" class="chosen-select chonqldkdt" style="width:550px !important" id="hoatdongnckh">
                    <option value=""> </option>
                    <?php $hoatdongnckh = $db -> getRows('NCKH_HoatDong',array('where' => array('AnHien' => 'on')), array('order_by' => 'ThuTu ASC'));
            		    foreach($hoatdongnckh as $hdnckh){?>
                      <option value="<?php echo $hdnckh['Ten_HoatDong'];?>"> <?php echo $hdnckh['Ten_HoatDong'];?></option>
                    <?php  }?>
                  </select>
                </td>
              </tr>

              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Phân loại NCKH:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" >
                  <select name="phanloainckh" data-placeholder="Chọn Phân Loại NCKH" class="chosen-select textbox auto chonphanloaikhnc" id="phanloainckh">
                    <option value=""></option>
                    <?php  $phanloainckh = $db -> getRows('NCKH_Phanloai_HD', array('where' => array('AnHien'=>'on')), array('order_by' => 'ThuTu ASC'));
            		    foreach($phanloainckh as $plnckh){?>
                      <option value="<?php echo $plnckh['Ten_PhanLoai_HD'];?>"> <?php echo $plnckh['Ten_PhanLoai_HD'];?></option>
                    <?php  }?>
                  </select>
                </td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Tạp chí/hội thảo:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" ><input type="text" id="tapchihoithao" name="tapchihoithao" class="col-xs-12 tapchihoithao" placeholder="Tap chi/hội thảo" ></td>
              </tr>
              <tr>
                <td style="text-align:left;  width:30%;border-top: 1px solid #fff !important">Mã hình thức:</td>
                <td style="text-align:left;  width:70%;border-top: 1px solid #fff !important" >
                  <select name="mahinhthucnckh" data-placeholder="Chọn Mã Hình Thức NCKH"   class="chosen-select textbox auto chonmahinhthuc " id="mahinhthucnckh">
                    <option value=""> </option>
                    <?php  $mahinhthucnckh = $db -> getRows('NCKH_MaHinhThuc', array('where' => array('AnHien' => 'on')), array('order_by' => 'ThuTu ASC'));
            		    foreach($mahinhthucnckh as $mhtnckh){?>
                      <option value="<?php echo $mhtnckh['Ten_MaHinhThuc'];?>"> <?php echo $mhtnckh['Ten_MaHinhThuc'];?></option>
                    <?php  }?>
                  </select>
                </td>
              </tr>
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Mã vai trò:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <select  name="mavaitronckh" data-placeholder="Chọn Mã vai trò NCKH"   class="chosen-select textbox auto mavaitro" id="mavaitronckh">
                    <option value=""> </option>
                    <?php  $mavaitronckh = $db -> getRows('NCKH_MaVaiTro', array('where' => array('AnHien'=>'on')), array('order_by' => 'ThuTu ASC'));
            		    foreach($mavaitronckh as $mvtnckh){?>
                      <option value="<?php echo $mvtnckh['Ten_MaVaiTro']; ?>"> <?php echo $mvtnckh['Ten_MaVaiTro'];?></option>
                    <?php  }?>
                  </select>
                </td>
              </tr>
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Tên sản phẩm:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <input type="text" class="col-xs-12 tensanpham" name="tensanpham" id="tensanpham">
                </td>
              </tr>
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Mã sản phẩm:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important" >
                  <input type="text" class="col-xs-12 masanphamkhnc" name="masosanpham" id="masosanpham" >
                </td>
              </tr>
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Số quyết đinh:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important">
                  <strong class=""></strong>
                  <input  type="text" class="col-xs-12 soquyetdinh" name="soquyetdinh" id="soquyetdinh" ></td>
              </tr>
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Thời gian hoàn thành:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important">
                  <div class="input-group">
                      <input class="form-control date-picker thoigianhoanthanh" name="thoigianhoanthanh" id="thoigianhoanthanh" type="text" data-date-format="dd/mm/yyyy" />
                      <span class="input-group-addon"> <i class="fa fa-calendar bigger-110"></i> </span> 
                  </div>
                </td>
              </tr>
              <tr>
                <td style="text-align:left; width:30%;border-top: 1px solid #fff !important">Sô tiết quy đổi:</td>
                <td style="text-align:left; width:70%;border-top: 1px solid #fff !important">
                  <input type="text" class="col-xs-12 sotietquydoi" name="sotietquydoi" id="sotietquydoi" >
                </td>
              </tr>
              <tr>
                <td colspan="2" ><input type="hidden" name="email" id="email" class="email-an">
                  <input  type="hidden" class="modal-id idkkstnckh"  name="idkkstnckh"/>
                  <input  type="hidden" value="<?php echo $_GET['key'];?>"  name="url"/>
                  <input type="hidden" name="action_type" class="action" value=""/>
                  <button type="submit" name="submit"class="btn btn-danger"> <i class="glyphicon glyphicon-send bigger-150"></i> Cập nhật</button>
                </td>
              </tr>
            </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </form>
</div><!-- END FORM -->

<div class="modal fade" id="stack4" role="dialog">
  <div class="modal-dialog modal-lg"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"  data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Quản lý file đính kèm</h4>
      </div>
      <div class="row">
        <div class="col-md-12" >
          <div id="qlfl"></div>
          <div id="tnad"></div>
        </div>
      </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>

<div id="stack5" class="modal fade" >
    <div class="modal-dialog modal-lg"> 
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Kê khai NCKH</h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger fade in" id="universalModal-alert" style="display: none;"> <span class="alert-body"></span> </div>
          <div class="modal-body">
            <div class="table-detail">
              <div class="row">
                <div class="col-xs-12 col-sm-12">
                  <div class="space visible-xs"></div>
                  <div class="profile-user-info profile-user-info-striped">
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Hoạt động </div>
                      <div class="profile-info-value"> <span class="chonqldkdt"></span> </div>
                    </div>
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Phân Loại </div>
                      <div class="profile-info-value"> <span class="chonphanloaikhnc"></span> </div>
                    </div>
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Tạp chí/hội thảo </div>
                      <div class="profile-info-value"> <span class="tapchihoithao"></span> </div>
                    </div>
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Mã Hình Thức </div>
                      <div class="profile-info-value"> <span class="masanphamkhnc"></span> </div>
                    </div>
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Mã Vai Trò </div>
                      <div class="profile-info-value"> <span class="mavaitro"></span> </div>
                    </div>
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Mã Sản phẩm </div>
                      <div class="profile-info-value"> <span class="masanphamkhnc"></span> </div>
                    </div>
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Tên Sản phẩm </div>
                      <div class="profile-info-value"> <span class="tensanpham"></span> </div>
                    </div>
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Số quyết định </div>
                      <div class="profile-info-value"> <span class="soquyetdinh"></span> </div>
                    </div>
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Thời gian hoàn thành </div>
                      <div class="profile-info-value"> <span class="thoigianhoanthanh"></span> </div>
                    </div>
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Đợt kê khai </div>
                      <div class="profile-info-value"> <span class="chondotkknckh"></span> </div>
                    </div>
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Năm kê khai </div>
                      <div class="profile-info-value"> <span class="namhoanthanh" ></span> </div>
                    </div>      
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Số tiết quy đổi </div>
                      <div class="profile-info-value"> <span class="sotietquydoi" ></span> </div>
                    </div>
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Số tiết Đơn vị duyệt </div>
                      <div class="profile-info-value"> <span class="sotietkhoaduyet" ></span> </div>
                    </div>
                    <div class="profile-info-row">
                      <div class="profile-info-name"> Số tiết Viện duyệt </div>
                      <div class="profile-info-value"> <span class="sotietvienduyet" ></span> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
</div><!-- /.main-content -->
