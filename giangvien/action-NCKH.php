<?php
session_start();
include '../MySQL/DB.php';
$db = new DB();

$dateFormatedtimex = split('/', $_POST['thoigianhoanthanh']);
$datetimex = $dateFormatedtimex[0].'-'.$dateFormatedtimex[1].'-'.$dateFormatedtimex[2];

$dateyears=$dateFormatedtimex[2];

$qlhdnckh = $db->getRows('nncms_cachoatdongNCKH',array('where'=>array('anHien'=>'on','id_nckh'=>$_POST['chonqldkdt'])),array('order_by'=>'ThuTu ASC'));


$b = rand(10,$datetimex);
$add="nckh_".$b;



if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){
    if($_REQUEST['action_type'] == 'add'){
		
		
				//ghi thông tin file đính kèm------------------------------------------------------------------		
		for($j=0; $j < count($_FILES["item_file"]['name']); $j++) { //loop the uploaded file array
		$filen = rand(1000,100000)."-"."user-".$add."-".$_FILES["item_file"]['name']["$j"]; //file name
			
			$path = 'uploads/'.$filen; //generate the destination path
			if(move_uploaded_file($_FILES["item_file"]['tmp_name']["$j"],$path))
			{
				$userData = array(
				'file' => $filen,
				'id_KKST_NCKH'=>$add,
				'user'=>$_POST['email'],
			);
			$tblName='tbl_uploads';
			$insert = $db->insert($tblName,$userData);
				} 
		}
        $statusMsg = $insert?'User data has been inserted successfully.':'Some problem occurred, please try again.';
        $_SESSION['statusMsg'] = $statusMsg;
        header("Location:index.php?key=viewds-NCKH");
		
		$ttdangkyData = array(
			'Ho_Ten' => $_POST['ho_ten'], 
			'dotkknckh'=>$_POST['chondotkknckh'],
			'chonqldkdt'=>$_POST['hoatdongnckh'],
			'chonphanloaikhnc'=>$_POST['phanloainckh'],
			'chonmahinhthuc'=>$_POST['mahinhthucnckh'],
			'mavaitro'=>$_POST['mavaitronckh'],
			'Thoi_Gian_Hoan_Thanh'=>$_POST['thoigianhoanthanh'],
			'namhoanthanh' => $dateyears,
			'SoTietQuyDoi'=>$_POST['sotietquydoi'],
			'TrangThai' => $_POST['trangthai'],
            'TenEmail' => $_POST['email'],
			'madv' => $_POST['madv'],
			'DC_NgayDangDK' => $datetimex,
			'AnHien' => $_POST['anhien'],
			'idfile'=>$add, 
			'file'=>$filen,
			'emailuser' => $_SESSION['sess_email'],
			'masosanpham' => $_POST['masosanpham'],
			'tensanpham' => $_POST['tensanpham'],
			'soquyetdinh' => $_POST['soquyetdinh'],
			'chondotkknckh' => $_POST['chondotkknckh'],
			'iddotkknckh' => $_POST['iddotkknckh'],
			'tapchihoithao' => $_POST['tapchihoithao'],
			
        );
        $insert = $db->insert('nncms_KKST_NCKH',$ttdangkyData);
		

		
    }
	elseif($_REQUEST['action_type'] == 'updatekknckh'){
        if(!empty($_POST['idkkstnckh'])){
 
			$userData = array(
			'Ho_Ten' => $_POST['ho_ten'], 
			'chonqldkdt'=>$_POST['hoatdongnckh'],
			'chonphanloaikhnc'=>$_POST['phanloainckh'],
			'chonmahinhthuc'=>$_POST['mahinhthucnckh'],
			'mavaitro'=>$_POST['mavaitronckh'],
			'Thoi_Gian_Hoan_Thanh'=>$_POST['thoigianhoanthanh'],
			'namhoanthanh' => $dateyears,
			'SoTietQuyDoi'=>$_POST['sotietquydoi'],
			'DC_Ngaycapnhat' => $datetimex,
			'masosanpham' => $_POST['masosanpham'],
			'tensanpham' => $_POST['tensanpham'],
			'soquyetdinh' => $_POST['soquyetdinh'],
			'tapchihoithao' => $_POST['tapchihoithao'],
			);
		
		$condition = array('id_KKST_NCKH' => $_POST['idkkstnckh']);
       $update = $db->update("nncms_KKST_NCKH",$userData,$condition);
            $statusMsg = $update?'User data has been updated successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:index.php?key=viewds-NCKH");
 }
    }	
	 elseif($_GET['action_type'] == 'deletekknckh'){
       if(!empty($_GET['id'])){
            $condition = array('id_KKST_NCKH' => $_GET['id']);
            $delete = $db->delete('nncms_KKST_NCKH',$condition);
            $statusMsg = $delete?'User data has been deleted successfully.':'Some problem occurred, please try again.';
            $_SESSION['statusMsg'] = $statusMsg;
            header("Location:index.php?key=viewds-NCKH");
        }
    }
}