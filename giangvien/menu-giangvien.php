<div id="sidebar" class="sidebar      h-sidebar                navbar-collapse collapse          ace-save-state"> 
  <script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>
  <div class="sidebar-shortcuts" id="sidebar-shortcuts">
    <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
      <button class="btn btn-success"> <i class="ace-icon fa fa-signal"></i> </button>
      <button class="btn btn-info"> <i class="ace-icon fa fa-pencil"></i> </button>
      <button class="btn btn-warning"> <i class="ace-icon fa fa-users"></i> </button>
      <button class="btn btn-danger"> <i class="ace-icon fa fa-cogs"></i> </button>
    </div>
    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini"> <span class="btn btn-success"></span> <span class="btn btn-info"></span> <span class="btn btn-warning"></span> <span class="btn btn-danger"></span> </div>
  </div>
  <!-- /.sidebar-shortcuts -->
  
  <ul class="nav nav-list">
    <?php $menus = $db->getRows('menus',array('where'=>array('vitri'=>'giangvien'),array('AnHien'=>'on')),array('order_by'=>'ThuTu ASC'));
            if(!empty($menus)){ $count = 0; foreach($menus as $menu){ $count++;?>
    <li class="hover"> <a href="?key=<?php echo $menu['LienKet'];?>" class="<?php echo $menu['class'];?>"> <i class="<?php echo $menu['icon'];?>"></i> <span class="menu-text"> <?php echo $menu['TieuDe'];?></span> </a> <b class="arrow"></b>
      <ul class="submenu">
        <?php 
		
		$sub = $db->getRowsgom('submenu',array('where'=>array('idCapCha'=>$menu['idMenu']),array('vitri'=>'giangvien'),array('AnHien'=>'on')),array('order_by'=>'ThuTu ASC'),array('start'=>"0"),array('limit'=>"10"));
		
						if(!empty($sub)){ $count = 0; foreach($sub as $subm){ $count++;?>
        <li class=""> <a href="?key=<?php echo $subm['LienKet'];?>"> <i class="<?php echo $subm['icon'];?>"></i> <?php echo $subm['TenSubmenu'];?> </a> <b class="arrow"></b> </li>
        <?php }
						 	}// ket thuc menu con?>
      </ul>
    </li>
    <?php }
			}// ket menu cha?>
  </ul>
  
  <!-- /.nav-list --> 
</div>
