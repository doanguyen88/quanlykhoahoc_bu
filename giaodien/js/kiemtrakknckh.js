  $(document).ready(function() {
    $('#hdTutoForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
           
		   ho_ten: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Nhập tên chủ nhiệm đề tài vào ô trên'
                    }
                }
            }, 
			hoatdongnckh: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Chọn Hoạt động NCKH'
								}
							}
						}, 
			phanloainckh: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Chọn Phân loại NCKH'
								}
							}
						}, 	
			mahinhthucnckh: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Chọn Mã hình thức'
								}
							}
						}, 			
			mavaitronckh: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Chọn Mã vai trò'
								}
							}
						}, 	
						
			tensanpham: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Nhập tên sản phẩm'
								}
							}
						},

			sotietquydoi: {
							validators: {
									integer: {
									min: 2,
								},
									notEmpty: {
									message: 'Nhập số tiết quy đổi'
								}
							}
						},
						
			/*tapchihoithao: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Nhập tạp chí/hội thảo'
								}
							}
						},							
*/
            city: {
                validators: {
                     stringLength: {
                        min: 4,
                    },
                    notEmpty: {
                        message: 'Please supply your city'
                    }
                }
            },
            hoatdongnckh: {
                validators: {
                    notEmpty: {
                        message: 'Chọn Hoạt động NCKH'
                    }
                }
            },
            zip: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your zip code'
                    },
                    zipCode: {
                        country: 'US',
                        message: 'Please supply a vaild zip code'
                    }
                }
            },
            comment: {
                validators: {
                      stringLength: {
                        min: 10,
                        max: 200,
                        message:'Please enter at least 10 characters and no more than 200'
                    },
                    notEmpty: {
                        message: 'Please supply a description of your project'
                    }
                    }
                }
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});