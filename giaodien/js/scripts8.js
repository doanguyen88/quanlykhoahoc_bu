	/*
	Tao object XmlHttpRequest. Vi trinh duyet IE va Mozilla ap dung ky thuat nay theo 2 cach khac nhau,
	de dam bao tinh tuong thich, chung ta se can phai kiem tra xem nguoi dung dang su dung trinh duyet nao
	va tao doi tuong XmlHttpRequest theo phuong thuc ho tro boi trinh duyet do.
	*/
	var req;
	function laylinhvucxet(str) // str='a'
	{	
		// branch for native XMLHttpRequest object
		var url="laylinhvucxet.php"+"?idlvdk="+str;// name.php?na=a
		if (window.XMLHttpRequest) {
			req = new XMLHttpRequest();
			req.onreadystatechange = processReqChange;
			req.open("GET", url, true);
			req.send(null);
		// branch for IE/Windows ActiveX version
		} else if (window.ActiveXObject) {
			req = new ActiveXObject("Microsoft.XMLHTTP");
			if (req) {
				req.onreadystatechange = processReqChange;
				req.open("GET", url, true);
				req.send();
			}
		}
	}
	
	/*
	Ham (function) processReqChange la ham chiu trach nhiem chinh xu ly viec goi du lieu va nhan du lieu.
	Cac buoc ma ham nay thuc hien:
	1. Doi cho den khi may chu gui phan hoi thong bao la no da xu ly xong
	2. Doc thong bao tu may chu (may chu se gui status=200 nen xu ly thanh cong, 404 neu file khong tim thay,...). 
	Neu may chu noi la xu ly thanh cong, tiep tuc buoc tiep theo
	Neu may chu noi la xu ly thanh cong, tiep tuc buoc tiep theo
	3. httpRequest.responseText � Trang Web duoc tra lai duoi dang chuoi van ban.
	*/
	function processReqChange() 
	{
		if (req.readyState == 4) // only if req shows "complete"
		{
			if (req.status == 200) // only if "OK"
			{
					document.getElementById("kq").innerHTML=req.responseText;
					
				
			} 
			else
			{
				alert("loi ket noi hoac truy van");
			}
		}
	}
	/*
	0: Yeu cau khong duoc khoi dong
	1: Yeu cau da duoc cai dat
	2: Yeu cau da duoc goi
	3: Yeu cau dang duoc xu ly
	4: yeu cau da hoan tat
	*/
	