  $(document).ready(function() {
    $('#DKDT').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
           
		   Ho_ten: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Nhập tên chủ nhiệm đề tài vào ô trên'
                    }
                }
            }, 
			nam_sinh: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Nhập năm sinh Chủ nhiệm đề tài vào ô trên'
								}
							}
						}, 
			chuc_vu: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Nhập Chức vụ vào ô trên'
								}
							}
						}, 	
			Hoc_Ham_Hoc_vi: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Nhập Học hàm, học vị vào ô trên'
								}
							}
						}, 			
			Dien_Thoai_Di_Dong: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Nhập điện thoại di động vào ô trên'
								}
							}
						}, 	
						
			Tinh_Cap_Thiet_DT: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Tính cấp thiết của đề tài vào ô trên'
								}
							}
						},
			Muc_Tieu_Noi_Dung_NC: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Nhập Mục tiêu và nội dung nghiên cứu của đề tài vào ô trên'
								}
							}
						},
			Du_Kien_Dong_Gop_DT: {
							validators: {
									stringLength: {
									min: 2,
								},
									notEmpty: {
									message: 'Vui lòng nhập Đơn vị áp dụng sáng kiến vào đây vào ô trên'
								}
							}
						},							
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your email address'
                    },
                    emailAddress: {
                        message: 'Please supply a valid email address'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your phone number'
                    },
                    phone: {
                        country: 'US',
                        message: 'Please supply a vaild phone number with area code'
                    }
                }
            },
            address: {
                validators: {
                     stringLength: {
                        min: 8,
                    },
                    notEmpty: {
                        message: 'Please supply your street address'
                    }
                }
            },
            city: {
                validators: {
                     stringLength: {
                        min: 4,
                    },
                    notEmpty: {
                        message: 'Please supply your city'
                    }
                }
            },
            state: {
                validators: {
                    notEmpty: {
                        message: 'Please select your state'
                    }
                }
            },
            zip: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your zip code'
                    },
                    zipCode: {
                        country: 'US',
                        message: 'Please supply a vaild zip code'
                    }
                }
            },
            comment: {
                validators: {
                      stringLength: {
                        min: 10,
                        max: 200,
                        message:'Please enter at least 10 characters and no more than 200'
                    },
                    notEmpty: {
                        message: 'Please supply a description of your project'
                    }
                    }
                }
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});