<?php
session_start();
include 'MySQL/DB.php';
$db = new DB();

if($_POST['start']==""||$_POST['end']=="")
{
	$datestart="";
	$dateend="";
}else
{
	$dateFormatedstart = explode('/',$_POST['start']);
	$datestart = $dateFormatedstart[2].'-'.$dateFormatedstart[0].'-'.$dateFormatedstart[1];
	$dateFormatedend = explode('/',$_POST['end']);
	$dateend  = $dateFormatedend[2].'-'.$dateFormatedend[0].'-'.$dateFormatedend[1];
}

$dateFormatedtimex = explode('/', $_POST['ngay']);
$datetimex = $dateFormatedtimex[2].'-'.$dateFormatedtimex[0].'-'.$dateFormatedtimex[1];
$isSuccess = true;


if(isset($_REQUEST['action_type']) && !empty($_REQUEST['action_type'])){
    if($_REQUEST['action_type'] == 'add'){
		$dem = $db->demrow('nncms_dkbiensoantlht', array('where'=>array('idPhanLoai_BSTL_GT'=>'BSTL')));
		
		echo "<h1>$dem</h1>";

		//kiểm tra số thứ tự chưa tồn tại ------------------------------------------------------------------		
		if($dem == 0) {
			//cập nhật stt đăng ký vào bảng user---------------------------------------------------				
			$dem=1;
			$Ma='BSTL00';
			$add="$Ma$dem"; //'BSTL001'
			//ghi thông tin file đính kèm--------------TRƯỚC KHI INSERT FILE----------------------------------------------------		
			$idFile = 0;
			for($j=0; $j < count($_FILES["item_file"]['name']); $j++) { 
				//loop the uploaded file array
				$filen = rand(1000,100000)."-"."user-".$add."-".$_FILES["item_file"]['name']; //file name
				if($_FILES["item_file"]['name'] == null)
				{
					echo "Bạn chưa nhấn chọn file để upload";
					$isSuccess = false;
				}
				else {

					$path = 'uploads/biensoantl/'.$filen; //generate the destination path
					$file_type =$_FILES["item_file"]["type"];
					$file_size =$_FILES["item_file"]["size"];
					$maxsize = 5 * 1024 * 1024;

					$file_item_name = $_FILES["item_file"]['tmp_name'];
					echo "<h1>file_name: $file_item_name</h1>";
					echo "<h1>link: $path</h1>";

					if($file_size > $maxsize) {
						die("<h1>Lỗi: Kích thước file lớn hơn giới hạn cho phép</h1>");
						$isSuccess = false;
					}
					else {
						if(move_uploaded_file($file_item_name,$path)){
							$userData = array(
								'file' => $filen,
								'idDKBienSoanTLHT'=>$add,
								'user'=>$_POST['email'],
							);
							$tenemail = $_POST['email'];
							$tblName='tbl_uploads';
							$idFile = $db->insert($tblName,$userData);
							$isSuccess = $idFile > 0;
						} 	
					}
				}
			}
			//cập nhật stt đăng ký vào bảng user---------------------------------------------------	
			if ($isSuccess) {
				$userData = array('stt_dt' => $add,);
				$condition = array('email' => $_SESSION['sess_email']);
				$update = $db->update('user',$userData,$condition);
				$sql = "SELECT idDotdknckh
						FROM nncms_quanlydotdangkynckh
						WHERE '$dateend' <= endtimes";
				$result = $db -> runSQL($sql); 
				$iddotdk = $result[0][0];

				//ghi thông tin đăng ký------------------------------------------------------------------			
				$ttdangkyData = array(
					'idPhanLoai_BSTL_GT' => 'BSTL', 
					'TrangThai' => $_POST['trangthai'],
					'DC_STT'=>$add,
					'DonVi' => $_POST['donvi'],
					'NamHoc' => $_POST['namhoc'],
					'TenTaiLieu' => $_POST['Ten_Tai_Lieu'],
					'LoaiTaiLieuDK' => $_POST['Loai_Tai_Lieu_DK'],
					'HinhThuc' => $_POST['Hinh_Thuc_DK'],
					'starttimes' => $datestart,
					'endtimes' => $dateend,
					'idDotDKNCKH' => $iddotdk,
					'HoTenChuNhiem' => $_POST['Ho_ten'], 
					'NamSinh' => $_POST['nam_sinh'],
					'HocHamHocVi' => $_POST['Hoc_Ham_Hoc_vi'],
					'DienThoaiDiDong' => $_POST['Dien_Thoai_Di_Dong'],
					'TenEmail' => $_POST['email'],
					'DungChoMonHoc' => $_POST['Dung_Cho_Mon_Hoc'],
					'DungChoChuyenNganh' => $_POST['Dung_Cho_Chuyen_Nganh'],

					'PhucVuDTBac' => $_POST['Phuc_Vu_Dao_Tao_Bac'],

					'DC_NgayDangDK' => $datetimex,
					'AnHien' => $_POST['anhien'],
					'Chuky'=>$_POST['Chu_ky'],
					'ID_DSThanhVien'=>$add,
					'idfile'=>$idFile,
					'file'=>$filen,
					'madv' => $_SESSION['sess_madv'],
					'emailuser' => $_SESSION['sess_email'],
				);
				
				$insert = $db->insert('nncms_dkbiensoantlht',$ttdangkyData);
				if (empty($_POST['hoten_ntg_1'])){
					echo 'Bạn chưa nhập họ tên';
				} else{
					$thanhvien1 = array(
						'HoTen_NTG' => $_POST['hoten_ntg_1'],
						'HocHamHocVi_NTG' => $_POST['hhhv_ntg_1'],
						'DonVi_NTG' => $_POST['donvi_ntg_1'],
						'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_1'],
						'ChuKy_NTG' => $_POST['chuky_ntg_1'],
						'idDKBienSoanTLHT' => $insert,
					);
					$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien1);
				}
				
				if (empty($_POST['hoten_ntg_2'])){
					echo 'Bạn chưa nhập họ tên';
				} else {
					$thanhvien2 = array(
						'HoTen_NTG' => $_POST['hoten_ntg_2'],
						'HocHamHocVi_NTG' => $_POST['hhhv_ntg_2'],
						'DonVi_NTG' => $_POST['donvi_ntg_2'],
						'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_2'],
						'ChuKy_NTG' => $_POST['chuky_ntg_2'],
						'idDKBienSoanTLHT' => $insert,
					);
					$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien2);
				}

				if (empty($_POST['hoten_ntg_3'])){
					echo 'Bạn chưa nhập họ tên';
				} else {
					$thanhvien3 = array(
						'HoTen_NTG' => $_POST['hoten_ntg_3'],
						'HocHamHocVi_NTG' => $_POST['hhhv_ntg_3'],
						'DonVi_NTG' => $_POST['donvi_ntg_3'],
						'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_3'],
						'ChuKy_NTG' => $_POST['chuky_ntg_3'],
						'idDKBienSoanTLHT' => $insert,
					);
					$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien3);
				}

				if (empty($_POST['hoten_ntg_4'])){
					echo 'Bạn chưa nhập họ tên';
				} else {
					$thanhvien4 = array(
						'HoTen_NTG' => $_POST['hoten_ntg_4'],
						'HocHamHocVi_NTG' => $_POST['hhhv_ntg_4'],
						'DonVi_NTG' => $_POST['donvi_ntg_4'],
						'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_4'],
						'ChuKy_NTG' => $_POST['chuky_ntg_4'],
						'idDKBienSoanTLHT' => $insert,
					);
					$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien4);
				}

				if (empty($_POST['hoten_ntg_5'])){
					echo 'Bạn chưa nhập họ tên';
				} else {
					$thanhvien5 = array(
						'HoTen_NTG' => $_POST['hoten_ntg_5'],
						'HocHamHocVi_NTG' => $_POST['hhhv_ntg_5'],
						'DonVi_NTG' => $_POST['donvi_ntg_5'],
						'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_5'],
						'ChuKy_NTG' => $_POST['chuky_ntg_5'],
						'idDKBienSoanTLHT' => $insert,
					);
					$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien5);
				}
			}
			//ghi thông tin file đính kèm------------------------------------------------------------------		
			for($j=0; $j < count($_FILES["item_file"]['name']); $j++) { 
				//loop the uploaded file array
				$filen = rand(1000,100000)."-"."user-".$add."-".$_FILES["item_file"]['name']["$j"]; //file name
				echo "<h1>$filen</h1>";
				
				//DÒNG NÀY ĐỂ LƯU FILE KHI UPLOAD LÊN
				$path = 'uploads/biensoantl/'.$filen; //generate the destination path
				echo "<h1>$filen</h1>";

				//4 CÁI BIẾN mớiTHÊM VÀO
				$file_type =$_FILES["item_file"]["type"];
				echo "<h1>$file_type</h1>";
				$file_size =$_FILES["item_file"]["size"];
				echo "<h1>$file_size</h1>";
				$maxsize = 5 * 1024 * 1024;
				$errors = array();
				echo "<h1>$errors</h1>";

				echo "<h1>file_size = $file_size</h1>";
				if($file_size > $maxsize){
					die("Lỗi: Kích thước file lớn hơn giới hạn cho phép");
					$isSuccess = false;
				}
				else {
					if(move_uploaded_file($_FILES["item_file"]['tmp_name']["$j"],$path)){
						$userData = array(
							'file' => $filen,
							'idDKBienSoanTLHT'=>$add,
							'user'=>$_POST['email'],
						);
						$tblName='tbl_uploads';
						$insert = $db->insert($tblName,$userData);
					} 
				}
								
			}	
		}
		else //kiểm tra số thứ tự đã tồn tại------------------------------------------------------------------
		{
			//==>Số thứ tự tồn tại nhỏ hơn 10
			if($dem<10){ 
				//Khởi tạo một số biến trước tiên
				$dem=$dem+1;
				$Ma='BSTL00';
				$add="$Ma$dem";		
				//ghi thông tin file đính kèm--------------TRƯỚC KHI INSERT FILE----------------------------------------------------		
				$idFile = 0;
				for($j=0; $j < count($_FILES["item_file"]['name']); $j++) { 
					//loop the uploaded file array
					$filen = rand(1000,100000)."-"."user-".$add."-".$_FILES["item_file"]['name']; //file name
					if($_FILES["item_file"]['name'] == null)
					{
						echo "Bạn chưa nhấn chọn file để upload";
						$isSuccess = false;
					}
					else {

						$path = 'uploads/biensoantl/'.$filen; //generate the destination path
						$file_type =$_FILES["item_file"]["type"];
						$file_size =$_FILES["item_file"]["size"];
						$maxsize = 5 * 1024 * 1024;

						$file_item_name = $_FILES["item_file"]['tmp_name'];
						echo "<h1>file_name: $file_item_name</h1>";
						echo "<h1>link: $path</h1>";

						if($file_size > $maxsize) {
							die("<h1>Lỗi: Kích thước file lớn hơn giới hạn cho phép</h1>");
							$isSuccess = false;
						}
						else {
							if(move_uploaded_file($file_item_name,$path)){
								$userData = array(
									'file' => $filen,
									'idDKBienSoanTLHT'=>$add,
									'user'=>$_POST['email'],
								);
								$tenemail = $_POST['email'];
								$tblName='tbl_uploads';
								$idFile = $db->insert($tblName,$userData);
								$isSuccess = $idFile > 0;
							} 	
						}
					}
				}
				//cập nhật stt đăng ký vào bảng user---------------------------------------------------	
				if ($isSuccess) {
					$userData = array('stt_dt' => $add,);
					$condition = array('email' => $_SESSION['sess_email']);
					$update = $db->update('user',$userData,$condition);
					
					$sql = "SELECT idDotdknckh
							FROM nncms_quanlydotdangkynckh
							WHERE '$dateend' <= endtimes";
					$result = $db -> runSQL($sql); 
					$iddotdk = $result[0][0];
					echo "<h1>Mã đợt: $iddotdk</h1>";
					echo "<h1>$sql</h1>";

					//ghi thông tin đăng ký------------------------------------------------------------------
					$ttdangkyData = array(
						'idPhanLoai_BSTL_GT' => 'BSTL', 
						'TrangThai' => $_POST['trangthai'],
						'DC_STT'=>$add,
						'DonVi' => $_POST['donvi'],
						'NamHoc' => $_POST['namhoc'],
						'TenTaiLieu' => $_POST['Ten_Tai_Lieu'],
						'LoaiTaiLieuDK' => $_POST['Loai_Tai_Lieu_DK'],
						'HinhThuc' => $_POST['Hinh_Thuc_DK'],
						'starttimes' => $datestart,
						'endtimes' => $dateend,
						'idDotDKNCKH' => $iddotdk,
						'HoTenChuNhiem' => $_POST['Ho_ten'], 
						'NamSinh' => $_POST['nam_sinh'],
						'HocHamHocVi' => $_POST['Hoc_Ham_Hoc_vi'],
						'DienThoaiDiDong' => $_POST['Dien_Thoai_Di_Dong'],
						'TenEmail' => $_POST['email'],
						'DungChoMonHoc' => $_POST['Dung_Cho_Mon_Hoc'],
						'DungChoChuyenNganh' => $_POST['Dung_Cho_Chuyen_Nganh'],

						'PhucVuDTBac' => $_POST['Phuc_Vu_Dao_Tao_Bac'],

						'DC_NgayDangDK' => $datetimex,
						'AnHien' => $_POST['anhien'],
						'Chuky'=>$_POST['Chu_ky'],
						'ID_DSThanhVien'=>$add,
						'idfile'=>$idFile,
						'file'=>$filen,
						'madv' => $_SESSION['sess_madv'],
						'emailuser' => $_SESSION['sess_email'],
					);

					$insert = $db->insert('nncms_dkbiensoantlht',$ttdangkyData);
					echo "<h1>$insert</h1>";

					if (empty($_POST['hoten_ntg_1'])){
						echo 'Bạn chưa nhập họ tên';
					} else{
						$thanhvien1 = array(
							'HoTen_NTG' => $_POST['hoten_ntg_1'],
							'HocHamHocVi_NTG' => $_POST['hhhv_ntg_1'],
							'DonVi_NTG' => $_POST['donvi_ntg_1'],
							'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_1'],
							'ChuKy_NTG' => $_POST['chuky_ntg_1'],
							'idDKBienSoanTLHT' => $insert,
						);
						$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien1);
					}
					
					if (empty($_POST['hoten_ntg_2'])){
						echo 'Bạn chưa nhập họ tên';
					} else {
						$thanhvien2 = array(
							'HoTen_NTG' => $_POST['hoten_ntg_2'],
							'HocHamHocVi_NTG' => $_POST['hhhv_ntg_2'],
							'DonVi_NTG' => $_POST['donvi_ntg_2'],
							'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_2'],
							'ChuKy_NTG' => $_POST['chuky_ntg_2'],
							'idDKBienSoanTLHT' => $insert,
						);
						$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien2);
					}

					if (empty($_POST['hoten_ntg_3'])){
						echo 'Bạn chưa nhập họ tên';
					} else {
						$thanhvien3 = array(
							'HoTen_NTG' => $_POST['hoten_ntg_3'],
							'HocHamHocVi_NTG' => $_POST['hhhv_ntg_3'],
							'DonVi_NTG' => $_POST['donvi_ntg_3'],
							'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_3'],
							'ChuKy_NTG' => $_POST['chuky_ntg_3'],
							'idDKBienSoanTLHT' => $insert,
						);
						$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien3);
					}

					if (empty($_POST['hoten_ntg_4'])){
						echo 'Bạn chưa nhập họ tên';
					} else {
						$thanhvien4 = array(
							'HoTen_NTG' => $_POST['hoten_ntg_4'],
							'HocHamHocVi_NTG' => $_POST['hhhv_ntg_4'],
							'DonVi_NTG' => $_POST['donvi_ntg_4'],
							'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_4'],
							'ChuKy_NTG' => $_POST['chuky_ntg_4'],
							'idDKBienSoanTLHT' => $insert,
						);
						$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien4);
					}

					if (empty($_POST['hoten_ntg_5'])){
						echo 'Bạn chưa nhập họ tên';
					} else {
						$thanhvien5 = array(
							'HoTen_NTG' => $_POST['hoten_ntg_5'],
							'HocHamHocVi_NTG' => $_POST['hhhv_ntg_5'],
							'DonVi_NTG' => $_POST['donvi_ntg_5'],
							'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_5'],
							'ChuKy_NTG' => $_POST['chuky_ntg_5'],
							'idDKBienSoanTLHT' => $insert,
						);
						$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien5);
					}
				}
			}
			//==>Số thứ tự tồn tại lơn hơn 10 nhỏ hơn 99
			if($dem>=10 and $dem<=99) {
				//Ở ĐÂY
				$dem=$dem+1;
				$Ma='BSTL0';
				$add="$Ma$dem";
				//ghi thông tin file đính kèm------------------------------------------------------------------	
				$idFile = 0;
				for($j=0; $j < count($_FILES["item_file"]['name']); $j++) { 
					//loop the uploaded file array
					$filen = rand(1000,100000)."-"."user-".$add."-".$_FILES["item_file"]['name']; //file name
					if($_FILES["item_file"]['name'] == null)
					{
						echo "Bạn chưa nhấn chọn file để upload";
						$isSuccess = false;
					}
					else {

						$path = 'uploads/biensoantl/'.$filen; //generate the destination path
						$file_type =$_FILES["item_file"]["type"];
						$file_size =$_FILES["item_file"]["size"];
						$maxsize = 5 * 1024 * 1024;
						
						$file_item_name = $_FILES["item_file"]['tmp_name'];
						echo "<h1>file_name: $file_item_name</h1>";
						echo "<h1>link: $path</h1>";

						if($file_size > $maxsize) {
							die("<h1>Lỗi: Kích thước file lớn hơn giới hạn cho phép</h1>");
							$isSuccess = false;
						}
						else {
							if(move_uploaded_file($file_item_name,$path)){
								$userData = array(
									'file' => $filen,
									'idDKBienSoanTLHT'=>$add,
									'user'=>$_POST['email'],
								);
								$tenemail = $_POST['email'];
								$tblName='tbl_uploads';
								$idFile = $db->insert($tblName,$userData);
								$isSuccess = $idFile > 0;
							} 	
						}
					}
				}
				//cập nhật stt đăng ký vào bảng user---------------------------------------------------
				if($isSuccess) {
					$userData = array(
						'stt_dt' => $add,
					);
					$condition = array('email' => $_SESSION['sess_email']);
					$update = $db->update('user',$userData,$condition);
					
					$sql = "SELECT idDotdknckh
							FROM nncms_quanlydotdangkynckh
							WHERE '$dateend' <= endtimes";
					$result = $db -> runSQL($sql); 
					$iddotdk = $result[0][0];
					echo "<h1>Mã đợt: $iddotdk</h1>";
					echo "<h1>$sql</h1>";
					//ghi thông tin đăng ký------------------------------------------------------------------
					$ttdangkyData = array(
						'idPhanLoai_BSTL_GT' => 'BSTL', 
						'TrangThai' => $_POST['trangthai'],
						'DC_STT'=>$add,
						'DonVi' => $_POST['donvi'],
						'NamHoc' => $_POST['namhoc'],
						'TenTaiLieu' => $_POST['Ten_Tai_Lieu'],
						'LoaiTaiLieuDK' => $_POST['Loai_Tai_Lieu_DK'],
						'HinhThuc' => $_POST['Hinh_Thuc_DK'],
						'starttimes' => $datestart,
						'endtimes' => $dateend,
						'idDotDKNCKH' => $iddotdk,
						'HoTenChuNhiem' => $_POST['Ho_ten'], 
						'NamSinh' => $_POST['nam_sinh'],
						'HocHamHocVi' => $_POST['Hoc_Ham_Hoc_vi'],
						'DienThoaiDiDong' => $_POST['Dien_Thoai_Di_Dong'],
						'TenEmail' => $_POST['email'],
						'DungChoMonHoc' => $_POST['Dung_Cho_Mon_Hoc'],
						'DungChoChuyenNganh' => $_POST['Dung_Cho_Chuyen_Nganh'],
						'PhucVuDTBac' => $_POST['Phuc_Vu_Dao_Tao_Bac'],
						'DC_NgayDangDK' => $datetimex,
						'AnHien' => $_POST['anhien'],
						'Chuky'=>$_POST['Chu_ky'],
						
						'idfile'=>$idFile,
						'file'=>$filen,
						'madv' => $_SESSION['sess_madv'],
						'emailuser' => $_SESSION['sess_email'],
					);
					$insert = $db->insert('nncms_dkbiensoantlht',$ttdangkyData);
					echo "<h1>$insert</h1>";
					if (empty($_POST['hoten_ntg_1'])){
						echo 'Bạn chưa nhập họ tên';
					} else{
						$thanhvien1 = array(
							'HoTen_NTG' => $_POST['hoten_ntg_1'],
							'HocHamHocVi_NTG' => $_POST['hhhv_ntg_1'],
							'DonVi_NTG' => $_POST['donvi_ntg_1'],
							'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_1'],
							'ChuKy_NTG' => $_POST['chuky_ntg_1'],
							'idDKBienSoanTLHT' => $insert,
						);
						$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien1);
					}
					
					if (empty($_POST['hoten_ntg_2'])){
						echo 'Bạn chưa nhập họ tên';
					} else {
						$thanhvien2 = array(
							'HoTen_NTG' => $_POST['hoten_ntg_2'],
							'HocHamHocVi_NTG' => $_POST['hhhv_ntg_2'],
							'DonVi_NTG' => $_POST['donvi_ntg_2'],
							'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_2'],
							'ChuKy_NTG' => $_POST['chuky_ntg_2'],
							'idDKBienSoanTLHT' => $insert,
						);
						$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien2);
					}

					if (empty($_POST['hoten_ntg_3'])){
						echo 'Bạn chưa nhập họ tên';
					} else {
						$thanhvien3 = array(
							'HoTen_NTG' => $_POST['hoten_ntg_3'],
							'HocHamHocVi_NTG' => $_POST['hhhv_ntg_3'],
							'DonVi_NTG' => $_POST['donvi_ntg_3'],
							'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_3'],
							'ChuKy_NTG' => $_POST['chuky_ntg_3'],
							'idDKBienSoanTLHT' => $insert,
						);
						$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien3);
					}

					if (empty($_POST['hoten_ntg_4'])){
						echo 'Bạn chưa nhập họ tên';
					} else {
						$thanhvien4 = array(
							'HoTen_NTG' => $_POST['hoten_ntg_4'],
							'HocHamHocVi_NTG' => $_POST['hhhv_ntg_4'],
							'DonVi_NTG' => $_POST['donvi_ntg_4'],
							'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_4'],
							'ChuKy_NTG' => $_POST['chuky_ntg_4'],
							'idDKBienSoanTLHT' => $insert,
						);
						$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien4);
					}

					if (empty($_POST['hoten_ntg_5'])){
						echo 'Bạn chưa nhập họ tên';
					} else {
						$thanhvien5 = array(
							'HoTen_NTG' => $_POST['hoten_ntg_5'],
							'HocHamHocVi_NTG' => $_POST['hhhv_ntg_5'],
							'DonVi_NTG' => $_POST['donvi_ntg_5'],
							'LinhVucCM_NTG' => $_POST['linhvuccm_ntg_5'],
							'ChuKy_NTG' => $_POST['chuky_ntg_5'],
							'idDKBienSoanTLHT' => $insert,
						);
						$idtv = $db->insert('nncms_thanhvienthamgia',$thanhvien5);
					}
				}
			}
			if($dem>=100) {

				$dem=$dem+1;
				$Ma='BSTL';
				$add="$Ma$dem";
				//ghi thông tin file đính kèm------------------------------------------------------------------	
				$idFile = 0;	
				for($j=0; $j < count($_FILES["item_file"]['name']); $j++) { 
					//loop the uploaded file array
					$filen = rand(1000,100000)."-"."user-".$add."-".$_FILES["item_file"]['name']; //file name
					if($_FILES["item_file"]['name'] == null)
					{
						echo "Bạn chưa nhấn chọn file để upload";
						$isSuccess = false;
					}
					else {
						$path = 'uploads/biensoantl/'.$filen; //generate the destination path
						$file_type =$_FILES["item_file"]["type"];
						$file_size =$_FILES["item_file"]["size"];
						$maxsize = 5 * 1024 * 1024;
						
						$file_item_name = $_FILES["item_file"]['tmp_name'];
						echo "<h1>file_name: $file_item_name</h1>";
						echo "<h1>link: $path</h1>";

						if($file_size > $maxsize) {
							die("<h1>Lỗi: Kích thước file lớn hơn giới hạn cho phép</h1>");
							$isSuccess = false;
						}
						else {
							if(move_uploaded_file($file_item_name,$path)){
								$userData = array(
									'file' => $filen,
									'idDKBienSoanTLHT'=>$add,
									'user'=>$_POST['email'],
								);
								$tenemail = $_POST['email'];
								$tblName='tbl_uploads';
								$idFile = $db->insert($tblName,$userData);
								$isSuccess = $idFile > 0;
							} 	
						}
					}
				}
				//cập nhật stt đăng ký vào bảng user---------------------------------------------------
				if ($isSuccess) {
					$userData = array(
						'stt_tt' => $add,
					);
					$condition = array('email' => $_SESSION['sess_email']);
					$update = $db->update('user',$userData,$condition);
					$sql = "SELECT idDotdknckh
							FROM nncms_quanlydotdangkynckh
							WHERE '$dateend' <= endtimes";
					$result = $db -> runSQL($sql); 
					$iddotdk = $result[0][0];
					echo "<h1>Mã đợt: $iddotdk</h1>";
					echo "<h1>$sql</h1>";
					//ghi thông tin đăng ký------------------------------------------------------------------
					$ttdangkyData = array(
						'idPhanLoai_BSTL_GT' => 'BSTL', 
						'TrangThai' => $_POST['trangthai'],
						'DC_STT'=>$add,
						'DonVi' => $_POST['donvi'],
						'NamHoc' => $_POST['namhoc'],
						'TenTaiLieu' => $_POST['Ten_Tai_Lieu'],
						'LoaiTaiLieuDK' => $_POST['Loai_Tai_Lieu_DK'],
						'HinhThuc' => $_POST['Hinh_Thuc_DK'],
						'starttimes' => $datestart,
						'endtimes' => $dateend,
						'idDotDKNCKH' => $iddotdk,
						'HoTenChuNhiem' => $_POST['Ho_ten'], 
						'NamSinh' => $_POST['nam_sinh'],
						'HocHamHocVi' => $_POST['Hoc_Ham_Hoc_vi'],
						'DienThoaiDiDong' => $_POST['Dien_Thoai_Di_Dong'],
						'TenEmail' => $_POST['email'],
						'DungChoMonHoc' => $_POST['Dung_Cho_Mon_Hoc'],
						'DungChoChuyenNganh' => $_POST['Dung_Cho_Chuyen_Nganh'],
						'PhucVuDTBac' => $_POST['Phuc_Vu_Dao_Tao_Bac'],
						'DC_NgayDangDK' => $datetimex,
						'AnHien' => $_POST['anhien'],
						'Chuky'=>$_POST['Chu_ky'],
						
						'idfile'=>$idFile,
						'file'=>$filen,
						'madv' => $_SESSION['sess_madv'],
						'emailuser' => $_SESSION['sess_email'],
					);
					$insert = $db->insert('nncms_dkbiensoantlht',$ttdangkyData);
					//ghi thông tin thành viên tham gia --------------------------------------------------------------
					$thanhvien = array(
						'HoTen_NTG' => $_POST['hoten_ntg1'],
						'HocHamHocVi_NTG' => $_POST['hhhv_ntg1'],
						'DonVi_NTG' => $_POST['donvi_ntg1'],
						'LinhVucCM_NTG' => $_POST['linhvuccm_ntg1'],
						'ChuKy_NTG' => $_POST['chuky_ntg1'],
		
					);
					$insert = $db->insert('nncms_thanhvienthamgia',$thanhvien);
				}
			}
		}

		$IDDSThanhVien=$add;
		$sothanhvien=$_POST['sothanhvien'];
		
        $statusMsg = $isSuccess?'User data has been inserted successfully.':'Some problem occurred, please try again.';
		$_SESSION['statusMsg'] = $statusMsg;

		echo "<h1>$isSuccess</h1>";

		if ($isSuccess) //thanh cong thi moi cho dang ky
			header("Location:giangvien.php?key=thanhviencs1&Line=$sothanhvien&IDDSThanhVien=$IDDSThanhVien");
		
    }
}
?>